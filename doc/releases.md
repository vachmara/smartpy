# image:/static/img/logo-only.svg[Logo,50] Releases of SmartPy and SmartPy.io
:nofooter:
:source-highlighter: coderay

:linkattrs:


https://SmartPy.io[SmartPy] is a Python library for constructing Tezos smart contracts.

We maintain several releases of both https://SmartPy.io and of the
corresponding command line interface https://SmartPy.io/cli.

## SmartPy.io

### The Current Release

The current release: https://SmartPy.io. +
Alternative server: https://alt.SmartPy.io.

### Recent History

2021-06-30::
* New release 0.6.9 is https://smartpy.io/releases/20210630-d86b17adb6d0fb15c9dc009bf3a62de6665a9ae3).
  ** Adds exception testing, users are now able to link:reference.html#_registering_and_displaying_calls_to_entry_points[test error messages] emitted by failed contract calls;
  ** File names in error location:
  ** New state channel based game platform templates:
    *(A work in progress)*
    *** link:ide?template=state_channel_games/game_platform.py[Game platform]
    *** link:ide?template=state_channel_games/game_tester.py[Game tester]
    *** link:ide?template=state_channel_games/model_wrap.py[Game wrapper]
    *** link:ide?template=state_channel_games/types.py[Game types]
    *** link:ide?template=state_channel_games/models/head_tail.py[Game: Head and Tail model]
    *** link:ide?template=state_channel_games/models/nim.py[Game: Nim model]
    *** link:ide?template=state_channel_games/models/tictactoe.py[Game: tic-tac-toe model]
    *** link:ide?template=state_channel_games/models/transfer.py[Game: transfer model]

  ** Online IDE now divides tests and compilation targets into two distinct sections;
  ** *(Bug fixes)*: +
    *** `originate-contract` CLI command can now be executed from any sub-folder;
    *** `scenario.simulation` debugging feature is now working again;
    *** Fixes an issue in the explorer page that would cause an exception when interpreting tuples;
    *** Allow invalid `packed` bytes to be tested with `run(valid = False);

2021-06-09::
* New release 0.6.8 is https://smartpy.io/releases/20210609-d64964633e98c1bd1fe8beb9f83138185cabdf90.
  ** SmartPy CLI now shows all created operations in log.txt (it used
  to only show recursive operations).
  ** More documentation and examples of link:reference.html#_lazy_and_updatable_entry_points[lazy and updatable entry points].
  ** *(Bug fixes)*: +
    *** Fix CLI `originate-contract` command.

2021-06-04::
* New release 0.6.7 is https://smartpy.io/releases/20210604-7f97dba13e914cb1915b7cea16b844208abf51e9.
  ** Add `granadanet` test network link:https://granadanet.smartpy.io[RPC];
  ** Published SmartML module on link:https://www.npmjs.com/package/@smartpy/smartml[npm];
  ** *(Bug fixes)*: +
    *** Fixes order of operations created inside `@sp.sub_entry_point`, this bug would cause the order of operations to be inverted;

    When creating new operations inside a `@sp.sub_entry_point`:
    ```
        sp.set_delegate(sp.source)
        sp.set_delegate(sp.none)
    ```

    The following behavior would occur:
      +--------+-----------+
      | Calls  | Emissions |
      +--------+-----------+
      | A, B   | B, A      |
      +--------+-----------+

    Correct behavior:
      +--------+-----------+
      | Calls  | Emissions |
      +--------+-----------+
      | A, B   | A, B      |
      +--------+-----------+

2021-05-26::
* New release 0.6.6 is https://smartpy.io/releases/20210526-6aa00c4986bbaa550be88d526bd3b290890b9f50.
  ** Adds contract originator to smartpy-cli, `SmartPy.sh originate-contract ...` link:reference.html#_deploying_a_contract[deploying a contract];
  ** Adds user input sanitization to explorer.html / wallet.html pages;
  ** Per-entry-point lazyfication and link:reference.html#_lazy_and_updatable_entry_points[lazy and updatable entry points];
  ** *(Bug fixes)*: +
    *** Fixes type and adds annotations to `token_metadata` on FA1.2 template;

2021-05-21::
* New release 0.6.5 is https://smartpy.io/releases/20210521-609e3034295b20ba9b54c707ad994eafe6f1b7ab.
  ** Increases the size of lists and maps parameters from 2 to 4 in the explorer;
  ** `sp.sub_entry_point` input type now defaults to `sp.unit` when no argument is provided;
  ** Add token_metadata annotations to FA2 template.
  ** Allow a zero scalar when multiplying G1/G2 points by Fr.

2021-05-12::
* New release 0.6.4 is https://smartpy.io/releases/20210512-c6a32a4036fa73e6f262c4035e87cdf9739b6679.
  ** Adds helper methods, contained in link:reference.html#_utils[`sp.utils`]:
    *** `sp.utils.same_underlying_address(a, b)` which allows underlying address comparisons.
      **** e.g.) It returns a boolean informing if `KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF%foo` has the same underlying address as `KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF`;
    *** `sp.utils.mutez_to_nat(m)` which converts a `TMutez` amount to `TNat`;
    *** `sp.utils.nat_to_mutez(n)` and `sp.utils.nat_to_tez(n)` convert `TNat` to `TMutez`;
  ** IO methods are now available at `sp.io.*` namespace:
    *** `sp.io.import_script_from_script`
    *** `sp.io.import_template`
    *** `sp.io.import_script_from_url`
    *** `sp.io.import_stored_contract`
  ** `FA2` template was updated, it now includes on-chain/off-chain `token_metadata`;
  ** *(Bug fixes)*: +
    *** Adds non-negativity checks to `sp.nat` and `sp.mutez`;
    *** ConseilJS now supports escaping sequences `\n`, `\\`, `\"`.
  ** *(Breaking changes)*: +
    *** Florence is now the default protocol.
    *** `sp.mutez(n)` and `sp.tez(n)` are now literals only, `sp.utils.nat_to_mutez(n)` and `sp.utils.nat_to_tez(n)` are used for non-constant expressions;

2021-04-29::
* New release 0.6.3 is https://smartpy.io/releases/20210429-2b02a443f7581973ec403bc7af9c67c5c4e4e4b6.
  ** `sp.set_delegate(baker)` will now fail when `baker` is not present in `voting_powers`;
  ** Efficiency improvements to recursion in code generated by `js_of_ocaml`.
  ** *(Bug fixes)*:
    *** In link:explorer.html[explorer page], `bls12_381_*` values are now represented with `bls12_381_*` types instead of `bytes`;

2021-04-28::
* New release 0.6.2 is https://smartpy.io/releases/20210428-7bdf4a71ac2001bf792918d38021fe2da7c631e7.
  ** Support `sp.sha3` and `sp.keccak` hashing algorithms in test interpreter;
  ** Add new test network RPC link:https://galphanet.smartpy.io;
  ** Efficiency improvements to recursion in code generated by `js_of_ocaml`.

2021-04-27::
* New release 0.6.1 is https://smartpy.io/releases/20210427-b20fef5af73cea46ccbe8557cfaaa4643bf5cc04.
  ** New link:reference.html#_flags[flags].
  ** `protocol` is now an initial link:reference.html#_flags[flag].
  ** Toplevel parameter annotation in generated Michelson code for
  single entry point contracts. This is controlled by a `single-entry-point-annotation` link:reference.html#_flags[flag]
  which is true by default.
  ** Exception launched when an entry point parameter is unused. This
  is controlled by a `warn-unused` link:reference.html#_flags[flag].
  No type defaulting anymore.
  ** `sp.init` is now an alias for more explicit `sp.init_storage`; new `sp.update_initial_storage` for initializing storage in link:reference.html#_contracts[contracts].
  ** FA1.2 template example with metadata.
  ** link:reference.html#_accessing_data_associated_to_contracts[typed] contract access including entry points.
  ** `delphinet.smartpy.io` node removed from origination and explorer pages.
  ** The origination page now uses known network types instead of `CUSTOM` when connecting to `kukai`.
  ** Improvements to contract linkage in scenarios after using `UNPACK`.
  ** Efficiency improvements to recursion in jsoo.
  ** *(Bug fixes)*:
    *** Fix amount field in origination page. (It did not work when an amount was specified)
  ** *(Breaking change)*: `sp.private_entry_point` is now done through
     link:reference.html#_entry_points[optional parameter] `sp.entry_point(private = True)`.

2021-04-05::
* New release is https://SmartPy.io/releases/20210405-4dc8e0b9b9fc5c58a933b461dbb10f643329716b.
  ** Misc improvements in error messages.
  ** Grouping of multiple operations in web output.
  ** Fix `c.set_initial_balance(expression)` in contract testing when using `sp.sub_entry_point` or `sp.global_lambdas`.
  ** *(Breaking change)*: `sp.create_contract` / `scenario.dynamic_contract` change of interface (from types to models).

2021-03-26::
* New release is https://SmartPy.io/releases/20210326-d2f24290eef00fe8cc3d482f052165a71a635fa3.
  ** Support lambda unpacking in the interpreter. No further execution of
  these michelson lambdas in the interpreter yet.
  ** Documentation of Michelson inlining.
  ** Multiple operations blocks in the UI.
  ** Fix of inner call validity issue.
  ** Use little endian in bls12_381_fr encoding.
  ** Delphinet is now deprecated.

2021-03-22::
* New release is https://SmartPy.io/releases/20210322-f6f10ab2bc9b5cc355ed6428ee08b6cdb31bda99.
  ** Support unpack in the test interpreter (Lambdas not supported yet).
  ** Remove Florence with Baking Accounts protocol.
  ** Add fake addresses for contract originations in test scenarios (e.g. `KT1FAKEooo1FAKEzSTATiCzSmartPyxAh632`).
  ** *(Breaking change)* `sp.modify_record(...)` was adapted to better deal with tickets link:reference.html#_records[Records^].

2021-03-06::
* New release is https://SmartPy.io/releases/20210306-a8f25571f193b940c121d1fc4bef40217fb281cf.
  ** Florence flag now unables DFS contract calls.
  ** Synchronized with open source version https://gitlab.com/SmartPy/smartpy/-/commit/473d402a18970aff0762aa011bba88bd366e3b64.

2021-03-04::
* New release is https://SmartPy.io/releases/20210304-8004e57f2b88b4212c65609abc1f9acff0f791d5. +
  ** Add (Florence, FlorenceNoBA) networks.
  ** Add protocol flag `florence` to enable Florence features.
  ** Add `baker_hash` Michelson type.
  ** Support `baker_hash` type in *SET_DELEGATE*, *VOTING_POWER*, *CREATE_CONTRACT* instructions.

2021-02::
* New release is
  ** Some small breaking changes in tickets operations. +
  ** Some changes in the flag system. +
  ** Complete removal of the `run-obsolete` and `compile-obsolete`
  commands in the CLI. +
  ** Better interaction with dynamically created contracts in test scenarios.

2021-02-18::
* New release is https://SmartPy.io/releases/20210218-663becf127363ec728080bcf3d29108f97572d49.
  ** Add Falphanet RPC.

2021-02-17::
* New release is https://SmartPy.io/releases/20210217-215a92bfa20c23e833c46dd6f75f8af5ea65e57b. +
  ** Add support for bls12-381 in the test interpreter. +
  ** Fix metadata generation. +
  ** Add support for offchain-view testing.

2021-02-12::
* New major release is https://SmartPy.io/releases/20210212-2affd5164a82b2238c9ee9593cf2a03a58bec979. +
  ** See announcement here: https://medium.com/@smartpy-io/21e2adb72ec3. +
  ** One breaking change in the CLI.

2021-01-21::
* New release is https://SmartPy.io/releases/20210121-73c41a4202aa44f8bd58098b31bceecc7edcf69b.

2021-01-18::
* New release is https://SmartPy.io/releases/20210118-6f466eee56038df8fda8b2f77819b83398346296.

2021-01-05::
* New release is https://SmartPy.io/releases/20210105-2d5cc6b5a46f5de84b3d303e67028d47587dbc68.

2020-12-24::
* New release is https://SmartPy.io/releases/20201224-436b0a5888070d6cf184489f3d7f589e4fd71e3c.

2020-12-15::
* New release is https://SmartPy.io/releases/20201215-e8463215e853d0a2aee6337c48a3f6b6a469c3ab.

2020-12-13::
* New release is https://SmartPy.io/releases/20201213-a502312f7c2cdd9deca8104ef79ef9d8a9787ff9.

2020-12-03::
* New release is https://SmartPy.io/releases/20201203-960e495db3c8cb1cfdbfd4b49a1790ba2654842e.

2020-11-30::
* New release is https://SmartPy.io/releases/20201130-57260ba1fab1eac0833935f2383beef65a909af5.

2020-11-28::
* New release is https://SmartPy.io/releases/20201128-b860cdfdedfe18b3c3538d581fb7d175284a8136.

2020-11-26::
* New release is https://SmartPy.io/releases/20201126-184c26c49ed49bf4167d8c9dbbc3b56374e51aa0.

2020-11-14::
* New release is https://SmartPy.io/releases/20201114-4b1a780963c9ffb723f285ac50f1b105b88beb46.
* New release is https://SmartPy.io/releases/20201114-5f74388ccb0f259a73262142c2f61c11d4071d2d.

2020-11-09::
* New release is https://SmartPy.io/releases/20201109-0e81e41f75db0f5676bb08f6f868477dc53755a8.

2020-11-08::
* Change of release version and mechanism. We now use our new react version.
* New release is tested on https://alt.smartpy.io/releases/20201108-33c4c60012ecf026ee0c0e2384a2c168c2a7cba8.

2020-10-28::
* New dev is https://SmartPy.io/releases/dev-20201028-3d1cc718c466afcc042b75988916c58f7cbee2d4.

2020-10-25::
* New dev is https://SmartPy.io/releases/dev-20201025-47d8831b87b9b38626939be9e23cbff966f4499a.

2020-10-24::
* New dev is https://SmartPy.io/releases/dev-20201024-715699df04b2c8eefba4be4f87d0dc148968cc8e.
* New dev is https://SmartPy.io/releases/dev-20201024-36f157966a1934fb8b24f65b2ac3cf2c2b7491d4.
  (new CLI installer)

2020-10-23::
* New dev is https://SmartPy.io/releases/dev-20201023-06858903030ad0ab23a1585820fc3fca918b1b3a.

2020-10-17::
* New dev is https://SmartPy.io/releases/dev-20201017-b53f502aaa227c9883433cc97e9ff6b783e847e7.

2020-10-14::
* New dev is https://SmartPy.io/releases/dev-20201014-b57366531b7ad14f19a863653d2f47a4a96a29d6.

2020-10-13::
* New dev is https://SmartPy.io/releases/dev-20201013-3f73b27286d189f3ef917c86aa556264b0d5dd8a.

2020-10-12::
* New dev is https://SmartPy.io/releases/dev-20201012-23263858a6143423a5dc45bab717afb4aec996a1.

2020-10-10::
* New dev is https://SmartPy.io/releases/dev-20201010-2d77c07f72777d44dcff933f4a77929904dadb7b.

2020-10-06::
* New dev is https://smartpy.io/releases/dev-20201006-d34afb8381bac41fd57f65f6f19ed50e42dcac4a.

2020-10-03::
* New dev is https://SmartPy.io/releases/dev-20201003-9cf98ec2da902382be63cec34b83904e8a378ad5.

2020-09-26::
* New dev is https://SmartPy.io/releases/dev-20200926-9b06248e88a7cf34e8d8109287f3210ec65b785f.

2020-09-24::
* New dev is https://SmartPy.io/releases/dev-20200924-23b26494361d96abf034bdbb1ad1af396f95fd61.

2020-09-22::
* New dev is https://SmartPy.io/releases/dev-20200922-b42e1a73658248e94bb1b2c86a7a1ea1898e521e.

2020-09-20::
* New dev is https://smartpy.io/releases/dev-20200920-c2657aec577b0b9c3cd42948a6d401f371a172f3.

2020-09-12::
* New dev is https://SmartPy.io/releases/dev-20200912-bbb4b34cb579f3d52320c3d2aed8ebcef04429b0.

2020-09-07::
* New dev is https://smartpy.io/releases/dev-20200907-b2b492cd2b9490e67fe0c21cf8e33a1d37e4b013.

2020-09-05::
* New dev is https://smartpy.io/releases/dev-20200905-df19dc528020cf84872871fdd38195da6602afb0.

2020-08-30::
* New dev is https://SmartPy.io/releases/dev-20200830-61244befe2c4c321c0ae6f807873e3a77811f20a.
* New react version of https://SmartPy.io
* New dev is https://SmartPy.io/releases/dev-20200830-c5ff990e1a430240fad527324bb33eeae811f71a.

2020-08-27::
* New dev is https://SmartPy.io/releases/dev-20200827-c11a23b00e589f9b3769a22933e0247f2b7bb5d7.

2020-08-24::
* New dev is https://SmartPy.io/releases/dev-20200824-49d2e75aa10f51a9194ba744ee19d27731d85e2d.

2020-08-22::
* New dev is https://SmartPy.io/releases/dev-20200822-dd0d095addb54a5227f7fb4f55150de4b065798e.
* New open source version https://gitlab.com/SmartPy/smartpy.

2020-07-22::
* New dev is https://SmartPy.io/releases/dev-20200722-cdf84730ca17634b3212be837f9604aea6abb4a3.
* Demo replaced by former dev version.
* Former demo version is: https://SmartPy.io/demo.20200317.

2020-04-10::
* Demo updated with former Development version.
* https://smartpy-io.medium.com/new-smartpy-io-version-with-support-for-the-ledger-hardware-wallet-and-a-few-other-improvements-5e6f296928d2[New Development Version] -- Ledger Hardware Wallet support, storage less contracts and inter-contract test scenarios.

2020-03-05::
* https://smartpy-io.medium.com/one-year-anniversary-release-b4e52813552c[New Development Version] -- One Year Anniversary Release with custom layouts, private entry points, lambdas, etc.

## SmartPy Command Line Interface

The command line interface is synchronized with the web version https://SmartPy.io/cli. +
Please see https://SmartPy.io/reference.html#_command_line_interface for install instructions.

## SmartPy Open Source Release

An open source release is accessible here: https://gitlab.com/SmartPy/smartpy.

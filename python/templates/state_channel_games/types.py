import smartpy as sp

class Types:
    def __init__(self, model):
        self.t_nonce      = sp.TString
        self.t_model_id   = sp.TBytes
        self.t_token_map  = sp.TMap(sp.TNat, sp.TNat)
        self.t_transfer   = sp.TRecord(sender = sp.TInt, receiver = sp.TInt, bonds = self.t_token_map)
        self.t_outcome    = sp.TVariant(game_finished = sp.TString, player_inactive = sp.TInt, player_double_played = sp.TInt)
        self.t_game_bonds = sp.TMap(sp.TInt, self.t_token_map)

        # Games #

        self.t_player = sp.TRecord(addr = sp.TAddress, pk = sp.TKey)

        self.t_constants = sp.TRecord(
            bonds                   = self.t_game_bonds,
            channel_id              = sp.TBytes,
            game_nonce              = self.t_nonce,
            model_id                = self.t_model_id,
            play_delay              = sp.TInt,
            players_addr            = sp.TMap(sp.TInt, sp.TAddress),
            settlements             = sp.TMap(self.t_outcome, sp.TList(self.t_transfer)),
        )

        self.t_meta = sp.TRecord(
            move_nb        = sp.TNat,
            current_player = sp.TInt,
            outcome        = sp.TOption(self.t_outcome),
        )

        self.t_timeouts = sp.TMap(sp.TInt, sp.TTimestamp)

        self.t_game = sp.TRecord(
            addr_players = sp.TMap(sp.TAddress, sp.TInt),
            constants    = self.t_constants,
            meta         = self.t_meta,
            state        = model.t_game_state,
            settled      = sp.TBool,
            timeouts     = self.t_timeouts,
        )

        self.t_apply_input = sp.TRecord(
            meta  = self.t_meta,
            move  = model.t_move_data,
            state = model.t_game_state,
        )

        self.t_apply_result = sp.TRecord(
            messages       = model.t_messages,
            new_state      = model.t_game_state,
            outcome        = sp.TOption(sp.TString),
        )

        self.t_model = sp.TRecord(
            init   = sp.TLambda(model.t_init_input, model.t_game_state),
            apply_ = sp.TLambda(self.t_apply_input, self.t_apply_result)
        )

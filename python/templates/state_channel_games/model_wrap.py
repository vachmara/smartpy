import smartpy as sp

def _wrap_apply(model):
    def apply_(params):
        meta  = sp.compute(params.meta)
        state = sp.unpack(params.state, model.t_game_state).open_some()
        move  = sp.unpack(params.move, model.t_move_data).open_some()
        r = sp.bind_block()
        with r:
            model.apply_(meta, state, move)
        sp.result(
            sp.record(
                outcome   = r.value.outcome,
                new_state = sp.pack(r.value.new_state),
                messages  = sp.pack(r.value.messages)
            )
        )
    return apply_

def _wrap_init_state(model):
    def init_state(params):
        params = sp.unpack(params, model.t_init_input).open_some()
        r = sp.bind_block()
        with r:
            model.init(params)
        sp.result(sp.pack(r.value))
    return init_state

def model_wrap(model):
    return sp.record(
        init   = sp.build_lambda(_wrap_init_state(model)),
        apply_ = sp.build_lambda(_wrap_apply(model))
    )

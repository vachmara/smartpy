import smartpy as sp

# N simultaneous games of M different models with code in a big_map of lambda

class BytesModel:
    t_init_input = sp.TBytes
    t_game_state = sp.TBytes
    t_move_data  = sp.TBytes
    t_messages   = sp.TBytes

model = BytesModel
types = sp.io.import_template("state_channel_games/types.py").Types(model)

t_init_type = sp.TRecord(
    bank = sp.TBigMap(
        sp.TAddress,
        types.t_token_map
    ),
    channels = sp.TBigMap(
        sp.TBytes,
        sp.TRecord(
            closed   = sp.TBool,
            nonce    = types.t_nonce,
            players  = sp.TMap(
                sp.TAddress,
                sp.TRecord(
                    bonds    = types.t_token_map,
                    pk       = sp.TKey,
                    withdraw = sp.TOption(
                        sp.TRecord(
                            challenge        = sp.TSet(sp.TBytes),
                            challenge_tokens = sp.TMap(sp.TNat, sp.TInt),
                            timeout          = sp.TTimestamp,
                            tokens           = types.t_token_map,
                        )
                    ),
                    withdraw_id = sp.TNat
                )
            ),
            withdraw_delay = sp.TInt,
        ),
    ),
    games = sp.TBigMap(
        sp.TBytes,
        types.t_game
    ),
    models = sp.TBigMap(
        types.t_model_id,
        sp.TRecord(
            init   = sp.TLambda(BytesModel.t_init_input, BytesModel.t_game_state),
            apply_ = sp.TLambda(types.t_apply_input, types.t_apply_result)
        )
    )
)

class GamePlatform_OffChainViews:
    @sp.offchain_view(pure = True)
    def offchain_init(self, params):
        """Compute the init state from a model and init params."""
        sp.result(self.data.models[params.model_id].init(params.init_params))

    @sp.offchain_view(pure = True)
    def offchain_new_game(self, params):
        game_id = sp.blake2b(sp.pack(params.constants))
        sp.set_type(params.constants.players_addr, sp.TMap(sp.TInt, sp.TAddress))
        sp.result(self.new_game_(game_id, params.constants, params.params, params.signatures))

    @sp.offchain_view(pure = True)
    def offchain_check_move(self, params):
        """Check a move performed by another party."""
        game_id   = params.game_id
        game      = sp.local('game', params.game).value
        sender    = params.sender
        move_data = params.move_data
        move_nb   = params.move_nb
        new_state = params.new_state
        new_meta  = params.new_meta
        signature = params.signature
        sp.set_type(game.constants.players_addr, sp.TMap(sp.TInt, sp.TAddress))

        sp.verify(~game.settled, "Platform_GameSettled")
        self.play_(game, move_nb, move_data, sender)

        sp.verify(new_state == game.state)
        sp.verify(new_meta  == game.meta)

        to_sign = action_new_state(game_id, game.meta, game.state)
        pk = self.data.channels[game.constants.channel_id].players[sender].pk
        sp.verify(sp.check_signature(pk, signature, to_sign), message = "Platform_badSig")

        sp.result("OK")

    @sp.offchain_view(pure = True)
    def offchain_apply_move(self, params):
        """Compute a new state from a move."""
        game      = params.game
        move_nb   = params.move_nb
        move_data = params.move_data
        sender    = params.sender
        sp.verify(~game.settled, "Platform_GameSettled")
        self.check_move_meta(game.meta, game.constants, sender, move_nb)
        params = sp.record(
            move  = move_data,
            meta  = game.meta,
            state = game.state
        )
        sp.result(self.data.models[game.constants.model_id].apply_(params))

    @sp.offchain_view(pure = True)
    def offchain_play(self, params):
        sp.verify(params.game.meta.outcome == sp.none, "Platform_NotRunning")
        sp.set_type(params.game.constants.players_addr, sp.TMap(sp.TInt, sp.TAddress))
        game = sp.local('game', params.game).value
        sp.verify(~game.settled, "Platform_GameSettled")
        sp.result(self.play_(game, params.move_nb, params.move_data, params.sender))

def action_new_game(constants, params):
    constants = sp.set_type_expr(constants, types.t_constants)
    params    = sp.set_type_expr(params, sp.TBytes)
    return sp.pack(("New Game", constants, params))

def action_new_state(game_id, new_meta, new_state):
    game_id    = sp.set_type_expr(game_id, sp.TBytes)
    new_meta   = sp.set_type_expr(new_meta, types.t_meta)
    new_state  = sp.set_type_expr(new_state, sp.TBytes)
    return sp.pack(("New State", game_id, new_meta, new_state))

def action_new_outcome(game_id, new_outcome):
    game_id     = sp.set_type_expr(game_id, sp.TBytes)
    new_outcome = sp.set_type_expr(new_outcome, sp.TString)
    return sp.pack(("New Outcome", game_id, new_outcome))

def transfer(sender, receiver, bonds):
    return sp.record(sender = sender, receiver = receiver, bonds = bonds)

def minus_tokens(a, b):
    sp.set_type(a, sp.TMap(sp.TNat, sp.TInt))
    sp.set_type(b, types.t_token_map)
    with sp.for_('token', b.items()) as token:
        with sp.if_(a.contains(token.key)):
            amount = sp.compute(a[token.key] - sp.to_int(b[token.key]))
            with sp.if_( amount == 0):
                del a[token.key]
            with sp.else_():
                a[token.key] = amount
        with sp.else_():
            a[token.key] = 0 - sp.to_int(b[token.key])

def add_tokens(a, b):
    sp.set_type(a, sp.TMap(sp.TNat, sp.TInt))
    sp.set_type(b, types.t_token_map)
    with sp.for_('token', b.items()) as token:
        with sp.if_(a.contains(token.key)):
            a[token.key] +=  sp.to_int(b[token.key])
        with sp.else_():
            amount = sp.to_int(b[token.key])
            with sp.if_(~(amount == 0)):
                a[token.key] = amount

class GamePlatform(sp.Contract, GamePlatform_OffChainViews):
    def __init__(self):
        self.add_flag('initial-cast')
        self.init_metadata("metadata", {"check": [self.offchain_check_move, self.offchain_apply_move, self.offchain_init,
                                                  self.offchain_new_game, self.offchain_play]})
        self.init_type(t_init_type)
        self.init(
            bank      = sp.big_map({}),
            channels  = sp.big_map({}),
            games     = sp.big_map({}),
            models    = sp.big_map({}),
        )

    # Methods #

    def get_player(self, constants, player_id):
        sp.set_type(constants.players_addr, sp.TMap(sp.TInt, sp.TAddress))
        return self.data.channels[constants.channel_id].players[constants.players_addr[player_id]]

    def get_running_game(self, game_id, verify_running = True):
        sp.verify(self.data.games.contains(game_id), message = sp.pair("Platform_GameNotFound: ", game_id))
        game = sp.local('game', self.data.games[game_id]).value
        if verify_running:
            sp.verify(game.meta.outcome == sp.none, "Platform_NotRunning")
        return game

    def get_opened_channel(self, channel_id):
        sp.verify(self.data.channels.contains(channel_id), message = sp.pair("Platform_ChannelNotFound: ", channel_id))
        channel = sp.local('channel', self.data.channels[channel_id]).value
        sp.verify(~channel.closed, "Platform_ChannelClosed")
        return channel

    def compute_timeouts(self, game):
        """ update timeout of current_player if other player is starving"""
        with sp.if_(game.timeouts.contains(game.meta.current_player)):
            game.timeouts[game.meta.current_player] = sp.now.add_seconds(game.constants.play_delay)
        return game

    def settle_transfers(self, players_addr, players, transfer):
        def settle_transfer(sender, sender_addr, receiver, token, amount):
            with sp.if_(~(amount == 0)):
                error_message = sp.pair("Platform_NotEnoughToken:", sp.record(sender = sender_addr, token = token, amount = amount))
                sp.verify(sender.bonds.contains(token), message = error_message)
                sender.bonds[token] = sp.as_nat(sender.bonds[token] - amount, message = error_message)
                with sp.if_(sender.bonds[token] == 0):
                    del sender.bonds[token]
                with sp.if_(receiver.bonds.contains(token)):
                    receiver.bonds[token] += amount
                with sp.else_():
                    receiver.bonds[token] = amount

        # We don't need to do anything if sender == receiver
        sender_addr   = sp.compute(players_addr[transfer.sender])
        receiver_addr = sp.compute(players_addr[transfer.receiver])
        with sp.if_(~(sender_addr == receiver_addr)):
            sender   = sp.local('sender',   players[sender_addr]).value
            receiver = sp.local('receiver', players[receiver_addr]).value
            with sp.for_("bond", transfer.bonds.items()) as bond:
                settle_transfer(sender, sender_addr, receiver, bond.key, bond.value)
            players[sender_addr]   = sender
            players[receiver_addr] = receiver

    def check_move_meta(self, meta, constants, sender, move_nb):
        """ Checks game is Running + sender is current player + move is current_move """
        sp.set_type(constants.players_addr, sp.TMap(sp.TInt, sp.TAddress))
        sp.verify(sender == constants.players_addr[meta.current_player], message="Platform_GameWrongPlayer")
        sp.verify(meta.move_nb == move_nb, message = "Platform_Wrongmove_nb")
        sp.verify(meta.outcome == sp.none, "Platform_GameNotRunning")

    # Entry points #

    @sp.entry_point
    def new_model(self, params):
        model_id = sp.blake2b(sp.pack(params.model))
        self.data.models[model_id] = params.model

    @sp.entry_point
    def new_channel(self, players, withdraw_delay, nonce):
        channel_id = sp.blake2b(sp.pack((sp.self_address, players, nonce)))
        sp.verify(~self.data.channels.contains(channel_id), message = "Platform_ChannelAlreadyExists")
        players_map = sp.local('players_map', sp.map())
        with sp.for_("player", players.items()) as player:
            players_map.value[player.key] = sp.record(
                bonds       = {},
                pk          = player.value,
                withdraw    = sp.none,
                withdraw_id = 0,
            )
        self.data.channels[channel_id] = sp.record(
            closed         = False,
            nonce          = nonce,
            players        = players_map.value,
            withdraw_delay = withdraw_delay,
        )

    def new_game_(self, game_id, constants, params, signatures):
        sp.set_type(constants, types.t_constants)
        sp.set_type(params, model.t_init_input)
        sp.set_type(signatures, sp.TMap(sp.TKey, sp.TSignature))
        sp.verify(~self.data.games.contains(game_id), message = "Platform_GameAlreadyExists")
        channel = self.get_opened_channel(constants.channel_id)

        addr_players = sp.local('addr_players', sp.map()).value
        with sp.for_("addr_player", constants.players_addr.items()) as addr_player:
            addr_players[addr_player.value] = addr_player.key
            sp.verify(channel.players.contains(addr_player.value), message = "Platform_GamePlayerNotInChannel")

        with sp.for_('player', channel.players.values()) as player:
            sp.verify(signatures.contains(player.pk), "Platform_MissingSig")
            sp.verify(sp.check_signature(player.pk, signatures[player.pk], action_new_game(constants, params)))

        return sp.record(
            addr_players = addr_players,
            constants    = constants,
            meta         = sp.record(
                move_nb        = 0,
                current_player = 1,
                outcome        = sp.set_type_expr(sp.none, sp.TOption(types.t_outcome))
                ),
            settled      = False,
            state        = self.data.models[constants.model_id].init(params),
            timeouts     = sp.set_type_expr(sp.map({}), types.t_timeouts)
        )

    @sp.entry_point
    def new_game(self, constants, params, signatures):
        game_id = sp.blake2b(sp.pack(constants))
        self.data.games[game_id] = self.new_game_(game_id, constants, params, signatures)

    @sp.entry_point
    def cancel_new_game(self, game_id):
        sp.verify(self.data.games.contains(game_id), ('Platform_GameNotFound: ', game_id))
        game = sp.local('game', self.data.games[game_id]).value
        sp.verify(sp.sender == game.constants.players_addr[1], "Platform_NotGameCreator")
        del self.data.games[game_id]

    def play_(self, game, move_nb, move_data, sender):
        sp.set_type(move_nb, sp.TNat)
        sp.set_type(move_data, model.t_move_data)
        self.check_move_meta(game.meta, game.constants, sender, move_nb)
        params = sp.record(
            move  = move_data,
            meta  = game.meta,
            state = game.state
        )
        apply_result = sp.compute(self.data.models[game.constants.model_id].apply_(params))
        game.meta = sp.record(
            move_nb        = game.meta.move_nb +1,
            current_player =  3 - game.meta.current_player,
            outcome        = sp.eif(apply_result.outcome.is_some(), sp.some(sp.variant("game_finished", apply_result.outcome.open_some())), sp.none),
        )
        game.state = apply_result.new_state
        return game

    @sp.entry_point
    def play(self, game_id, move_nb, move_data):
        game = self.get_running_game(game_id)
        self.play_(game, move_nb, move_data, sp.sender)
        self.data.games[game_id] = self.compute_timeouts(game)

    @sp.entry_point
    def set_outcome(self, game_id, outcome, signatures):
        sp.set_type(signatures, sp.TMap(sp.TKey, sp.TSignature))
        game = self.get_running_game(game_id)
        to_sign = sp.compute(action_new_outcome(game_id, outcome))
        players = sp.local('players', self.data.channels[game.constants.channel_id].players).value
        with sp.for_('player', players.values()) as player:
            sp.verify(signatures.contains(player.pk), "Platform_MissingSig")
            sp.verify(sp.check_signature(player.pk, signatures[player.pk], to_sign), message = "Platform_badSig")
        game.meta.outcome = sp.some(sp.variant("game_finished", outcome))
        self.data.games[game_id] = game

    @sp.entry_point
    def settle(self, game_id):
        """ Set game to settled and update channel bonds depending on outcome.

            3 Types of outcome exists:
                - `player_double_played`: set by `double_signed`
                                              or `double_signed_offchain`
                - `player_inactive`: set by `starved` entrypoint
                - `game_finished`: set by `apply_` lambda after a `play`
                                    or by the meta.outcome of `update_state`

            Outcome value of `player_double_played` and `player_inactive`
            represents the id of the player associated with the fault.
            Outcome value of `game_finished` is freely defined by the game.

            Transfers are defined by the game constant `settlements`.
            Each outcome and outcome value is associated with a list of transfers.

            No transfer occurs if the outcome and outcome value aren't in `settlements`.

            Transfer decreases bonds of the sender and increases bonds of the receiver.

            If the sender doesn't own enough to perform the transfer, the WHOLE settle FAILS.
        """
        game = self.get_running_game(game_id, verify_running = False)
        sp.verify(~game.settled, message = "Plateform_GameSettled")

        # Don't forget to check if outcome is not none
        with sp.if_(game.constants.settlements.contains(game.meta.outcome.open_some("Plateform_GameIsntOver"))):
            transfers = sp.compute(game.constants.settlements[game.meta.outcome.open_some()])
            players = self.get_opened_channel(game.constants.channel_id).players
            with sp.for_("transfer", transfers) as transfer:
                self.settle_transfers(game.constants.players_addr, players, transfer)

            self.data.channels[game.constants.channel_id].players = players

        game.settled = True
        self.data.games[game_id] = game

    # State channels #

    @sp.entry_point
    def update_state(self, game_id, new_meta, new_state, signatures):
        """ Update game state if new_meta.nbMoves > game.nbMoves."""
        sp.set_type(signatures, sp.TMap(sp.TKey, sp.TSignature))
        game = self.get_running_game(game_id)
        sp.verify(new_meta.move_nb > game.meta.move_nb)
        to_sign = sp.compute(action_new_state(game_id, new_meta, new_state))
        players = sp.local('players', self.data.channels[game.constants.channel_id].players).value
        with sp.for_('player', players.values()) as player:
            sp.verify(sp.check_signature(player.pk, signatures[player.pk], to_sign), message = "Platform_badSig")

        game.meta  = new_meta
        game.state = new_state
        self.data.games[game_id] = self.compute_timeouts(game)

    @sp.entry_point
    def starving(self, game_id, flag):
        """ If flag is True, the player indicates that the other player
            no longer has his confidence.
            The latter must play on chain until he removes the flag"""
        game = self.get_running_game(game_id)
        player_id = game.addr_players[sp.sender]
        with sp.if_(flag):
            game.timeouts[3-player_id] = sp.now.add_seconds(game.constants.play_delay)
        with sp.else_():
            del game.timeouts[3-player_id]
        self.data.games[game_id] = game

    @sp.entry_point
    def starved(self, game_id, player_id):
        game = self.get_running_game(game_id)
        sp.verify(game.timeouts.contains(player_id), message = "Platform_NotTimeoutSetup")
        sp.verify(sp.now > game.timeouts[player_id], message = "Platform_NotTimedOut")
        game.meta.outcome = sp.some(sp.variant("player_inactive", player_id))
        self.data.games[game_id] = game

    @sp.entry_point
    def double_signed(self, game_id, new_meta, new_state, sig):
        game = self.get_running_game(game_id)
        # The verification that game_id of the signed new State equals the game_id is implicit in get_running_game
        sp.verify(new_meta.move_nb == game.meta.move_nb, message = "Platform_NotSameMove")
        sp.verify(~( (new_meta == game.meta) & (new_state == game.state)), message = "Platform_NotDifferentStates")
        to_sign = sp.compute(action_new_state(game_id, new_meta, new_state))
        sp.verify(sp.check_signature(self.get_player(game.constants, 3-game.meta.current_player).pk, sig, to_sign), message = "Platform_badSig")
        game.meta.outcome = sp.some(sp.variant("player_double_played", game.meta.current_player))
        self.data.games[game_id] = game

    @sp.entry_point
    def double_signed_offchain(self, game_id, new_meta1, new_state1, new_meta2, new_state2, sig1, sig2, player):
        sp.set_type(new_meta1, types.t_meta)
        sp.set_type(new_meta2, types.t_meta)
        sp.set_type(new_state1, BytesModel.t_game_state)
        sp.set_type(new_state2, BytesModel.t_game_state)
        game = self.get_running_game(game_id)
        # The verification that game_id of the signed New State #1 equals signed New State #2 is implicit
        # Because we verify the validity of sig1 and sig2 with the same game_id in the record.
        sp.verify(new_meta1.move_nb == new_meta2.move_nb)
        sp.verify(~( (new_meta1 == new_meta2) & (new_state1 == new_state2)), message = "Platform_NotSameStates")
        to_sign1 = sp.compute(action_new_state(game_id, new_meta1, new_state1))
        to_sign2 = sp.compute(action_new_state(game_id, new_meta2, new_state2))
        sp.verify(sp.check_signature(self.get_player(game.constants, player).pk, sig1, to_sign1), message = "Platform_badSig")
        sp.verify(sp.check_signature(self.get_player(game.constants, player).pk, sig2, to_sign2), message = "Platform_badSig")
        game.meta.outcome = sp.some(sp.variant("player_double_played", player))
        self.data.games[game_id] = game

    # Tokens #

    @sp.entry_point
    def push_bonds(self, player_addr, channel_id, bonds):
        # TODO: real transfer receive
        players = self.get_opened_channel(channel_id).players
        sp.verify(players.contains(player_addr), "Platform_NotChannelPlayer")
        player = sp.local('player', players[player_addr]).value
        with sp.for_("token", bonds.keys()) as token:
            with sp.if_(player.bonds.contains(token)):
                player.bonds[token] += bonds[token]
            with sp.else_():
                player.bonds[token] = bonds[token]
        self.data.channels[channel_id].players[player_addr] = player

    @sp.entry_point
    def bank_to_bonds(self, channel_id, receiver, tokens):
        sp.verify(self.data.bank.contains(sp.sender), "Platform_NotEnoughToken")
        channel = self.get_opened_channel(channel_id)
        sp.verify(channel.players.contains(receiver), "Platform_ReceiverNotInChannel")
        sender = sp.local('sender', self.data.bank[sp.sender]).value
        to = sp.local('to', channel.players[receiver]).value
        with sp.for_("token", tokens.items()) as token:
            with sp.if_(token.value > 0):
                sp.verify(sender.contains(token.key), message = "Platform_NotEnoughToken")
                sender[token.key] = sp.as_nat(sender[token.key] - token.value, message = "Platform_NotEnoughToken")
                with sp.if_(to.bonds.contains(token.key)):
                    to.bonds[token.key] += token.value
                with sp.else_():
                    to.bonds[token.key] = token.value
        self.data.channels[channel_id].players[receiver] = to
        self.data.bank[sp.sender] = sender

    @sp.entry_point
    def withdraw_request(self, channel_id, tokens):
        channel = self.get_opened_channel(channel_id)
        sp.verify(channel.players.contains(sp.sender), "Platform_SenderNotInChannel")
        player = sp.local("player", channel.players[sp.sender])
        player.value.withdraw_id += 1
        player.value.withdraw = sp.some(
            sp.record(
                challenge        = sp.set({}),
                challenge_tokens = sp.map({}),
                timeout          = sp.now.add_seconds(channel.withdraw_delay),
                tokens           = tokens,
            )
        )
        channel.players[sp.sender] = player.value
        self.data.channels[channel_id] = channel

    @sp.entry_point
    def withdraw_challenge(self, channel_id, withdrawer, withdraw_id, game_ids):
        """ A withdraw can be challenged by giving a set of game_id.
            The challenge will be valid if the total bonds of the non settled game exceed the withdraw.
        """
        sp.set_type(game_ids, sp.TSet(sp.TBytes))
        channel = self.get_opened_channel(channel_id)
        sp.verify(channel.players.contains(sp.sender), "Platform_SenderNotInChannel")
        sp.verify(channel.players.contains(withdrawer), "Platform_WithdrawerNotInChannel")
        sp.verify(channel.players[withdrawer].withdraw.is_some(), "Platform_NoWithdrawRequestOpened")
        sp.verify(channel.players[withdrawer].withdraw_id == withdraw_id, "Platform_WithdrawIdMismatch")
        withdraw = sp.local('withdraw', channel.players[withdrawer].withdraw.open_some()).value
        with sp.for_('game_id', game_ids.elements()) as game_id:
            game = self.get_running_game(game_id, verify_running = False)
            sp.verify(game.constants.channel_id == channel_id, "Platform_ChannelIdMismatch")
            with sp.if_(game.settled):
                sp.verify(withdraw.challenge.contains(game_id), "Platform_GameSettled")
                withdraw.challenge.remove(game_id)
                minus_tokens(withdraw.challenge_tokens, game.constants.bonds[game.addr_players[withdrawer]])
            with sp.else_():
                with sp.if_(~withdraw.challenge.contains(game_id)):
                    withdraw.challenge.add(game_id)
                    add_tokens(withdraw.challenge_tokens, game.constants.bonds[game.addr_players[withdrawer]])
        with sp.if_(~(withdrawer==sp.sender)):
            withdraw.timeout = sp.now
        channel.players[withdrawer].withdraw = sp.some(withdraw)
        self.data.channels[channel_id] = channel

    @sp.entry_point
    def finalise_withdraw(self, channel_id, withdraw_id):
        """ A withdraw challenge can be answered if both of the following conditions are met:
            - the `timeout` timedout
            - challenge_tokens doesn't contain positive value
        """
        channel = self.get_opened_channel(channel_id)
        sp.verify(channel.players.contains(sp.sender), "Platform_SenderNotInChannel")
        player = sp.local('player', channel.players[sp.sender]).value
        sp.verify(player.withdraw.is_some(), "Platform_NoWithdrawOpened")
        sp.verify(player.withdraw_id == withdraw_id, "Platform_WithdrawIdMismatch")
        withdraw = sp.local('withdraw', player.withdraw.open_some()).value
        sp.verify(sp.now >= withdraw.timeout, message = "Platform_ChallengeDelayNotOver")

        with sp.for_('token', withdraw.challenge_tokens.items()) as token:
            with sp.if_((token.value > 0) & player.bonds.contains(token.key)):
                sp.verify((player.bonds[token.key]-withdraw.tokens[token.key]) > token.value, "Platform_TokenChallenged")

        # Transfer withdraw
        bank = sp.local('bank', {})
        with sp.if_(self.data.bank.contains(sp.sender)):
            bank.value = self.data.bank[sp.sender]
        with sp.for_('token', withdraw.tokens.items()) as token:
            with sp.if_(token.value > 0):
                sp.verify(player.bonds.contains(token.key), message = "Platform_NotEnoughTokens")
                player.bonds[token.key] = sp.as_nat(player.bonds[token.key] - token.value, message = "Platform_NotEnoughTokens")
                with sp.if_(bank.value.contains(token.key)):
                    bank.value[token.key] += token.value
                with sp.else_():
                    bank.value[token.key] = token.value

        self.data.bank[sp.sender] = bank.value
        player.withdraw = sp.none
        channel.players[sp.sender] = player
        self.data.channels[channel_id] = channel


# Test utils #

def outcome(gamePlatform, game_id):
        return gamePlatform.data.games[game_id].meta.outcome.open_some()

def finished_outcome(gamePlatform, game_id):
    return outcome(gamePlatform, game_id).open_variant('game_finished')

def player_bonds(gamePlatform, channel_id, player_addr):
    return gamePlatform.data.channels[channel_id].players[player_addr].bonds

sp.add_compilation_target("platform", GamePlatform(), flags = ["no-simplify-via-michel", "no-decompile"])

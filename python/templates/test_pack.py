import smartpy as sp

nats = [
    0,
    1,
    2,
    3,
    10,
    10000,
    1000000,
    1000000000000
    ]

implicit_accounts = ['tz1fextP23D6Ph2zeGTP8EwkP5Y8TufeFCHA',
                     'tz2FCNBrERXtaTtNX6iimR1UJ5JSDxvdHM93',
                     'tz3WMqdzXqRWXwyvj5Hp2H7QEepaUuS7vd9K'
                     ]

def f1(x):
    sp.if x.a:
        sp.result(123)
    sp.else:
        sp.result(x.b)

def f2(x):
    sp.if x.a:
        sp.result(123)
    sp.else:
        sp.result(x.b + x.c)

with_unpack = ([True, False, ()]
               + [sp.mutez(x) for x in nats]
               + [sp.nat(x) for x in nats]
               + [sp.int(x) for x in nats]
               + [sp.int(-x) for x in nats]
               + ["", "a", "ab", "abc"]
               + [sp.bytes("0x"), sp.bytes("0x00"), sp.bytes("0xab00")]
               + [(1, 2), ("a", True)]
               + [sp.timestamp(123)]
               + [sp.signature("sigTBpkXw6tC72L2nJ2r2Jm5iB6uidTWqoMNd4oEawUbGBf5mHVfKawFYL8X8MJECpL73oBnfujyUZNLK2LQWD1FaCkYMP4j"),
                  sp.signature("spsig1PPUFZucuAQybs5wsqsNQ68QNgFaBnVKMFaoZZfi1BtNnuCAWnmL9wVy5HfHkR6AeodjVGxpBVVSYcJKyMURn6K1yknYLm"),
                  sp.signature("p2sigsceCzcDw2AeYDzUonj4JT341WC9Px4wdhHBxbZcG1FhfqFVuG7f2fGCzrEHSAZgrsrQWpxduDPk9qZRgrpzwJnSHC3gZJ"),
                  sp.make_signature(sp.test_account("keystore").secret_key, sp.pack("value"), message_format = 'Raw')]
               + [sp.address(account) for account in implicit_accounts]
               + [sp.key_hash(account) for account in implicit_accounts]
               + [sp.address('KT1Mpqi89gRyUuoXUPAWjHkqkk1F48eUKUVy'),
                  sp.address('KT1Mpqi89gRyUuoXUPAWjHkqkk1F48eUKUVy%'),
                  sp.address('KT1Mpqi89gRyUuoXUPAWjHkqkk1F48eUKUVy%foo')]
               + [range(0, n) for n in range(1, 8)]
               + [sp.set([1, 2, 3, 4])]
               + [sp.timestamp(123), sp.address('tz1fextP23D6Ph2zeGTP8EwkP5Y8TufeFCHA')]
               + [lambda x: x + 2, f1, f2, sp.lambda_michelson("DUP; PUSH int 42; ADD; MUL", sp.TInt, sp.TInt)]
               )

no_unpack = ([sp.baker_hash('SG1jfZeHRzeWAM1T4zrwunEyUpwWc82D4tbv')])

class Pack(sp.Contract):
    def __init__(self, l, unpack = True):
        self.l = l
        self.unpack = unpack
        self.init()

    @sp.entry_point
    def run(self):
        for x in self.l:
            packed = sp.to_constant(sp.pack(x))
            sp.verify(sp.pack(x) == packed)
            if self.unpack:
                sp.verify_equal(x, sp.to_constant(sp.unpack(packed).open_some()))

@sp.add_test(name = "Pack")
def test():
    scenario = sp.test_scenario()

    c1 = Pack(with_unpack[0:30])
    scenario.register(c1)
    c1.run()

    c1 = Pack(with_unpack[30:])
    scenario.register(c1)
    c1.run()

    c1 = Pack(no_unpack, unpack = False)
    scenario.register(c1)
    c1.run()

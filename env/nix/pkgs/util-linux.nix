{ stdenv, fetchurl, lib }:

stdenv.mkDerivation rec {
  pname = "util-linux";
  version = "2.36";
  src = fetchurl {
    url = "mirror://kernel/linux/utils/util-linux/v${lib.versions.majorMinor version}/${pname}-${version}.tar.xz";
    sha256 = "1cg0m4psswg71v6wrqc2bngcw20fsp01vbijxdzvdf8kxdkiqjwy";
  };
  configureFlags = [
    "--disable-ipcrm"
    "--disable-ipcs"
    "--disable-use-tty-group"
    "--disable-libfdisk"
    "--disable-libblkid"
    "--disable-libuuid"
    "--disable-libsmartcols"
    "--disable-libblkid"
  ];
}

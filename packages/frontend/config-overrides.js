//
//  ONLY REQUIRED IN DEVELOPMENT ENVIRONMENT
//
//  THIS CAN BE REMOVED AFTER EDITOR PAGE GETS MIGRATED TO REACT
//  - yarn remove customize-cra react-app-rewired
//  - delete this file
//  - replace (react-app-rewired start) with (react-scripts start) in package.json
//

const { overrideDevServer } = require('customize-cra');

const routes = ['ide$', 'michelson$', 'nodes$', 'wallet$'];

module.exports = {
    devServer: overrideDevServer((config) => ({
        ...config,
        historyApiFallback: true,
        proxy: routes.reduce(
            (state, route) => ({
                ...state,
                [`/${route}`]: {
                    target: `${process.env.HTTPS === 'true' ? 'https' : 'http'}://[::1]:${process.env.PORT || 3000}`,
                    pathRewrite: { [`^/${route}`]: '' },
                    secure: false,
                    changeOrigin: true,
                },
            }),
            {},
        ),
    }))
};

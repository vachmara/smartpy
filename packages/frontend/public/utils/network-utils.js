const Networks = {
    MAINNET: 'Mainnet',
    EDO2NET: 'Edonet',
    FLORENCENOBANET: 'Florencenet',
    GRANADANET: 'Granadanet',
};

const EXPLORER = {
    MAINNET: {
        tzstats: 'https://tzstats.com',
        tzkt: 'https://tzkt.io',
        bcd: 'https://better-call.dev',
    },
    EDO2NET: {
        tzstats: 'https://edo.tzstats.com',
        tzkt: 'https://edo2net.tzkt.io',
        bcd: 'https://better-call.dev',
    },
    FLORENCENOBANET: {
        tzstats: '',
        tzkt: 'https://florencenet.tzkt.io',
        bcd: 'https://better-call.dev',
    },
    GRANADANET: {
        tzstats: '',
        tzkt: '',
        bcd: '',
    },
};

const API = {
    MAINNET: {
        tzkt: 'https://api.tzkt.io/v1',
        tzstats: 'https://api.tzstats.com',
    },
    EDO2NET: {
        tzkt: 'https://edonet.smartpy.io/indexer',
        tzstats: 'https://api.edo.tzstats.com',
    },
    FLORENCENOBANET: {
        tzkt: 'https://api.florencenet.tzkt.io/v1',
        tzstats: '',
    },
    GRANADANET: {
        tzstats: '',
        tzkt: '',
    },
};

const smartPyNodes = {
    'https://mainnet.smartpy.io': Networks.MAINNET,
    'https://edonet.smartpy.io': Networks.EDO2NET,
    'https://florencenet.smartpy.io': Networks.FLORENCENOBANET,
    'https://granadanet.smartpy.io': Networks.GRANADANET,
};

const gigaNodes = {
    'https://mainnet-tezos.giganode.io': Networks.MAINNET,
    'https://edonet-tezos.giganode.io': Networks.EDO2NET,
    'https://florence-tezos.giganode.io': Networks.FLORENCENOBANET,
};

const networkFilterByFeature = {
    faucet: ['mainnet'],
};

/**
 * Filter network by feature
 *
 * @param {string} feature
 * @param {string} node
 *
 * @returns true to skip, false otherwise
 */
const shouldSkipNetwork = (feature, node) => {
    const featureFilter = networkFilterByFeature[feature];
    return featureFilter && featureFilter.some((filter) => node.toLowerCase().includes(filter));
};

/**
 *  GET Request
 */
const requestGET = (url) =>
    new Promise((resolve, reject) => {
        const req = new XMLHttpRequest();
        req.timeout = 2000;
        req.onreadystatechange = () => {
            if (req.readyState === 4) {
                if (req.status === 200) {
                    resolve(JSON.parse(req.response));
                } else {
                    reject(req.responseText);
                }
            }
        };
        req.open('GET', url, true);
        req.send();
    });

/**
 * Get the RPC network.
 * @param {string} rpcAddress - RPC address ( e.g. https://mainnet.smartpy.io )
 * @return {string} One of the following networks [MAINNET, EDONET, FLORENCENET]
 */
const getRpcNetwork = async (rpcAddress) => {
    const {
        network_version: { chain_name },
    } = await requestGET(`${rpcAddress}/version`).catch(async (e) => {
        console.warn(e);
        return {
            network_version: await requestGET(`${rpcAddress}/network/version`),
        };
    });

    const network = chain_name.split('_')[1];
    if (!Object.keys(Networks).includes(network)) {
        console.error(`Unknown network: ${network}.`);
    }

    return network?.replace(Networks.EDO2NET, Networks.EDONET);
};

import { Protocol, ProtocolHash, DefaultProtocol } from './protocol';

export enum Network {
    MAINNET = 'Mainnet',
    EDONET = 'Edonet',
    FLORENCENET = 'Florencenet',
    GRANADANET = 'Granadanet',
}

export const NetworkProtocolHash: Record<string, string> = {
    [Network.MAINNET]: ProtocolHash[DefaultProtocol],
    [Network.EDONET]: ProtocolHash[Protocol.EDO],
    [Network.FLORENCENET]: ProtocolHash[Protocol.FLORENCE],
    [Network.GRANADANET]: ProtocolHash[Protocol.GRANADA],
};

export const DeprecatedNetworks: Network[] = [];

import { Network } from './networks';

export const smartpy = {
    [Network.MAINNET]: 'https://mainnet.smartpy.io',
    [Network.EDONET]: 'https://edonet.smartpy.io',
    [Network.FLORENCENET]: 'https://florencenet.smartpy.io',
    [Network.GRANADANET]: 'https://granadanet.smartpy.io',
};

const giga = {
    [Network.MAINNET]: 'https://mainnet-tezos.giganode.io',
    [Network.EDONET]: 'https://edonet-tezos.giganode.io',
    [Network.FLORENCENET]: 'https://florence-tezos.giganode.io',
};

export const Nodes = {
    // SMARTPY
    [smartpy.Mainnet]: Network.MAINNET,
    [smartpy.Edonet]: Network.EDONET,
    [smartpy.Florencenet]: Network.FLORENCENET,
    [smartpy.Granadanet]: Network.GRANADANET,
    // GIGANODE
    [giga.Mainnet]: Network.MAINNET,
    [giga.Florencenet]: Network.FLORENCENET,
    [giga.Edonet]: Network.EDONET,
};

export enum Endpoint {
    HEADER = '/chains/main/blocks/head/header',
    ERRORS = '/chains/main/blocks/head/context/constants/errors',
    VERSION = '/version',
    CHECKPOINT = '/chains/main/checkpoint',
    CONSTANTS = '/chains/main/blocks/head/context/constants',
    METADATA = '/chains/main/blocks/head/metadata',
}

export default Nodes;

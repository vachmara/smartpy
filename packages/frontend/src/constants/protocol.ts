export enum Protocol {
    DELPHI = 'Delphi',
    EDO = 'Edo',
    FLORENCE = 'Florence',
    GRANADA = 'Granada',
}

export const ProtocolHash: Record<string, string> = {
    [Protocol.DELPHI]: 'PsDELPH1Kxsxt8f9eWbxQeRxkjfbxoqM52jvs5Y5fBxWWh4ifpo',
    [Protocol.EDO]: 'PtEdo2ZkT9oKpimTah6x2embF25oss54njMuPzkJTEi5RqfdZFA',
    [Protocol.FLORENCE]: 'PsFLorenaUUuikDWvMDr6fGBRG8kt3e3D3fHoXK1j1BFRxeSH4i',
    [Protocol.GRANADA]: 'PtGRANADsDU8R9daYKAgWnQYAJ64omN1o3KMGVCykShA97vQbvV',
};

export const DefaultProtocol = Protocol.FLORENCE;

import { Ace } from 'ace-builds';

import completers from '../constants/completers';
import { DefaultProtocol } from '../../../constants/protocol';

export const evalCode = (t: (str: string) => void, code: string, protocol = String(DefaultProtocol)) => {
    if (!code) {
        return;
    }
    try {
        window.smartmlCtx.call_exn_handler('update_michelson_view', code, protocol);
    } catch (error) {
        if (typeof window.smartpyContext.setFirstMessage === 'function') {
            window.smartpyContext?.setFirstMessage(`${t('michelsonIde.errorsWhileParsingMichelson')}\n${error}`);
        }
    }
};

export const getCompleters = () => ({
    getCompletions: (
        editor: Ace.Editor,
        session: Ace.EditSession,
        point: Ace.Point,
        prefix: string,
        callback: (a: unknown, b: Ace.Completion[]) => void,
    ) => {
        callback(null, completers);
    },
});

import { combineReducers } from 'redux';
import { createReducer } from 'typesafe-actions';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

import {
    updateContract,
    addContract,
    removeContract,
    updateSettings,
    selectContract,
    showError,
    hideError,
    showShortcuts,
    hideShortcuts,
    setVolatileContract,
} from './actions';
import { IDEContract, MichelsonIDESettings } from 'SmartPyModels';
import { generateID } from '../../utils/rand';
import { DefaultProtocol } from '../../constants/protocol';

const DEFAULT_SETTINGS: MichelsonIDESettings = {
    layout: 'side-by-side',
    electricEvaluation: true,
    protocol: DefaultProtocol,
};

// CONTRACTS REDUCER

const currentContractReducer = createReducer('')
    // Reset the current contract
    .handleAction(removeContract, (state, { payload }) => (payload === state ? '' : state))
    .handleAction(selectContract, (_, { payload }) => payload);

const contractsReducer = createReducer([] as IDEContract[])
    .handleAction(removeContract, (state, { payload }) => state.filter(({ id }) => id !== payload))
    .handleAction(addContract, (state, { payload, meta: overrideTemplate }) => {
        const contractID = generateID();
        return [
            ...state,
            {
                name: `unnamed_${contractID}`,
                ...payload,
                id: contractID,
                updatedAt: new Date().toISOString(),
            },
        ];
    })
    .handleAction(updateContract, (state, { payload }) => {
        const contractIndex = state.findIndex(({ id }) => id === payload.id);
        if (contractIndex !== -1) {
            state[contractIndex] = {
                ...state[contractIndex],
                ...payload,
                name: payload.name || `unnamed_${payload.id}`,
                updatedAt: new Date().toISOString(),
            };
        }
        return [...state];
    });

// VOLATILE CONTRACT REDUCER

const volatileContractReducer = createReducer('').handleAction(setVolatileContract, (_, { payload }) => payload);

// ERRORS REDUCER

const errorReducer = createReducer('')
    .handleAction(hideError, () => '')
    .handleAction(showError, (_, action) => action.payload);

// SHORTCUTS

const shortcutsReducer = createReducer('')
    .handleAction(hideShortcuts, () => '')
    .handleAction(showShortcuts, (_, action) => action.payload);

// SETTINGS

const settingsReducer = createReducer(DEFAULT_SETTINGS).handleAction(updateSettings, (state, action) => ({
    ...state,
    ...action.payload,
}));

const persistConfig = {
    key: 'michelsonEditor',
    storage: storage,
    whitelist: ['currentContract', 'contracts', 'settings'],
};

const editorReducer = combineReducers({
    volatileContract: volatileContractReducer,
    currentContract: currentContractReducer,
    contracts: contractsReducer,
    settings: settingsReducer,
    error: errorReducer,
    shortcuts: shortcutsReducer,
});

export default persistReducer(persistConfig, editorReducer);
export type EditorState = ReturnType<typeof editorReducer>;

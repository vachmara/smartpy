import React from 'react';
import { useDispatch } from 'react-redux';

// Material UI
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Menu from '@material-ui/core/Menu';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Switch from '@material-ui/core/Switch';

import { IDELayout } from 'SmartPyModels';
import selectors from '../../../store/selectors';
import actions from '../../../store/root-action';

// Local Elements
import SettingsButton from '../../common/elements/SettingsButton';
import useTranslation from '../../i18n/hooks/useTranslation';
import ProtocolSelector from './ProtocolSelector';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        menuRoot: {
            width: 230,
            margin: theme.spacing(1),
        },
        select: {
            padding: 12,
            minWidth: 90,
        },
        menuItem: {
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
            margin: theme.spacing(1),
        },
        settingSection: {
            fontWeight: 'bold',
        },
    }),
);

interface OwnProps {
    onlyIcon: boolean;
}

const SettingsMenuItem: React.FC<OwnProps> = ({ onlyIcon }) => {
    const classes = useStyles();
    const t = useTranslation();
    const [open, setOpen] = React.useState(false);
    const anchorRef = React.useRef<HTMLButtonElement>(null);
    const settings = selectors.michelsonEditor.useSettings();
    const dispatch = useDispatch();

    const handleToggle = () => {
        setOpen((prevState) => !prevState);
    };

    const handleClose = (event: React.MouseEvent<EventTarget>) => {
        if (anchorRef.current && anchorRef.current.contains(event.target as HTMLElement)) {
            return;
        }
        setOpen(false);
    };

    const handleLayoutChange = (event: React.ChangeEvent<{ value: unknown }>) => {
        dispatch(actions.michelsonEditor.updateSettings({ layout: event.target.value as IDELayout }));
    };

    const handleElectricEvaluationChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        dispatch(actions.michelsonEditor.updateSettings({ electricEvaluation: event.target.checked }));
    };

    return (
        <React.Fragment>
            <SettingsButton
                ref={anchorRef}
                aria-controls="settings-menu"
                aria-haspopup="true"
                onClick={handleToggle}
                onlyIcon={onlyIcon}
            />
            <Menu
                anchorEl={anchorRef.current}
                keepMounted
                open={Boolean(open)}
                onClose={handleClose}
                getContentAnchorEl={null}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
            >
                <div className={classes.menuRoot}>
                    <Typography variant="overline" gutterBottom className={classes.settingSection}>
                        UI
                    </Typography>
                    <Divider />
                    <div className={classes.menuItem}>
                        <Typography variant="caption">{t('ide.settings.layout')}</Typography>
                        <Select
                            variant="filled"
                            value={settings.layout}
                            classes={{ filled: classes.select }}
                            onChange={handleLayoutChange}
                        >
                            <MenuItem value="side-by-side">{t('ide.settings.sideBySide')}</MenuItem>
                            <MenuItem value="stacked">{t('ide.settings.stacked')}</MenuItem>
                            <MenuItem value="editor-only">{t('ide.settings.editorOnly')}</MenuItem>
                            <MenuItem value="output-only">{t('ide.settings.outputOnly')}</MenuItem>
                        </Select>
                    </div>
                    <Divider />
                    <div className={classes.menuItem}>
                        <Typography variant="caption">{t('michelsonIde.electricEvaluation')}</Typography>
                        <Switch
                            color="primary"
                            checked={settings.electricEvaluation}
                            onChange={handleElectricEvaluationChange}
                            name="electricEvaluation"
                            inputProps={{ 'aria-label': t('michelsonIde.electricEvaluation') }}
                        />
                    </div>
                    <Divider />
                    <div className={classes.menuItem}>
                        <Typography variant="caption">{t('ide.settings.protocol')}</Typography>
                        <ProtocolSelector />
                    </div>
                </div>
            </Menu>
        </React.Fragment>
    );
};

export default SettingsMenuItem;

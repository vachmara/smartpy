import React from 'react';
import { useDispatch } from 'react-redux';

import { useError } from '../selectors';
import actions from '../actions';
import ErrorDialog from '../../common/components/ErrorDialog';
import OutputWrapper from '../../common/styles/OutputCss';

const EditorErrorDialog = () => {
    const error = useError();
    const dispatch = useDispatch();

    const handleClose = () => dispatch(actions.hideError());

    return (
        <ErrorDialog open={!!error} onClose={handleClose}>
            <OutputWrapper>
                <div dangerouslySetInnerHTML={{ __html: error }}></div>
            </OutputWrapper>
        </ErrorDialog>
    );
};

export default EditorErrorDialog;

import React from 'react';
import { useDispatch } from 'react-redux';

// Material UI
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import Grid from '@material-ui/core/Grid';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Tooltip from '@material-ui/core/Tooltip';
import Chip from '@material-ui/core/Chip';
import DialogActions from '@material-ui/core/DialogActions';
import Paper from '@material-ui/core/Paper';
// Material UI Icons
import DeleteIcon from '@material-ui/icons/DeleteSweep';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import CompilationIcon from '@material-ui/icons/Description';

import { IDENewcomerGuideSteps } from '../types.d';

// Local Components
import SettingsMenuItem from './SettingsMenuItem';
import ShareMenuItem from '../../common/components/ShareMenuItem';
import ToolsMenuItem from './ToolsMenuItem';
import PopperWithArrow from '../../common/components/PopperWithArrow';
import TemplatesMenuItem from './TemplatesMenuItem';
// Redux Logic
import actions from '../actions';
import { useNewcomerGuideStep, useSelectedContract } from '../selectors';
// Local Hooks
import useCommonStyles from '../../../hooks/useCommonStyles';
import useTranslation from '../../i18n/hooks/useTranslation';
import TestIcon from '../../common/elements/icons/Test';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex',
            alignItems: 'center',
            height: 50,
            borderWidth: 1,
            backgroundColor: theme.palette.background.paper,
        },
        gridItem: {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
        },
        noMargin: {
            margin: 0,
        },
    }),
);

interface OwnProps {
    clearOutputs: () => void;
    tests: { kind: string; name: string }[];
    downloadOutputPanel: () => void;
    compileContract: (withTests: boolean) => void;
    runEvalTest: (testName: string) => void;
}
const EditorToolBar: React.FC<OwnProps> = (props) => {
    const classes = { ...useStyles(), ...useCommonStyles() };
    const [open, setOpen] = React.useState(false);
    const refs = {
        testsButton: React.useRef<HTMLButtonElement>(null),
        playButton: React.useRef<HTMLButtonElement>(null),
    };
    const onlyIcon = useMediaQuery((theme: Theme) => theme.breakpoints.down('lg'));
    const t = useTranslation();
    const newcomerGuideStep = useNewcomerGuideStep();
    const contract = useSelectedContract();
    const dispatch = useDispatch();

    const { clearOutputs, tests, downloadOutputPanel, compileContract, runEvalTest } = props;

    const compileTargets = React.useMemo(
        () => tests.filter((t) => t.kind === 'compilation').sort((t1, t2) => (t1.name < t2.name ? -1 : 1)),
        [tests],
    );
    const testTargets = React.useMemo(
        () => tests.filter((t) => t.kind === 'test').sort((t1, t2) => (t1.name < t2.name ? -1 : 1)),
        [tests],
    );

    const handleToggle = () => {
        setOpen((prevState) => !prevState);
        handleNewcomerInteraction();
    };

    const handleClose = (event: React.MouseEvent<EventTarget>) => {
        if (refs.testsButton.current && refs.testsButton.current.contains(event.target as HTMLElement)) {
            return;
        }
        setOpen(false);
    };

    const handleNewcomerInteraction = () => {
        dispatch(actions.updateNewcomerGuideStep(IDENewcomerGuideSteps.NONE));
    };

    return (
        <div className={classes.root}>
            <Grid container justifyContent="space-between" alignItems="center">
                <Grid item xs={12} sm={6} className={classes.gridItem}>
                    <ButtonGroup disableElevation>
                        <Tooltip title={t('ide.toolBar.runCode') as string} aria-label="run-code" placement="left">
                            <Button
                                ref={refs.playButton}
                                onClick={() => compileContract(true)}
                                endIcon={onlyIcon ? null : <PlayArrowIcon />}
                                variant="contained"
                            >
                                {onlyIcon ? <PlayArrowIcon /> : 'Run'}
                            </Button>
                        </Tooltip>
                        <Button
                            endIcon={<ArrowDropDownIcon />}
                            variant="outlined"
                            disabled={tests.length === 0}
                            ref={refs.testsButton}
                            aria-controls="tests-menu"
                            aria-haspopup="true"
                            onClick={handleToggle}
                        >
                            {t('ide.output.targets')}
                        </Button>
                        <Tooltip
                            title={t('ide.toolBar.clearOutputs') as string}
                            aria-label="clear-outputs"
                            placement="right"
                        >
                            <Button onClick={clearOutputs} variant="outlined">
                                <DeleteIcon />
                            </Button>
                        </Tooltip>
                    </ButtonGroup>
                </Grid>
                <Grid item xs={12} sm={6} className={classes.gridItem}>
                    <Grid container spacing={2} justifyContent="center" className={classes.noMargin}>
                        <Grid item>
                            <TemplatesMenuItem onlyIcon={onlyIcon} />
                        </Grid>
                        <Grid item>
                            <ToolsMenuItem onlyIcon={onlyIcon} downloadOutputPanel={downloadOutputPanel} />
                        </Grid>
                        <Grid item>
                            <ShareMenuItem
                                onlyIcon={onlyIcon}
                                code={contract?.code}
                                baseUrl={`${window.location.origin}${process.env.PUBLIC_URL}/ide`}
                            />
                        </Grid>
                        <Grid item>
                            <SettingsMenuItem onlyIcon={onlyIcon} />
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
            {/* Tests menu */}
            <Menu
                anchorEl={refs.testsButton.current}
                open={Boolean(open)}
                onClose={handleClose}
                getContentAnchorEl={null}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
            >
                {compileTargets.length > 0 ? (
                    <>
                        <MenuItem disabled>
                            <ListItemText primary={t('ide.output.compilations')} />
                        </MenuItem>
                        {compileTargets.map(({ name }) => (
                            <MenuItem key={name} onClick={() => runEvalTest(name)}>
                                <ListItemIcon>
                                    <CompilationIcon />
                                </ListItemIcon>
                                <ListItemText primary={name} />
                            </MenuItem>
                        ))}
                    </>
                ) : null}
                {testTargets.length > 0 ? (
                    <>
                        <MenuItem disabled>
                            <ListItemText primary={t('ide.output.tests')} />
                        </MenuItem>
                        {testTargets.map(({ name }) => (
                            <MenuItem key={name} onClick={() => runEvalTest(name)}>
                                <ListItemIcon>
                                    <TestIcon />
                                </ListItemIcon>
                                <ListItemText primary={name} />
                            </MenuItem>
                        ))}
                    </>
                ) : null}
            </Menu>
            {/* Play Button Guide Step (Used in newcomer mode) */}
            <PopperWithArrow
                open={newcomerGuideStep === IDENewcomerGuideSteps.RUN_CODE}
                withGlowAnimation
                anchorEl={refs.playButton.current}
            >
                <Paper>
                    <Chip
                        className={classes.spacingTwo}
                        variant="outlined"
                        label={t('ide.newcomer.runCodeStep.popperChip')}
                    />
                    <DialogActions>
                        <Button fullWidth variant="contained" onClick={handleNewcomerInteraction}>
                            {t('common.understood')}
                        </Button>
                    </DialogActions>
                </Paper>
            </PopperWithArrow>
        </div>
    );
};

export default EditorToolBar;

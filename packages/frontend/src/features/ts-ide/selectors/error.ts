import { RootState } from 'SmartPyTypes';
import { useSelector } from 'react-redux';

export const useError = () => useSelector<RootState, string>((state) => state.tsIDE.error);

import { Route, RouteProps } from 'react-router';

import Home from '../../home/views/Home';
import Editor from '../../editor/containers/Editor';
//import EditorJS from '../../ts-ide/containers/Editor';
import Wallet from '../../wallet/views/Wallet';
import Nodes from '../../nodes/views/Nodes';
import Help from '../../help/views/Help';
import Origination from '../../origination/containers/Origination';
import Michelson from '../../michelson-editor/containers/MichelsonEditor';
import RouteWithNavigation from '../containers/RouteWithNavigation';
import NotFound from '../../not-found/views/NotFound';

interface SmartPyRoute {
    title: string;
    Route: typeof Route | typeof RouteWithNavigation;
    routeProps: RouteProps;
    Component: React.FC<any>;
}

export default [
    // Root
    {
        title: 'Home - SmartPy',
        Route: Route,
        routeProps: {
            exact: true,
            path: '/',
        },
        Component: Home,
    },
    // IDE
    {
        title: 'Editor - SmartPy',
        Route: RouteWithNavigation,
        routeProps: { path: '/ide/:cid?' },
        Component: Editor,
    },
    // IDE
    // {
    //     title: 'Editor - SmartPy',
    //     Route: RouteWithNavigation,
    //     routeProps: { path: '/ts-ide/:cid?' },
    //     Component: EditorJS,
    // },
    // Origination
    {
        title: 'Origination - SmartPy',
        Route: RouteWithNavigation,
        routeProps: { path: '/origination' },
        Component: Origination,
    },
    // Michelson Helper
    {
        title: 'Michelson - SmartPy',
        Route: RouteWithNavigation,
        routeProps: { path: '/michelson' },
        Component: Michelson,
    },
    // Help
    {
        title: 'Help - SmartPy',
        Route: RouteWithNavigation,
        routeProps: { path: '/help' },
        Component: Help,
    },
    // Wallet
    {
        title: 'Wallet - SmartPy',
        Route: RouteWithNavigation,
        routeProps: { path: '/wallet' },
        Component: Wallet,
    },
    // Nodes Monitor
    {
        title: 'Nodes - SmartPy',
        Route: RouteWithNavigation,
        routeProps: { path: '/nodes' },
        Component: Nodes,
    },
    // Not Found Page
    {
        title: 'Nodes - Not Found',
        Route: RouteWithNavigation,
        routeProps: { path: '*' },
        Component: NotFound,
    },
] as SmartPyRoute[];

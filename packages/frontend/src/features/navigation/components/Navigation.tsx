import React from 'react';
import { Link } from 'react-router-dom';
import { withRouter, RouteComponentProps } from 'react-router-dom';
// Material UI
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Button from '@material-ui/core/Button';
import CodeIcon from '@material-ui/icons/Code';
import Search from '@material-ui/icons/Search';
import ChromeReaderMode from '@material-ui/icons/ChromeReaderMode';
import ContactSupport from '@material-ui/icons/ContactSupport';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

// Local Icons
import TezosWallet from '../../common/elements/icons/TezosWallet';
// Local Elements
import DarkLightSwitch from '../../theme/components/DarkLightSwitch';
import RouterButton from '../elements/RouterButton';
import Logo from '../../common/elements/Logo';

// Local Utils
import { version } from '../../../../package.json';
import { getBase } from '../../../utils/url';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        appBar: {
            position: 'relative',
            height: 80,
            backgroundColor: 'transparent',
            boxShadow: 'none',
        },
        logoSection: {
            flexGrow: 1,
            display: 'flex',
            padding: 20,
        },
        gridMenu: {
            width: 'auto',
        },
        paper: {
            display: 'flex',
            backgroundColor: theme.palette.background.default,
            borderBottomWidth: 2,
            borderBottomStyle: 'solid',
            borderBottomColor: theme.palette.primary.light,
        },
        button: {
            minWidth: 140,
            backgroundColor: theme.palette.background.paper,
            borderWidth: 2,
            '&:hover': {
                borderWidth: 2,
            },
        },
        activeButton: {
            borderColor: theme.palette.mode === 'dark' ? '#66aaff' : '#0160cc',
            color: theme.palette.mode === 'dark' ? '#66aaff' : '#0160cc',
        },
        logoParagraph: {
            margin: '-10px 0 0 40px',
            fontVariant: 'small-caps',
            fontFamily: 'Verdana, sans-serif',
            color: theme.palette.mode === 'dark' ? '#66aacc' : '#01608c',
        },
        logoLink: {
            textDecoration: 'none',
        },
        version: {
            position: 'absolute',
            left: 200,
            top: 28,
            fontSize: '8pt',
            color: theme.palette.mode === 'dark' ? '#FFF' : '#000',
            textDecoration: 'none',
        },
    }),
);

const menuButtons = [
    {
        component: RouterButton,
        props: {
            fullWidth: true,
            variant: 'outlined',
            size: 'large',
            startIcon: <CodeIcon />,
            to: '/ide',
        } as any,
        text: 'Editor',
    },
    {
        component: Button,
        props: {
            fullWidth: true,
            href: `${getBase()}/explorer.html`,
            variant: 'outlined',
            size: 'large',
            startIcon: <Search />,
        } as any,
        text: 'Explorer',
    },
    {
        component: Button,
        props: {
            fullWidth: true,
            href: `${getBase()}/wallet.html`,
            variant: 'outlined',
            size: 'large',
            startIcon: <TezosWallet />,
        } as any,
        text: 'Wallet',
    },
    {
        component: RouterButton,
        props: {
            fullWidth: true,
            variant: 'outlined',
            size: 'large',
            startIcon: <ChromeReaderMode />,
            to: '/michelson',
        } as any,
        text: 'Michelson',
    },
    {
        component: RouterButton,
        props: {
            fullWidth: true,
            variant: 'outlined',
            size: 'large',
            startIcon: <ContactSupport />,
            to: '/help',
        } as any,
        text: 'Help',
    },
];

const Navigation: React.FC<RouteComponentProps> = (props) => {
    const showWideMenu = useMediaQuery('(min-width:1300px)');
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const classes = useStyles();

    const {
        location: { pathname },
    } = props;

    const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const wideMenuItems = menuButtons.map((item, index) => {
        item.props.className = classes.button;
        if (pathname.includes(item.props.href) || pathname.includes(item.props.to)) {
            item.props.className = `${item.props.className} ${classes.activeButton}`;
        }
        return (
            <Grid item key={index}>
                <item.component {...item.props}>{item.text}</item.component>
            </Grid>
        );
    });

    const mobileMenuItems = menuButtons.map((item, index) => {
        item.props.className = classes.button;
        if (pathname.includes(item.props.href) || pathname.includes(item.props.to)) {
            item.props.className = `${item.props.className} ${classes.activeButton}`;
        }
        return (
            <MenuItem key={index}>
                <item.component {...item.props}>{item.text}</item.component>
            </MenuItem>
        );
    });

    return (
        <React.Fragment>
            <AppBar className={classes.appBar}>
                <Toolbar className={classes.paper}>
                    <div className={classes.logoSection}>
                        <Link to="/" className={classes.logoLink}>
                            <Logo />
                            <p className={classes.logoParagraph}>by Smart Chain Arena</p>
                        </Link>

                        <Typography
                            variant="caption"
                            className={classes.version}
                            component="a"
                            href={`${getBase()}/releases.html`}
                        >
                            {version}
                        </Typography>
                    </div>
                    <DarkLightSwitch />
                    {showWideMenu ? (
                        <Grid spacing={1} container className={classes.gridMenu}>
                            {wideMenuItems}
                        </Grid>
                    ) : (
                        <React.Fragment>
                            <IconButton
                                aria-label="menu"
                                aria-controls="menu"
                                aria-haspopup="true"
                                onClick={handleClick}
                            >
                                <MenuIcon fontSize="large" />
                            </IconButton>
                            <Menu
                                id="simple-menu"
                                anchorEl={anchorEl}
                                keepMounted
                                open={Boolean(anchorEl)}
                                onClose={handleClose}
                            >
                                {mobileMenuItems}
                            </Menu>
                        </React.Fragment>
                    )}
                </Toolbar>
            </AppBar>
        </React.Fragment>
    );
};

export default withRouter(Navigation);

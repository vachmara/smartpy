import React from 'react';
import Button, { ButtonProps } from '@material-ui/core/Button';
import { Link as RouterLink } from 'react-router-dom';

import { LocationDescriptor } from 'history';

interface OwnProps extends ButtonProps {
    to: LocationDescriptor;
}

const Component: React.FC<OwnProps> = ({ to, ...props }) => (
    <Button
        {...props}
        component={React.forwardRef((linkProps, ref) => (
            <RouterLink {...linkProps} to={to} />
        ))}
    />
);

export default Component;

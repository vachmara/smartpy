import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            width: 150,
        },
        text: {
            fill: theme.palette.text.primary,
        },
    }),
);

interface OwnProps {
    className?: string;
}

const Logo = ({ className }: OwnProps) => {
    const classes = useStyles();
    return (
        <svg viewBox="0 0 1270 317" className={className || classes.root}>
            <path d="M 50,124 C 16.666667,41.333333 33.333333,82.666667 50,124 Z" fill="#66aacc" stroke="none" />
            <path
                d="M 0,153 V 72 L 125.5,0 252,73 v 54.5 l -72,42.5 -73,-42 19,-11 53,31 53,-31 V 84.5 L 126,22 20,84 v 58 z"
                fill="#006dcc"
                strokeWidth={0.767855}
            />
            <path d="m 51,102 74.20465,-43.07113 71.5907,41.14226 L 178.5,111 126,80 51,123 Z" fill="#66aacc" />
            <path
                d="m 252,164 v 81 L 126.5,317 0,244 v -54.5 l 72,-42.5 73,42 -19,11 -53,-31 -53,31 v 32.5 L 126,295 232,233 v -58 z"
                fill="#006dcc"
                strokeWidth={0.767855}
            />
            <path
                d="M 199.79534,215 125.59069,258.07114 54,216.92887 72.29535,206 l 52.49999,31 75,-43 z"
                fill="#66aacc"
            />
            <text
                className={classes.text}
                fontSize="160"
                fontFamily="sans-serif"
                letterSpacing="30"
                x="319.1488"
                y="221.00032"
            >
                <tspan>SMART</tspan>
                <tspan fontWeight="bold">PY</tspan>
            </text>
        </svg>
    );
};

export default Logo;

import React from 'react';

// Material UI
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';
// Material Icons
import AddIcon from '@material-ui/icons/NoteAdd';

// Local Hooks
import useTranslation from '../../i18n/hooks/useTranslation';

interface OwnProps {
    onlyIcon: boolean;
    onClick: () => void;
    disabled: boolean;
}

const CreateContractButton: React.FC<OwnProps> = ({ onlyIcon, ...props }) => {
    const t = useTranslation();

    return (
        <Tooltip
            title={t('ide.contractManagement.createContract') as string}
            aria-label="create-contract"
            placement="bottom"
        >
            <span>
                <Button {...props} variant="outlined">
                    {onlyIcon ? <AddIcon /> : t('ide.contractManagement.createContract')}
                </Button>
            </span>
        </Tooltip>
    );
};

export default CreateContractButton;

import React from 'react';
// Material UI
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';
import SettingsIcon from '@material-ui/icons/Settings';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';

interface OwnProps {
    onClick: (event: React.MouseEvent<HTMLButtonElement>) => void;
    onlyIcon: boolean;
}

export default React.forwardRef<HTMLButtonElement, OwnProps>(({ onlyIcon, ...props }, ref) => (
    <Tooltip title="Settings">
        <Button
            startIcon={onlyIcon ? null : <SettingsIcon />}
            variant="contained"
            aria-label="settings"
            endIcon={onlyIcon ? null : <ArrowDropDownIcon />}
            {...{ ...props, ref }}
        >
            {onlyIcon ? <SettingsIcon /> : 'Settings'}
        </Button>
    </Tooltip>
));

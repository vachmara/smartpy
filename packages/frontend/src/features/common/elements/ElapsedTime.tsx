import React from 'react';

import { getElapsedTime } from '../../../utils/time';

interface Props {
    timestamp: string;
}

const ElapsedTime: React.FC<Props> = ({ timestamp }) => {
    const [elapsedTime, setElapsedTime] = React.useState(getElapsedTime(timestamp));

    React.useEffect(() => {
        setTimeout(() => {
            setElapsedTime(getElapsedTime(timestamp));
        }, 1000);
    });

    return <React.Fragment>{elapsedTime}</React.Fragment>;
};

export default ElapsedTime;

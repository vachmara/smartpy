import React from 'react';

// Material UI
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';
// Material Icons
import EditIcon from '@material-ui/icons/Edit';

// Local Hooks
import useTranslation from '../../i18n/hooks/useTranslation';

interface OwnProps {
    onlyIcon: boolean;
    onClick: () => void;
    disabled: boolean;
}

const EditContractButton: React.FC<OwnProps> = ({ onlyIcon, ...props }) => {
    const t = useTranslation();

    return (
        <Tooltip
            title={t('ide.contractManagement.editContract') as string}
            aria-label="edit-contract"
            placement="bottom"
        >
            <span>
                <Button {...props} variant="outlined">
                    {onlyIcon ? <EditIcon /> : t('ide.contractManagement.editContract')}
                </Button>
            </span>
        </Tooltip>
    );
};

export default EditContractButton;

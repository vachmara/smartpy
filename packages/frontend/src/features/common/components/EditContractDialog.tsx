import React from 'react';

// Material UI
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
// Material Icons
import SaveIcon from '@material-ui/icons/Save';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';

// Local Hooks
import useTranslation from '../../i18n/hooks/useTranslation';
import { IDEContract } from 'SmartPyModels';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        deleteButton: {
            backgroundColor: theme.palette.error.light,
            marginTop: 20,
        },
    }),
);

interface OwnProps {
    open: boolean;
    contract?: IDEContract;
    handleClose: () => void;
    handleDeleteContract: (contractId: string) => void;
    handleUpdateContract: (contractId: string, name: string) => void;
}

const EditContractDialog: React.FC<OwnProps> = ({
    open,
    contract,
    handleClose,
    handleDeleteContract,
    handleUpdateContract,
}) => {
    const classes = useStyles();
    const t = useTranslation();
    const textInputRef = React.useRef(null as unknown as HTMLInputElement);
    const [name, setName] = React.useState(contract?.name || '');

    const handleNameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setName(event.target.value);
    };

    const handleSave = () => {
        contract?.id && handleUpdateContract(contract.id, name);
    };

    const handleDelete = () => contract?.id && handleDeleteContract(contract.id);

    return (
        <Dialog fullWidth open={open} onClose={handleClose}>
            <DialogTitle>{t('ide.contractManagement.editContract')}</DialogTitle>
            <DialogContent dividers>
                <TextField
                    inputProps={{
                        maxLength: 32,
                        minLength: 1,
                    }}
                    ref={textInputRef}
                    fullWidth
                    label={t('common.contract.name')}
                    defaultValue={contract?.name || ''}
                    variant="filled"
                    size="small"
                    onChange={handleNameChange}
                    InputProps={{
                        endAdornment: (
                            <Tooltip
                                title={t('ide.contractManagement.updateContract') as string}
                                aria-label="update-contract"
                                placement="left"
                            >
                                <span>
                                    <IconButton
                                        disabled={!contract || name === contract.name}
                                        color="primary"
                                        onClick={handleSave}
                                    >
                                        <SaveIcon />
                                    </IconButton>
                                </span>
                            </Tooltip>
                        ),
                    }}
                />
                <Button
                    onClick={handleDelete}
                    startIcon={<DeleteForeverIcon />}
                    autoFocus
                    variant="contained"
                    className={classes.deleteButton}
                >
                    {t('ide.contractManagement.deleteContract')}
                </Button>
            </DialogContent>
            <DialogActions>
                <Button autoFocus color="primary" onClick={handleClose}>
                    {t('common.close')}
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default EditContractDialog;

import React from 'react';
import { Link as RouterLink } from 'react-router-dom';

// Material UI
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Divider from '@material-ui/core/Divider';

// Local Components
import SmartPyIcon from '../elements/icons/SmartPy';

// State Management
import useTranslation from '../../i18n/hooks/useTranslation';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        card: {
            borderWidth: 1,
            borderStyle: 'solid',
            borderColor: theme.palette.primary.main,
            boxShadow: `5px 5px 0 0 ${theme.palette.primary.main}`,
        },
        marginBottom: {
            marginBottom: 20,
        },
        innerDescription: {
            fontWeight: 'bold',
            margin: 10,
        },
        list: {
            width: '100%',
            backgroundColor: theme.palette.background.default,
            padding: 0,
        },
    }),
);

const SelectionOfTemplates: React.FC = ({ children }) => {
    const classes = useStyles();
    const t = useTranslation();

    return (
        <Paper className={`${classes.card} ${classes.marginBottom}`}>
            {/* TEMPLATES */}
            <Divider />
            <Typography gutterBottom variant="body1" className={classes.innerDescription}>
                {t('commonComponents.selectionOfTemplates.title')}
            </Typography>
            <Divider />
            <List className={classes.list}>
                <ListItem dense divider button to="ide?template=calculator.py" component={RouterLink}>
                    <ListItemAvatar>
                        <SmartPyIcon fontSize="large" />
                    </ListItemAvatar>
                    <ListItemText primary={t('commonComponents.selectionOfTemplates.calculatorDescription')} />
                </ListItem>
                <ListItem dense divider button to="ide?template=storeValue.py" component={RouterLink}>
                    <ListItemAvatar>
                        <SmartPyIcon fontSize="large" />
                    </ListItemAvatar>
                    <ListItemText primary={t('commonComponents.selectionOfTemplates.storeValueDescription')} />
                </ListItem>
                <ListItem dense button to="ide?template=chess.py" component={RouterLink}>
                    <ListItemAvatar>
                        <SmartPyIcon fontSize="large" />
                    </ListItemAvatar>
                    <ListItemText primary={t('commonComponents.selectionOfTemplates.chessDescription')} />
                </ListItem>
                <Divider />
                <ListItem dense divider button to="ide?template=FA2.py" component={RouterLink}>
                    <ListItemAvatar>
                        <SmartPyIcon fontSize="large" />
                    </ListItemAvatar>
                    <ListItemText primary={t('commonComponents.selectionOfTemplates.fa2Description')} />
                </ListItem>
                <ListItem dense button to="ide?template=oracle.py" component={RouterLink}>
                    <ListItemAvatar>
                        <SmartPyIcon fontSize="large" />
                    </ListItemAvatar>
                    <ListItemText primary={t('commonComponents.selectionOfTemplates.chainLinkDescription')} />
                </ListItem>
            </List>
            {children}
        </Paper>
    );
};

export default SelectionOfTemplates;

import React from 'react';

import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import Button from '@material-ui/core/Button';
import Slide from '@material-ui/core/Slide';

import useTranslation from '../../i18n/hooks/useTranslation';
import { TransitionProps } from '@material-ui/core/transitions/transition';

const Transition = React.forwardRef(function Transition(
    props: TransitionProps & { children?: React.ReactElement<any, any> },
    ref: React.Ref<unknown>,
) {
    return <Slide direction="down" ref={ref} {...props} />;
});

interface OwnProps {
    open: boolean;
    onClose: () => void;
}

const ErrorDialog: React.FC<OwnProps> = ({ children, open, onClose }) => {
    const t = useTranslation();

    return (
        <Dialog
            maxWidth="md"
            fullWidth
            open={open}
            onClose={onClose}
            aria-labelledby="error-dialog-title"
            TransitionComponent={Transition}
        >
            <DialogTitle id="error-dialog-title">{'Error'}</DialogTitle>
            <DialogContent dividers>{children}</DialogContent>
            <DialogActions>
                <Button onClick={onClose} color="primary">
                    {t('common.close')}
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default ErrorDialog;

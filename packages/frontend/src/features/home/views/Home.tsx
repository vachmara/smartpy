import React from 'react';

// Material UI
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Fab from '@material-ui/core/Fab';
import Divider from '@material-ui/core/Divider';
import CodeIcon from '@material-ui/icons/Code';
import PublicIcon from '@material-ui/icons/Public';
import BookIcon from '@material-ui/icons/MenuBook';

// Local Components
import Header from '../components/HomeHeader';
import Footer from '../../navigation/components/Footer';
import Community from '../components/Community';
import Overview from '../components/Overview';
import Introduction from '../components/Introduction';
import ArticlesGuides from '../../common/components/ArticlesGuides';
import MeshAnimation from '../../common/components/animated/Mesh';

// Local Element
import RouterFab from '../../navigation/elements/RouterFab';

// Local Constants
import info from '../../../constants/info';
import { getBase } from '../../../utils/url';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        top: {
            position: 'relative',
            background: theme.palette.background.paper,
            borderTopWidth: 2,
            borderTopStyle: 'solid',
            borderTopColor: theme.palette.primary.main,
            borderBottomWidth: 2,
            borderBottomStyle: 'solid',
            borderBottomColor: theme.palette.primary.main,
        },
        gridTop: {
            position: 'relative',
            padding: theme.spacing(2),
            zIndex: 1000,
        },
        mesh: {
            top: 0,
            left: 0,
            position: 'absolute',
            width: '100%',
            height: '100%',
        },
        bannerTitle: {
            color: theme.palette.primary.dark,
            fontWeight: 'bold',
            margin: 5,
        },
        bannerButtons: {
            marginTop: 20,
            [theme.breakpoints.down('sm')]: {
                display: 'flex',
                flexDirection: 'column',
            },
        },
        bannerLogo: {
            [theme.breakpoints.down('sm')]: {
                width: 180,
            },
            [theme.breakpoints.down('sm')]: {
                marginTop: 30,
            },
        },
        bannerButton: {
            margin: 5,
        },
        bannerButtonSecondary: {
            margin: 5,
            borderWidth: 2,
            borderStyle: 'solid',
            borderColor: theme.palette.primary.main,
        },
        learnMoreButton: {
            backgroundColor: theme.palette.primary.light,
        },
        centered: {
            display: 'flex',
            justifyContent: 'center',
        },
        padding: {
            padding: theme.spacing(2),
        },
        content: {
            backgroundColor: theme.palette.background.paper,
        },
        innerIcon: {
            marginRight: theme.spacing(1),
        },
    }),
);

const Home = () => {
    const classes = useStyles();
    return (
        <React.Fragment>
            <Header />
            <div className={classes.top}>
                <Container maxWidth="lg" className={classes.gridTop}>
                    <Grid container spacing={2} alignItems="center" justifyContent="center">
                        <Grid item xs={12} sm={6} className={classes.padding}>
                            <Typography variant="h5" align="justify" className={classes.bannerTitle}>
                                {info.description}
                            </Typography>
                            <div className={classes.bannerButtons}>
                                <RouterFab color="primary" variant="extended" className={classes.bannerButton} to="ide">
                                    <CodeIcon className={classes.innerIcon} />
                                    Online Editor
                                </RouterFab>
                                <Fab
                                    color="secondary"
                                    variant="extended"
                                    className={classes.bannerButtonSecondary}
                                    href={`${getBase()}/cli`}
                                >
                                    SmartPy CLI
                                </Fab>
                                <Fab
                                    color="secondary"
                                    variant="extended"
                                    className={classes.bannerButtonSecondary}
                                    href={`${getBase()}/releases.html`}
                                >
                                    Releases
                                </Fab>
                                <Fab
                                    color="secondary"
                                    variant="extended"
                                    href="https://smartpy-io.medium.com"
                                    className={classes.bannerButtonSecondary}
                                >
                                    Blog posts
                                </Fab>
                                <Fab
                                    color="secondary"
                                    variant="extended"
                                    href={`${getBase()}/explorer.html`}
                                    className={classes.bannerButtonSecondary}
                                >
                                    Explorer
                                </Fab>
                                <RouterFab
                                    color="secondary"
                                    variant="extended"
                                    className={classes.bannerButtonSecondary}
                                    to="nodes"
                                >
                                    <PublicIcon className={classes.innerIcon} />
                                    Public Nodes
                                </RouterFab>
                                <Fab
                                    color="secondary"
                                    variant="extended"
                                    href={`${getBase()}/reference.html`}
                                    className={classes.bannerButtonSecondary}
                                >
                                    <BookIcon className={classes.innerIcon} />
                                    Documentation
                                </Fab>
                            </div>
                        </Grid>
                        <Grid item xs={12} sm={4}>
                            <div className={classes.centered}>
                                <img
                                    src={`${getBase()}/static/img/logo-only.svg`}
                                    alt="SmartPy.io"
                                    className={classes.bannerLogo}
                                />
                            </div>
                        </Grid>
                    </Grid>
                </Container>
                <MeshAnimation className={classes.mesh} />
            </div>
            <div className={classes.content}>
                <Introduction />
                <Divider />
                <Overview />
                <Divider />
                <ArticlesGuides />
                <Divider />
                <Community />
            </div>
            <Footer />
        </React.Fragment>
    );
};

export default Home;

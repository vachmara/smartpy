import React from 'react';
import httpRequester from '../../../services/httpRequester';

import Skeleton from '@material-ui/core/Skeleton';

import nodes from '../constants/nodes';
import NodesMonitorComponent, { NodeStatus } from '../components/NodesMonitor';
import { Network } from '../../../constants/networks';

const NodesMonitor = () => {
    const [ready, setReady] = React.useState(false);
    const intervalId = React.useRef(null as unknown as ReturnType<typeof setInterval>);
    const [nodesStatus, setNodesStatus] = React.useState<NodeStatus[]>([]);

    const updateNodeStatus = React.useCallback(async () => {
        setNodesStatus(
            await Promise.all(
                Object.keys(Network)
                    .map((network) => network.toLowerCase())
                    .map(async (node: string) => {
                        let status: NodeStatus = await httpRequester
                            .get(`https://${node}.smartpy.io/chains/main/blocks/head/header`)
                            .then(({ data: { timestamp, level } }) => ({
                                network: node.toUpperCase(),
                                synchronized: Date.now() - new Date(timestamp).getTime() < nodes.allowedDelay,
                                timestamp,
                                blockLevel: level,
                            }))
                            .catch(() => ({
                                network: node.toUpperCase(),
                                synchronized: false,
                            }));

                        status = await httpRequester
                            .get(`https://${node}.smartpy.io/version`)
                            .then(({ data }) => ({
                                ...status,
                                ...data,
                            }))
                            .catch(() => ({
                                ...status,
                            }));

                        status = await httpRequester
                            .get(`https://${node}.smartpy.io/chains/main/checkpoint`)
                            .then(({ data: { history_mode } }) => ({
                                ...status,
                                historyMode:
                                    typeof history_mode === 'string' ? history_mode : Object.keys(history_mode)[0],
                            }))
                            .catch(() => ({
                                ...status,
                            }));

                        return status;
                    }),
            ),
        );
        setReady(true);
    }, []);

    React.useEffect(() => {
        updateNodeStatus();
        intervalId.current = setInterval(updateNodeStatus, nodes.healthCheckInterval);
        return () => {
            clearInterval(intervalId.current);
        };
    }, [updateNodeStatus]);

    return ready ? (
        <NodesMonitorComponent nodes={nodesStatus} />
    ) : (
        <div style={{ width: '100%' }}>
            <Skeleton animation="wave" />
            <Skeleton animation="wave" />
            <Skeleton animation="wave" />
            <Skeleton animation="wave" />
        </div>
    );
};

export default NodesMonitor;

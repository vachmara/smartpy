import TezMonitor from 'tezmonitor';

import {
    OperationKindType,
    Origination,
    TezosConstants,
    TezosNodeReader,
    TezosNodeWriter,
} from '../../../services/conseil';

import WalletError, { WalletGenericErrorCodes } from '../exceptions/WalletError';
import { OperationCostsEstimation, OriginationInformation, OriginationResults, ScriptParams } from 'SmartPyWalletTypes';
import { Signer } from './Signer';
import { OriginationParameters } from 'OriginationTypes';
import { getBlake2BHash } from '../../../utils/hash';
import { arrayOrEmptyArray } from '../../../utils/object';
import { getContractFromOriginationResult } from '../../origination/util/operation';
import { AccountSource } from '../constants/sources';
import { AccountInformation } from 'SmartPyModels';
import { AmountUnit, convertUnitNumber } from '../../../utils/units';

abstract class AbstractWallet {
    protected source;
    protected rpc: string | null = null;
    protected pkh?: string;
    protected signer?: Signer;

    constructor(source: AccountSource) {
        this.source = source;
    }

    /**
     * @description Set RPC address.
     *
     * @returns {void}
     */
    public async setRPC(rpc: string) {
        this.rpc = rpc;
    }

    /**
     * @description Estimate origination costs
     *
     * @param parameters Script parameters
     *
     * @returns {Promise<OperationCostsEstimation>}
     */
    public async estimateOrigination(parameters: ScriptParams): Promise<OperationCostsEstimation> {
        if (!this.pkh) {
            throw new WalletError(WalletGenericErrorCodes.NOT_ABLE_TO_GET_SOURCE);
        }
        if (!this.rpc) {
            throw new WalletError(WalletGenericErrorCodes.NOT_ABLE_TO_GET_RPC);
        }

        const counter = await TezosNodeReader.getCounterForAccount(this.rpc, this.pkh);
        const estimate = await TezosNodeWriter.estimateOperation(this.rpc, 'main', {
            kind: OperationKindType.Origination,
            script: {
                code: parameters.code,
                storage: parameters.storage,
            },
            balance: '0',
            fee: String(TezosConstants.DefaultAccountOriginationFee),
            gas_limit: String(TezosConstants.OperationGasCap),
            storage_limit: String(TezosConstants.OperationStorageCap),
            counter: String(counter + 1),
            source: this.pkh,
        });
        return {
            fee: estimate?.estimatedFee || TezosConstants.DefaultAccountOriginationFee,
            gasLimit: estimate?.gas || TezosConstants.DefaultAccountOriginationGasLimit,
            storageLimit: estimate?.storageCost || TezosConstants.DefaultAccountOriginationStorageLimit,
        };
    }

    /**
     * @description Retrieve the Public Key Hash of the account that is currently in use by the wallet.
     *
     * @param options Options to use while fetching the PKH.
     *
     * @returns A promise that resolves to the Public Key Hash string.
     */
    protected async getPkh() {
        return this.pkh;
    }

    /**
     * @description Get the account balance.
     *
     * @returns A promise that resolves to a BigNumber.
     */
    public getBalance = async () => {
        if (this.rpc && this.pkh) {
            return TezosNodeReader.getSpendableBalanceForAccount(this.rpc, this.pkh);
        }
        throw new WalletError(WalletGenericErrorCodes.NOT_ABLE_TO_GET_BALANCE);
    };

    /**
     * @description Verifies if the selected account needs to be revealed.
     *
     * @returns {boolean} true if the account was not yet revealed, false otherwise
     */
    public async isRevealRequired() {
        return !!(this.pkh && this.rpc) && !(await TezosNodeReader.isManagerKeyRevealedForAccount(this.rpc, this.pkh));
    }

    /**
     * @description Prepare the origination operation.
     *
     * @param originationParams Origination Parameters.
     *
     * @returns A promise that results to the origination information, it includes a send callback to commit the operation.
     */
    public prepareOrigination = async (originationParams: OriginationParameters): Promise<OriginationInformation> => {
        const source = await this.getPkh();
        if (this.rpc && this.signer && source) {
            const counter = await TezosNodeReader.getCounterForAccount(this.rpc, source);
            const blockHeader = await TezosNodeReader.getBlockHead(this.rpc);

            if (counter && blockHeader && originationParams.code && originationParams.storage) {
                const operationContents: Origination = {
                    kind: OperationKindType.Origination,
                    counter: String(counter + 1),
                    source,
                    fee: String(convertUnitNumber(originationParams.fee, AmountUnit.tez).toNumber()),
                    balance: String(convertUnitNumber(originationParams.balance, AmountUnit.tez).toNumber()),
                    storage_limit: String(originationParams.storage_limit),
                    gas_limit: String(originationParams.gas_limit),
                    script: {
                        code: JSON.parse(originationParams.code),
                        storage: JSON.parse(originationParams.storage),
                    },
                    delegate: originationParams.delegate || undefined,
                };

                const bytes = await TezosNodeWriter.forgeOperations(blockHeader.hash, [operationContents]);

                return {
                    bytes,
                    blockHash: blockHeader.hash,
                    operationContents,
                    blake2bHash: getBlake2BHash(bytes),
                    // The lambda function is required to keep the batch context.
                    send: ((rpc: string, signer: Signer) => async (): Promise<OriginationResults> => {
                        const { sbytes, prefixSig } = await signer.sign(bytes);

                        const signedOpBytes = Buffer.from(sbytes, 'hex');
                        const signedOpGroup = { bytes: signedOpBytes, signature: prefixSig };

                        const preApplyResults = await TezosNodeWriter.preapplyOperation(
                            rpc,
                            blockHeader.hash,
                            blockHeader.protocol,
                            [operationContents],
                            signedOpGroup,
                        );

                        if (preApplyResults && preApplyResults.length > 0) {
                            const results = preApplyResults[0];
                            const opHash = JSON.parse(await TezosNodeWriter.injectOperation(rpc, signedOpGroup));

                            return {
                                hash: opHash,
                                results: arrayOrEmptyArray(results.contents) as any,
                                contractAddress: getContractFromOriginationResult(results.contents),
                                monitor: TezMonitor.operationConfirmations(rpc, opHash, 10),
                            };
                        }
                        throw new WalletError(WalletGenericErrorCodes.NOT_ABLE_TO_ORIGINATE_CONTRACT);
                    })(this.rpc, this.signer),
                };
            }
        }
        throw new WalletError(WalletGenericErrorCodes.NOT_ABLE_TO_PREPARE_ORIGINATION);
    };

    /**
     * @description reveal account
     *
     * @throws {WalletError(WalletGenericErrorCodes.NOT_ABLE_TO_REVEAL_ACCOUNT)} Throws this exception if reveal operation failed.
     */
    public async reveal() {
        const source = await this.getPkh();
        if (this.rpc && this.signer && source) {
            const counter = await TezosNodeReader.getCounterForAccount(this.rpc, source);
            const blockHeader = await TezosNodeReader.getBlockHead(this.rpc);

            if (blockHeader) {
                const operationContents = {
                    kind: OperationKindType.Reveal,
                    counter: String(counter + 1),
                    source,
                    fee: String(TezosConstants.DefaultKeyRevealFee),
                    storage_limit: String(TezosConstants.DefaultKeyRevealStorageLimit),
                    gas_limit: String(TezosConstants.DefaultKeyRevealGasLimit),
                    public_key: await this.signer.publicKey(),
                };

                const bytes = await TezosNodeWriter.forgeOperations(blockHeader.hash, [operationContents]);
                const { sbytes, prefixSig } = await this.signer.sign(bytes);

                const signedOpBytes = Buffer.from(sbytes, 'hex');
                const signedOpGroup = { bytes: signedOpBytes, signature: prefixSig };

                const preApplyResults = await TezosNodeWriter.preapplyOperation(
                    this.rpc,
                    blockHeader.hash,
                    blockHeader.protocol,
                    [operationContents],
                    signedOpGroup,
                );

                if (preApplyResults && preApplyResults.length > 0) {
                    const results = preApplyResults[0];
                    const opHash = await TezosNodeWriter.injectOperation(this.rpc, signedOpGroup);

                    await await TezMonitor.operationConfirmations(this.rpc, JSON.parse(opHash), 1).toPromise();

                    return {
                        hash: opHash,
                        results: arrayOrEmptyArray(results.contents),
                    };
                }
            }
        }
        throw new WalletError(WalletGenericErrorCodes.NOT_ABLE_TO_REVEAL_ACCOUNT);
    }

    /**
     * @description Get Account Information.
     *
     * @returns A promise that resolves to the account information.
     */
    public getInformation = async (): Promise<AccountInformation> => {
        const balance = await this.getBalance();
        const revealRequired = await this.isRevealRequired();

        return {
            isLoading: false,
            source: this.source,
            pkh: this.pkh,
            balance: balance,
            revealRequired,
        };
    };

    /**
     * @description Return the public key of the account used by the signer
     */
    public publicKey = () => this.signer?.publicKey();

    /**
     * @description Optionally return the secret key of the account used by the signer
     */
    public secretKey = () => this.signer?.secretKey();
}

export default AbstractWallet;

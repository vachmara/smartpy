enum FaucetErrorCodes {
    NOT_ABLE_TO_GET_ACCOUNT = 'wallet.faucet.not_able_to_get_account',
    NOT_ABLE_TO_REVEAL_ACCOUNT = 'wallet.faucet.not_able_to_reveal_account',
    NOT_ABLE_TO_ACTIVATE_ACCOUNT = 'wallet.faucet.not_able_to_activate_account',
    NOT_ABLE_TO_IMPORT_ACCOUNT = 'wallet.faucet.not_able_to_import_account',
    NOT_ABLE_TO_ORIGINATE_CONTRACT = 'wallet.faucet.not_able_to_originate_contract',
    NOT_ABLE_TO_GET_BALANCE = 'wallet.faucet.not_able_to_get_balance',
    NOT_ABLE_TO_PREPARE_ORIGINATION = 'wallet.faucet.not_able_to_prepare_origination',
}

export default FaucetErrorCodes;

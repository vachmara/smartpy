import { FC, ReactElement } from 'react';

/**
 * Render react element conditionally
 * @param {boolean} isTrue - Condition result
 * @param {ReactElement} children - React children nodes
 * @return {ReactElement} Return children if the condition is true or null otherwise
 */
export const RenderIfTrue: FC<{ isTrue: boolean }> = ({ isTrue, children }) => {
    return (isTrue ? children : null) as ReactElement;
};

import * as editorSelectors from '../features/editor/selectors';
import * as tsEditorSelectors from '../features/ts-ide/selectors';
import * as michelsonEditorSelectors from '../features/michelson-editor/selectors';
import * as themeSelectors from '../features/theme/selectors';

const selectors = {
    editor: editorSelectors,
    tsIDE: tsEditorSelectors,
    michelsonEditor: michelsonEditorSelectors,
    theme: themeSelectors,
};

export default selectors;

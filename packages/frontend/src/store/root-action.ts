import { routerActions } from 'connected-react-router';

import editorActions from '../features/editor/actions';
import tsEditorActions from '../features/ts-ide/actions';
import michelsonEditorActions from '../features/michelson-editor/actions';
import walletActions from '../features/wallet/actions';
import themeActions from '../features/theme/actions';
import i18nActions from '../features/i18n/actions';
import originationActions from '../features/origination/actions';

const actions = {
    router: routerActions,
    editor: editorActions,
    tsEditor: tsEditorActions,
    michelsonEditor: michelsonEditorActions,
    theme: themeActions,
    wallet: walletActions,
    origination: originationActions,
    i18n: i18nActions,
};

export default actions;

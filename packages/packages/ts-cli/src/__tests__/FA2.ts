// Declare the storage type definition
interface FA2Storage {
    config: {
        admin: TAddress;
        paused: TBool;
    };
    assets: {
        ledger: TBig_map<TTuple<[TAddress, TNat]>, TNat>;
        operators: TBig_map<{ operator: TAddress; owner: TAddress; token_id: TNat }, TUnit>;
        token_metadata: TBig_map<TNat, { token_id: TNat; token_info: TMap<TString, TBytes> }>;
        token_total_supply: TBig_map<TNat, TNat>;
    };
    metadata: TBig_map<TString, TBytes>;
}

// Declare the initial storage
const storage: FA2Storage = {
    config: {
        admin: 'tz1',
        paused: false,
    },
    assets: {
        ledger: [],
        operators: [],
        token_metadata: [],
        token_total_supply: [],
    },
    metadata: [],
};

// Entrypoint parameters
type TransferParams = TList<
    TRecord<
        {
            from_: TAddress;
            txs: TList<
                TRecord<
                    {
                        to_: TAddress;
                        token_id: TNat;
                        amount: TNat;
                    },
                    ['to_', ['token_id', 'amount']]
                >
            >;
        },
        ['from_', 'txs']
    >
>;

@Contract
class FA2 {
    storage = storage;

    /**
     * @description transfer token ownership
     * @param
     */
    @EntryPoint
    transfer(transfer: TransferParams) {}

    /**
     * @description mint tokens
     * @param
     */
    @EntryPoint
    mint(tokenId: TNat, address: TAddress, amount: TNat) {
        Sp.verify(Sp.sender == this.storage.config.admin, 'FA2_NOT_ADMIN');
        // We don't check for pauseness because we're the admin.
        const user: TTuple<[TAddress, TNat]> = [address, tokenId];
    }

    /**
     * @description Pause the contract
     * @param {TBool} paused
     */
    @EntryPoint
    pause(paused: TBool) {
        this.storage.config.paused = paused;
    }

    /**
     * @description Update the administrator address
     * @param {TAddress} address - New admin address
     */
    @EntryPoint
    set_admin(address: TAddress) {
        this.storage.config.admin = address;
    }

    /**
     * @description Update contract metadata
     * @param {TAddress} address - New admin address
     */
    @EntryPoint
    update_metadata(metadata: TBig_map<TString, TBytes>) {
        this.storage.metadata = metadata;
    }
}

import Commander from '../commands';

// Can take a few seconds
jest.setTimeout(5000);

describe('Transpile SmartTS', () => {
    it('Get Scenario JSON', async () => {
        Commander.parse([
            '',
            '',
            'scenario',
            '--file',
            'src/__tests__/FA2.ts',
            '--outDir',
            'src/__tests__/',
        ]);

        // Await for the result (5 seconds)
        await new Promise(r => setInterval(r, 4000));
    });

});

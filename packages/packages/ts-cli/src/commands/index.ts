import Commander from 'commander';
import * as fs from 'fs';
import SmartTS from '@smartpy/ts-syntax';

/**
 * @description Apply file validation
 * @param filePath file (relative or absolute) path
 */
const fileValidation = (filePath:string): string => {
    if (!filePath.endsWith(".ts")) {
        process.stderr.write(`Expected file "${filePath}" to have (.ts) extension.`);
    }
    if (!fs.existsSync(filePath)) {
        process.stderr.write(`File "${filePath}" doesn't exist.`);
    }

    return filePath.split("/").reverse()[0];
}

Commander.command('scenario')
    .requiredOption('--file <path>')
    .requiredOption('--outDir <path>')
    .action(async (args) => {

        const filePath: string = args.file;
        let outDir: string = args.outDir;

        // Remove leading slash
        if (outDir.endsWith("/")) {
            outDir = outDir.slice(0, outDir.lastIndexOf("/"));
        }

        // Apply file validations
        const fileName = fileValidation(filePath);

        try {
            // Read file
            const code = fs.readFileSync(filePath, { encoding: 'utf8', flag: 'r' });

            // Transpile file
            const scenario = await SmartTS.transpile({
                name: fileName,
                code,
            })

            // Write the result into a json file.
            const outputPath = `${outDir}/scenario.json`;
            fs.writeFileSync(outputPath, scenario, { encoding: 'utf-8', flag: 'w' });

            // Write to the user
            process.stdout.write(`Generated (${fileName}) scenario at (${outputPath}).`);
        } catch (e) {
            process.stderr.write(e);
        }
    });

export default Commander;

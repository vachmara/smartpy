import path from 'path';
import typescript from '@rollup/plugin-typescript';
import json from '@rollup/plugin-json';
import commonjs from '@rollup/plugin-commonjs';
import resolve from '@rollup/plugin-node-resolve';
import { terser } from 'rollup-plugin-terser';

export default {
    input: 'src/index.ts',
    output: [
        {
            file: 'bin/smart-ts-cli.js',
            format: 'cjs',
            banner: '#!/usr/bin/env node',
        },
    ],
    plugins: [
        typescript({
            tsconfig: path.resolve(__dirname, './tsconfig.json'),
        }),
        json(),
        commonjs(),
        resolve(),
        terser(),
    ],
};

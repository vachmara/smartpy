import path from 'path';
import typescript from '@rollup/plugin-typescript';
import resolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import { terser } from 'rollup-plugin-terser';
import pkg from './package.json';
import nodePolyfills from 'rollup-plugin-polyfill-node';

const config = {
    input: 'src/index.ts',
    output: [
        {
            file: pkg.main,
            format: 'cjs',
            exports: 'default',
            banner: `/* === Version: ${pkg.version} === */`,
        },
        {
            file: pkg.module,
            format: 'es',
            banner: `/* === Version: ${pkg.version} === */`,
        },
        {
            name: 'SmartTS',
            file: pkg.minified,
            format: 'iife',
            exports: 'default',
            compact: true,
            banner: `/* === Version: ${pkg.version} === */`,
        },
    ],
    plugins: [
        typescript({
            tsconfig: path.resolve(__dirname, './tsconfig.build.json'),
        }),
        commonjs(),
        nodePolyfills(),
        resolve({ browser: true }),
        terser(),
    ],
};

export default config;

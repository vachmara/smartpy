import i from '../src/index';

describe('Tests', () => {
    it('test1', () => {
        console.log(
            i.compile({
                name: 'test.ts',
                code: `​
                type TStorage = {
                    storedValue: TInt;
                };

                type ReplaceParams = {
                    value: TInt;
                };

                type DivideParams = {
                    divisor: TInt;
                };

                @Contract({
                    flags: [
                        "initial-cast",
                        ["protocol", "edo"]
                    ]
                })
                class StoreValue {
                    storage = {
                        a: 1,
                        b: 2,
                        c: "Test",
                        d: [1],
                        storedValue: 1
                    }

                    constructor({ storage }) {
                        this.storage = storage;
                    }

                    @EntryPoint
                    ep_replace(params: ReplaceParams) {
                        this.storage.storedValue = params.value;
                    }

                    @EntryPoint
                    ep_double() {
                        this.storage.storedValue *= 2;
                    }

                    @EntryPoint
                    ep_divide(params: DivideParams) {
                        this.storage.storedValue /= params.divisor;
                    }
                }
        `,
            }),
        );
    });
});

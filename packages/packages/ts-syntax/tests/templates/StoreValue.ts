import '../../src/ide';

type TStorage = {
    storedValue: TInt;
};

type ReplaceParams = {
    value: TInt;
};

type DivideParams = {
    divisor: TInt;
};

@Contract({
    flags: ['initial-cast', ['protocol', 'edo']],
})
class StoreValue {
    storage: TStorage = {
        storedValue: 1,
    };

    @EntryPoint
    ep_replace(params: ReplaceParams) {
        this.storage.storedValue = params.value;
    }

    @EntryPoint
    ep_double() {
        this.storage.storedValue *= 2;
    }

    @EntryPoint
    ep_divide(params: DivideParams) {
        this.storage.storedValue /= params.divisor;
    }
}

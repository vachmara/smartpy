import Transpiler from '../../src/lib/transpiler/Transpiler';

test('Contract Decorator', () => {
    const sourceCode = {
        name: 'code.ts',
        code: `
        @Contract({
            flags: [
                "initial-cast",
                ["protocol", "edo"]
            ]
        })
        class StoreValue {}
        `,
    };
    const transpiler = new Transpiler(sourceCode);
    transpiler.transpile();

    expect(transpiler.output.contracts).toMatchSnapshot(['StoreValue']);
    expect(transpiler.output.classes['StoreValue'].decorators['Contract']).toMatchSnapshot({
        info: {
            flags: [
                {
                    args: [],
                    name: 'initial-cast',
                },
                {
                    args: ['edo'],
                    name: 'protocol',
                },
            ],
        },
        kind: 'Contract',
    });
});

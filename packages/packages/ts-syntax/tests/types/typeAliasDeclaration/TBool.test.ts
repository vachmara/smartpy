import Transpiler from '../../../src/lib/transpiler/Transpiler';

test('TBool', () => {
    const sourceCode = {
        name: 'code.ts',
        code: `​type bool_type = boolean;`,
    };
    const transpiler = new Transpiler(sourceCode);
    transpiler.transpile();

    expect(transpiler.output.typeDefs).toMatchSnapshot({ bool_type: { type: 'TBool' } });
});

import Transpiler from '../../../src/lib/transpiler/Transpiler';

test('TMutez', () => {
    const sourceCode = {
        name: 'code.ts',
        code: `​type mutez_type = TMutez;`,
    };
    const transpiler = new Transpiler(sourceCode);
    transpiler.transpile();

    expect(transpiler.output.typeDefs).toMatchSnapshot({ mutez_type: { type: 'TMutez' } });
});

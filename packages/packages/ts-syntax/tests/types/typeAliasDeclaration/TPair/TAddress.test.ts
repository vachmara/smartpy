import Transpiler from '../../../../src/lib/transpiler/Transpiler';

test('TPair<TNat, TNat>', () => {
    const sourceCode = {
        name: 'code.ts',
        code: `​type pair__nat_nat__type = TPair<TNat, TNat>;`,
    };
    const transpiler = new Transpiler(sourceCode);
    transpiler.transpile();

    expect(transpiler.output.typeDefs).toMatchSnapshot({
        pair__nat_nat__type: { type: 'TPair', keyType: { type: 'TNat' }, valueType: { type: 'TNat' } },
    });
});

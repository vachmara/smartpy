import Transpiler from '../../../src/lib/transpiler/Transpiler';

test('TAddress', () => {
    const sourceCode = {
        name: 'code.ts',
        code: `​type address_type = TAddress;`,
    };
    const transpiler = new Transpiler(sourceCode);
    transpiler.transpile();

    expect(transpiler.output.typeDefs).toMatchSnapshot({ address_type: { type: 'TAddress' } });
});

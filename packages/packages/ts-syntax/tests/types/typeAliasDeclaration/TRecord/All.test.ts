import Transpiler from '../../../../src/lib/transpiler/Transpiler';

describe('TRecord', () => {
    test('(Simple 1)', () => {
        const sourceCode = {
            name: 'code.ts',
            code: `​type record__type = TRecord;`,
        };
        const transpiler = new Transpiler(sourceCode);
        transpiler.transpile();

        expect(transpiler.output.typeDefs).toMatchSnapshot({
            record__type: { type: 'TRecord' },
        });
    });

    test('(Simple 2)', () => {
        const sourceCode = {
            name: 'code.ts',
            code: `type record__type = {};`,
        };
        const transpiler = new Transpiler(sourceCode);
        transpiler.transpile();

        expect(transpiler.output.typeDefs).toMatchSnapshot({
            record__type: { type: 'TRecord' },
        });
    });

    test('Untyped property (Must generate errors)', () => {
        const sourceCode = {
            name: 'code.ts',
            code: `
            ​type record__type = {
                a
            };
            `,
        };
        const transpiler = new Transpiler(sourceCode);
        transpiler.transpile();

        expect(transpiler.output.errors).toMatchSnapshot([
            {
                msg: 'Property (a) does not have a type.',
                line: { line: 2, character: 16 },
            },
        ]);
    });

    test('(TMap<TNat, TInt>)', () => {
        const sourceCode = {
            name: 'code.ts',
            code: `
            ​type record__type = {
                a: TMap<TNat, TInt>
            };
            `,
        };
        const transpiler = new Transpiler(sourceCode);
        transpiler.transpile();

        expect(transpiler.output.typeDefs).toMatchSnapshot({
            record__type: {
                type: 'TRecord',
                properties: { a: { type: 'TMap', keyType: { type: 'TNat' }, valueType: { type: 'TInt' } } },
            },
        });
    });
});

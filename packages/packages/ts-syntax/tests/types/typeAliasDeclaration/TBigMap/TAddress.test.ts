import Transpiler from '../../../../src/lib/transpiler/Transpiler';

test('TBigMap<TNat, TNat>', () => {
    const sourceCode = {
            name: "code.ts",
        code: `​type big_map__nat_nat__type = TBigMap<TNat, TNat>;`,
    };
    const transpiler = new Transpiler(sourceCode);
    transpiler.transpile();

    expect(transpiler.output.typeDefs).toMatchSnapshot({
        big_map__nat_nat__type: { type: 'TBigMap', keyType: { type: 'TNat' }, valueType: { type: 'TNat' } },
    });
});

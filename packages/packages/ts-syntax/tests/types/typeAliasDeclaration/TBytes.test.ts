import Transpiler from '../../../src/lib/transpiler/Transpiler';

test('TBytes', () => {
    const sourceCode = {
        name: 'code.ts',
        code: `​type bytes_type = TBytes;`,
    };
    const transpiler = new Transpiler(sourceCode);
    transpiler.transpile();

    expect(transpiler.output.typeDefs).toMatchSnapshot({ bytes_type: { type: 'TBytes' } });
});

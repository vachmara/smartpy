import Transpiler from '../../../../src/lib/transpiler/Transpiler';

test('TList<TNat>', () => {
    const sourceCode = {
        name: 'code.ts',
        code: `​type list__nat__type = TList<TNat>;`,
    };
    const transpiler = new Transpiler(sourceCode);
    transpiler.transpile();

    expect(transpiler.output.typeDefs).toMatchSnapshot({
        list__nat__type: { type: 'TList', innerType: { type: 'TNat' } },
    });
});

import Transpiler from '../../../src/lib/transpiler/Transpiler';

test('void => TUnit', () => {
    const sourceCode = {
        name: 'code.ts',
        code: `​type unit_type = void;`,
    };
    const transpiler = new Transpiler(sourceCode);
    transpiler.transpile();

    expect(transpiler.output.typeDefs).toEqual({ unit_type: { type: 'TUnit' } });
});

test('TUnit => TUnit', () => {
    const sourceCode = {
        name: 'code.ts',
        code: `​type unit_type = void;`,
    };
    const transpiler = new Transpiler(sourceCode);
    transpiler.transpile();

    expect(transpiler.output.typeDefs).toMatchSnapshot({ unit_type: { type: 'TUnit' } });
});

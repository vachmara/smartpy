import Transpiler from '../../../src/lib/transpiler/Transpiler';

test('never => TNever', () => {
    const sourceCode = {
            name: "code.ts",
        code: `​type never_type = never;`,
    };
    const transpiler = new Transpiler(sourceCode);
    transpiler.transpile();

    expect(transpiler.output.typeDefs).toMatchSnapshot({ never_type: { type: 'TNever' } });
});

test('TNever => TNever', () => {
    const sourceCode = {
            name: "code.ts",
        code: `​type never_type = TNever;`,
    };
    const transpiler = new Transpiler(sourceCode);
    transpiler.transpile();

    expect(transpiler.output.typeDefs).toMatchSnapshot({ never_type: { type: 'TNever' } });
});

import Transpiler from '../../../src/lib/transpiler/Transpiler';

test('TIntOrNat', () => {
    const sourceCode = {
            name: "code.ts",
        code: `​type number_type = number;`,
    };
    const transpiler = new Transpiler(sourceCode);
    transpiler.transpile();

    expect(transpiler.output.typeDefs).toMatchSnapshot({ number_type: { type: 'TIntOrNat' } });
});

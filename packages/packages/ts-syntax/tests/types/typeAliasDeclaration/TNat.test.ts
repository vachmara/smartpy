import Transpiler from '../../../src/lib/transpiler/Transpiler';

test('TNat', () => {
    const sourceCode = {
        name: 'code.ts',
        code: `​type nat_type = TNat;`,
    };
    const transpiler = new Transpiler(sourceCode);
    transpiler.transpile();

    expect(transpiler.output.typeDefs).toMatchSnapshot({ nat_type: { type: 'TNat' } });
});

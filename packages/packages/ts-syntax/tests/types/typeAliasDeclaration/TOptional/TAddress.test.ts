import Transpiler from '../../../../src/lib/transpiler/Transpiler';

test('TOptional<TNat>', () => {
    const sourceCode = {
        name: 'code.ts',
        code: `​type optional_nat_type = TOptional<TNat>;`,
    };
    const transpiler = new Transpiler(sourceCode);
    transpiler.transpile();

    expect(transpiler.output.typeDefs).toMatchSnapshot({
        optional_nat_type: { type: 'TOptional', innerType: { type: 'TNat' } },
    });
});

import Transpiler from '../../../src/lib/transpiler/Transpiler';

test('TInt', () => {
    const sourceCode = {
        name: 'code.ts',
        code: `​type int_type = TInt;`,
    };
    const transpiler = new Transpiler(sourceCode);
    transpiler.transpile();

    expect(transpiler.output.typeDefs).toMatchSnapshot({ int_type: { type: 'TInt' } });
});

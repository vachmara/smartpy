import Transpiler from '../../../../src/lib/transpiler/Transpiler';

test('TMap<TNat, TNat>', () => {
    const sourceCode = {
        name: 'code.ts',
        code: `​type map__nat_nat__type = TMap<TNat, TNat>;`,
    };
    const transpiler = new Transpiler(sourceCode);
    transpiler.transpile();

    expect(transpiler.output.typeDefs).toMatchSnapshot({
        map__nat_nat__type: { type: 'TMap', keyType: { type: 'TNat' }, valueType: { type: 'TNat' } },
    });
});

import Transpiler from '../../../src/lib/transpiler/Transpiler';

test('TSignature', () => {
    const sourceCode = {
        name: 'code.ts',
        code: `​type signature_type = TSignature;`,
    };
    const transpiler = new Transpiler(sourceCode);
    transpiler.transpile();

    expect(transpiler.output.typeDefs).toMatchSnapshot({ signature_type: { type: 'TSignature' } });
});

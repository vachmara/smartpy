import Transpiler from '../../../../src/lib/transpiler/Transpiler';

test('TSet<TNat>', () => {
    const sourceCode = {
        name: 'code.ts',
        code: `​type set__nat__type = TSet<TNat>;`,
    };
    const transpiler = new Transpiler(sourceCode);
    transpiler.transpile();

    expect(transpiler.output.typeDefs).toMatchSnapshot({
        set__nat__type: { type: 'TSet', innerType: { type: 'TNat' } },
    });
});

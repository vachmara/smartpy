import Transpiler from '../../../src/lib/transpiler/Transpiler';

test('string => TString', () => {
    const sourceCode = {
            name: "code.ts",
        code: `​type string_type = string;`,
    };
    const transpiler = new Transpiler(sourceCode);
    transpiler.transpile();

    expect(transpiler.output.typeDefs).toMatchSnapshot({ string_type: { type: 'TString' } });
});

test('TString => TString', () => {
    const sourceCode = {
            name: "code.ts",
        code: `​type string_type = TString;`,
    };
    const transpiler = new Transpiler(sourceCode);
    transpiler.transpile();

    expect(transpiler.output.typeDefs).toMatchSnapshot({ string_type: { type: 'TString' } });
});

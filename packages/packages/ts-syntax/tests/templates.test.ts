import fs from 'fs';
import path from 'path';
import Transpiler from '../src/lib/transpiler/Transpiler';

const basePath = './tests/templates';

const getCode = (file: string) => fs.readFileSync(path.resolve(basePath, file), { encoding: 'utf8', flag: 'r' });

describe('Contract templates', () => {
    it('StoreValue', () => {
        const code = getCode('StoreValue.ts');
        const transpiler = new Transpiler({
            name: 'code.ts',
            code,
        });
        transpiler.transpile();

        expect(transpiler.output.contracts).toMatchSnapshot(['StoreValue']);
        expect(transpiler.output.errors).toMatchSnapshot([]);
        expect(transpiler.output.warnings).toMatchSnapshot([]);
        expect(transpiler.output.typeDefs).toMatchSnapshot({
            DivideParams: {
                properties: {
                    divisor: {
                        type: 'TInt',
                    },
                },
                type: 'TRecord',
            },
            ReplaceParams: {
                properties: {
                    value: {
                        type: 'TInt',
                    },
                },
                type: 'TRecord',
            },
            TStorage: {
                properties: {
                    storedValue: {
                        type: 'TInt',
                    },
                },
                type: 'TRecord',
            },
        });
        expect(transpiler.output.getResult()).toMatchSnapshot();
    });
});

type TInt = number;
type TNat = number;
type TMutez = number;
type TBytes = string;
type TString = string;
type TBool = boolean;
type TAddress = string;
type TNever = never;
type TSignature = string;
type TUnit = void;
type TTimestamp = string | number;

type TOptional<T> = {
    some: (msg?: string) => T;
};
type TList<T> = T[];
type TSet<T> = T[];
type TPair<F, S> = [F, S];
type TMap<K, V> = [K, V][];
type TBigMap<K, V> = [K, V][];
type TRecord = Record<string, any>;
type TVariant = Record<string, any>;
const Contract = (_: { flags: ((string | number | boolean)[] | string)[] }) => {
    // eslint-disable-next-line @typescript-eslint/ban-types
    return function (constructor: Function) {
        //
    };
};

const EntryPoint = (target: any, propertyKey: string, descriptor: PropertyDescriptor) => {
    const originalMethod = descriptor.value;
    descriptor.value = function (...args: any): { name: string; result: any; run: (valid: boolean) => void } {
        const result = originalMethod.apply(this, args);
        // Some logic
        return result;
    };
    return descriptor;
};

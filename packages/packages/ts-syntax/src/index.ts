import { FileInfo } from 'SmartTS';
//import { Transpiler } from './lib/transpiler/Transpiler';
import ide from './ideRaw';

const SmartTS = {
    ide: ide,
    //Transpiler,
    compile: (sourceCode: FileInfo) => {
    //   return new Transpiler(sourceCode).transpile();
    },
};

export default SmartTS;

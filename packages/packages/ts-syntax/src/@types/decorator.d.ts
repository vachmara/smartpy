import type { STDecoratorKind } from '../enums/decorator';

declare module 'SmartTS' {
    export type ST_ContractDecoratorInfo = {
        name?: string;
        flags: ST_Flag[];
    };
    export type ST_Decorator = {
        kind: STDecoratorKind.Contract;
        info: ST_ContractDecoratorInfo;
    };
}

declare module 'SmartTS' {
    export type ST_Flag = {
        name: string;
        args: (string | boolean | number)[];
    };

    export type ST_Atom = {
        name: string;
        args: (string | ST_Atom)[];
    };

    export type ST_NewContractExport = {
        id: number;
        name: string;
        declarationLine: number;
        properties: ST_Properties;
        storage_type: ST_Atom[];
        messages: ST_Atom[];
        flags: ST_Flag[];
        globals: ST_Atom[];
        views: ST_Atom[];
        entry_points_layout: ST_Atom[];
        initial_metadata: ST_Atom[];
        balance: ST_Atom[];
    };

    export type ST_Output = {
        scenario: unknown[];
        shortname: string;
        kind: string;
    };
}

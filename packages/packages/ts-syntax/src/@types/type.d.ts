import type { FrontendType } from '../enums/type';

declare module 'SmartTS' {
    export type ST_TypeDef =
        | {
              type: FrontendType.TRecord | FrontendType.TVariant;
              properties: ST_TypeDefs;
          }
        | {
              type: FrontendType.TList | FrontendType.TSet | FrontendType.TOptional;
              innerType: ST_TypeDef;
          }
        | {
              type: FrontendType.TPair | FrontendType.TMap | FrontendType.TBigMap;
              keyType: ST_TypeDef;
              valueType: ST_TypeDef;
          }
        // References to user defined types
        | {
              type: FrontendType.TypeReference;
              args: ST_TypeDef;
              refType: string;
          }
        // Types without generics
        | {
              type: FrontendType;
          };

    export type ST_TypeDefs = Record<string, ST_TypeDef>;
}

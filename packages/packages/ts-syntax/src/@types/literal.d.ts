import type { FrontendType } from '../enums/type';

declare module 'SmartTS' {
    export type ST_Literal =
        | {
              type: FrontendType.TUnit | FrontendType.TUnknown;
          }
        | {
              type: FrontendType.TInt | FrontendType.TIntOrNat | FrontendType.TNat | FrontendType.TString;
              value: string;
          }
        | {
              type: FrontendType.TBool;
              value: boolean;
          }
        | {
              type: FrontendType.TList;
              value: ST_Literal[];
          }
        | {
              type: FrontendType.TRecord;
              value: ST_Properties;
          };
}

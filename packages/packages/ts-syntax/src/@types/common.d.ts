declare module 'SmartTS' {
    export type Nullable<T> = T | null | undefined;
    export type FileInfo = {
        name: string;
        code: string;
    };
}

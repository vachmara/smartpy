declare module 'SmartTS' {
    export type ST_Property = {
        name: string;
        type: ST_TypeDef;
        value: ST_Literal;
    };
    export type ST_Properties = Record<string, ST_Property>;
}

import type { CompilerHost, CompilerOptions, SourceFile } from 'typescript';
import type { FileInfo } from 'SmartTS';

import { createSourceFile, getDefaultLibFileName, ScriptKind, ScriptTarget } from 'typescript';

export class STCompilerHost implements CompilerHost {
    private sourceFiles: {
        [name: string]: SourceFile;
    } = {};

    constructor(files: FileInfo[]) {
        // Instantiate source files
        this.sourceFiles = files.reduce(
            (l, c) => ({
                ...l,
                [c.name]: createSourceFile(c.name, c.code, ScriptTarget.ES2020, false, ScriptKind.TS),
            }),
            {},
        );
    }

    getSourceFile = (fileName: string) => this.sourceFiles[fileName];

    getDefaultLibFileName = (defaultLibOptions: CompilerOptions) => '/' + getDefaultLibFileName(defaultLibOptions);

    writeFile = () => {
        /* do nothing */
    };

    getCurrentDirectory = () => '/';

    getDirectories = (_: string) => [];

    fileExists = (fileName: string) => this.sourceFiles[fileName] !== null;

    readFile = (fileName: string) => this.sourceFiles[fileName]?.getFullText();

    getCanonicalFileName = (fileName: string) => fileName;

    useCaseSensitiveFileNames = () => true;

    getNewLine = () => '\n';

    getEnvironmentVariable = () => '';
}

export default CompilerHost;

import type { ST_Flag, ST_TypeDefs, ST_TypeDef, ST_Property, ST_Literal, ST_Properties, ST_Decorator } from 'SmartTS';

import type { LineAndCharacter, Node, SourceFile } from 'typescript';
import types from '../constants/types';
import { STDecoratorKind } from '../enums/decorator';
import { FrontendType } from '../enums/type';
import Logger from '../services/Logger';
import { nonFalsy } from './utils/filters/falsy';
import guards from './utils/guard';

interface ST_Class {
    name: string;
    id: number;
    declarationLine: number;
    decorators: {
        [name: string]: ST_Decorator;
    };
    types: ST_TypeDefs;
    properties: ST_Properties;
    methods: {
        [name: string]: {
            name: string;
        };
    };
}

export class OutputContext {
    contracts: string[] = [];
    typeDefs: ST_TypeDefs = {};
    private currentClassName = '';
    classes: Record<string, ST_Class> = {};

    errors: { msg: string; line?: LineAndCharacter }[] = [];
    warnings: { msg: string; line?: LineAndCharacter }[] = [];

    constructor(private sourceFile: SourceFile) {}

    /**
     * @description Record class declaration;
     * @param name Class name
     * @param typeDef Type specification
     */
    emitClassDeclaration = (name: string, declarationLine: number) => {
        this.currentClassName = name;
        this.classes[name] = {
            name,
            id: Object.keys(this.contracts).length + 1,
            declarationLine,
            decorators: {},
            types: {},
            properties: {},
            methods: {},
        };
    };

    /**
     * @description Record type definition.
     * @param name Type name
     * @param typeDef Type specification
     */
    emitTypeDef = (name: string, typeDef: ST_TypeDef): void => {
        this.typeDefs[name] = typeDef;
    };

    /**
     * @description Record class property declaration.
     * @param property Property value and type
     */
    emitClassProperty = (property: ST_Property): void => {
        this.classes[this.currentClassName].properties[property.name] = property;
    };

    /**
     * @description Record class decorator.
     * @param name De name
     * @param typeDef Property value and type
     */
    emitClassDecorator = (decorator: ST_Decorator) => {
        if (decorator.kind === STDecoratorKind.Contract) {
            // Means that current class is a contract
            this.contracts.push(this.currentClassName);
        }
        this.classes[this.currentClassName].decorators[decorator.kind] = decorator;
    };

    emitError = (node: Node, msg: string) => {
        console.log(msg);
        // this.errors = [
        //     ...this.errors,
        //     {
        //         msg,
        //         line: this.sourceFile.getLineAndCharacterOfPosition(node.getStart(this.sourceFile)),
        //     },
        // ];
    };

    emitWarning = (node: Node, msg: string) => {
        this.warnings = [
            ...this.warnings,
            {
                msg,
                line: this.sourceFile.getLineAndCharacterOfPosition(node.getStart(this.sourceFile)),
            },
        ];
    };

    getResult() {
        Logger.debug(JSON.stringify(this.classes, null, 2), this.contracts);
        return JSON.stringify(
            {
                scenario: this.contracts.map((name) => this.getNewContractAction(this.classes[name])),
                shortname: 'aaa',
                kind: 'test',
                errors: this.errors,
                warnings: this.warnings,
            },
            null,
            2,
        );
    }

    private getNewContractAction = (contract: ST_Class) => {
        const storage = contract.properties['storage'];
        const templateId = `(static_id ${contract.id} ${contract.declarationLine})`;
        const storageString = `${this.stringifyProperty(storage, true)}`;
        const storageType = `${this.stringifyType(storage.type)}`;
        const messages = ``;
        const exportInfo = `(
            template_id ${templateId}
            storage ${storageString}
            storage_type (${nonFalsy(storage) ? storageType : 'unit'})
            messages (${messages})
            flags (${this.stringifyFlags(contract)})
            globals ()
            views ()
            entry_points_layout ()
            initial_metadata ()
            balance ()
        )`;
        return {
            action: 'newContract',
            id: templateId,
            export: exportInfo,
            line_no: contract.declarationLine,
            show: true,
            accept_unknown_types: false,
        };
    };

    private stringifyType(typeDef: ST_TypeDef): string {
        if (guards.type.hasOneArg(typeDef)) {
            return `(${types[typeDef.type]} ${this.stringifyType(typeDef.innerType)})`;
        }

        return `(${types[typeDef.type]})`;
    }

    private stringifyProperty(property: ST_Property, ignorePropName = false): string {
        switch (property?.type?.type) {
            case FrontendType.TRecord:
            case FrontendType.TNat:
            case FrontendType.TInt:
            case FrontendType.TIntOrNat:
            case FrontendType.TBool:
            case FrontendType.TString:
            case FrontendType.TList:
            case FrontendType.TypeReference:
            case FrontendType.TUnknown:
                if (ignorePropName) {
                    return this.stringifyValue(property.value);
                } else {
                    return `(${property.name} ${this.stringifyValue(property.value)})`;
                }

            case FrontendType.TUnit:
            default:
                return '()';
        }
    }

    private stringifyValue(value: ST_Literal): string {
        switch (value.type) {
            case FrontendType.TNat:
                return `(literal (nat ${value.value}) ${0})`;
            case FrontendType.TInt:
                return `(literal (int ${value.value}) ${0})`;
            case FrontendType.TIntOrNat:
                return `(literal (intOrNat ${value.value}) ${0})`;
            case FrontendType.TBool:
                return `(literal (bool ${value.value ? 'True' : 'False'}) ${0})`;
            case FrontendType.TString:
                return `(literal (string ${value.value}) ${0})`;
            case FrontendType.TRecord:
                return `(${types[FrontendType.TRecord]} ${0} ${Object.entries(value.value)
                    .map(([_, v]) => this.stringifyProperty(v))
                    .join(' ')})`;
            case FrontendType.TList:
                return `(${types[FrontendType.TList]} ${0} ${value.value
                    .map((v) => this.stringifyValue(v))
                    .join(' ')})`;
            default:
                return '(unknown)';
        }
    }

    private stringifyFlags(contract: ST_Class): string {
        const flags = contract.decorators?.Contract?.info?.flags || [];
        const getFlag = ({ name, args }: ST_Flag): string => {
            return '(' + `${name} ${args.join(' ')}`.trim() + ')';
        };
        return flags.map<string>(getFlag).join(' ');
    }
}

export default OutputContext;

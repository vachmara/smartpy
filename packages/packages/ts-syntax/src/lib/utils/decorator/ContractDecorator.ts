import type { ST_Literal, ST_Flag, ST_Decorator } from 'SmartTS';
import { STDecoratorKind } from '../../../enums/decorator';

import { FrontendType } from '../../../enums/type';

/**
 * @description Get flags
 * @param {STDecorator[]} decorators
 * @returns {ST_Flag[]} flags
 */
const getFlags = (decorators: ST_Decorator[]): ST_Flag[] => {
    for (const decorator of decorators) {
        if (decorator.kind === STDecoratorKind.Contract) {
            return decorator.info.flags;
        }
    }
    return [];
};

/**
 * @description Extract flag from literal
 * @param {Literal} literal
 * @returns {ST_Flag[]} Flag
 */
const extractFlags = (literal: ST_Literal): ST_Flag[] => {
    if (literal.type !== FrontendType.TList) {
        // @TODO - Add Warning (literal must be of type ArrayLiteralExpression)
        return [];
    }

    const elements = <ST_Literal[]>literal.value;
    const flags = elements.reduce<ST_Flag[]>((state, el) => {
        switch (el.type) {
            case FrontendType.TString:
                state = [
                    ...state,
                    {
                        name: el.value.toString(),
                        args: [],
                    },
                ];
                break;
            case FrontendType.TList:
                const literals = <ST_Literal[]>el.value;
                if (literals.length === 0) {
                    // @TODO - Add Warning (array literal must have at least 1 element)
                    break;
                }
                const flagName = literals[0];
                if (flagName.type !== FrontendType.TString) {
                    // @TODO - Add Warning (flagName must be a string literal)
                    break;
                }

                const args = literals.slice(1).reduce<(boolean | number | string)[]>((state, lit) => {
                    switch (lit.type) {
                        case FrontendType.TIntOrNat:
                        case FrontendType.TBool:
                        case FrontendType.TString:
                            state = [...state, lit.value];
                    }
                    return state;
                }, []);

                state = [
                    ...state,
                    {
                        name: flagName.value.toString(),
                        args: args,
                    },
                ];
                break;
            default:
            // @TODO - Add Warning (Unexpected literal in flags)
        }
        return state;
    }, []);

    return flags;
};

export const ContractDecoratorUtils = {
    extractFlags,
};

export default ContractDecoratorUtils;

import { Nullable } from 'SmartTS';
import {
    CallExpression,
    ClassDeclaration,
    Decorator,
    Identifier,
    Node,
    PropertyAssignment,
    PropertyDeclaration,
    PropertySignature,
    SyntaxKind,
    TypeAliasDeclaration,
    TypeReferenceNode,
} from 'typescript';
import Logger from '../../services/Logger';

/**
 * @description Extract node name.
 * @param {NamedDeclaration} node
 * @returns {Nullable<string>} node name
 */
const extractName = (node: Node): Nullable<string> => {
    switch (node.kind) {
        case SyntaxKind.Identifier:
            return (<Identifier>node)?.escapedText?.toString();
        case SyntaxKind.TypeAliasDeclaration:
            return extractName((<TypeAliasDeclaration>node).name);
        case SyntaxKind.TypeReference:
            return extractName((<TypeReferenceNode>node).typeName);
        case SyntaxKind.Decorator:
            return extractName((<Decorator>node).expression);
        case SyntaxKind.CallExpression:
            return extractName((<CallExpression>node).expression);
        case SyntaxKind.PropertySignature:
            return extractName((<PropertySignature>node).name);
        case SyntaxKind.PropertyAssignment:
            return extractName((<PropertyAssignment>node).name);
        case SyntaxKind.PropertyDeclaration:
            return extractName((<PropertyDeclaration>node).name);
        case SyntaxKind.ClassDeclaration:
            const identifier = (<ClassDeclaration>node).name;
            if (identifier) {
                return extractName(identifier);
            }
    }
    Logger.error(node);
    return null;
};

export const IdentifierUtils = {
    extractName,
};

export default IdentifierUtils;

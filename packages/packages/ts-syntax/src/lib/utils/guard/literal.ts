import type { ST_Literal } from 'SmartTS';

import { FrontendType } from '../../../enums/type';

export const isRecord = (literal: ST_Literal): literal is Extract<ST_Literal, { type: FrontendType.TRecord }> =>
    literal.type === FrontendType.TRecord;

const literal = {
    isRecord,
};

export default literal;

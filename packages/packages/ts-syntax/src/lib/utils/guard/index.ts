import type from './type';
import literal from './literal';

const guards = {
    type,
    literal,
};

export default guards;

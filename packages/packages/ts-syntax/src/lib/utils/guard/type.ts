import type { ST_TypeDef } from 'SmartTS';

import { FrontendType } from '../../../enums/type';

export const hasProperties = (
    typeDef: ST_TypeDef,
): typeDef is Extract<ST_TypeDef, { type: FrontendType.TRecord | FrontendType.TVariant }> =>
    [FrontendType.TRecord].includes(typeDef.type);

export const hasOneArg = (
    typeDef: ST_TypeDef,
): typeDef is Extract<ST_TypeDef, { type: FrontendType.TList | FrontendType.TSet | FrontendType.TOptional }> =>
    typeDef.type === FrontendType.TList;

export const hasReference = (
    typeDef: ST_TypeDef,
): typeDef is Extract<ST_TypeDef, { type: FrontendType.TypeReference }> => typeDef.type === FrontendType.TypeReference;

export const isUnknown = (t: FrontendType) => t === FrontendType.TUnknown;

const type = {
    hasProperties,
    hasOneArg,
    hasReference,
    isUnknown,
};

export default type;

import { ST_TypeDef } from 'SmartTS';
import guards from '../guard';

const toString = (typeDef: ST_TypeDef): string => {
    if (guards.type.hasProperties(typeDef)) {
        return `${typeDef.type} as ${JSON.stringify(
            Object.entries<ST_TypeDef>(typeDef.properties).reduce((s, e) => ({ ...s, [e[0]]: toString(e[1]) }), {}),
        )}`;
    }
    if (guards.type.hasOneArg(typeDef)) {
        return `${typeDef.type}<${toString(typeDef.innerType)}>`;
    }
    return typeDef.type;
};

const type = {
    toString,
};

export default type;

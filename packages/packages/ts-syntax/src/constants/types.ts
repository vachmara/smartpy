import { BackendType, FrontendType } from '../enums/type';

const types: Record<string, BackendType> = {
    [FrontendType.TNat]: BackendType.nat,
    [FrontendType.TInt]: BackendType.int,
    [FrontendType.TUnit]: BackendType.unit,
    [FrontendType.TString]: BackendType.string,
    [FrontendType.TBool]: BackendType.bool,
    [FrontendType.TList]: BackendType.list,
    [FrontendType.TIntOrNat]: BackendType.intOrNat,
    [FrontendType.TRecord]: BackendType.record,
    [FrontendType.TUnknown]: BackendType.unknown,
};

export default types;

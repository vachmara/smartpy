declare type LogLevelMethod = (...d: unknown[]) => void;
declare const logger: {
    log: LogLevelMethod;
    info: LogLevelMethod;
    debug: LogLevelMethod;
    error: LogLevelMethod;
    trace: LogLevelMethod;
    warn: LogLevelMethod;
};
export default logger;

import type { CompilerHost, CompilerOptions, SourceFile } from 'typescript';
import type { FileInfo } from 'SmartTS';
export declare class STCompilerHost implements CompilerHost {
    #private;
    constructor(files: FileInfo[]);
    getSourceFile: (fileName: string) => SourceFile;
    getDefaultLibFileName: (defaultLibOptions: CompilerOptions) => string;
    writeFile: () => void;
    getCurrentDirectory: () => string;
    getDirectories: (_: string) => never[];
    fileExists: (fileName: string) => boolean;
    readFile: (fileName: string) => string;
    getCanonicalFileName: (fileName: string) => string;
    useCaseSensitiveFileNames: () => boolean;
    getNewLine: () => string;
    getEnvironmentVariable: () => string;
}
export default CompilerHost;

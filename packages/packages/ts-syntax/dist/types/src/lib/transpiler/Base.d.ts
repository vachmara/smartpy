import type { Node } from 'typescript';
import Transpiler from './Transpiler';
export declare class TranspilerBase {
    transpiler: Transpiler;
    constructor(transpiler: Transpiler);
    get transpilers(): Readonly<{
        type: import("./TypeTranspiler").default;
        literal: import("./LiteralTranspiler").default;
        decorator: import("./DecoratorTransformer").default;
        declaration: import("./DeclarationTranspiler").default;
    }>;
    get output(): import("../OutputContext").OutputContext;
    get sourceFile(): import("typescript").SourceFile;
    visitNode: (node: Node) => void;
    reportError(n: Node, message: string): void;
}

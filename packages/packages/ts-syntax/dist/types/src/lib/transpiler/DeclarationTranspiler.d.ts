import { Node, ClassDeclaration, NodeArray } from 'typescript';
import type { Nullable, ST_Decorator, ST_Literal, ST_Properties, ST_Property, ST_TypeDef } from 'SmartTS';
import { TranspilerBase } from './Base';
export default class DeclarationTranspiler extends TranspilerBase {
    visitClassNode(node: ClassDeclaration): void;
    visitClassMember: (node: Node) => void;
    /**
     * @description Extract class decorators from class declaration.
     * @param {ClassDeclaration} node
     * @returns {STDecorator[]} Class decorators
     */
    extractClassDecorators: (node: ClassDeclaration) => ST_Decorator[];
    /**
     * @description Extract property declaration.
     * @param {Node} nodes
     * @returns {Nullable<ST_Property>} Property
     */
    extractPropertyDeclaration: (node: Node) => Nullable<ST_Property>;
    extractPropertyValue: (typeDef: ST_TypeDef, node: Node) => ST_Literal;
    /**
     * @description Extract properties declarations.
     * @param {NodeArray<Node>} nodes
     * @returns {ST_Properties} Properties
     */
    extractProperties: (nodes: NodeArray<Node>) => ST_Properties;
    /**
     * @description Extract node name.
     * @param {Node} node
     * @returns {Nullable<string>} node name
     */
    extractName: (node: Node) => Nullable<string>;
}

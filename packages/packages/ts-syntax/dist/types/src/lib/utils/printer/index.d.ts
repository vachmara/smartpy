/// <reference path="../../../../../src/@types/common.d.ts" />
/// <reference path="../../../../../src/@types/output.d.ts" />
/// <reference path="../../../../../src/@types/property.d.ts" />
/// <reference path="../../../../../src/@types/decorator.d.ts" />
/// <reference path="../../../../../src/@types/literal.d.ts" />
/// <reference path="../../../../../src/@types/type.d.ts" />
declare const printer: {
    type: {
        toString: (typeDef: import("SmartTS").ST_TypeDef) => string;
    };
};
export default printer;

/// <reference path="../../../../../src/@types/common.d.ts" />
/// <reference path="../../../../../src/@types/output.d.ts" />
/// <reference path="../../../../../src/@types/property.d.ts" />
/// <reference path="../../../../../src/@types/decorator.d.ts" />
/// <reference path="../../../../../src/@types/literal.d.ts" />
/// <reference path="../../../../../src/@types/type.d.ts" />
import type { ST_TypeDef } from 'SmartTS';
import { FrontendType } from '../../../enums/type';
export declare const hasProperties: (typeDef: ST_TypeDef) => typeDef is {
    type: FrontendType.TRecord | FrontendType.TVariant;
    properties: import("SmartTS").ST_TypeDefs;
};
export declare const hasOneArg: (typeDef: ST_TypeDef) => typeDef is {
    type: FrontendType.TOptional | FrontendType.TList | FrontendType.TSet;
    innerType: ST_TypeDef;
};
export declare const hasReference: (typeDef: ST_TypeDef) => typeDef is {
    type: FrontendType.TypeReference;
    args: ST_TypeDef;
    refType: string;
};
export declare const isUnknown: (t: FrontendType) => boolean;
declare const type: {
    hasProperties: (typeDef: ST_TypeDef) => typeDef is {
        type: FrontendType.TRecord | FrontendType.TVariant;
        properties: import("SmartTS").ST_TypeDefs;
    };
    hasOneArg: (typeDef: ST_TypeDef) => typeDef is {
        type: FrontendType.TOptional | FrontendType.TList | FrontendType.TSet;
        innerType: ST_TypeDef;
    };
    hasReference: (typeDef: ST_TypeDef) => typeDef is {
        type: FrontendType.TypeReference;
        args: ST_TypeDef;
        refType: string;
    };
    isUnknown: (t: FrontendType) => boolean;
};
export default type;

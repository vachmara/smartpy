import { ST_TypeDef } from 'SmartTS';
declare const type: {
    toString: (typeDef: ST_TypeDef) => string;
};
export default type;

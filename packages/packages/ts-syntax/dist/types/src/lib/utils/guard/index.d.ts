/// <reference path="../../../../../src/@types/common.d.ts" />
/// <reference path="../../../../../src/@types/output.d.ts" />
/// <reference path="../../../../../src/@types/property.d.ts" />
/// <reference path="../../../../../src/@types/decorator.d.ts" />
/// <reference path="../../../../../src/@types/literal.d.ts" />
/// <reference path="../../../../../src/@types/type.d.ts" />
declare const guards: {
    type: {
        hasProperties: (typeDef: import("SmartTS").ST_TypeDef) => typeDef is {
            type: import("../../../enums/type").FrontendType.TRecord | import("../../../enums/type").FrontendType.TVariant;
            properties: import("SmartTS").ST_TypeDefs;
        };
        hasOneArg: (typeDef: import("SmartTS").ST_TypeDef) => typeDef is {
            type: import("../../../enums/type").FrontendType.TOptional | import("../../../enums/type").FrontendType.TList | import("../../../enums/type").FrontendType.TSet;
            innerType: import("SmartTS").ST_TypeDef;
        };
        hasReference: (typeDef: import("SmartTS").ST_TypeDef) => typeDef is {
            type: import("../../../enums/type").FrontendType.TypeReference;
            args: import("SmartTS").ST_TypeDef;
            refType: string;
        };
        isUnknown: (t: import("../../../enums/type").FrontendType) => boolean;
    };
    literal: {
        isRecord: (literal: import("SmartTS").ST_Literal) => literal is {
            type: import("../../../enums/type").FrontendType.TRecord;
            value: import("SmartTS").ST_Properties;
        };
    };
};
export default guards;

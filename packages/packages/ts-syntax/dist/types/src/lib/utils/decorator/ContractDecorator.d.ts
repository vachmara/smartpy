import type { ST_Literal, ST_Flag } from 'SmartTS';
export declare const ContractDecoratorUtils: {
    extractFlags: (literal: ST_Literal) => ST_Flag[];
};
export default ContractDecoratorUtils;

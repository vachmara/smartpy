import { FileInfo } from 'SmartTS';
import { Transpiler } from './lib/transpiler/Transpiler';
declare const SmartTS: {
    ide: {
        types: string;
    };
    Transpiler: typeof Transpiler;
    compile: (sourceCode: FileInfo) => string;
};
export default SmartTS;

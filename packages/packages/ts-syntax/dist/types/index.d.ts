import { FileInfo } from 'SmartTS';
declare const SmartTS: {
    ide: {
        types: string;
    };
    compile: (sourceCode: FileInfo) => void;
};
export default SmartTS;

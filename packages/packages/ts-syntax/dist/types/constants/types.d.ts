import { BackendType } from '../enums/type';
declare const types: Record<string, BackendType>;
export default types;

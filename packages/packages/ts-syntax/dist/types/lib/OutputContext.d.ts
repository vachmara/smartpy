import type { ST_TypeDefs, ST_TypeDef, ST_Property, ST_Properties, ST_Decorator } from 'SmartTS';
import type { LineAndCharacter, Node, SourceFile } from 'typescript';
interface ST_Class {
    name: string;
    id: number;
    declarationLine: number;
    decorators: {
        [name: string]: ST_Decorator;
    };
    types: ST_TypeDefs;
    properties: ST_Properties;
    methods: {
        [name: string]: {
            name: string;
        };
    };
}
export declare class OutputContext {
    private sourceFile;
    contracts: string[];
    typeDefs: ST_TypeDefs;
    private currentClassName;
    classes: Record<string, ST_Class>;
    errors: {
        msg: string;
        line?: LineAndCharacter;
    }[];
    warnings: {
        msg: string;
        line?: LineAndCharacter;
    }[];
    constructor(sourceFile: SourceFile);
    /**
     * @description Record class declaration;
     * @param name Class name
     * @param typeDef Type specification
     */
    emitClassDeclaration: (name: string, declarationLine: number) => void;
    /**
     * @description Record type definition.
     * @param name Type name
     * @param typeDef Type specification
     */
    emitTypeDef: (name: string, typeDef: ST_TypeDef) => void;
    /**
     * @description Record class property declaration.
     * @param property Property value and type
     */
    emitClassProperty: (property: ST_Property) => void;
    /**
     * @description Record class decorator.
     * @param name De name
     * @param typeDef Property value and type
     */
    emitClassDecorator: (decorator: ST_Decorator) => void;
    emitError: (node: Node, msg: string) => void;
    emitWarning: (node: Node, msg: string) => void;
    getResult(): string;
    private getNewContractAction;
    private stringifyType;
    private stringifyProperty;
    private stringifyValue;
    private stringifyFlags;
}
export default OutputContext;

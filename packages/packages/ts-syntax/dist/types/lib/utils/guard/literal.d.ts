/// <reference path="../../../../src/@types/common.d.ts" />
/// <reference path="../../../../src/@types/output.d.ts" />
/// <reference path="../../../../src/@types/property.d.ts" />
/// <reference path="../../../../src/@types/decorator.d.ts" />
/// <reference path="../../../../src/@types/literal.d.ts" />
/// <reference path="../../../../src/@types/type.d.ts" />
import type { ST_Literal } from 'SmartTS';
import { FrontendType } from '../../../enums/type';
export declare const isRecord: (literal: ST_Literal) => literal is {
    type: FrontendType.TRecord;
    value: import("SmartTS").ST_Properties;
};
declare const literal: {
    isRecord: (literal: ST_Literal) => literal is {
        type: FrontendType.TRecord;
        value: import("SmartTS").ST_Properties;
    };
};
export default literal;

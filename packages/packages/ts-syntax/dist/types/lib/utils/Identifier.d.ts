import { Nullable } from 'SmartTS';
import { Node } from 'typescript';
export declare const IdentifierUtils: {
    extractName: (node: Node) => Nullable<string>;
};
export default IdentifierUtils;

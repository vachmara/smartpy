import type { Node, Decorator } from 'typescript';
import type { Nullable, ST_ContractDecoratorInfo, ST_Decorator } from 'SmartTS';
import { TranspilerBase } from './Base';
import { STDecoratorKind } from '../../enums/decorator';
export default class DecoratorTransformer extends TranspilerBase {
    visitDecorator(node: Decorator): {
        kind: STDecoratorKind;
        info: ST_ContractDecoratorInfo;
    } | null;
    /**
     * @description Extract decorator information
     * @param {Node} node
     * @returns {Nullable<STDecorator>} Decorator Information
     */
    extractDecoratorInformation: (node: Node) => Nullable<ST_Decorator>;
    /**
     * @description Extract information from @Contract decorator
     * @param {Node} node
     * @returns {ST_ContractDecoratorInfo} Flags
     */
    extractInfo: (node: Node) => ST_ContractDecoratorInfo;
}

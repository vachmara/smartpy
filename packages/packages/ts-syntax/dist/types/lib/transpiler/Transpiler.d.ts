import type { Node, SourceFile } from 'typescript';
import type { FileInfo, ST_TypeDef } from 'SmartTS';
import OutputContext from '../OutputContext';
import TypeTranspiler from './TypeTranspiler';
import LiteralTranspiler from './LiteralTranspiler';
import DecoratorTransformer from './DecoratorTransformer';
import DeclarationTranspiler from './DeclarationTranspiler';
export declare class Transpiler {
    sourceFile: SourceFile;
    output: OutputContext;
    transpilers: Readonly<{
        type: TypeTranspiler;
        literal: LiteralTranspiler;
        decorator: DecoratorTransformer;
        declaration: DeclarationTranspiler;
    }>;
    constructor(source: FileInfo);
    transpile: () => string;
    visitNode: (node: Node) => void;
    reportError: (node: Node, error: string) => void;
    normalizeType: (typeDef: ST_TypeDef) => ST_TypeDef;
}
export default Transpiler;

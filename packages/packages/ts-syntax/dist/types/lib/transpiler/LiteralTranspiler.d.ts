import type { Node } from 'typescript';
import type { ST_Literal } from 'SmartTS';
import { TranspilerBase } from './Base';
export default class LiteralTranspiler extends TranspilerBase {
    /**
     * @description Extract literal
     * @param {Node} node
     * @returns {ST_Literal} A literal
     */
    extractLiteral: (node: Node) => ST_Literal;
}

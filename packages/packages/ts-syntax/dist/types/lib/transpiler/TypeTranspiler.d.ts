import type { ST_TypeDef } from 'SmartTS';
import type { Node } from 'typescript';
import { TranspilerBase } from './Base';
export default class TypeTranspiler extends TranspilerBase {
    visitNode: (node: Node) => void;
    /**
     * @description Extract type definition from type declaration.
     * @param {TypeNode} node
     * @returns {ST_TypeDef} Type definition
     */
    extractTypeDef: (node?: Node | undefined) => ST_TypeDef;
    private composeFrontendType;
}

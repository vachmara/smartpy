import type { Node } from 'typescript';
import type { ST_Literal } from 'SmartTS';
/**
 * @description Extract literal
 * @param {Node} node
 * @returns {ST_Literal} A literal
 */
export declare const extractLiteral: (node: Node) => ST_Literal;

declare type TInt = number;
declare type TNat = number;
declare type TMutez = number;
declare type TBytes = string;
declare type TString = string;
declare type TBool = boolean;
declare type TAddress = string;
declare type TNever = never;
declare type TSignature = string;
declare type TUnit = void;
declare type TTimestamp = string | number;
declare type TOptional<T> = {
    some: (msg?: string) => T;
};
declare type TList<T> = T[];
declare type TSet<T> = T[];
declare type TPair<F, S> = [F, S];
declare type TMap<K, V> = [K, V][];
declare type TBigMap<K, V> = [K, V][];
declare type TRecord = Record<string, any>;
declare type TVariant = Record<string, any>;
declare const Contract: (_: {
    flags: ((string | number | boolean)[] | string)[];
}) => (constructor: Function) => void;
declare const EntryPoint: (target: any, propertyKey: string, descriptor: PropertyDescriptor) => PropertyDescriptor;

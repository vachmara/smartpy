import Bls12 from 'tezos-bls12-381';
import eztz from './eztz';

globalThis.eztz = eztz;
globalThis.smartpyContext = {
    Bls12
};

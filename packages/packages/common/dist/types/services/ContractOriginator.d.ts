import { ContractAbstraction, TezosToolkit, Wallet } from '@taquito/taquito';
export declare function originateContract(tezos: TezosToolkit, code: string | object[], initialStorage: string | object, balance?: string): Promise<ContractAbstraction<Wallet>>;
declare const _default: {
    originateContract: typeof originateContract;
};
export default _default;

export declare function originateContract(code: string, storage: string, secret_key: string, config: Record<string, any>, amount?: number): Promise<string>;
export declare function callContract(address: string, secret_key: string, config: Record<string, any>, parameter: object, entrypoint?: string, amount?: number): Promise<string>;
declare const _default: {
    originateContract: typeof originateContract;
    callContract: typeof callContract;
};
export default _default;


import typescript from '@rollup/plugin-typescript';
import json from '@rollup/plugin-json';
import commonjs from '@rollup/plugin-commonjs';
import resolve from '@rollup/plugin-node-resolve';
import { terser } from 'rollup-plugin-terser';
import polyfills from 'rollup-plugin-polyfill-node';
import pkg from './package.json';

export default {
    input: 'src/index.ts',
    output: [
        {
            file: pkg.module,
            globals: { crypto: 'crypto' },
            format: 'es',
            sourcemap: true,
            banner: `/*! === Version: ${pkg.version} === */`,
        },
        {
            file: pkg.main,
            format: 'cjs',
            sourcemap: true,
            globals: { crypto: 'crypto' },
            exports: 'default',
            banner: `/*! === Version: ${pkg.version} === */`,
        },
        {
            name: 'SmartPyCommon',
            file: pkg.minified,
            format: 'iife',
            globals: { crypto: 'crypto' },
            exports: 'default',
            compact: true,
            banner: `/*! === Version: ${pkg.version} === */`,
        },
    ],
    plugins: [
        typescript(),
        commonjs(),
        resolve(),
        json(),
        terser()
    ],
};

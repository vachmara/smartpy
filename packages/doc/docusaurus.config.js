/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
    title: 'SmartPy Docs',
    tagline: 'SmartPy Docs',
    url: 'https://smartpy.io',
    baseUrl: '/',
    onBrokenLinks: 'throw',
    onBrokenMarkdownLinks: 'warn',
    favicon: 'img/favicon.ico',
    organizationName: 'SmartPy',
    projectName: 'SmartPy',
    onBrokenLinks: 'warn',
    themeConfig: {
        navbar: {
            logo: {
                alt: 'SmartPy',
                src: 'img/logo.svg',
                srcDark: 'img/logo_dark.svg',
            },
            items: [
                {
                    type: 'docsVersionDropdown',

                    //// Optional
                    position: 'left',
                    // Add additional dropdown items at the beginning/end of the dropdown.
                    dropdownItemsBefore: [],
                    dropdownItemsAfter: [],
                    // Do not add the link active class when browsing docs.
                    dropdownActiveClassDisabled: true,
                },
                {
                    type: 'doc',
                    docId: 'intro',
                    position: 'left',
                    label: 'Docs',
                    docsPluginId: 'default',
                },
                {
                    type: 'doc',
                    docId: 'releases',
                    position: 'left',
                    label: 'Releases',
                },
                {
                    href: 'https://smartpy.io/ide',
                    label: 'IDE',
                    position: 'right',
                },
                {
                    href: 'https://gitlab.com/smartpy/smartpy',
                    label: 'Gitlab',
                    position: 'right',
                },
            ],
        },
        footer: {
            links: [
                {
                    title: 'Docs',
                    items: [
                        {
                            label: 'Docs',
                            to: '/docs/intro',
                        },
                        {
                            label: 'Previous Releases',
                            to: '/docs/releases',
                        },
                    ],
                },
                {
                    title: 'Community',
                    items: [
                        {
                            label: 'Tezos Stack Exchange',
                            href: 'https://tezos.stackexchange.com/questions/ask?tags=smartpy',
                        },
                        {
                            label: 'Telegram',
                            href: 'https://t.me/SmartPy_io',
                        },
                        {
                            label: 'Twitter',
                            href: 'https://twitter.com/SmartPy_io',
                        },
                    ],
                },
                {
                    title: 'More',
                    items: [
                        {
                            label: 'Medium',
                            href: 'https://smartpy-io.medium.com',
                        },
                        {
                            label: 'GitLab',
                            href: 'https://gitlab.com/smartpy/smartpy',
                        },
                    ],
                },
            ],
            copyright: `Copyright © ${new Date().getFullYear()} SmartPy.`,
        },
        prism: {
            theme: require('prism-react-renderer/themes/github'),
            darkTheme: require('prism-react-renderer/themes/dracula'),
        },
    },
    presets: [
        [
            '@docusaurus/preset-classic',
            {
                docs: {
                    sidebarPath: require.resolve('./sidebars.js'),
                    editUrl: 'https://gitlab.com/smartpy/smartpy/edit/master/packages/docs/',
                },
                theme: {
                    customCss: [
                        require.resolve('./src/css/custom.css'),
                        require.resolve('./src/components/CodeBlock.css'),
                        'prismjs/themes/prism-dark.css',
                    ],
                },
            },
        ],
    ],
    i18n: {
        defaultLocale: 'en',
        locales: ['en'],
    },
};

import useSyntaxContext, { SYNTAX } from '../hooks/useSyntaxContext';


const Snippet = ({ syntax: _syntax, children }) => {
    const { syntax } = useSyntaxContext();

    if (syntax === _syntax) {
        return children;
    }

    return null;
}

export {
    SYNTAX
}

export default Snippet;

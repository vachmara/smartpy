import React from 'react';

function MichelsonArgs(props: any) {
    return (
        <div className="michelsonArgsSection">
            <h4>
                <span className="entrypointVariableName">{props.name || "Parameter"}</span>
                <span className="michelsonType">{props.type || "unit"}</span>
            </h4>
            <div className="michelsonArgs">
                {props.children}
            </div>
        </div>
    )
}

export default MichelsonArgs;
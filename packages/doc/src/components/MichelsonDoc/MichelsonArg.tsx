import React from 'react';
import replaceJSX from '../utils/replaceJSX'

const variablename_replacements = [
    [
        /(\$[a-zA-Z0-9_]+)/g,
        (content) => {
            return <span className="variableName">{content}</span>;
        },
    ],
]

function MichelsonArg(props: any) {
    const type = replaceJSX(props.type, variablename_replacements);
    return (
        <div className="michelsonArg">
            <span className="michelsonAnnotation">{props.name}</span>
            <span className="michelsonType">{type}</span>:
            <span className="michelsonArgDescription">{props.children}</span>
        </div>
    )
}

export default MichelsonArg;
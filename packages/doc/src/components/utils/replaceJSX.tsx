import React from 'react';

function replaceJSX(inputText, replacements) {
    if (replacements[0] === undefined) {
        return inputText;
    }
    let pattern = replacements[0][0];
    let func = replacements[0][1];

    inputText = inputText.split(pattern);
    for (let i = 0; i < inputText.length; i++) {
        if (i % 2 === 1) {
            inputText[i] = func(inputText[i]);
        } else {
            inputText[i] = replaceJSX(
                inputText[i],
                replacements.slice(1, replacements.length)
            );
        }
    }
    return <React.Fragment>{inputText}</React.Fragment>;
}

export default replaceJSX;
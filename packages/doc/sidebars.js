/**
 * Creating a sidebar enables you to:
 - create an ordered group of docs
 - render a sidebar for each doc of that group
 - provide next/previous navigation

 The sidebars can be generated from the filesystem, or explicitly defined here.

 Create as many sidebars as you want.
 */

module.exports = {
    sidebar: [
        ...[
            {
                type: 'category',
                label: 'Introduction',
                items: ['intro'],
            },
            {
                type: 'category',
                label: 'Basics',
                items: ['basics/types'],
            },
            {
                type: 'doc',
                label: 'CLI',
                id: 'cli',
            },
            {
                type: 'category',
                label: 'State channels',
                items: [
                    {
                        type: 'autogenerated',
                        dirName: 'state_channels',
                    },
                ],
            },
        ],
        // Development guides (not available in production)
        ...(process.env.NODE_ENV === 'development'
            ? [
                  {
                      type: 'category',
                      label: 'Docusaurus Guides',
                      items: [
                          {
                              type: 'autogenerated',
                              dirName: 'docusaurus-guides',
                          },
                      ],
                  },
              ]
            : []),
    ],
};

# Command Line Interface

The command line interface is called **smartpy-cli** and has been introduced in the following **[Medium Post](https://smartpy-io.medium.com/introducing-smartpybasic-a-simple-cli-to-build-tezos-smart-contract-in-python-f5bd8772b74a)**.

## Installation

```shell
sh <(curl -s https://smartpy.io/cli/install.sh)
```

More info available at **[https://smartpy.io/cli](https://smartpy.io/cli)**

## Check version

This command tells which CLI version is installed.

```shell
~/smartpy-cli/SmartPy.sh --version
```

## Dependencies

`smartpy-cli` depends on **[python3](https://www.python.org/downloads/)** and **[Node.js](https://nodejs.org/)**.

## Execution

### Executing a SmartPy Script with its tests

#### `SmartPy.sh test`
Perform tests defined in a `script.py` (see **[Tests and Scenarios](../tests-and-scenarios)**).

```shell
~/smartpy-cli/SmartPy.sh test <script.py> <output-directory>
```

This includes many outputs: `types`, `generated michelson code`, `pretty-printed scenario`, etc.

### Compiling Contracts and Expressions

#### `SmartPy.sh compile`

Compute the Compilation Targets defined in a `script.py`.

```shell
~/smartpy-cli/SmartPy.sh compile <script.py> <output-directory>
```

**Example:**
```shell
~/smartpy-cli/SmartPy.sh compile welcome.py /tmp/welcome
```

### Custom Targets

#### `SmartPy.sh kind <kind>`

Similar to tests. Perform scenarios defined in a `script.py` introduced by the custom target.

```shell
~/smartpy-cli/SmartPy.sh kind <kind> <script.py> <output-directory>
```

### Deploying a contract
```shell
# Using Micheline format (.tz)

~/smartpy-cli/SmartPy.sh originate-contract --code code.tz --storage storage.tz --rpc https://florencenet.smartpy.io

# Using Michelson format (.json)

~/smartpy-cli/SmartPy.sh originate-contract --code code.json --storage storage.json --rpc https://florencenet.smartpy.io

# By default, the originator will use a faucet account.
# But you can provide your own private key as an argument

~/smartpy-cli/SmartPy.sh originate-contract --code code.json --storage storage.json --rpc https://florencenet.smartpy.io --private-key edsk...
```

### CLI optional arguments

`SmartPy.sh` takes several optional arguments.

- `--purge` -> Empty the output directory before writting to it.

- `--html` -> Adds `.html` outputs such as a `log.html` which is identical to the output panel in the Web IDE.

- `--protocol <delphi|edo|florence|granada>` -> Select a Tezos protocol. (Default is `florence`)

- `--<flag> arguments` -> Set some `<flag>` with `arguments`.

- `--<flag>` -> Activate some boolean `<flag>`.

- `--no-<flag>` -> Deactivate some boolean `<flag>`.

- `--mockup` -> Run in mockup (experimental, needs installed source).

- `--sandbox` -> Run in sandbox (experimental, needs installed source).

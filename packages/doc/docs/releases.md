
# Releases

## v0.6.9

|   |   |
|---|---|
| Date | 2021-06-30  |
| Commit  | d86b17adb6d0fb15c9dc009bf3a62de6665a9ae3  |
| Link  | [v0.6.9](https://smartpy.io/releases/20210630-d86b17adb6d0fb15c9dc009bf3a62de6665a9ae3)  |

### Change Log
  - Adds exception testing, users are now able to link:reference.html#_registering_and_displaying_calls_to_entry_points[test error messages] emitted by failed contract calls;
  - File names in error location;
  - Online IDE now divides tests and compilation targets into two distinct sections;
#### Bug Fixes
  - `originate-contract` CLI command can now be executed from any sub-folder;
  - `scenario.simulation` debugging feature is now working again;
  - Fixes an issue in the explorer page that would cause an exception when interpreting tuples;
  - Allow invalid `packed` bytes to be tested with `run(valid = False)`;
  - **(A work in progress)** New state channel based game platform templates:
    - [Game platform](https://smartpy.io/ide?template=state_channel_games/game_platform.py)
    - [Game tester](https://smartpy.io/ide?template=state_channel_games/game_tester.py)
    - [Game wrapper](https://smartpy.io/ide?template=state_channel_games/model_wrap.py)
    - [Game types](https://smartpy.io/ide?template=state_channel_games/types.py)
    - [Game: Head and Tail model](https://smartpy.io/ide?template=state_channel_games/models/head_tail.py)
    - [Game: Nim model](https://smartpy.io/ide?template=state_channel_games/models/nim.py)
    - [Game: tic-tac-toe model](https://smartpy.io/ide?template=state_channel_games/models/tictactoe.py)
    - [Game: transfer model](https://smartpy.io/ide?template=state_channel_games/models/transfer.py)

## v0.6.8

|   |   |
|---|---|
| Date | 2021-06-09  |
| Commit  | d64964633e98c1bd1fe8beb9f83138185cabdf90  |
| Link  | [v0.6.8](https://smartpy.io/releases/20210609-d64964633e98c1bd1fe8beb9f83138185cabdf90)  |

### Change Log

  - SmartPy CLI now shows all created operations in log.txt (it used to only show recursive operations).
  - More documentation and examples of lazy and updatable entry points.

#### Bug Fixes
  - Fix CLI originate-contract command.

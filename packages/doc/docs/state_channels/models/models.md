---
sidebar_position: 1
---
# List of models


|      Name       | Hash | Random | InitParams |
| :-------------: | ---- | :----: | :--------: |
|    Transfer     |      |        |            |
| [Tictactoe][m2] |      |        |            |
|       Nim       |      |        |     ✅     |
|    Head tail    |      |   ✅   |            |


[m2]: tictactoe
---
sidebar_position: 1
---

# Overview

### What are State Channels?

State Channel is a way to combine the security offered by the Tezos blockchain with the throughput offered by the offchain operations.

In particular it offers a **high throughput**, a **trustless framework**, a **minimal cost** and is **fully customizable**.

> With real life contracts between parties you don't need the judge to be here everytime. <br/>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Same thing applies with State Channels.


<!-- If everything goes perfectly you open a channel with another party, you push your initial tokens and you only interact with Tezos when you want to withdraw tokens or close the channel. If there is a conflict, the blockchain will give you the arbitration framework related to your contrats. -->


### What does State Channels offer?



### What does SmartPy offer?

SmartPy offers:

- a State Channels test platform
- a [set of models](models/models)
- a lot of tests and scenarios
- a complete documentation
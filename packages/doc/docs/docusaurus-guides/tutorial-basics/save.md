

import SyntaxSelector from '@theme/Syntax/Selector';
import Snippet, {SYNTAX} from '@theme/Syntax/Snippet';

<SyntaxSelector />


<Snippet syntax={SYNTAX.PY}>

```python
import smartpy as sp

class StoreValue(sp.Contract):
  def __init__(self, value):
      self.init(storedValue = value)

  @sp.entry_point
  def replace(self, value):
      self.data.storedValue = value

  @sp.entry_point
  def double(self):
      self.data.storedValue *= 2

@sp.add_test(name = "StoreValue")
def test():
  scenario = sp.test_scenario()
  scenario.h1("Store Value")
  contract = StoreValue(1)
  scenario += contract
  scenario += contract.replace(2)
  scenario += contract.double()
```

</Snippet>
<Snippet syntax={SYNTAX.TS}>

```typescript
interface TStorage {
    storedValue: TNat;
}

@Contract
class StoreValue {
    storage: TStorage = {
        storedValue: 1,
    };

    @EntryPoint
    replace(newValue: TInt) {
        this.storage.storedValue = newValue;
    }

    @EntryPoint
    double() {
        this.storage.storedValue *= 2;
    }

    @EntryPoint
    divide(divisor: TInt) {
        this.storage.storedValue /= divisor;
    }
}
```

</Snippet>
<Snippet syntax={SYNTAX.ML}>

```ocaml
open Smartml

module Contract = struct

  let%entry_point divide self params =
    verify (params.divisor > 5);
    self.data.storedValue //= params.divisor

  let%entry_point double self () =
    self.data.storedValue *= 2

  let%entry_point replace self params =
    self.data.storedValue <- params.value

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(storedValue = nat).layout("storedValue")
      ~storage:[%expr {storedValue = 12}]
      [divide; double; replace]
end
```

</Snippet>

SmartPy offers {syntax}

Or **try Docusaurus immediately** with **[new.docusaurus.io](https://new.docusaurus.io)**.

## Generate a new site

Generate a new Docusaurus site using the **classic template**:

```shell
npx @docusaurus/init@latest init my-website classic
```

## Start your site

Run the development server:

```shell
cd my-website

npx docusaurus start
```

Your site starts at `http://localhost:3000`.

Open `docs/intro.md` and edit some lines: the site **reloads automatically** and display your changes.

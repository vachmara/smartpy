# Typing

import SyntaxSelector from '@theme/Syntax/Selector';
import Snippet, {SYNTAX} from '@theme/Syntax/Snippet';

## Type Inference

<Snippet syntax={SYNTAX.PY}>

Just like in Python, most of the time there is no need to specify the type of an object in SmartPy. For a number of reasons (e.g., because SmartPy's target language, Michelson, requires types), each SmartPy expression does however need a type. Therefore SmartPy uses type inference in order to determine each expressions type.

In practice, this means that information about an expression is gathered according to its usage: for example, when somewhere in your contract you write `self.data.x == "abc"`, SmartPy will automatically determine and remember that `self.data.x` is a string.

Note that SmartPy types are distinct from Python types: `self.data.x == "abc"` has the Python type `sp.Expr` (simply because it is a SmartPy expression), whereas it has the SmartPy type `sp.TBool` (see below).

While most of the time the user will not write many types explicitly it is beneficial to at least have a basic understanding of what they are. This also helps understanding error messages better.

</Snippet>
<Snippet syntax={SYNTAX.TS}>

Note that Michelson types are distinct from Typescript types: `const value = 2` implicitly has the Typescript type `number`, where it can be either a `nat` or an `int` in Michelson. (See below: [nat](#nat), [int](#int))

Depending on how `value` is used, SmartML will be able to automatically determine if the value is being used as a `nat` or an `int`.

It is a best practice to always explicitly specify the type. (e.g., `const value: TNat = 2`)

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>



## Basic Types



### nat
The type of non-negative integer values. ( **[Michelson Reference](https://tezos.gitlab.io/michelson-reference/#type-nat)** )
<Snippet syntax={SYNTAX.PY}>

```python
import smartpy as sp

# value is of type sp.TNat
value = sp.nat(42)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
let value: TNat = 42
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
let value = 42
```

</Snippet>



### int
The type of integer values. ( **[Michelson Reference](https://tezos.gitlab.io/michelson-reference/#type-int)** )
<Snippet syntax={SYNTAX.PY}>

```python
import smartpy as sp

# value is of type sp.TInt
value = sp.int(42)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
let value: TInt = 42
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
let value = 42
```

</Snippet>



### string
The type of string values. ( **[Michelson Reference](https://tezos.gitlab.io/michelson-reference/#type-string)** )

The current version of Michelson restricts strings to be the printable subset of `7-bit ASCII`, namely characters with codes from within **[32, 126]** range, plus the following escape characters `\n`, `\\`, `\"`. Unescaped line breaks (both `\n` and `\r`) cannot appear in a string.

<Snippet syntax={SYNTAX.PY}>

```python
import smartpy as sp

# value is of type sp.TString
value = "Hello World" # or sp.string("Hello World")
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
let type: TString = "Hello World"
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
let value = "Hello World"
```

</Snippet>



### bool
The type of boolean values. ( **[Michelson Reference](https://tezos.gitlab.io/michelson-reference/#type-bool)** )

<Snippet syntax={SYNTAX.PY}>

```python
import smartpy as sp

# value is of type sp.TBool
value = True
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
let value: TBool = true
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
let value = true
```

</Snippet>



### bytes
The type of serialized data. ( **[Michelson Reference](https://tezos.gitlab.io/michelson-reference/#type-bytes)** )

Bytes are used for serializing data, in order to check signatures and compute hashes on them. They can also be used to incorporate data from the wild and untyped outside world.

<Snippet syntax={SYNTAX.PY}>

```python
import smartpy as sp

# value is of type sp.TBytes
value = sp.bytes("0x00")
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
let value: TBytes = "0x00"
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>



### mutez
A specific type for manipulating tokens. ( **[Michelson Reference](https://tezos.gitlab.io/michelson-reference/#type-mutez)** )

Mutez (micro-Tez) are internally represented by 64-bit signed integers. These are restricted to prevent creating a negative amount of mutez. Instructions are limited to prevent overflow and mixing them with other numerical types by mistake. They are also mandatorily checked for overflows.

<Snippet syntax={SYNTAX.PY}>

```python
import smartpy as sp

# value is of type sp.TMutez
value = sp.mutez(1000000)
# or
value = sp.tez(1)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
let value: TMutez = 1000000
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>



### timestamp
A moment in time. ( **[Michelson Reference](https://tezos.gitlab.io/michelson-reference/#type-timestamp)** )

Literal timestamp is written either using [RFC3339](https://datatracker.ietf.org/doc/html/rfc3339) notation in a string (readable), or as the number of seconds since Epoch in a natural (optimized).

<Snippet syntax={SYNTAX.PY}>

```python
import smartpy as sp

# value is of type sp.TTimestamp
value = sp.timestamp(1571761674)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
let value: TTimestamp = 1571761674
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>



### address
An address of a contract or implicit account. ( **[Michelson Reference](https://tezos.gitlab.io/michelson-reference/#type-address)** )

The address type merely gives the guarantee that the value has the form of a Tezos address, as opposed to [contract](#contract) that guarantees that the value is indeed a valid, existing account.

A valid Tezos address is a string prefixed by either tz1, tz2, tz3 or KT1 and followed by a Base58 encoded hash and terminated by a 4-byte checksum.

The prefix designates the type of address:

- `tz1` addresses are followed by a ed25519 public key hash
- `tz2` addresses are followed by a Secp256k1 public key hash
- `tz3` addresses are followed by a NIST p256r1 public key hash
- `KT1` addresses are followed by a contract hash

Addresses prefixed by `tz1`, `tz2` and `tz3` designate implicit accounts, whereas those prefixed `KT1` designate originated accounts.

Finally, addresses can specify an entrypoint by providing a `%<entrypoint_name>` suffix.

<Snippet syntax={SYNTAX.PY}>

```python
import smartpy as sp

# value is of type sp.TAddress
value = sp.address("tz1YtuZ4vhzzn7ssCt93Put8U9UJDdvCXci4")
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
let value: TAddress = "tz1YtuZ4vhzzn7ssCt93Put8U9UJDdvCXci4"
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>



## Container Types



### contract t
Address of a contract, where `t` is the contract's parameter type. ( **[Michelson Reference](https://tezos.gitlab.io/michelson-reference/#type-contract)** )

A value of type `contract t` is guaranteed to be a valid, existing account whose parameter type is `t`. This can be opposed to the `address` type, that merely gives the guarantee that the value has the form of a Tezos address. Values of the contract type cannot be stored. There are not literal values of type contract. Instead, such values are created using instructions such as **[CONTRACT](https://tezos.gitlab.io/michelson-reference/#instr-CONTRACT)** or **[IMPLICIT_ACCOUNT](https://tezos.gitlab.io/michelson-reference/#instr-IMPLICIT_ACCOUNT)**.

<Snippet syntax={SYNTAX.PY}>

```python
import smartpy as sp

# value is of type sp.TContract(sp.TNat)
value = sp.contract(sp.TNat, "tz1YtuZ4vhzzn7ssCt93Put8U9UJDdvCXci4", entry_point = "foo")
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
Work in progress
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>

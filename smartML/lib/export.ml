(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Utils

let export_layout layout =
  match Unknown.get layout with
  | None -> "None"
  | Some layout ->
      let leaf Layout.{source; target} =
        if source = target
        then Printf.sprintf "(***%s***)" source
        else Printf.sprintf "(***%s as %s***)" source target
      in
      let node l1 l2 = Printf.sprintf "(%s %s)" l1 l2 in
      Printf.sprintf "(Some %s)" (Binary_tree.cata leaf node layout)

let export_type =
  let rec export_type t =
    match Type.getRepr t with
    | T0 t ->
        let t, memo = Michelson_base.Type.string_of_type0 t in
        assert (memo = None);
        t
    | TInt {isNat} ->
      ( match Unknown.get isNat with
      | None -> "intOrNat"
      | Some true -> "nat"
      | Some false -> "int" )
    | TBounded {t; cases} ->
        let final, cases =
          match Unknown.get cases with
          | None -> (false, [])
          | Some {final; cases} -> (final, cases)
        in
        Printf.sprintf
          "(bounded %s %s %s)"
          (export_type t)
          (if final then "True" else "False")
          (String.concat " " (List.map (fun _ -> "bounded not handled") cases))
    | TRecord {row; layout} ->
        Printf.sprintf
          "(record (%s) %s)"
          (String.concat
             " "
             (List.map
                (fun (s, t) -> Printf.sprintf "(%s %s)" s (export_type t))
                row))
          (export_layout layout)
    | (T1 (T_option, _) | TVariant _) as t ->
        let layout, row =
          Option.get ~msg:"export: variant" (Type.view_variant (F t))
        in
        Printf.sprintf
          "(variant (%s) %s)"
          (String.concat
             " "
             (List.map
                (fun (s, t) -> Printf.sprintf "(%s %s)" s (export_type t))
                row))
          (export_layout layout)
    | T1 (T_set, telement) -> Printf.sprintf "(set %s)" (export_type telement)
    | TMap {tkey; tvalue} ->
        Printf.sprintf "(map %s %s)" (export_type tkey) (export_type tvalue)
        (* TODO big ? *)
    | T1 (T_contract, t) -> Printf.sprintf "(contract %s)" (export_type t)
    | TSecretKey -> "secret_key"
    | TUnknown {contents = UExact t} -> export_type t
    | TUnknown {contents = UUnknown _} -> "unknown"
    | TUnknown {contents = URecord row} ->
        export_type (Type.record_default_layout Config.Comb row)
    | TUnknown {contents = UTuple _} -> failwith "export_type: TODO UTuple"
    | TUnknown {contents = UVariant row} ->
        export_type (Type.variant_default_layout Config.Comb row)
    | TTuple ts ->
        let ts = List.map export_type ts in
        Printf.sprintf "(tuple %s)" (String.concat " " ts)
    | T1 (T_list, t) -> Printf.sprintf "(list %s)" (export_type t)
    | T2 (T_lambda, t1, t2) ->
        Printf.sprintf "(lambda %s %s)" (export_type t1) (export_type t2)
    | TSaplingState {memo} ->
        let memo =
          match Unknown.get memo with
          | None -> "unknown"
          | Some i -> string_of_int i
        in
        Printf.sprintf "(sapling_state %s)" memo
    | TSaplingTransaction {memo} ->
        let memo =
          match Unknown.get memo with
          | None -> "unknown"
          | Some i -> string_of_int i
        in
        Printf.sprintf "(sapling_transaction %s)" memo
    | T1 (T_ticket, t) -> Printf.sprintf "(ticket %s)" (export_type t)
    | T2 ((T_map | T_big_map | T_pair _ | T_or _), _, _) -> assert false
  in
  export_type

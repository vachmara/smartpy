(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Utils
open Control
open Basics
open Typed
open Utils.Misc
open Primitives

type context_ =
  { sender : string option
  ; source : string option
  ; chain_id : string
  ; time : Bigint.t
  ; amount : Bigint.t
  ; level : Bigint.t
  ; voting_powers : (string * Bigint.t) list
  ; line_no : line_no
  ; debug : bool
  ; contract_id : contract_id option }

type context = context_ ref

let context
    ?contract_id
    ?sender
    ?source
    ?(chain_id = "")
    ~time
    ~amount
    ~level
    ~voting_powers
    ~line_no
    ~debug
    () =
  let source =
    match source with
    | None -> sender
    | _ -> source
  in
  ref
    { sender
    ; source
    ; chain_id
    ; time
    ; amount
    ; level
    ; voting_powers
    ; line_no
    ; debug
    ; contract_id }

let context_sender c = !c.sender

let context_time c = !c.time

let context_contract_id c = !c.contract_id

let context_line_no c = !c.line_no

let context_debug c = !c.debug

(** Internal exception: raised by {!interpret_command} and caught by {!interpret_contract}. *)

exception ExecFailure of value * smart_except list

type root =
  | R_storage
  | R_local   of string

type step =
  | S_attr      of string
  | S_item_map  of value
  | S_item_list of int

type path =
  { root : root
  ; steps : step list }

let string_of_step config =
  let module Printer = (val Printer.get config : Printer.Printer) in
  function
  | S_attr attr -> "." ^ attr
  | S_item_list i -> Printf.sprintf "[%d]" i
  | S_item_map key -> Printf.sprintf "[%s]" (Printer.value_to_string key)

let _string_of_steps config steps =
  String.concat "" (List.map (string_of_step config) steps)

let extend_path {root; steps} steps' = {root; steps = steps @ steps'}

let ( @. ) = Lens.( @. )

type variable_value =
  | Iter        of (value * path option)
  | Heap_ref    of value ref
  | Variant_arg of value
  | Match_list  of value

type in_contract =
  { parameters : value
  ; context : context_
  ; contract_state : contract_state ref
  ; tparameter : Type.t }

type in_contract_option =
  | No_contract of smart_except list
  | In_contract of in_contract

let get_in_contract message = function
  | No_contract l ->
      raise
        (SmartExcept
           [`Text "Missing environment to compute"; `Text message; `Br; `Rec l])
  | In_contract x -> x

type env =
  { in_contract : in_contract_option
  ; primitives : (module Primitives)
  ; variables : (string * variable_value) list ref
  ; operations : operation list ref
  ; debug : bool
  ; steps : Execution.step list ref
  ; lambda_params : (int * value) list
  ; global_variables : (string * texpr) list
  ; current_locals : string list ref }

let lens_heap_ref =
  Lens.bi
    (function
      | Heap_ref x -> x
      | _ -> failwith "not a heap reference")
    (fun x -> Heap_ref x)

let lens_of_root env = function
  | R_storage ->
      let f (s : contract_state) =
        Lens.{focus = s.storage; zip = (fun storage -> {s with storage})}
      in
      Lens.ref (get_in_contract "variable" env.in_contract).contract_state
      @. Lens.make f
      @. Lens.some ~err:"no storage"
  | R_local name ->
      Lens.ref env.variables
      @. Lens.assoc_exn ~equal:( = ) ~key:name ~err:("local not found: " ^ name)
      @. lens_heap_ref
      @. Lens.unref

(** Convert a step into a lens that points to an optional value,
   accompanied by an error message for when the value is missing. *)
let lens_of_step ~config ~line_no =
  let module Printer = (val Printer.get config : Printer.Printer) in
  let line_no = string_of_line_no line_no in
  function
  | S_item_list i ->
      ( Value.lens_list_nth i
      , Printf.sprintf "Line %s: Index '%d' out of range." line_no i )
  | S_item_map key ->
      ( Value.lens_map_at ~key
      , Printf.sprintf
          "Line %s: Key '%s' not found."
          line_no
          (Printer.value_to_string key) )
  | S_attr attr ->
      ( Value.lens_record_at ~attr
      , Printf.sprintf "Line %s: Impossible: attribute not found" line_no )

let rec lens_of_steps ~config ~line_no acc = function
  | [s] ->
      let l, err = lens_of_step ~config ~line_no s in
      (acc @. l, err)
  | s :: steps ->
      let l, err = lens_of_step ~config ~line_no s in
      lens_of_steps ~config ~line_no (acc @. l @. Lens.some ~err) steps
  | [] -> (Lens.option ~err:"lens_of_steps", "lens_of_steps")

let lens_of_path ~config ~line_no env {root; steps} =
  let l, err = lens_of_steps ~config ~line_no Lens.id steps in
  (lens_of_root env root @. l, err)

let addStep ?(sub_steps = ref []) upperSteps env c elements =
  let buildStep () =
    let in_contract = get_in_contract "not in contract" env.in_contract in
    let storage =
      Option.get ~msg:"no storage" !(in_contract.contract_state).storage
    in
    let balance = !(in_contract.contract_state).balance in
    { Execution.iters =
        Base.List.filter_map
          ~f:(function
            | n, Iter (v, _) -> Some (n, (v, None))
            | _ -> None)
          !(env.variables)
    ; locals =
        Base.List.filter_map
          ~f:(function
            | n, Heap_ref x -> Some (n, !x)
            | _ -> None)
          !(env.variables)
    ; storage
    ; balance
    ; operations = [] (*env.operations*)
    ; command = c
    ; substeps = sub_steps
    ; elements }
  in
  if env.debug then upperSteps := buildStep () :: !upperSteps

let add_variable name value vars =
  match List.assoc_opt name vars with
  | Some _ -> failwith ("Variable '" ^ name ^ "' already defined")
  | None -> (name, value) :: vars

let assert_unit ~config c =
  let module Printer = (val Printer.get config : Printer.Printer) in
  function
  | {v = Literal Unit} -> ()
  | _ ->
      raise
        (SmartExcept
           [ `Text "Command doesn't return unit"
           ; `Br
           ; `Text (Printer.tcommand_to_string c)
           ; `Br
           ; `Line c.line_no ])

let update_contract_address ~config scenario_state id =
  let module Printer = (val Printer.get config : Printer.Printer) in
  let address = Printer.address_of_contract_id ~html:false id None in
  Hashtbl.replace scenario_state.addresses id address;
  Hashtbl.replace scenario_state.rev_addresses address id;
  address

let get_contract_address ~config scenario_state id =
  match Hashtbl.find_opt scenario_state.addresses id with
  | Some addr -> addr
  | None -> update_contract_address ~config scenario_state id

let update_contract_address ~config scenario_state id =
  let (_ : string) = update_contract_address ~config scenario_state id in
  ()

let rec interpret_expr ~config upper_steps env scenario_state =
  let module Printer = (val Printer.get config : Printer.Printer) in
  let rec interp e =
    let primitives = env.primitives in
    let module P = (val primitives : Primitives) in
    match e.e with
    | EMPrim0 p -> interpret_mprim0 ~config env e scenario_state p
    | EMPrim1 (p, x) ->
        interpret_mprim1 ~config upper_steps env e scenario_state p x
    | EMPrim1_fail _ -> assert false
    | EMPrim2 (p, x1, x2) ->
        interpret_mprim2 ~config upper_steps env e scenario_state p x1 x2
    | EMPrim3 (p, x1, x2, x3) ->
        interpret_mprim3 ~config upper_steps env e scenario_state p x1 x2 x3
    | EPrim0 prim ->
      begin
        match prim with
        | ECst x -> Value.literal x
        | EBounded x -> Value.bounded x
        | ECstContract {address; entry_point; type_} ->
            Value.contract ?entry_point address type_
        | ELevel ->
            Value.nat (get_in_contract "Level" env.in_contract).context.level
        | EGlobal id ->
          ( match List.assoc_opt id env.global_variables with
          | Some e -> interp e
          | None -> Printf.ksprintf failwith "Missing global variable %s" id )
        | EVariant_arg n ->
          ( match List.assoc_opt n !(env.variables) with
          | Some (Variant_arg x) -> x
          | _ -> failwith (Printf.sprintf "Not a variant argument: %s" n) )
        | EMatchCons name ->
          ( match List.assoc_opt name !(env.variables) with
          | Some (Match_list v) -> v
          | _ ->
              failwith
                (Printf.sprintf
                   "Missing var %s in env [%s]"
                   name
                   (String.concat
                      "; "
                      (List.map (fun (x, _) -> x) !(env.variables)))) )
        | EIter name ->
          ( match List.assoc_opt name !(env.variables) with
          | Some (Iter (v, _)) -> v
          | _ ->
              failwith
                (Printf.sprintf
                   "Missing var %s in env [%s]"
                   name
                   (String.concat
                      "; "
                      (List.map (fun (x, _) -> x) !(env.variables)))) )
        | ELocal "__parameter__" ->
            (get_in_contract "__parameter__" env.in_contract).parameters
        | ELocal "__storage__" ->
            let in_contract = get_in_contract "__storage__" env.in_contract in
            Option.get ~msg:"no storage" !(in_contract.contract_state).storage
        | ELocal "__operations__" ->
            Value.list
              (List.map Value.operation !(env.operations))
              Type.operation
        | ELocal name ->
          ( match List.assoc_opt name !(env.variables) with
          | Some (Heap_ref {contents = x}) -> x
          | _ -> failwith ("Not a local: " ^ name) )
        | EMetaLocal name ->
          ( match List.assoc_opt name !(env.variables) with
          | Some (Heap_ref {contents = x}) -> x
          | Some _ -> failwith ("Strange meta local: " ^ name)
          | None -> failwith ("Not a meta local: " ^ name) )
        | EContract_address (id, entry_point) ->
            Value.address
              ?entry_point
              (get_contract_address ~config scenario_state id)
        | EContract_balance id ->
          ( match Hashtbl.find_opt scenario_state.contracts id with
          | Some t -> Value.mutez t.state.balance
          | None ->
              raise
                (SmartExcept
                   [ `Text "Missing contract balance for id: "
                   ; `Text (Printer.string_of_contract_id id)
                   ; `Br
                   ; `Line e.line_no ]) )
        | EContract_baker id ->
          ( match Hashtbl.find_opt scenario_state.contracts id with
          | Some {state = {baker = Some baker}} ->
              Value.some (Value.baker_value_of_protocol config.protocol baker)
          | Some {state = {baker = None}} ->
              Value.none (Type.baker_type_of_protocol config.protocol)
          | None ->
              raise
                (SmartExcept
                   [ `Text "Missing contract for id: "
                   ; `Text (Printer.string_of_contract_id id)
                   ; `Br
                   ; `Line e.line_no ]) )
        | EContract_data id ->
          ( match Hashtbl.find_opt scenario_state.contracts id with
          | Some t ->
            ( match t.state.storage with
            | Some storage -> storage
            | None ->
                raise
                  (SmartExcept
                     [ `Text "Missing contract storage for id: "
                     ; `Text (Printer.string_of_contract_id id)
                     ; `Br
                     ; `Line e.line_no ]) )
          | None ->
              raise
                (SmartExcept
                   [ `Text "Missing contract for id: "
                   ; `Text (Printer.string_of_contract_id id)
                   ; `Br
                   ; `Line e.line_no ]) )
        | EContract_typed (id, entry_point) ->
          ( match Hashtbl.find_opt scenario_state.contracts id with
          | Some t ->
              let t_ep =
                match entry_point with
                | None -> (get_extra t.template.tcontract.derived).tparameter
                | Some entry_point ->
                    let t_ep =
                      match
                        Type.getRepr
                          (get_extra t.template.tcontract.derived).tparameter
                      with
                      | TVariant {row} ->
                          Option.of_some
                            ~msg:""
                            (List.assoc_opt entry_point row)
                      | _ -> assert false
                    in
                    t_ep
              in
              Value.contract
                ?entry_point
                (get_contract_address ~config scenario_state id)
                t_ep
          | None ->
              raise
                (SmartExcept
                   [ `Text "Missing contract for id: "
                   ; `Text (Printer.string_of_contract_id id)
                   ; `Br
                   ; `Line e.line_no ]) )
        | EScenario_var id ->
          ( match Hashtbl.find_opt scenario_state.variables id with
          | Some x -> x
          | None ->
              raise
                (SmartExcept
                   [ `Text "Missing scenario variable for id: "
                   ; `Text (string_of_int id)
                   ; `Br
                   ; `Line e.line_no ]) )
        | EAccount_of_seed {seed} ->
            let account = P.Crypto.account_of_seed seed in
            let seed = Value.string seed in
            let pkh = Value.key_hash account.pkh in
            let address = Value.address account.pkh in
            let pk = Value.key account.pk in
            let sk = Value.secret_key account.sk in
            Value.record_comb
              [ ("seed", seed)
              ; ("address", address)
              ; ("public_key", pk)
              ; ("public_key_hash", pkh)
              ; ("secret_key", sk) ]
      end
    | EPrim1 (prim, x) ->
        let pp () = [] in
        ( match prim with
        | EToInt ->
          ( match (interp x).v with
          | Literal (Bls12_381_fr hex) ->
              Value.int
                (Big_int.big_int_of_string
                   ( try P.Bls12.convertFrToInt (Misc.Hex.hexcape hex) with
                   | e ->
                       Printf.ksprintf
                         failwith
                         "convertFrToInt: %s"
                         (Base.Exn.to_string e) ))
          | Literal (Int {i}) -> Value.int i
          | _ -> raise (SmartExcept [`Text "Cannot cast to Int"; `Expr e]) )
        | ENeg ->
          begin
            match (interp x).v with
            | Literal (Bls12_381_g1 hex) ->
                Value.bls12_381_g1
                  (Misc.Hex.unhex
                     ( try P.Bls12.negateG1 (Misc.Hex.hexcape hex) with
                     | e ->
                         Printf.ksprintf
                           failwith
                           "negateG1: %s"
                           (Base.Exn.to_string e) ))
            | Literal (Bls12_381_g2 hex) ->
                Value.bls12_381_g2
                  (Misc.Hex.unhex
                     ( try P.Bls12.negateG2 (Misc.Hex.hexcape hex) with
                     | e ->
                         Printf.ksprintf
                           failwith
                           "negateG2: %s"
                           (Base.Exn.to_string e) ))
            | Literal (Bls12_381_fr hex) ->
                Value.bls12_381_fr
                  (Misc.Hex.unhex
                     ( try P.Bls12.negateFr (Misc.Hex.hexcape hex) with
                     | e ->
                         Printf.ksprintf
                           failwith
                           "negateFr: %s"
                           (Base.Exn.to_string e) ))
            | Literal (Int {i}) -> Value.int (Big_int.minus_big_int i)
            | _ ->
                raise
                  (SmartExcept
                     [ `Text "Type-error: Cannot negate expression"
                     ; `Expr x
                     ; `Text "of value"
                     ; `Value (interp x) ])
          end
        | ESign ->
            let x = Value.unInt ~pp (interp x) in
            let result = Big_int.compare_big_int x Big_int.zero_big_int in
            Value.int (Bigint.of_int result)
        | ESum ->
          ( match (interp x).v with
          | List (_, l) | Set (_, l) ->
              List.fold_left
                (fun x y -> Value.plus_inner ~primitives x y)
                (Value.zero_of_type e.et)
                l
          | Map (_, _, _, l) ->
              List.fold_left
                (fun x (_, y) -> Value.plus_inner ~primitives x y)
                (Value.zero_of_type e.et)
                l
          | _ -> failwith ("bad sum " ^ Printer.texpr_to_string e) )
        | EListRev ->
            let l = interp x in
            ( match (l.v, Type.getRepr x.et) with
            | Basics.List (_, l), T1 (T_list, t) -> Value.list (List.rev l) t
            | _ -> assert false )
        | EListItems rev ->
            let l = interp x in
            ( match (l.v, Type.getRepr x.et) with
            | Basics.Map (_, _, _, l), TMap {tkey; tvalue} ->
                Value.list
                  ((if rev then List.rev_map else List.map)
                     (fun (k, v) ->
                       Value.record_comb [("key", k); ("value", v)])
                     l)
                  (Type.key_value tkey tvalue)
            | _ -> assert false )
        | EListKeys rev ->
            let l = interp x in
            ( match (l.v, Type.getRepr x.et) with
            | Basics.Map (_, _, _, l), TMap {tkey} ->
                Value.list ((if rev then List.rev_map else List.map) fst l) tkey
            | _ -> assert false )
        | EListValues rev ->
            let l = interp x in
            ( match (l.v, Type.getRepr x.et) with
            | Basics.Map (_, _, _, l), TMap {tvalue} ->
                Value.list
                  ((if rev then List.rev_map else List.map) snd l)
                  tvalue
            | _ -> assert false )
        | EListElements rev ->
            let l = interp x in
            ( match (l.v, Type.getRepr x.et) with
            | Basics.Set (_, l), T1 (T_set, telement) ->
                Value.list (if rev then List.rev l else l) telement
            | _ -> assert false )
        | EPack ->
            let r = interp x in
            begin
              try
                r
                |> Compiler.pack_value ~config:scenario_state.config
                |> Value.bytes
              with
              | Data_encoding.Binary.Write_error write_error ->
                  raise
                    (ExecFailure
                       ( Value.string "Pack error"
                       , [ `Text "Could not pack bytes due to invalid content"
                         ; `Br
                         ; `Expr e
                         ; `Br
                         ; `Value r
                         ; `Text
                             (Format.asprintf
                                "%a"
                                Data_encoding.Binary.pp_write_error
                                write_error)
                         ; `Line e.line_no ] ))
            end
        | EUnpack t ->
            let r = interp x in
            ( match r.v with
            | Literal (Bytes b) ->
              begin
                try Compiler.unpack_value ~config t b |> Value.some with
                | Data_encoding.Binary.Read_error read_error ->
                    raise
                      (ExecFailure
                         ( Value.string "Unpack error"
                         , [ `Text
                               (Format.asprintf
                                  "Could not unpack bytes (%s) due to invalid \
                                   content: %a."
                                  (Misc.Hex.hexcape b)
                                  Data_encoding.Binary.pp_read_error
                                  read_error)
                           ; `Br
                           ; `Expr e
                           ; `Br
                           ; `Value r
                           ; `Line e.line_no ] ))
              end
            | _ ->
                failwith
                  ( "Interpreter. sp.unpack expects bytes as parameter: "
                  ^ Printer.texpr_to_string e ) )
        | EConcat_list ->
            let vals =
              match (interp x).v with
              | List (_, sl) ->
                  Base.List.map sl ~f:(fun sb ->
                      match sb.v with
                      | Literal (String s) | Literal (Bytes s) -> s
                      | _ ->
                          Format.kasprintf
                            failwith
                            "Error while concatenating string/bytes:@ not a \
                             list of strings/bytes:@ %s"
                            (Printer.texpr_to_string e))
              | _ ->
                  Format.kasprintf
                    failwith
                    "Error while concatenating string/bytes:@ not a list:@ %s"
                    (Printer.texpr_to_string e)
            in
            ( match Type.getRepr e.et with
            | T0 T_string -> Value.string (String.concat "" vals)
            | T0 T_bytes -> Value.bytes (String.concat "" vals)
            | _ ->
                raise
                  (SmartExcept
                     [ `Text "Bad type in concat (should apply to lists of "
                     ; `Type Type.string
                     ; `Text "or"
                     ; `Type Type.bytes
                     ; `Text ")"
                     ; `Expr e
                     ; `Br
                     ; `Line e.line_no ]) )
        | ESize ->
            let result length = Value.nat (Bigint.of_int length) in
            ( match (interp x).v with
            | Literal (String s) | Literal (Bytes s) -> result (String.length s)
            | List (_, l) | Set (_, l) -> result (List.length l)
            | Map (_, _, _, l) -> result (List.length l)
            | _ ->
                raise
                  (SmartExcept
                     [`Text "Bad type for sp.len"; `Expr x; `Line x.line_no]) )
        | EToConstant -> interp x
        | EProject i ->
            let vs = Value.untuple ~pp (interp x) in
            if i < List.length vs
            then List.nth vs i
            else failwith "interpreter: short tuple"
        | EAddress ->
          ( match (interp x).v with
          | Contract {address; entry_point} ->
              Value.literal (Literal.address ?entry_point address)
          | _ ->
              failwith
                ( "Interpreter. sp.to_address is not available in this context: "
                ^ Printer.texpr_to_string x ) )
        | EImplicit_account ->
          ( match (interp x).v with
          | Literal (Key_hash key_hash) -> Value.contract key_hash Type.unit
          | _ ->
              failwith
                ( "Interpreter. sp.implicit_account is not available in this \
                   context: "
                ^ Printer.texpr_to_string e ) )
        | ESetDelegate ->
            let baker = Value.un_baker (interp x) in
            (* The delegate must be registered *)
            ( match baker with
            | None -> ()
            | Some key_hash ->
                let registered =
                  List.exists
                    (fun (k, _) -> k = key_hash)
                    (get_in_contract "Voting_power" env.in_contract).context
                      .voting_powers
                in
                if not registered
                then
                  failwith
                    (Printf.sprintf "Unregistered delegate (%s)" key_hash) );
            Value.operation (SetDelegate baker)
        | EType_annotation _ -> interp x
        | EAttr name ->
          ( match (interp x).v with
          | Record (_, l) ->
            ( match List.assoc_opt name l with
            | Some v -> v
            | None ->
                failwith
                  (Printf.sprintf
                     "Missing field in record [%s] [%s] [%s]."
                     name
                     (Printer.texpr_to_string e)
                     (String.concat ", " (List.map fst l))) )
          | _ -> failwith ("Not a record " ^ Printer.texpr_to_string e) )
        | EIsVariant name ->
            let x = interp x in
            ( match x.v with
            | Variant (_layout, _row, cons, _) -> Value.bool (cons = name)
            | _ ->
                failwith
                  (Printf.sprintf
                     "Not a variant %s: %s"
                     (Printer.texpr_to_string e)
                     (Printer.value_to_string x)) )
        | EVariant name ->
            Value.variant_with_type
              ~no_check:()
              ~line_no:x.line_no
              name
              (interp x)
              e.et
        | EReadTicket ->
            let t = interp x in
            ( match (interp x).v with
            | Ticket (address, content, amount) ->
                let address = Value.literal (Literal.address address) in
                let amount = Value.nat amount in
                Value.tuple [Value.tuple [address; content; amount]; t]
            | _ -> assert false )
        | EJoinTickets ->
          ( match (interp x).v with
          | Tuple
              [ {v = Ticket (ticketer1, content1, amount1)}
              ; {v = Ticket (ticketer2, content2, amount2)} ] ->
              if ticketer1 = ticketer2 && Value.equal content1 content2
              then
                Value.some
                  (Value.ticket
                     ticketer1
                     content1
                     (Bigint.add_big_int amount1 amount2))
              else assert false
          | _ -> assert false )
        | EPairingCheck ->
            let vals =
              match (interp x).v with
              | List (_, sl) ->
                  Base.List.map sl ~f:(fun sb ->
                      match sb.v with
                      | Tuple [i1; i2] ->
                          ( Misc.Hex.hexcape (Value.unBls12_381 i1)
                          , Misc.Hex.hexcape (Value.unBls12_381 i2) )
                      | _ ->
                          Format.kasprintf
                            failwith
                            "Expected a list of pairs (g1, g2) @ %s"
                            (Printer.texpr_to_string e))
              | _ ->
                  Format.kasprintf
                    failwith
                    "Expected a list @ %s"
                    (Printer.texpr_to_string e)
            in
            Value.bool
              ( try P.Bls12.pairingCheck vals with
              | e ->
                  Printf.ksprintf
                    failwith
                    "pairingCheck: %s"
                    (Base.Exn.to_string e) )
        | EVotingPower ->
            let key_hash =
              match (interp x).v with
              | Literal (Key_hash key_hash) -> key_hash
              | _ ->
                  failwith
                    ( "Interpreter. sp.voting_power is not available in this \
                       context: "
                    ^ Printer.texpr_to_string e )
            in
            let res =
              List.find_opt
                (fun (k, _) ->
                  Value.equal (Value.key_hash k) (Value.key_hash key_hash))
                (get_in_contract "Voting_power" env.in_contract).context
                  .voting_powers
            in
            Value.nat
              ( match res with
              | Some (_, v) -> v
              | None -> Big_int.zero_big_int )
        | EUnbounded ->
          ( match (interp x).v with
          | Bounded l -> Value.literal l
          | _ -> assert false ) )
    | EOpenVariant (name, (x as x0), missing_message) ->
        let x = interp x in
        ( match x.v with
        | Variant (_row, _layout, cons, v) ->
            if cons <> name
            then
              let default_message =
                Printf.sprintf
                  "Not the proper variant constructor [%s] != [%s]"
                  name
                  cons
              in
              let missing_message =
                Base.Option.map
                  ~f:(interpret_expr ~config upper_steps env scenario_state)
                  missing_message
              in
              let missing_message, message =
                match missing_message with
                | Some except -> (except, [`Br; `Text "Message:"; `Value except])
                | None -> (Value.string default_message, [])
              in
              raise
                (ExecFailure
                   ( missing_message
                   , [ `Text default_message
                     ; `Br
                     ; `Expr e
                     ; `Rec message
                     ; `Line e.line_no ] ))
            else v
        | _ ->
            failwith
              (Printf.sprintf
                 "Not a variant %s (from %s)"
                 (Printer.value_to_string x)
                 (Printer.texpr_to_string x0)) )
    | EItem {items; key; default_value; missing_message} ->
        let def =
          match default_value with
          | None -> None
          | Some def -> Some (lazy (interp def))
        in
        let missing_message =
          match missing_message with
          | None -> None
          | Some x -> Some (lazy (interp x))
        in
        Value.getItem
          (interp items)
          (interp key)
          def
          missing_message
          ~pp:(fun () ->
            Printf.sprintf
              "%s[%s]"
              (Printer.texpr_to_string items)
              (Printer.texpr_to_string key))
    | EPrim2 (prim2, x, y) ->
      begin
        match prim2 with
        | EGetOpt ->
            let m, k = (x, y) in
            let tm = Type.unF m.et in
            let m = interp m in
            let k = interp k in
            ( match (tm, m.v) with
            | Type.TMap {tvalue}, Map (_, _, _, m) ->
              ( match List.assoc_opt ~equal:Value.equal k m with
              | Some v -> Value.some v
              | None -> Value.none tvalue )
            | _ -> assert false )
        | EBinOpInf BEq ->
            let x = interp x in
            let y = interp y in
            Value.bool (Value.equal x y)
        | EBinOpInf BNeq ->
            let x = interp x in
            let y = interp y in
            Value.bool (not (Value.equal x y))
        | EBinOpInf BAdd -> Value.plus ~primitives (interp x) (interp y)
        | EBinOpInf (BMul _) -> Value.mul ~primitives (interp x) (interp y)
        | EBinOpInf BMod -> Value.e_mod (interp x) (interp y)
        | EBinOpInf BDiv -> Value.div (interp x) (interp y)
        | EBinOpInf BSub -> Value.minus (interp x) (interp y)
        | EBinOpInf BLt ->
            let x = interp x in
            let y = interp y in
            Value.bool (Value.lt x y)
        | EBinOpInf BLe ->
            let x = interp x in
            let y = interp y in
            Value.bool (Value.le x y)
        | EBinOpInf BGt ->
            let x = interp x in
            let y = interp y in
            Value.bool (Value.lt y x)
        | EBinOpInf BGe ->
            let x = interp x in
            let y = interp y in
            Value.bool (Value.le y x)
        | EBinOpInf BAnd ->
            let pp () = [] in
            Value.bool
              (Value.unBool ~pp (interp x) && Value.unBool ~pp (interp y))
        | EBinOpInf BOr ->
            let pp () = [] in
            Value.bool
              (Value.unBool ~pp (interp x) || Value.unBool ~pp (interp y))
        | EBinOpInf BXor -> Value.xor (interp x) (interp y)
        | EBinOpInf BEDiv -> Value.ediv (interp x) (interp y)
        | ECons -> Value.cons (interp x) (interp y)
        | EBinOpPre BMax ->
            let pp () = [] in
            let isNat =
              match Type.getRepr x.et with
              | TInt {isNat} -> isNat
              | _ -> assert false
            in
            Value.intOrNat
              isNat
              (Big_int.max_big_int
                 (Value.unInt ~pp (interp x))
                 (Value.unInt ~pp (interp y)))
        | EBinOpPre BMin ->
            let pp () = [] in
            let isNat =
              match Type.getRepr x.et with
              | TInt {isNat} -> isNat
              | _ -> assert false
            in
            Value.intOrNat
              isNat
              (Big_int.min_big_int
                 (Value.unInt ~pp (interp x))
                 (Value.unInt ~pp (interp y)))
        | EAdd_seconds ->
            let t, s = (x, y) in
            ( match ((interp t).v, (interp s).v) with
            | Literal (Timestamp t), Literal (Int {i = s}) ->
                Value.timestamp (Big_int.add_big_int t s)
            | _ -> failwith ("Cannot add timestamp " ^ Printer.texpr_to_string e)
            )
        | EContains ->
            let items, member = (x, y) in
            let member = interp member in
            ( match (interp items).v with
            | Map (_, _, _, l) ->
                Value.bool (List.exists (fun (x, _) -> Value.equal x member) l)
            | Set (_, l) ->
                Value.bool (List.exists (fun x -> Value.equal x member) l)
            | _ -> failwith ("Cannot compute " ^ Printer.texpr_to_string e) )
        | EApplyLambda ->
            let f, x = (x, y) in
            Value.closure_apply (interp f) (interp x)
        | ECallLambda ->
            let lambda, parameter = (x, y) in
            call_lambda
              ~config
              upper_steps
              env
              scenario_state
              (interp lambda)
              (interp parameter)
        | ETicket ->
            let content = interp x in
            let ticketer =
              match
                (get_in_contract "ETicket" env.in_contract).context.contract_id
              with
              | Some i -> get_contract_address ~config scenario_state i
              | None -> assert false
            in
            ( match (interp y).v with
            | Literal (Int {i}) -> Value.ticket ticketer content i
            | _ -> assert false )
        | ESplitTicket ->
            let ticket = interp x in
            ( match ((interp x).v, (interp y).v) with
            | ( Ticket (ticketer, content, amount)
              , Tuple
                  [{v = Literal (Int {i = a1})}; {v = Literal (Int {i = a2})}] )
              ->
                if Bigint.equal (Bigint.add_big_int a1 a2) amount
                then
                  let t1 = Value.ticket ticketer content a1 in
                  let t2 = Value.ticket ticketer content a2 in
                  Value.some (Value.tuple [t1; t2])
                else
                  Value.none
                    (Type.pair (type_of_value ticket) (type_of_value ticket))
            | _ -> assert false )
      end
    | ERecord [] -> Value.unit
    | ERecord l ->
        let layout =
          match Type.getRepr e.et with
          | TRecord {layout} -> layout
          | _ ->
              raise
                (SmartExcept
                   [`Text "Bad type while evaluating"; `Expr e; `Line e.line_no])
        in
        let layout =
          Option.get ~msg:"interpreter: layout" (Unknown.get layout)
        in
        Value.record ~layout (List.map (fun (s, e) -> (s, interp e)) l)
    | EPrim3 (prim3, x, y, z) ->
      begin
        match prim3 with
        | ESplit_tokens ->
            let mutez, quantity, total = (x, y, z) in
            let pp () = [] in
            Value.mutez
              (Big_int.div_big_int
                 (Big_int.mult_big_int
                    (Value.unMutez ~pp (interp mutez))
                    (Value.unInt ~pp (interp quantity)))
                 (Value.unInt ~pp (interp total)))
        | ERange ->
            let a, b, step = (x, y, z) in
            let item = a.et in
            ( match ((interp a).v, (interp b).v, (interp step).v) with
            | ( Literal (Int {i = a})
              , Literal (Int {i = b})
              , Literal (Int {i = step}) ) ->
                let a = Big_int.int_of_big_int a in
                let b = Big_int.int_of_big_int b in
                let step = Big_int.int_of_big_int step in
                if step = 0
                then failwith (Printf.sprintf "Range with 0 step")
                else if step * (b - a) < 0
                then Value.list [] item
                else
                  let isNat =
                    match Type.getRepr item with
                    | TInt {isNat} -> isNat
                    | _ -> assert false
                  in
                  let rec aux a acc =
                    if (b - a) * step <= 0
                    then List.rev acc
                    else
                      aux
                        (a + step)
                        (Value.intOrNat isNat (Bigint.of_int a) :: acc)
                  in
                  Value.list (aux a []) item
            | _ -> failwith ("bad range" ^ Printer.texpr_to_string e) )
        | EIf ->
            let c, t, e = (x, y, z) in
            let condition = interp c in
            let pp () = [] in
            if Value.unBool ~pp condition then interp t else interp e
        | EUpdate_map ->
            let open Value in
            let map, key, value = (interp x, interp y, interp z) in
            ( match (Type.unF (Type.normalize x.et), map.v) with
            | TMap {big; tkey; tvalue}, Map (_, _, _, xs) ->
                let value = unOption value in
                let m' = Lens.set (Lens.assoc ~equal ~key) value xs in
                Value.map ~big ~tkey ~tvalue m'
            | _ -> assert false )
        | EGet_and_update ->
            let open Value in
            let map, key, value = (interp x, interp y, interp z) in
            ( match (Type.unF (Type.normalize x.et), map.v) with
            | TMap {big; tkey; tvalue}, Map (_, _, _, xs) ->
                let value' = unOption value in
                let old, m' =
                  Lens.get_and_set (Lens.assoc ~equal ~key) value' xs
                in
                let map = Value.map ~big ~tkey ~tvalue m' in
                Value.tuple [Value.option (type_of_value value) old; map]
            | _ -> assert false )
        | ETest_ticket ->
            let ticketer, content, amount = (interp x, interp y, interp z) in
            ( match amount.v with
            | Literal (Int {i}) ->
                Value.ticket
                  (Value.unAddress
                     ~pp:(fun () ->
                       [ `Text "Bad ticketer in test_ticket"
                       ; `Expr x
                       ; `Text "in"
                       ; `Expr e
                       ; `Line e.line_no ])
                     ticketer)
                  content
                  i
            | _ -> assert false )
      end
    | EMapFunction {l; f} ->
        let f = interp f in
        ( match ((interp l).v, Type.getRepr (type_of_value f)) with
        | List (_, l), T2 (T_lambda, _, t) ->
            l
            |> List.map (call_lambda ~config upper_steps env scenario_state f)
            |> fun l -> Value.list l t
        | ( Map (_, _, _, l)
          , T2
              ( T_lambda
              , F (TRecord {layout; row = [("key", tkey); ("value", _)]})
              , t ) ) ->
            let layout =
              Option.get ~msg:"interpreter: layout" (Unknown.get layout)
            in
            l
            |> List.map (fun (k, v) ->
                   ( k
                   , call_lambda
                       ~config
                       upper_steps
                       env
                       scenario_state
                       f
                       (Value.record ~layout [("key", k); ("value", v)]) ))
            |> fun l -> Value.map ~big:(Unknown.value false) ~tkey ~tvalue:t l
        | _ ->
            Printf.ksprintf
              failwith
              "Not a list or a map %s"
              (Printer.type_to_string (type_of_value f)) )
    | ESlice {offset; length; buffer} ->
        Basics.(
          ( match ((interp offset).v, (interp length).v, (interp buffer).v) with
          | ( Literal (Int {i = ofs_bi})
            , Literal (Int {i = len_bi})
            , Literal (String s) ) ->
            ( try
                Value.some
                  (Value.string
                     (String.sub
                        s
                        (Big_int.int_of_big_int ofs_bi)
                        (Big_int.int_of_big_int len_bi)))
              with
            | _ -> Value.none Type.string )
          | ( Literal (Int {i = ofs_bi})
            , Literal (Int {i = len_bi})
            , Literal (Bytes s) ) ->
            ( try
                Value.some
                  (Value.bytes
                     (String.sub
                        s
                        (Big_int.int_of_big_int ofs_bi)
                        (Big_int.int_of_big_int len_bi)))
              with
            | _ -> Value.none Type.bytes )
          | _ ->
              Format.kasprintf
                failwith
                "Error while slicing string/bytes: %s"
                (Printer.texpr_to_string e) ))
    | EMap (_, entries) ->
      ( match Type.getRepr e.et with
      | TMap {big; tkey; tvalue} ->
          Value.map
            ~big
            ~tkey
            ~tvalue
            (List.fold_left
               (fun entries (key, value) ->
                 let key = interp key in
                 (key, interp value)
                 :: Base.List.Assoc.remove ~equal:Value.equal entries key)
               []
               entries)
      | _ ->
          Format.kasprintf
            failwith
            "%s is not a map: %s"
            (Printer.type_to_string e.et)
            (Printer.texpr_to_string e) )
    | EList items ->
      ( match Type.unF (Type.normalize e.et) with
      | T1 (T_list, t) -> Value.list (List.map interp items) t
      | _ -> assert false )
    | ESet l ->
      begin
        match Type.getRepr e.et with
        | T1 (T_set, telement) -> Value.set ~telement (List.map interp l)
        | _ -> assert false
      end
    | EContract {arg_type; address; entry_point} ->
      ( match (entry_point, interp address) with
      | entry_point, {v = Literal (Literal.Address (addr, None))}
       |None, {v = Literal (Literal.Address (addr, entry_point))} ->
          begin
            match Hashtbl.find_opt scenario_state.rev_addresses addr with
            | None -> ()
            | Some id ->
              begin
                match
                  (entry_point, Hashtbl.find_opt scenario_state.contracts id)
                with
                | _, None -> ()
                | Some entry_point, Some contract ->
                  ( match
                      Type.getRepr
                        (get_extra contract.template.tcontract.derived)
                          .tparameter
                    with
                  | TVariant {row = [_]} ->
                      raise
                        (SmartExcept
                           [ `Text "Entry point annotation ("
                           ; `Text entry_point
                           ; `Text ") for contract ("
                           ; `Text (Printer.string_of_contract_id id)
                           ; `Text ") with only one entry point."
                           ; `Br
                           ; `Line e.line_no ])
                  | _ -> () )
                | None, Some contract ->
                  ( match
                      Type.getRepr
                        (get_extra contract.template.tcontract.derived)
                          .tparameter
                    with
                  | TVariant {row = [(_, t)]} ->
                      if not (Type.equal arg_type t)
                      then
                        raise
                          (SmartExcept
                             [ `Text "Wrong type for contract("
                             ; `Text (Printer.string_of_contract_id id)
                             ; `Text ")."
                             ; `Br
                             ; `Expr e
                             ; `Br
                             ; `Type t
                             ; `Br
                             ; `Text "instead of"
                             ; `Br
                             ; `Type arg_type
                             ; `Br
                             ; `Text "Please set a stable type."
                             ; `Br
                             ; `Line e.line_no ])
                  | _ -> () )
              end
          end;
          Value.some (Value.contract ?entry_point addr arg_type)
      | _, res ->
          raise (SmartExcept [`Text "wrong result for EContract:"; `Value res])
      )
    | ETuple es -> Value.tuple (List.map interp es)
    | ELambda l -> Value.closure_init l
    | ELambdaParams {id; name = _} -> List.assoc id env.lambda_params
    | EMake_signature {secret_key; message; message_format} ->
        let secret_key =
          (interp secret_key).v
          |> function
          | Literal (Secret_key k) -> k
          | _ ->
              raise
                (SmartExcept
                   [ `Text "Type error in secret key"
                   ; `Expr e
                   ; `Expr secret_key
                   ; `Line e.line_no ])
        in
        let message =
          (interp message).v
          |> function
          | Literal (Bytes k) ->
            ( match message_format with
            | `Raw -> k
            | `Hex ->
                Misc.Hex.unhex
                  Base.(
                    String.chop_prefix k ~prefix:"0x" |> Option.value ~default:k)
            )
          | _ ->
              Format.kasprintf
                failwith
                "Type-error: make_signature expects bytes, not: %s"
                (Printer.texpr_to_string e)
        in
        P.Crypto.sign ~secret_key message |> Value.signature
    | EMichelson _ ->
        failwith
          ( "Interpreter. Instruction not supported in interpreter: "
          ^ Printer.texpr_to_string e )
    | ETransfer {destination; arg = params; amount} ->
        let params = interp params in
        let destination =
          match interp destination with
          | {v = Contract d} -> d
          | _ -> assert false
        in
        let amount =
          match interp amount with
          | {v = Literal (Mutez x)} -> x
          | _ -> assert false
        in
        Value.operation (Transfer {params; destination; amount})
    | ECreate_contract {contract_template; baker; balance; storage} ->
        let baker = Value.un_baker (interp baker) in
        let balance =
          match interp balance with
          | {v = Literal (Mutez x)} -> x
          | _ -> assert false
        in
        let storage = interp storage in
        let {template; state} =
          interpret_contract
            ~config
            ~primitives
            ~scenario_state
            {tcontract = {contract_template with storage = None}}
        in
        let state = {state with baker; balance; storage = Some storage} in
        let dynamic_id = !(scenario_state.next_dynamic_address_id) in
        incr scenario_state.next_dynamic_address_id;
        let id = C_dynamic {dynamic_id} in
        let address = get_contract_address ~config scenario_state id in
        Value.record_comb
          [ ( "operation"
            , Value.operation
                (CreateContract {id; instance = {template; state}}) )
          ; ("address", Value.literal (Literal.address address)) ]
    | EMatch _ -> failwith "TODO: ematch"
    | ESaplingVerifyUpdate {state; transaction} ->
        let pp () = [] in
        let state = interp state in
        let transaction = interp transaction in
        let source, target, amount =
          Value.unSaplingTransaction ~pp transaction
        in
        let output =
          match (source, target) with
          | None, None -> assert false
          | None, Some _ -> Bigint.minus_big_int amount
          | Some _, None -> amount
          | Some _, Some _ -> Bigint.zero_big_int
        in
        let memo, state = Value.unSaplingState ~pp state in
        let state =
          match source with
          | None -> Some state
          | Some source ->
            ( match List.assoc_opt source state with
            | Some x when Bigint.ge_big_int x amount ->
                Some
                  ( (source, Bigint.sub_big_int x amount)
                  :: List.remove_assoc source state )
            | _ -> None )
        in
        let state =
          match (state, target) with
          | None, _ -> None
          | Some state, None -> Some state
          | Some state, Some target ->
            ( match List.assoc_opt target state with
            | Some x ->
                Some
                  ( (target, Bigint.add_big_int x amount)
                  :: List.remove_assoc target state )
            | _ -> Some ((target, amount) :: state) )
        in
        ( match state with
        | None ->
            Value.none
              (Type.pair (Type.int ()) (Type.sapling_state (Some memo)))
        | Some state ->
            Value.some
              (Value.tuple
                 [ Value.int output
                 ; Value.literal (Literal.sapling_test_state memo state) ]) )
    | EHasEntryPoint ep_name ->
        let in_contract =
          get_in_contract "sp.has_entry_point" env.in_contract
        in
        let leps = !(in_contract.contract_state).lazy_entry_points in
        let has =
          match List.assoc_opt ep_name leps with
          | None -> assert false
          | Some `Absent -> false
          | Some (`Closure _ | `Initial) -> true
        in
        Value.bool has
  in
  interp

and interpret_mprim0 ~config env e0 scenario_state :
    _ Michelson_base.Primitive.prim0 -> _ = function
  | Sender ->
    ( match (get_in_contract "Sender" env.in_contract).context.sender with
    | Some address -> Value.literal (Literal.address address)
    | None -> raise (SmartExcept [`Text "Sender is undefined"; `Expr e0]) )
  | Source ->
    ( match (get_in_contract "Source" env.in_contract).context.source with
    | Some address -> Value.literal (Literal.address address)
    | None -> raise (SmartExcept [`Text "Source is undefined"; `Expr e0]) )
  | Amount ->
      Value.mutez (get_in_contract "Amount" env.in_contract).context.amount
  | Balance ->
      Value.mutez
        !((get_in_contract "balance" env.in_contract).contract_state).balance
  | Now -> Value.timestamp (get_in_contract "Now" env.in_contract).context.time
  | Self None ->
      let env = get_in_contract "Self" env.in_contract in
      ( match env.context.contract_id with
      | Some i ->
          Value.contract
            (get_contract_address ~config scenario_state i)
            env.tparameter
      | None ->
          raise
            (SmartExcept
               [ `Text "Interpreter. sp.self is not available in this context: "
               ; `Expr e0 ]) )
  | Self (Some entry_point) ->
      let in_contract = get_in_contract "self_Entry_point" env.in_contract in
      let i =
        match in_contract.context.contract_id with
        | Some i -> i
        | None ->
            raise
              (SmartExcept
                 [ `Text
                     "Interpreter. sp.self is not available in this context: "
                 ; `Expr e0 ])
      in
      let msg = "interpreter: self_entry_point" in
      let {template} =
        Hashtbl.find_opt scenario_state.contracts i |> Option.of_some ~msg
      in
      let tparameter = (get_extra template.tcontract.derived).tparameter in
      let row =
        match tparameter with
        | F (TVariant {row}) -> row
        | _ -> failwith "self_entry_point: tparameter not a variant"
      in
      let t = Option.of_some ~msg (List.assoc_opt entry_point row) in
      Value.contract
        ~entry_point
        (get_contract_address ~config scenario_state i)
        t
  | Self_address ->
      let env = get_in_contract "Self_address" env.in_contract in
      ( match env.context.contract_id with
      | Some i ->
          Value.literal
            (Literal.address (get_contract_address ~config scenario_state i))
      | None ->
          raise
            (SmartExcept
               [ `Text "Interpreter. sp.self is not available in this context: "
               ; `Expr e0 ]) )
  | Chain_id ->
      Value.chain_id
        (get_in_contract "Chainid" env.in_contract).context.chain_id
  | Total_voting_power ->
      Value.nat
        (List.fold_left
           (fun acc (_, v) -> Big_int.add_big_int acc v)
           Big_int.zero_big_int
           (get_in_contract "total_voting_power" env.in_contract).context
             .voting_powers)
  | Sapling_empty_state {memo} ->
      Value.literal (Literal.sapling_test_state memo [])
  | Unit_ -> Value.literal Literal.unit
  | None_ t -> Value.none (Type.of_mtype t)
  | Nil t -> Value.list [] (Type.of_mtype t)
  | Empty_set telement ->
      let telement = Type.of_mtype telement in
      Value.set ~telement []
  | Empty_map (tkey, tvalue) ->
      let tkey = Type.of_mtype tkey in
      let tvalue = Type.of_mtype tvalue in
      Value.map ~big:(Unknown.value false) ~tkey ~tvalue []
  | Empty_bigmap (tkey, tvalue) ->
      let tkey = Type.of_mtype tkey in
      let tvalue = Type.of_mtype tvalue in
      Value.map ~big:(Unknown.value true) ~tkey ~tvalue []

and interpret_mprim1 ~config upper_steps env e0 scenario_state p x =
  let module Printer = (val Printer.get config : Printer.Printer) in
  let primitives = env.primitives in
  let module P = (val primitives : Primitives) in
  let interp = interpret_expr ~config upper_steps env scenario_state in
  let pp () = [] in
  let hash_algo h =
    match (interp x).v with
    | Literal (Bytes b) -> Value.bytes (h b)
    | _ -> assert false
  in
  match (p : _ Michelson_base.Primitive.prim1) with
  | Abs -> Value.nat (Big_int.abs_big_int (Value.unInt ~pp (interp x)))
  | IsNat ->
      let x = Value.unInt ~pp (interp x) in
      if 0 <= Big_int.compare_big_int x Big_int.zero_big_int
      then Value.some (Value.nat x)
      else Value.none (Type.nat ())
  | Not -> Value.bool (not (Value.unBool ~pp (interp x)))
  | Hash_key ->
      let public_key =
        (interp x).v
        |> function
        | Literal (Key k) -> k
        | _ ->
            Format.kasprintf
              failwith
              "Type-error: hash_key expects a key, not: %s"
              (Printer.texpr_to_string e0)
      in
      Value.key_hash (P.Crypto.hash_key public_key)
  | Blake2b -> hash_algo P.Crypto.blake2b
  | Sha256 -> hash_algo P.Crypto.sha256
  | Sha512 -> hash_algo P.Crypto.sha512
  | Keccak -> hash_algo P.Crypto.keccak
  | Sha3 -> hash_algo P.Crypto.sha3
  | _ -> assert false

and interpret_mprim2 ~config upper_steps env _e0 scenario_state p x1 x2 =
  let interp = interpret_expr ~config upper_steps env scenario_state in
  match (p : Michelson_base.Primitive.prim2) with
  | Lsl -> Value.shift_left (interp x1) (interp x2)
  | Lsr -> Value.shift_right (interp x1) (interp x2)
  | _ -> assert false

and interpret_mprim3 ~config upper_steps env e0 scenario_state p x1 x2 x3 =
  let module Printer = (val Printer.get config : Printer.Printer) in
  let primitives = env.primitives in
  let module P = (val primitives : Primitives) in
  let interp = interpret_expr ~config upper_steps env scenario_state in
  match (p : Michelson_base.Primitive.prim3) with
  | Check_signature ->
      let pk_expr, sig_expr, msg_expr = (x1, x2, x3) in
      Dbg.on := false;
      let public_key =
        (interp pk_expr).v
        |> function
        | Literal (Key k) -> k
        | _ ->
            Format.kasprintf
              failwith
              "Type-error: check_signature expects a key, not: %s"
              (Printer.texpr_to_string e0)
      in
      let signature =
        (interp sig_expr).v
        |> function
        | Literal (Signature s) -> s
        | _ ->
            Format.kasprintf
              failwith
              "Type-error: check_signature expects a signature, not: %s"
              (Printer.texpr_to_string e0)
      in
      let msg =
        (interp msg_expr).v
        |> function
        | Literal (Bytes b) -> b
        | _ ->
            Format.kasprintf
              failwith
              "Type-error: check_signature expects bytes (3rd argument), not: \
               %s"
              (Printer.texpr_to_string e0)
      in
      Value.bool (P.Crypto.check_signature ~public_key ~signature msg)
  | _ -> assert false

and path_of_expr ~config upper_steps env scenario_state =
  let rec of_expr acc e =
    match e.e with
    | EPrim0 (ELocal "__storage__") -> Some {root = R_storage; steps = acc}
    | EPrim0 (EMetaLocal name | ELocal name) ->
        Some {root = R_local name; steps = acc}
    | EItem {items = cont; key; default_value = None; missing_message = None} ->
        let key = interpret_expr ~config upper_steps env scenario_state key in
        ( match Type.getRepr cont.et with
        | TMap _ -> of_expr (S_item_map key :: acc) cont
        | _ ->
            raise
              (SmartExcept
                 [ `Text "Interpreter Error"
                 ; `Br
                 ; `Text "GetItem"
                 ; `Expr e
                 ; `Expr cont
                 ; `Text "is not a map"
                 ; `Line e.line_no ]) )
    | EPrim1 (EAttr name, expr) -> of_expr (S_attr name :: acc) expr
    | EPrim0 (EIter name) ->
      ( match List.assoc name !(env.variables) with
      | Iter (_, Some p) -> Some (extend_path p acc)
      | Iter (_, None) -> None (* Iteratee not an l-expression. *)
      | _ -> failwith "not an iter" )
    | _ -> None
  in
  of_expr []

and interpret_command
    ~config
    upper_steps
    env
    scenario_state
    ({line_no} as initialCommand : tcommand) : value =
  let module Printer = (val Printer.get config : Printer.Printer) in
  let interpret_command = interpret_command ~config in
  let interpret_expr = interpret_expr ~config in
  let path_of_expr = path_of_expr ~config in
  let pp () = [] in
  match initialCommand.c with
  | CNever message ->
      ignore (interpret_expr upper_steps env scenario_state message : Value.t);
      failwith "Evaluation of sp.never. It should not happen."
  | CFailwith message ->
      let value = interpret_expr upper_steps env scenario_state message in
      raise
        (ExecFailure
           ( value
           , [`Text "Failure:"; `Text (Printer.value_to_string value)]
             @ ( match message.e with
               | EPrim0 (ECst _) -> []
               | _ -> [`Br; `Text "("; `Expr message; `Text ")"] )
             @ [`Br; `Line line_no] ))
  | CIf (c, t, e) ->
      let sub_steps = ref [] in
      let condition = interpret_expr upper_steps env scenario_state c in
      let r =
        if Value.unBool ~pp condition
        then interpret_command sub_steps env scenario_state t
        else interpret_command sub_steps env scenario_state e
      in
      addStep
        ~sub_steps
        upper_steps
        env
        initialCommand
        [("condition", condition)];
      r
  | CMatchCons {expr; id; ok_match; ko_match} ->
    ( match
        Value.unList ~pp (interpret_expr upper_steps env scenario_state expr)
      with
    | [] ->
        let sub_steps = ref [] in
        interpret_command sub_steps env scenario_state ko_match
    | head :: tail ->
        let sub_steps = ref [] in
        let env =
          { env with
            variables =
              ref
                (add_variable
                   id
                   (Match_list
                      (Value.record_comb
                         [ ("head", head)
                         ; ("tail", Value.list tail (type_of_value head)) ]))
                   !(env.variables)) }
        in
        interpret_command sub_steps env scenario_state ok_match )
  | CMatchProduct (s, p, c) ->
      let sub_steps = ref [] in
      let variables = !(env.variables) in
      let vs =
        match p with
        | Pattern_single x ->
            let v = interpret_expr upper_steps env scenario_state s in
            [(x, Variant_arg v)]
        | Pattern_tuple ns ->
            let vs =
              Value.untuple
                ~pp
                (interpret_expr upper_steps env scenario_state s)
            in
            List.map2 ~err:"match" (fun n v -> (n, Variant_arg v)) ns vs
        | Pattern_record (_, bs) ->
            let r =
              Value.un_record (interpret_expr upper_steps env scenario_state s)
            in
            let f {var; field} =
              (var, Variant_arg (List.assoc_exn ~msg:"match" field r))
            in
            List.map f bs
      in
      let variables = List.fold_right (uncurry add_variable) vs variables in
      let env = {env with variables = ref variables} in
      interpret_command sub_steps env scenario_state c
  | CModifyProduct (lhs, p, c) ->
    ( match path_of_expr upper_steps env scenario_state lhs with
    | None ->
        failwith
          (Printf.sprintf
             "Line %s: Cannot assign to '%s'"
             (string_of_line_no initialCommand.line_no)
             (Printer.texpr_to_string lhs))
    | Some lhs_path ->
        let lhs_lens, _err = lens_of_path ~config ~line_no env lhs_path in
        let sub_steps = ref [] in
        let variables = !(env.variables) in
        let v = interpret_expr upper_steps env scenario_state lhs in
        let vs =
          match p with
          | Pattern_single x -> [(x, Heap_ref (ref v))]
          | Pattern_tuple ns ->
              let vs = Value.untuple ~pp v in
              List.map2 ~err:"match" (fun n v -> (n, Heap_ref (ref v))) ns vs
          | Pattern_record (name, _bs) -> [(name, Heap_ref (ref v))]
          (* let r = Value.un_record v in
           * let f {var; field} =
           *   (var, Heap_ref (ref (List.assoc_exn ~msg:"match" field r)))
           * in
           * List.map f bs *)
        in
        let variables = List.fold_right (uncurry add_variable) vs variables in
        let env = {env with variables = ref variables} in
        let inner = interpret_command sub_steps env scenario_state c in
        ( match p with
        | Pattern_single _ | Pattern_tuple _ ->
            let rhs = Some inner in
            Lens.set lhs_lens rhs ();
            Value.unit
        | Pattern_record (name, _bs) ->
            assert_unit ~config initialCommand inner;
            let value =
              match List.assoc_opt name !(env.variables) with
              | Some (Heap_ref {contents = x}) -> x
              | _ -> failwith ("Not a local: " ^ name)
            in
            Lens.set lhs_lens (Some value) ();
            Value.unit ) )
  | CMatch (scrutinee, cases) ->
      let sub_steps = ref [] in
      let scrutinee = interpret_expr upper_steps env scenario_state scrutinee in
      ( match scrutinee.v with
      | Variant (_row, _layout, cons, arg) ->
          let res =
            match
              List.find_opt
                (fun (constructor, _, _) -> constructor = cons)
                cases
            with
            | None -> Value.unit
            | Some (_constructor, arg_name, body) ->
                interpret_command
                  sub_steps
                  { env with
                    variables =
                      ref
                        (add_variable
                           arg_name
                           (Variant_arg arg)
                           !(env.variables)) }
                  scenario_state
                  body
          in
          addStep
            ~sub_steps
            upper_steps
            env
            initialCommand
            [("match", scrutinee)];
          res
      | _ -> assert false )
  | CBind (x, c1, c2) ->
      let outer_locals = !(env.current_locals) in
      env.current_locals := [];
      let y = interpret_command upper_steps env scenario_state c1 in
      ( match x with
      | None -> ()
      | Some x ->
          env.variables := add_variable x (Heap_ref (ref y)) !(env.variables) );
      let r = interpret_command upper_steps env scenario_state c2 in
      env.variables :=
        List.filter
          (fun (n, _) ->
            (not (List.mem n !(env.current_locals)))
            && Option.cata true (( <> ) n) x)
          !(env.variables);
      env.current_locals := outer_locals;
      r
  | CSetVar ({e = EPrim0 (ELocal "__operations__")}, rhs) ->
      let rhs = Some (interpret_expr upper_steps env scenario_state rhs) in
      ( match rhs with
      | None -> assert false
      | Some rhs ->
          env.operations :=
            List.map (Value.unoperation ~pp) (Value.unList ~pp rhs);
          (* todo add steps  *)
          Value.unit )
  | CSetVar (lhs, rhs) ->
      ( match path_of_expr upper_steps env scenario_state lhs with
      | None ->
          failwith
            (Printf.sprintf
               "Line %s: Cannot assign to '%s'"
               (string_of_line_no initialCommand.line_no)
               (Printer.texpr_to_string lhs))
      | Some lhs ->
          let lhs, _err = lens_of_path ~config ~line_no env lhs in
          let rhs = Some (interpret_expr upper_steps env scenario_state rhs) in
          Lens.set lhs rhs () );
      addStep upper_steps env initialCommand [];
      Value.unit
  | CDelItem (map, key) ->
      ( match path_of_expr upper_steps env scenario_state map with
      | None ->
          failwith
            (Printf.sprintf
               "Line %s: Cannot assign to '%s'"
               (string_of_line_no line_no)
               (Printer.texpr_to_string map))
      | Some path ->
          let key = interpret_expr upper_steps env scenario_state key in
          let l, _err =
            lens_of_path
              ~config
              ~line_no
              env
              (extend_path path [S_item_map key])
          in
          Lens.set l None () );
      addStep upper_steps env initialCommand [];
      Value.unit
  | CUpdateSet (set, elem, add) ->
      ( match path_of_expr upper_steps env scenario_state set with
      | None ->
          failwith
            (Printf.sprintf
               "Line %s: Cannot remove from '%s'"
               (string_of_line_no line_no)
               (Printer.texpr_to_string set))
      | Some path ->
          let elem = interpret_expr upper_steps env scenario_state elem in
          let l, err = lens_of_path ~config ~line_no env path in
          Lens.set (l @. Lens.some ~err @. Value.lens_set_at ~elem) add () );
      addStep upper_steps env initialCommand [];
      Value.unit
  | CDefineLocal (name, e) ->
      env.current_locals := name :: !(env.current_locals);
      env.variables :=
        add_variable
          name
          (Heap_ref (ref (interpret_expr upper_steps env scenario_state e)))
          !(env.variables);
      addStep upper_steps env initialCommand [];
      Value.unit
  | CFor (name, iteratee, body) ->
      let sub_steps = ref [] in
      let iteratee_v = interpret_expr upper_steps env scenario_state iteratee in
      let iteratee_l = path_of_expr upper_steps env scenario_state iteratee in
      ( match iteratee_v.v with
      | List (_, elems) ->
          let step i v =
            let path =
              Base.Option.map iteratee_l ~f:(fun p ->
                  extend_path p [S_item_list i])
            in
            assert_unit
              ~config
              initialCommand
              (interpret_command
                 sub_steps
                 { env with
                   variables =
                     ref (add_variable name (Iter (v, path)) !(env.variables))
                 }
                 scenario_state
                 body)
          in
          List.iteri step elems
      | _ -> assert false );
      addStep ~sub_steps upper_steps env initialCommand [];
      Value.unit
  | CWhile (e, cmd) ->
      let sub_steps = ref [] in
      while
        Value.unBool ~pp (interpret_expr upper_steps env scenario_state e)
      do
        assert_unit
          ~config
          initialCommand
          (interpret_command sub_steps env scenario_state cmd)
      done;
      addStep ~sub_steps upper_steps env initialCommand [];
      Value.unit
  | CVerify (x, message) ->
      let v = interpret_expr upper_steps env scenario_state x in
      addStep upper_steps env initialCommand [];
      ( match v.v with
      | Literal (Bool true) -> Value.unit
      | Literal (Bool false) ->
          let except =
            Base.Option.map
              ~f:(interpret_expr upper_steps env scenario_state)
              message
          in
          let except, message =
            match except with
            | Some except -> (except, [`Br; `Text "Message:"; `Value except])
            | None -> (Value.string (Printer.wrong_condition_string x), [])
          in
          raise
            (ExecFailure
               ( except
               , [`Text "Wrong condition:"; `Expr x; `Rec message; `Line line_no]
               ))
      | _ ->
          failwith
            (Printf.sprintf
               "Failed condition [%s] in line %s"
               (Printer.value_to_string v)
               (string_of_line_no line_no)) )
  | CResult e -> interpret_expr upper_steps env scenario_state e
  | CComment _ -> Value.unit
  | CSetType _ -> Value.unit
  | CSetResultType (c, _) -> interpret_command upper_steps env scenario_state c
  | CSetEntryPoint (ep_name, l) ->
      let l = interpret_expr upper_steps env scenario_state l in
      let l, args = Value.unclosure ~pp:(fun () -> assert false) l in
      let in_contract = get_in_contract "set_entry_point" env.in_contract in
      let leps = !(in_contract.contract_state).lazy_entry_points in
      let leps = List.remove_assoc ep_name leps in
      let leps = (ep_name, `Closure (l, args)) :: leps in
      in_contract.contract_state :=
        {!(in_contract.contract_state) with lazy_entry_points = leps};
      Value.unit
  | CTrace e ->
      let e = interpret_expr upper_steps env scenario_state e in
      print_endline (Printer.value_to_string e);
      Value.unit

and call_closure ~config upper_steps env scenario_state (l, args) parameter =
  let parameter =
    List.fold_left (fun acc v -> Value.tuple [v; acc]) parameter args
  in
  let steps = ref [] in
  let env =
    { env with
      operations = ref []
    ; lambda_params = (l.id, parameter) :: env.lambda_params }
  in
  let r = interpret_command ~config steps env scenario_state l.body in
  addStep
    ~sub_steps:steps
    upper_steps
    env
    l.body
    [("lambda input", parameter); ("lambda output", r)];
  r

and call_lambda ~config upper_steps env scenario_state lambda parameter =
  let l, args = Value.unclosure ~pp:(fun () -> assert false) lambda in
  call_closure ~config upper_steps env scenario_state (l, args) parameter

and interpret_message
    ~config
    ~primitives
    ~scenario_state
    context
    { template =
        {tcontract = {entry_points; global_variables; derived}} as template
    ; state }
    {channel; params} =
  let interpret_command = interpret_command ~config in
  let tparameter = (get_extra derived).tparameter in
  let filtered_entry_points =
    match
      List.filter
        (fun ({channel = x} : _ entry_point) -> channel = x)
        entry_points
    with
    | [] ->
      ( match (channel, entry_points) with
      | "default", [ep] -> [ep]
      | _ -> [] )
    | l -> l
  in
  match filtered_entry_points with
  | [] ->
      ( None
      , []
      , Some
          (Execution.Exec_failure
             (Printf.ksprintf Value.string "Channel not found: %s" channel, []))
      , [] )
  | _ :: _ :: _ -> failwith "Too many channels"
  | [{channel = dt; lazify; body}] ->
      let lazify = Option.default config.lazy_entry_points lazify in
      let storage =
        match state.storage with
        | Some storage -> storage
        | None ->
            raise
              (SmartExcept
                 [ `Text "Interpreter Error"
                 ; `Br
                 ; `Text "Missing storage in contract"
                 ; `Line !context.line_no ])
      in
      ( match Value.checkType dt storage with
      | Some _ as error -> (None, [], error, [])
      | None ->
          let contract_state =
            { state with
              balance = Big_int.add_big_int !context.amount state.balance }
          in
          let in_contract =
            { parameters = params
            ; context = !context
            ; contract_state = ref contract_state
            ; tparameter }
          in
          let env =
            { in_contract = In_contract in_contract
            ; variables = ref []
            ; current_locals = ref []
            ; primitives
            ; operations = {contents = []}
            ; debug = !context.debug
            ; steps = ref []
            ; lambda_params = []
            ; global_variables }
          in
          let ep =
            if lazify
            then
              match List.assoc_opt channel state.lazy_entry_points with
              | None -> assert false
              | Some ep -> ep
            else `Initial
          in
          ( try
              match ep with
              | `Absent ->
                  let err = [`Text "lazy entry point not found"] in
                  ( None
                  , []
                  , Some
                      (Execution.Exec_failure
                         (Value.string "Missing entry point", err))
                  , !(env.steps) )
              | `Initial ->
                  let (_ : value) =
                    interpret_command env.steps env scenario_state body
                  in
                  ( Some {template; state = !(in_contract.contract_state)}
                  , List.rev !(env.operations)
                  , None
                  , !(env.steps) )
              | `Closure (l, args) ->
                  let storage =
                    match state.storage with
                    | Some storage -> storage
                    | None -> failwith "no storage"
                  in
                  let params =
                    Value.variant_with_type
                      ~line_no:[]
                      channel
                      params
                      tparameter
                  in
                  let r =
                    call_closure
                      ~config
                      env.steps
                      env
                      scenario_state
                      (l, args)
                      (Value.tuple [params; storage])
                  in
                  ( match r.v with
                  | Tuple [ops; storage] ->
                      let ops =
                        let pp () = assert false in
                        List.map (Value.unoperation ~pp) (Value.unList ~pp ops)
                      in
                      let state =
                        { !(in_contract.contract_state) with
                          storage = Some storage }
                      in
                      (Some {template; state}, List.rev ops, None, !(env.steps))
                  | _ -> assert false )
            with
          | Failure f ->
              ( None
              , []
              , Some
                  (Execution.Exec_failure
                     (Value.string f, [`Text "Failure:"; `Text f]))
              , !(env.steps) )
          | ExecFailure (value, message) ->
              ( None
              , []
              , Some (Execution.Exec_failure (value, message))
              , !(env.steps) )
          | SmartExcept l ->
              ( None
              , []
              , Some (Execution.Exec_failure (Value.string "Error", l))
              , !(env.steps) ) ) )

and interpret_expr_external ~config ~primitives ~no_env ~scenario_state =
  let env =
    { in_contract = No_contract no_env
    ; variables = ref []
    ; current_locals = ref []
    ; primitives
    ; operations = {contents = []}
    ; debug = false
    ; steps = ref []
    ; lambda_params = []
    ; global_variables = [] }
  in
  interpret_expr ~config (ref []) env scenario_state

and interpret_contract
    ~config
    ~primitives
    ~scenario_state
    ({tcontract = {balance; storage; baker; metadata}} as c : tcontract) =
  let conv name =
    interpret_expr_external
      ~config
      ~primitives
      ~no_env:[`Text ("Compute " ^ name)]
      ~scenario_state
  in
  let balance =
    match Option.map (conv "balance") balance with
    | None -> Big_int.zero_big_int
    | Some {v = Literal (Mutez x)} -> x
    | _ -> assert false
  in
  let storage = Option.map (conv "storage") storage in
  let baker = Option.bind baker (fun x -> Value.un_baker (conv "baker" x)) in
  let metadata = List.map (map_snd (map_meta (conv "storage"))) metadata in
  let lazy_entry_points =
    let f ep =
      let lazify = Option.default config.lazy_entry_points ep.lazify in
      let lazy_no_code = Option.default false ep.lazy_no_code in
      if lazify
      then Some (ep.channel, if lazy_no_code then `Absent else `Initial)
      else None
    in
    List.map_some f c.tcontract.entry_points
  in
  {template = c; state = {balance; storage; baker; metadata; lazy_entry_points}}

(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Utils
open Basics
open Typed

type t =
  | HO_none
  | HO_at_most_one
  | HO_many
[@@deriving show {with_path = false}, eq, ord]

let or_ ho1 ho2 =
  match (ho1, ho2) with
  | HO_none, x | x, HO_none -> x
  | HO_at_most_one, x | x, HO_at_most_one -> x
  | _ -> HO_many

let add ho1 ho2 =
  match (ho1, ho2) with
  | HO_none, x | x, HO_none -> x
  | _ -> HO_many

let widen = function
  | HO_none -> HO_none
  | _ -> HO_many

let has_operations_f = function
  | CIf (_, x, y) -> or_ (snd x) (snd y)
  | CMatch (_, cases) ->
      List.fold_left (fun acc (_, _, body) -> or_ acc (snd body)) HO_none cases
  | CMatchCons {ok_match; ko_match} -> or_ (snd ok_match) (snd ko_match)
  | CMatchProduct (_, _, c) -> snd c
  | CModifyProduct (_, _, c) -> snd c
  | CSetVar (({e = EPrim0 (ELocal "__operations__")}, _), rhs) ->
    ( match (fst rhs).e with
    | EList [] -> HO_none
    | EPrim2 (ECons, _, {e = EPrim0 (ELocal "__operations__")}) ->
        HO_at_most_one
    | _ -> HO_many )
  | CSetVar _ -> HO_none
  | CBind (_, c1, c2) -> add (snd c1) (snd c2)
  | CFor (_, _, c) | CWhile (_, c) -> widen (snd c)
  | CSetResultType (c, _) -> snd c
  | CComment _ | CSetType _ | CResult _ | CUpdateSet _ | CDelItem _
   |CDefineLocal _ | CNever _ | CVerify _ | CFailwith _ | CTrace _
   |CSetEntryPoint _ ->
      HO_none

let has_operations =
  let p_texpr _line_no _et _e = () in
  let p_tcommand _ _ = has_operations_f in
  let p_ttype _ = () in
  para_tcommand (para_talg ~p_texpr ~p_tcommand ~p_ttype)

(* Copyright 2019-2021 Smart Chain Arena LLC. *)

val intType : bool Unknown.t -> [> `Int | `Nat | `Unknown ]

(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Utils
open Basics
open Typed
open Control

let empty_env = String.Map.empty

let mk_env xs = String.Map.of_list (List.map (fun (x, t) -> (x, (t, 0))) xs)

let string_of_static_id {static_id} =
  Printf.sprintf "static_contract%d" static_id

let string_of_dynamic_id {dynamic_id} =
  Printf.sprintf "dynamic_contract%d" dynamic_id

let string_of_contract_id = function
  | C_static id -> string_of_static_id id
  | C_dynamic id -> string_of_dynamic_id id

let string_of_scenario_var i = Printf.sprintf "scenario_var%d" i

module type ACTION = sig
  include MONAD_CORE

  type state =
    { tvar_counter : int
    ; vars : (Type.t * int) String.Map.t
    ; constraints : (line_no * typing_constraint) list
    ; inside_contract : bool
    ; config : Config.t
    ; warnings : smart_except list list }

  val set : state -> unit t

  val get : state t

  val warn : smart_except list -> unit t

  val run : 'a t -> state -> 'a * state

  val map_list : ('a -> 'b t) -> 'a list -> 'b list t
end

module Action : ACTION = struct
  type state =
    { tvar_counter : int
    ; vars : (Type.t * int) String.Map.t
    ; constraints : (line_no * typing_constraint) list
    ; inside_contract : bool
    ; config : Config.t
    ; warnings : smart_except list list }

  type 'a t = state -> 'a * state

  let run x s = x s

  let return (x : 'a) : 'a t = fun s -> (x, s)

  let bind (x : 'a t) (f : 'a -> 'b t) : 'b t =
   fun s ->
    let y, s = x s in
    f y s

  let map f x = bind x (fun x -> return (f x))

  let apply f x = bind f (fun f -> bind x (fun x -> return (f x)))

  let set s _ = ((), s)

  let get s = (s, s)

  let warn w s = ((), {s with warnings = w :: s.warnings})

  let map_list f =
    let rec map_list acc s = function
      | [] -> (List.rev acc, s)
      | x :: xs ->
          let y, s = f x s in
          map_list (y :: acc) s xs
    in
    flip (map_list [])
end

module ActionM = struct
  include Action
  include Monad (Action)
end

open ActionM

open Syntax (ActionM)

let get_vars =
  let* s = get in
  return s.vars

let get_config =
  let* s = get in
  return s.config

let modify f =
  let* s = get in
  set (f s)

let set_vars vars = modify (fun s -> {s with vars})

let set_config config = modify (fun s -> {s with config})

let set_inside_contract inside_contract =
  modify (fun s -> {s with inside_contract})

let modify_vars f = modify (fun s -> {s with vars = f s.vars})

let fresh_tvar =
  let* ({tvar_counter = i} as s) = get in
  let* () = set {s with tvar_counter = i + 1} in
  return (Type.unknown_raw (ref (Type.UUnknown ("a" ^ string_of_int i))))

let pp_bin ~line_no op x y () = [`Text op; `Exprs [x; y]; `Br; `Line line_no]

let pp_bin_op ~line_no op x y () =
  [`Expr x; `Text op; `Expr y; `Br; `Line line_no]

let add_constraint ~line_no c =
  modify (fun st -> {st with constraints = (line_no, c) :: st.constraints})

let assertEqual ~line_no ~pp t1 t2 =
  if Type.unF t1 != Type.unF t2
  then add_constraint ~line_no (AssertEqual (t1, t2, pp))
  else return ()

let checkComparable ~line_no x y =
  let pp = pp_bin ~line_no "comparison between" x y in
  let* () = assertEqual ~line_no x.et y.et ~pp in
  let* () = add_constraint ~line_no (IsComparable x) in
  let* () = add_constraint ~line_no (IsComparable y) in
  return ()

let solve_constraints =
  let* s = get in
  let* config = get_config in
  Solver.run ~config s.constraints;
  modify (fun s -> {s with constraints = []})

let err ~line_no msg =
  raise (SmartExcept [`Text "Declaration Error"; `Br; `Text msg; `Line line_no])

let add_var ~line_no name t =
  modify_vars (fun vars ->
      match String.Map.find_opt name vars with
      | None -> String.Map.add name (t, 0) vars
      | Some _ ->
          err ~line_no (Printf.sprintf "Variable name %S already in use." name))

let check_var ~line_no ?meta kind name =
  let* {vars; inside_contract} = get in
  match String.Map.find_opt name vars with
  | Some (t, _occs) when not inside_contract -> return t
  | Some (t, occs) ->
      let* () =
        if occs = 1 && not (meta = Some ())
        then add_constraint ~line_no (IsNotHot (name, t))
        else return ()
      in
      let* () = set_vars (String.Map.add name (t, occs + 1) vars) in
      return t
  | None ->
      err
        ~line_no
        (Format.asprintf "%s variable %S escapes its scope." kind name)

let remove_var ~line_no name =
  modify_vars (fun vars ->
      match String.Map.find_opt name vars with
      | None -> err ~line_no (Printf.sprintf "Missing variable %S." name)
      | Some _ -> String.Map.filter (fun x _ -> not (String.equal name x)) vars)

let scoped x =
  let* old_vars = get_vars in
  let* r = x in
  let* () = set_vars old_vars in
  return r

let scoped_no_occs x =
  let* old_vars = get_vars in
  let* () = set_vars (String.Map.map (fun (n, _) -> (n, 0)) old_vars) in
  let* r = x in
  let* () = set_vars old_vars in
  return r

let scoped_branch x =
  let* old_vars = get_vars in
  let* r = x in
  let* new_vars = get_vars in
  let* () = set_vars old_vars in
  return (String.Map.map snd (String.Map.intersect old_vars new_vars), r)

let max_occs b1 b2 =
  let f _ x y =
    match (x, y) with
    | Some (t, occs1), Some (_, occs2) -> Some (t, max occs1 occs2)
    | Some (t, occs), None | None, Some (t, occs) -> Some (t, occs)
    | None, None -> None
  in
  String.Map.merge f b1 b2

let no_new_occs ~line_no old_vars new_vars =
  let f name x y =
    match (x, y) with
    | Some (t, 0), Some (_, 1) ->
        Some (add_constraint ~line_no (IsNotHot (name, t)))
        (* TODO better error message *)
    | _ -> None
  in
  iter_list id String.Map.(values (merge f old_vars new_vars))

let for_variant ~line_no name t =
  let open Type in
  match name with
  | "None" ->
      let* () =
        assertEqual ~line_no t Type.unit ~pp:(fun () ->
            [`Text "Argument to None must be unit."])
      in
      let* t = fresh_tvar in
      return (option t)
  | "Some" -> return (option t)
  | "Left" ->
      let* u = fresh_tvar in
      return (tor t u)
  | "Right" ->
      let* u = fresh_tvar in
      return (tor u t)
  | _ -> return (uvariant name t)

let check_command_f line_no c =
  let* config = get_config in
  let module Printer = (val Printer.get config : Printer.Printer) in
  let assertEqual = assertEqual ~line_no in
  let add_var = add_var ~line_no in
  let remove_var = remove_var ~line_no in
  match c with
  | CMatch (scrutinee, cases) ->
      let* scrutinee = scrutinee in
      let* rt = fresh_tvar in
      let check_case (constructor, arg_name, body) =
        scoped_branch
          (let* at = fresh_tvar in
           let* vt = for_variant ~line_no constructor at in
           let* () =
             assertEqual scrutinee.et vt ~pp:(fun () ->
                 [ `Text "match scrutinee has incompatible type:"
                 ; `Expr scrutinee
                 ; `Br
                 ; `Text "Expected type:"
                 ; `Type vt
                 ; `Line line_no ])
           in
           let* () = add_var arg_name at in
           let* body = body in
           let* () =
             assertEqual body.ct rt ~pp:(fun () ->
                 [ `Text "match branch has type"
                 ; `Type body.ct
                 ; `Text "instead of"
                 ; `Type rt
                 ; `Line line_no ])
           in
           let* () = remove_var arg_name in
           return (constructor, arg_name, body))
      in
      let* cases = map_list check_case cases in
      let* () =
        set_vars List.(fold_left max_occs String.Map.empty (map fst cases))
      in
      let c = CMatch (scrutinee, List.map snd cases) in
      return (build_tcommand ~line_no c rt)
  | CMatchCons {expr; id; ok_match; ko_match} ->
      let* expr = expr in
      let* at = fresh_tvar in
      let vt = Type.list at in
      let* () =
        assertEqual expr.et vt ~pp:(fun () ->
            [ `Text "match list scrutinee is not a list:"
            ; `Expr expr
            ; `Br
            ; `Text "Expected type:"
            ; `Type vt
            ; `Line line_no ])
      in
      let* vars1, ok_match =
        scoped_branch
          (let t =
             Type.record_default_layout Config.Comb [("head", at); ("tail", vt)]
           in
           let* () = add_var id t in
           ok_match)
      in
      let* vars2, ko_match = scoped_branch ko_match in
      let* () = set_vars (max_occs vars1 vars2) in
      let* () =
        assertEqual ok_match.ct ko_match.ct ~pp:(fun () ->
            [`Text "sp.match_cons: cannot unify branches"; `Line line_no])
      in
      let c = CMatchCons {expr; id; ok_match; ko_match} in
      return (build_tcommand ~line_no c ok_match.ct)
  | CMatchProduct (scrutinee, Pattern_record (name, bs), c) ->
      if List.length bs < 1
      then
        raise
          (SmartExcept
             [`Text "Must match at least one record field"; `Line line_no]);
      let* bs' =
        map_list
          (fun x ->
            let* t = fresh_tvar in
            return (x, t))
          bs
      in
      let vars = List.map (fun ({var}, t) -> (var, t)) bs' in
      let fields = List.map (fun ({field}, t) -> (field, t)) bs' in
      let* scrutinee = scrutinee in
      let t = Type.urecord fields in
      let* () =
        assertEqual scrutinee.et t ~pp:(fun () ->
            [ `Text "match tuple scrutinee is not a record:"
            ; `Expr scrutinee
            ; `Br
            ; `Text "Expected type:"
            ; `Type t
            ; `Line line_no ])
      in
      let* c =
        scoped
          (let* () = iter_list (uncurry add_var) vars in
           c)
      in
      let c' = CMatchProduct (scrutinee, Pattern_record (name, bs), c) in
      return (build_tcommand ~line_no c' c.ct)
  | CMatchProduct (scrutinee, Pattern_tuple ns, c) ->
      if List.length ns < 2
      then
        raise
          (SmartExcept
             [ `Text "Matched tuple must have at least two elements"
             ; `Line line_no ]);
      let* scrutinee = scrutinee in
      let* ts =
        map_list
          (fun n ->
            let* t = fresh_tvar in
            return (n, t))
          ns
      in
      let t = Type.tuple (List.map snd ts) in
      let* () =
        assertEqual scrutinee.et t ~pp:(fun () ->
            [ `Text "match tuple scrutinee is not a tuple:"
            ; `Expr scrutinee
            ; `Br
            ; `Text "Expected type:"
            ; `Type t
            ; `Line line_no ])
      in
      let* c =
        scoped
          (let* () = iter_list (uncurry add_var) ts in
           c)
      in
      let c' = CMatchProduct (scrutinee, Pattern_tuple ns, c) in
      return (build_tcommand ~line_no c' c.ct)
  | CModifyProduct (e, Pattern_record (name, bs), body) ->
      let* e = e in
      let t = Type.urecord [] in
      let* () =
        assertEqual e.et t ~pp:(fun () ->
            [ `Text "sp.match_record scrutinee is not a record:"
            ; `Expr e
            ; `Br
            ; `Text "Expected type:"
            ; `Type t
            ; `Line line_no ])
      in
      let* () = add_var name e.et in
      let* body = body in
      let* () =
        assertEqual body.ct Type.unit ~pp:(fun () ->
            [ `Text "sp.modify_record result type must not have an sp.result"
            ; `Line line_no ])
      in
      let c' = CModifyProduct (e, Pattern_record (name, bs), body) in
      return (build_tcommand ~line_no c' Type.unit)
  | CModifyProduct (lhs, Pattern_tuple vars, body) ->
      if List.length vars < 2
      then
        raise
          (SmartExcept
             [ `Text "sp.modify_tuple requires at least two arguments "
             ; `Line line_no ]);
      let* lhs = lhs in
      let* vars' =
        map_list
          (fun n ->
            let* t = fresh_tvar in
            return (n, t))
          vars
      in
      let t = Type.tuple (List.map snd vars') in
      let* () =
        assertEqual lhs.et t ~pp:(fun () ->
            [ `Text "scrutinee is not a tuple:"
            ; `Expr lhs
            ; `Br
            ; `Text "Expected type:"
            ; `Type t
            ; `Line line_no ])
      in
      let* body =
        scoped
          (let* () = iter_list (uncurry add_var) vars' in
           body)
      in
      let* () =
        assertEqual body.ct t ~pp:(fun () ->
            [ `Text "sp.match_record result type:"
            ; `Type body.ct
            ; `Br
            ; `Text "Expected type:"
            ; `Type t
            ; `Line line_no ])
      in
      let c' = CModifyProduct (lhs, Pattern_tuple vars, body) in
      return (build_tcommand ~line_no c' Type.unit)
  | CModifyProduct (lhs, Pattern_single var, body) ->
      let* lhs = lhs in
      let* body =
        scoped
          (let* () = add_var var lhs.et in
           body)
      in
      let* () =
        assertEqual body.ct lhs.et ~pp:(fun () ->
            [ `Text "sp.match result type:"
            ; `Type body.ct
            ; `Br
            ; `Text "Expected type:"
            ; `Type lhs.et
            ; `Line line_no ])
      in
      let c' = CModifyProduct (lhs, Pattern_single var, body) in
      return (build_tcommand ~line_no c' Type.unit)
  | CDefineLocal (name, e) ->
      let* e = e in
      let* () = add_var name e.et in
      let c' = CDefineLocal (name, e) in
      return (build_tcommand ~line_no c' Type.unit)
  | CBind (None, c1, c2) ->
      let* state = get in
      let c1, state = run c1 state in
      let c2, state = run c2 state in
      let* () = set state in
      return (build_tcommand ~line_no (CBind (None, c1, c2)) c2.ct)
  | CBind (Some x, c1, c2) ->
      let* c1 = c1 in
      let* () = add_var x c1.ct in
      let* c2 = c2 in
      return (build_tcommand ~line_no (CBind (Some x, c1, c2)) c2.ct)
  | CFor (name, e, body) ->
      let* e = e in
      let* t = fresh_tvar in
      let* () =
        assertEqual e.et (Type.list t) ~pp:(fun () ->
            [ `Text
                (Printf.sprintf
                   "for (%s : %s) in"
                   (Printer.variable_to_string name Iter)
                   (Printer.type_to_string t))
            ; `Expr e
            ; `Line line_no ])
      in
      let* old_vars = get_vars in
      let* new_vars, body =
        scoped_branch
          (let* () = add_var name t in
           body)
      in
      let* () = no_new_occs ~line_no old_vars new_vars in
      let* () =
        assertEqual body.ct Type.unit ~pp:(fun () ->
            [ `Text "for-loop body has non-unit type"
            ; `Type body.ct
            ; `Line line_no ])
      in
      return (build_tcommand ~line_no (CFor (name, e, body)) Type.unit)
  | CIf (cond, c1, c2) ->
      let* cond = cond in
      let* vars1, c1 = scoped_branch c1 in
      let* vars2, c2 = scoped_branch c2 in
      let* () = set_vars (max_occs vars1 vars2) in
      let* () =
        assertEqual cond.et Type.bool ~pp:(fun () ->
            [`Text "sp.if"; `Expr cond; `Text ":"; `Line line_no])
      in
      let* () =
        assertEqual c1.ct c2.ct ~pp:(fun () ->
            [`Text "sp.if: cannot unify branches"; `Line line_no])
      in
      return (build_tcommand ~line_no (CIf (cond, c1, c2)) c1.ct)
  | CSetResultType (c, t) ->
      let* c = c in
      let* () =
        assertEqual c.ct t ~pp:(fun () ->
            [ `Type c.ct
            ; `Text "is not compatible with type"
            ; `Type t
            ; `Line line_no ])
      in
      return (build_tcommand ~line_no (CSetResultType (c, t)) t)
  | CWhile (e, body) ->
      let* e = e in
      let* () =
        assertEqual e.et Type.bool ~pp:(fun () ->
            [`Text "while"; `Expr e; `Line line_no])
      in
      let* old_vars = get_vars in
      let* new_vars, body = scoped_branch body in
      let* () = no_new_occs ~line_no old_vars new_vars in
      let* () =
        assertEqual body.ct Type.unit ~pp:(fun () ->
            [`Text "while body has non-unit type"; `Type body.ct; `Line line_no])
      in
      return (build_tcommand ~line_no (CWhile (e, body)) Type.unit)
  | CSetType (e, t) ->
      let* e = scoped_no_occs e in
      let* () =
        assertEqual e.et t ~pp:(fun () ->
            [ `Expr e
            ; `Text "is not compatible with type"
            ; `Type t
            ; `Line line_no ])
      in
      return (build_tcommand ~line_no (CSetType (e, t)) Type.unit)
  | CSetVar (lhs, rhs) ->
      let* lhs = scoped_no_occs lhs in
      let* rhs = rhs in
      let* () =
        assertEqual lhs.et rhs.et ~pp:(fun () ->
            [`Text "set"; `Expr lhs; `Text "="; `Expr rhs; `Line line_no])
      in
      return (build_tcommand ~line_no (CSetVar (lhs, rhs)) Type.unit)
  | c ->
      (* Commands that don't modify the context. *)
      let* c = sequence_command_f c in
      let* t =
        match c with
        | CMatch _ | CMatchCons _ | CMatchProduct _ | CModifyProduct _
         |CDefineLocal _ | CFor _ | CBind _ | CIf _ | CSetType _
         |CSetResultType _ | CWhile _ | CSetVar _ ->
            assert false (* Already treated above. *)
        | CVerify (e, _message) ->
            let* () =
              assertEqual e.et Type.bool ~pp:(fun () ->
                  [`Text "not a boolean expression"; `Line line_no])
            in
            return Type.unit
        | CDelItem (x, y) ->
            let pp () =
              [ `Text "del"
              ; `Expr x
              ; `Text "["
              ; `Expr y
              ; `Text "]"
              ; `Line line_no ]
            in
            let* tvalue = fresh_tvar in
            let* () =
              assertEqual
                x.et
                (Type.map ~big:(Unknown.unknown ()) ~tkey:y.et ~tvalue)
                ~pp
            in
            return Type.unit
        | CUpdateSet (x, y, add) ->
            let pp () =
              [ `Expr x
              ; `Text "."
              ; `Text (if add then "add" else "remove")
              ; `Text "("
              ; `Expr y
              ; `Text ")"
              ; `Br
              ; `Line line_no ]
            in
            let* () = assertEqual x.et (Type.set ~telement:y.et) ~pp in
            return Type.unit
        | CFailwith _ -> fresh_tvar
        | CNever e ->
            let* () =
              assertEqual e.et Type.never ~pp:(fun () ->
                  [`Text "Bad type in sp.never"; `Expr e; `Line line_no])
            in
            fresh_tvar
        | CResult e -> return e.et
        | CComment _ | CTrace _ -> return Type.unit
        | CSetEntryPoint (name, ep) ->
            let* tparameter_lazy =
              check_var ~line_no "Self parameter (lazy)" "__parameter_lazy__"
            in
            let* () =
              let* t_ep = fresh_tvar in
              assertEqual
                (Type.uvariant name t_ep)
                tparameter_lazy
                ~pp:(fun () ->
                  [`Text "sp.set_entry_point: incompatible type"; `Line line_no])
            in
            let* tstorage = check_var ~line_no "Storage" "__storage__" in
            let expected =
              Type.(
                lambda
                  (pair tparameter_lazy tstorage)
                  (pair (list operation) tstorage))
            in
            let* () =
              assertEqual ep.et expected ~pp:(fun () ->
                  [ `Text "Expected entry point of type"
                  ; `Type expected
                  ; `Text ", but got"
                  ; `Type ep.et
                  ; `Line line_no ])
            in
            return Type.unit
      in
      return (build_tcommand ~line_no c t)

let check_view ~line_no tstorage global_variables (ov : _ offchain_view) =
  scoped_no_occs
    (let* () = add_var ~line_no "__storage__" tstorage in
     let* t = fresh_tvar in
     let* () = when_ ov.has_param (add_var ~line_no "__parameter__" t) in
     let* () =
       iter_list (fun (n, e) -> add_var ~line_no n e.et) global_variables
     in
     let* body = ov.body in
     return
       { ov with
         body
       ; tparameter_derived = T (if ov.has_param then Some t else None) })

let check_entry_point
    ~config
    ~line_no
    ~tparameter
    ~tparameter_lazy
    ~tstorage
    ~global_variables
    (ep : _ entry_point) =
  let is_param = function
    | {e = EPrim0 (ELocal "__parameter__")} -> true
    | _ -> false
  in
  let uses_param =
    exists_command ~exclude_create_contract:false is_param (fun _ -> false)
  in
  scoped_no_occs
    (let* () = add_var ~line_no "__operations__" (Type.list Type.operation) in
     let* () = add_var ~line_no "__storage__" tstorage in
     let* tparameter_ep_derived =
       match ep.tparameter_ep with
       | `Absent -> return Type.unit
       | `Present -> fresh_tvar
       | `Annotated t -> return t
     in
     let* () = add_var ~line_no "__parameter__" tparameter_ep_derived in
     let* () = add_var ~line_no "__contract__" (Type.contract tparameter) in
     let* () = add_var ~line_no "__parameter_lazy__" tparameter_lazy in
     let* () =
       iter_list (fun (n, e) -> add_var ~line_no n e.et) global_variables
     in
     let* body = ep.body in
     let* () =
       assertEqual ~line_no body.ct Type.unit ~pp:(fun () ->
           [ `Text "Entry point "
           ; `Text ep.channel
           ; `Text " has a sp.result."
           ; `Br
           ; `Line body.line_no ])
     in
     let* () =
       if config.Config.warn_unused
          && ep.tparameter_ep = `Present
          && not (uses_param body)
       then
         warn
           [ `Text "Entry point"
           ; `Text ep.channel
           ; `Text "has unused parameter."
           ; `Line ep.line_no ]
       else return ()
     in
     return
       { ep with
         body
       ; lazify = Some (Option.default config.lazy_entry_points ep.lazify)
       ; tparameter_ep_derived = T tparameter_ep_derived })

let check_contract_f
    ~line_no (c : (texpr t, tcommand t, Type.t, untyped) contract_f) =
  let ({ balance
       ; storage
       ; baker
       ; tstorage_explicit
       ; metadata
       ; flags
       ; entry_points
       ; entry_points_layout }
        : _ contract_f) =
    c
  in
  let* config = get_config in
  let config = List.fold_left Config.apply_flag config flags in
  let* balance = sequence_option balance in
  let* storage = sequence_option storage in
  let* baker = sequence_option baker in
  let* () =
    when_some balance (fun balance ->
        assertEqual ~line_no balance.et Type.token ~pp:(fun () ->
            [ `Expr balance
            ; `Text "is not a valid amount in sp.create_contract"
            ; `Line line_no ]))
  in
  let* () =
    when_some baker (fun baker ->
        assertEqual ~line_no baker.et (Type.option Type.key_hash) ~pp:(fun () ->
            [ `Expr baker
            ; `Text "is not a valid optional baker in sp.create_contract"
            ; `Line line_no ]))
  in
  let* tstorage = fresh_tvar in
  let* () =
    when_some storage (fun s ->
        assertEqual ~line_no tstorage s.et ~pp:(fun () ->
            [ `Expr s
            ; `Text "is not a valid storage in sp.create_contract"
            ; `Type tstorage
            ; `Line line_no ]))
  in
  let* () =
    when_some tstorage_explicit (fun t ->
        assertEqual ~line_no tstorage t ~pp:(fun () ->
            [ `Text "Invalid storage type:"
            ; `Type t
            ; `Text "instead of"
            ; `Type tstorage
            ; `Line line_no ]))
  in
  let* metadata =
    map_list
      (fun (x, y) ->
        let+ y = sequence_meta y in
        (x, y))
      metadata
  in
  scoped
    (let line_no = [] in
     let* () = set_inside_contract true in
     let* () = set_vars String.Map.empty in
     let* global_variables = map_list sequence_snd c.global_variables in
     let* views =
       map_list (check_view ~line_no tstorage global_variables) c.views
     in
     let* tparameter = fresh_tvar in
     let* tparameter_lazy = fresh_tvar in
     let* tparameter_non_lazy = fresh_tvar in
     let* () =
       when_some entry_points_layout (fun l ->
           let f ({target} : Layout.l) =
             let* t = fresh_tvar in
             return (target, t)
           in
           let* row = map_list f (Binary_tree.to_list l) in
           let t = Type.variant (Unknown.value l) row in
           assertEqual ~line_no:[] t tparameter_non_lazy ~pp:(fun () ->
               [ `Text "incompatible entry points layout"
               ; `Type t
               ; `Type tparameter_non_lazy ]))
     in
     let* entry_points =
       map_list
         (check_entry_point
            ~config
            ~line_no
            ~tparameter
            ~tparameter_lazy
            ~tstorage
            ~global_variables)
         entry_points
     in
     let eps_public = List.filter (fun ep -> ep.originate) entry_points in
     let eps_lazy, eps_non_lazy =
       List.partition (fun ep -> Option.get ep.lazify) eps_public
     in
     let mk_row =
       List.map (fun (ep : _ entry_point) ->
           (ep.channel, get_extra ep.tparameter_ep_derived))
     in
     let mk_variant = function
       | [] -> Type.unit
       | eps -> Type.variant (Unknown.unknown ()) (mk_row eps)
     in
     let* () =
       let t = mk_variant eps_lazy in
       assertEqual ~line_no:[] t tparameter_lazy ~pp:(fun () ->
           [`Text "incompatible lazy parameter type"])
     in
     let* () =
       let t = mk_variant eps_non_lazy in
       assertEqual ~line_no:[] t tparameter_non_lazy ~pp:(fun () ->
           [`Text "incompatible non-lazy parameter type"])
     in
     let* () =
       let t = mk_variant eps_public in
       assertEqual ~line_no:[] t tparameter ~pp:(fun () ->
           [`Text "incompatible parameter type"])
     in
     let tparameter_lazy =
       match eps_lazy with
       | [] -> None
       | _ -> Some tparameter_lazy
     in
     let tparameter_non_lazy =
       match eps_non_lazy with
       | [] -> None
       | _ -> Some tparameter_non_lazy
     in
     let* () = set_inside_contract false in
     return
       { c with
         balance
       ; storage
       ; baker
       ; metadata
       ; entry_points
       ; global_variables
       ; views
       ; derived =
           T
             { tparameter
             ; tparameter_lazy
             ; tparameter_non_lazy
             ; tparameter_full = mk_variant entry_points
             ; tstorage } })

let check_mprim0 ~line_no :
    Michelson_base.Type.mtype Michelson_base.Primitive.prim0 -> _ = function
  | Self None -> check_var ~line_no "Self parameter" "__parameter_contract__"
  | Self (Some name) ->
      let* t_contract = check_var ~line_no "Self parameter" "__contract__" in
      let* t_ep = fresh_tvar in
      let* () =
        assertEqual
          ~line_no
          (Type.contract (Type.uvariant name t_ep))
          t_contract
          ~pp:(fun () ->
            [`Text "sp.self_entry_point: incompatible type"; `Line line_no])
      in
      return (Type.contract t_ep)
  | p ->
      let* config = get_config in
      let module Printer = (val Printer.get config : Printer.Printer) in
      let t = Michelson_base.Typing.type_prim0 p in
      return (Type.of_mtype t)

let check_mprim1 ~line_no p x =
  let* config = get_config in
  let module Printer = (val Printer.get config : Printer.Printer) in
  let name = Printer.mprim1_to_string ~language:SmartPy p in
  let t1, t = Michelson_base.Typing.type_prim1 p in
  let t1 = Type.of_mtype t1 in
  let* () =
    assertEqual ~line_no x.et t1 ~pp:(fun () ->
        [ `Text name
        ; `Text "expects a"
        ; `Type t1
        ; `Br
        ; `Text "Got:"
        ; `Type x.et
        ; `Line line_no ])
  in
  return (Type.of_mtype t)

let check_mprim2 ~line_no p x1 x2 =
  let* config = get_config in
  let module Printer = (val Printer.get config : Printer.Printer) in
  let name = Printer.mprim2_to_string p in
  let t1, t2, t = Michelson_base.Typing.type_prim2 p in
  let t1 = Type.of_mtype t1 in
  let t2 = Type.of_mtype t2 in
  let* () =
    assertEqual ~line_no x1.et t1 ~pp:(fun () ->
        [ `Text name
        ; `Text "expects a"
        ; `Type t1
        ; `Text "as its first argument"
        ; `Br
        ; `Text "Got:"
        ; `Type x1.et
        ; `Line line_no ])
  in
  let* () =
    assertEqual ~line_no x2.et t2 ~pp:(fun () ->
        [ `Text name
        ; `Text "expects a"
        ; `Type t2
        ; `Text "as its second argument"
        ; `Br
        ; `Text "Got:"
        ; `Type x2.et
        ; `Line line_no ])
  in
  return (Type.of_mtype t)

let check_mprim3 ~line_no p x1 x2 x3 =
  let* config = get_config in
  let module Printer = (val Printer.get config : Printer.Printer) in
  let name = Printer.mprim3_to_string p in
  let t1, t2, t3, t = Michelson_base.Typing.type_prim3 p in
  let t1 = Type.of_mtype t1 in
  let t2 = Type.of_mtype t2 in
  let t3 = Type.of_mtype t3 in
  let* () =
    assertEqual ~line_no x1.et t1 ~pp:(fun () ->
        [ `Text name
        ; `Text "expects a"
        ; `Type t1
        ; `Text "as its first argument"
        ; `Br
        ; `Text "Got:"
        ; `Type x1.et
        ; `Line line_no ])
  in
  let* () =
    assertEqual ~line_no x2.et t2 ~pp:(fun () ->
        [ `Text name
        ; `Text "expects a"
        ; `Type t2
        ; `Text "as its second argument"
        ; `Br
        ; `Text "Got:"
        ; `Type x2.et
        ; `Line line_no ])
  in
  let* () =
    assertEqual ~line_no x3.et t3 ~pp:(fun () ->
        [ `Text name
        ; `Text "expects a"
        ; `Type t3
        ; `Text "as its third argument"
        ; `Br
        ; `Text "Got:"
        ; `Type x3.et
        ; `Line line_no ])
  in
  return (Type.of_mtype t)

let check_prim0 ~line_no =
  let check_var = check_var ~line_no in
  let assertEqual = assertEqual ~line_no in
  function
  | ELevel -> return (Type.nat ())
  | ECstContract {type_} -> return (Type.contract type_)
  | ECst (Int {i; is_nat} as x) ->
    ( match Unknown.get is_nat with
    | Some true when Big_int.sign_big_int i < 0 ->
        raise
          (SmartExcept
             [`Text "sp.nat cannot take negative values"; `Br; `Line line_no])
    | _ -> return (Type.type_of_literal x) )
  | ECst (Mutez i) when Big_int.sign_big_int i < 0 ->
      raise
        (SmartExcept
           [ `Text "sp.tez/sp.mutez cannot take negative values"
           ; `Br
           ; `Line line_no ])
  | ECst x -> return (Type.type_of_literal x)
  | EBounded x -> return (Type.bounded (Type.type_of_literal x) false [x])
  | EGlobal n -> check_var "Global var" n
  | EIter name -> check_var "Iterator" name
  | ELocal name -> check_var "Local" name
  | EMetaLocal name -> check_var ~meta:() "Meta Local" name
  | EMatchCons name -> check_var "Match list" name
  | EVariant_arg name -> check_var "Pattern" name
  | EScenario_var id ->
      let name = string_of_scenario_var id in
      check_var "Scenario var" name
  | EContract_data id ->
      let name = string_of_contract_id id ^ "_storage" in
      check_var "Contract var" name
  | EAccount_of_seed {seed = _} -> return Type.account
  | EContract_address _ -> return Type.address
  | EContract_balance _ -> return Type.token
  | EContract_baker _ ->
      let* config = get_config in
      return (Type.option (Type.baker_type_of_protocol config.protocol))
  | EContract_typed (id, entry_point) ->
      let name = string_of_contract_id id in
      let* t = check_var "Contract var" name in
      ( match entry_point with
      | None -> return t
      | Some entry_point ->
          let* t_ep = fresh_tvar in
          let* () =
            assertEqual
              (Type.contract (Type.uvariant entry_point t_ep))
              t
              ~pp:(fun () ->
                [ `Text "sp.contract: incompatible type or missing entry_point "
                ; `Text entry_point
                ; `Type t_ep
                ; `Type t
                ; `Line line_no ])
          in
          return (Type.contract t_ep) )

let check_prim1 ~line_no prim x =
  let assertEqual = assertEqual ~line_no in
  let add_constraint = add_constraint ~line_no in
  match prim with
  | ESum ->
      let t = Type.intOrNat () in
      let* () =
        assertEqual x.et (Type.list t) ~pp:(fun () ->
            [`Text "sum"; `Expr x; `Line line_no])
      in
      return t
  | EToConstant -> return x.et
  | ESetDelegate ->
      let* config = get_config in
      let* () =
        assertEqual
          x.et
          (Type.option (Type.baker_type_of_protocol config.protocol))
          ~pp:(fun () -> [`Text "not an optional hash"; `Line line_no])
      in
      return Type.operation
  | EUnpack t ->
      let pp () = [`Text "unpack"; `Expr x; `Type t; `Line line_no] in
      let* () = assertEqual x.et Type.bytes ~pp in
      return (Type.option t)
  | EPack -> return Type.bytes
  | EImplicit_account ->
      let* () =
        assertEqual x.et Type.key_hash ~pp:(fun () ->
            [`Text "sp.implicit_account("; `Expr x; `Text ")"; `Line line_no])
      in
      return (Type.contract Type.unit)
  | EToInt ->
      let* () = add_constraint (HasInt x) in
      return (Type.int ())
  | ENeg ->
      let* t = fresh_tvar in
      let* () = add_constraint (HasNeg (x, t)) in
      return t
  | ESign ->
      let* () =
        assertEqual x.et (Type.int ()) ~pp:(fun () ->
            [`Text "sp.sign"; `Expr x; `Line line_no])
      in
      return (Type.int ())
  | EConcat_list ->
      let* t = fresh_tvar in
      let pp expr () = [`Text "sp.concat"; `Expr expr; `Line line_no] in
      let* () = assertEqual x.et (Type.list t) ~pp:(pp x) in
      return t
  | ESize ->
      let* () = add_constraint (HasSize x) in
      return (Type.nat ())
  | EListRev ->
      let* t = fresh_tvar in
      let* () =
        assertEqual x.et (Type.list t) ~pp:(fun () ->
            [`Expr x; `Text ".rev()"; `Line line_no])
      in
      return (Type.list t)
  | EListItems _rev ->
      let* tkey = fresh_tvar in
      let* tvalue = fresh_tvar in
      let* () =
        assertEqual
          x.et
          (Type.map ~big:(Unknown.value false) ~tkey ~tvalue)
          ~pp:(fun () -> [`Expr x; `Text ".items()"; `Line line_no])
      in
      return (Type.list (Type.key_value tkey tvalue))
  | EListKeys _rev ->
      let* tkey = fresh_tvar in
      let* tvalue = fresh_tvar in
      let* () =
        assertEqual
          x.et
          (Type.map ~big:(Unknown.value false) ~tkey ~tvalue)
          ~pp:(fun () -> [`Expr x; `Text ".keys()"; `Line line_no])
      in
      return (Type.list tkey)
  | EListValues _rev ->
      let* tkey = fresh_tvar in
      let* tvalue = fresh_tvar in
      let* () =
        assertEqual
          x.et
          (Type.map ~big:(Unknown.value false) ~tkey ~tvalue)
          ~pp:(fun () -> [`Expr x; `Text ".values()"; `Line line_no])
      in
      return (Type.list tvalue)
  | EListElements _rev ->
      let* telement = fresh_tvar in
      let* () =
        assertEqual x.et (Type.set ~telement) ~pp:(fun () ->
            [`Expr x; `Text ".elements()"; `Line line_no])
      in
      return (Type.list telement)
  | EAddress ->
      let* t = fresh_tvar in
      let* () =
        assertEqual x.et (Type.contract t) ~pp:(fun () ->
            [`Text "sp.to_address("; `Expr x; `Text ")"; `Line line_no])
      in
      return Type.address
  | EType_annotation t ->
      let pp () =
        [ `Text "Mismatch type annotation"
        ; `Expr x
        ; `Br
        ; `Text "Expected type:"
        ; `Br
        ; `Type t
        ; `Line line_no ]
      in
      let* () = assertEqual x.et t ~pp in
      return t
  | EVariant name -> for_variant ~line_no name x.et
  | EIsVariant name ->
      let* t = fresh_tvar in
      let tvariant =
        match name with
        | "None" | "Some" -> Type.option t
        | _ -> Type.uvariant name t
      in
      let* () =
        assertEqual x.et tvariant ~pp:(fun () ->
            [ `Text "Incompatible variant types"
            ; `Br
            ; `Type x.et
            ; `Br
            ; `Text "and"
            ; `Br
            ; `Type tvariant
            ; `Br
            ; `Line line_no ])
      in
      return Type.bool
  | EProject i ->
      let* t = fresh_tvar in
      let* () =
        assertEqual x.et (Type.utuple i t) ~pp:(fun () ->
            [ `Text "Attribute error"
            ; `Expr x
            ; `Text "has no component"
            ; `Text (string_of_int i)
            ; `Text "of type"
            ; `Type t
            ; `Br
            ; `Line line_no ])
      in
      return t
  | EAttr name ->
      let* t = fresh_tvar in
      let* () =
        assertEqual
          x.et
          (Type.urecord [(name, t)])
          ~pp:(fun () ->
            [ `Text "Attribute error"
            ; `Expr x
            ; `Text "has no field"
            ; `Text name
            ; `Text "of type"
            ; `Type t
            ; `Br
            ; `Line line_no ])
      in
      return t
  | EReadTicket ->
      let* t = fresh_tvar in
      let* () =
        assertEqual x.et (Type.ticket t) ~pp:(fun () ->
            [`Expr x; `Text "is not a valid ticket"; `Line line_no])
      in
      let t_data = Type.tuple [Type.address; t; Type.nat ()] in
      return (Type.pair t_data x.et)
  | EJoinTickets ->
      let* t = fresh_tvar in
      let ticket_t = Type.ticket t in
      let* () =
        assertEqual x.et (Type.pair ticket_t ticket_t) ~pp:(fun () ->
            [`Expr x; `Text "is not a valid ticket list"; `Line line_no])
      in
      return (Type.option ticket_t)
  | EPairingCheck ->
      let* () =
        assertEqual
          x.et
          (Type.list (Type.pair Type.bls12_381_g1 Type.bls12_381_g2))
          ~pp:(fun () ->
            [`Expr x; `Text "is not a valid list of BLS12 pairs"; `Line line_no])
      in
      return Type.bool
  | EVotingPower ->
      let* config = get_config in
      let* () =
        assertEqual
          x.et
          (Type.baker_type_of_protocol config.protocol)
          ~pp:(fun () ->
            [ `Expr x
            ; `Text "voting_power requires tz1[1-3] address as parameter"
            ; `Line line_no ])
      in
      return (Type.nat ())
  | EUnbounded ->
      let* t = fresh_tvar in
      let* () =
        assertEqual x.et (Type.bounded t false []) ~pp:(fun () ->
            [ `Expr x
            ; `Text "voting_power requires tz1[1-3] address as parameter"
            ; `Line line_no ])
      in
      return t

let check_prim2 ~line_no e p x y =
  let assertEqual = assertEqual ~line_no in
  let add_constraint = add_constraint ~line_no in
  let pp_bin = pp_bin ~line_no in
  let pp_bin_op = pp_bin_op ~line_no in
  match p with
  | EBinOpInf op ->
    ( match op with
    | BLe | BLt | BGe | BGt | BEq | BNeq ->
        let* () = checkComparable ~line_no x y in
        return Type.bool
    | BAdd ->
        let* () = assertEqual x.et y.et ~pp:(pp_bin_op "+" x y) in
        let e = build_texpr ~line_no e x.et in
        let* () = add_constraint (HasAdd (e, x, y)) in
        return x.et
    | BMul {overloaded} ->
        let* t = fresh_tvar in
        let e = build_texpr ~line_no e t in
        let* () = add_constraint (HasMul (e, x, y, overloaded)) in
        if overloaded
        then return t
        else
          let pp = pp_bin_op "*" x y in
          let* () = assertEqual x.et y.et ~pp in
          let* () = assertEqual e.et x.et ~pp in
          return x.et
    | BMod ->
        let pp = pp_bin_op "%" x y in
        let* () = assertEqual x.et y.et ~pp in
        let* () = assertEqual x.et (Type.intOrNat ()) ~pp in
        return (Type.nat ())
    | BEDiv ->
        let* a = fresh_tvar in
        let* b = fresh_tvar in
        let et = Type.option (Type.pair a b) in
        let e = build_texpr ~line_no e et in
        let* () = add_constraint (HasDiv (e, x, y)) in
        return et
    | BDiv ->
        let pp () =
          [ `Text "in expression"
          ; `Expr x
          ; `Text "/"
          ; `Expr y
          ; `Br
          ; `Text "Both expressions must be of type"
          ; `Type (Type.nat ())
          ; `Br
          ; `Line line_no ]
        in
        let* () = assertEqual x.et y.et ~pp in
        let* () = assertEqual x.et (Type.nat ()) ~pp in
        return (Type.nat ())
    | BSub ->
        let pp = pp_bin_op "-" x y in
        let* () = assertEqual x.et y.et ~pp in
        let* et = fresh_tvar in
        let e = build_texpr ~line_no e et in
        let* () = add_constraint (HasSub (e, x, y)) in
        return et
    | BOr ->
        let pp () =
          [ `Text "bad type for or:"
          ; `Expr x
          ; `Text "|"
          ; `Expr y
          ; `Text "is not a nat or a bool"
          ; `Br
          ; `Line line_no ]
        in
        let* () = assertEqual x.et y.et ~pp in
        let e = build_texpr ~line_no e x.et in
        let* () = add_constraint (HasBitArithmetic (e, x, y)) in
        return x.et
    | BAnd ->
        let pp () =
          [ `Text "bad type for and:"
          ; `Expr x
          ; `Text "&"
          ; `Expr y
          ; `Text "is not a nat or a bool"
          ; `Br
          ; `Line line_no ]
        in
        let* () = assertEqual x.et y.et ~pp in
        let e = build_texpr ~line_no e x.et in
        let* () = add_constraint (HasBitArithmetic (e, x, y)) in
        return x.et
    | BXor ->
        let pp () =
          [ `Text "bad type for xor:"
          ; `Expr x
          ; `Text "^"
          ; `Expr y
          ; `Text "is not a nat or a bool"
          ; `Br
          ; `Line line_no ]
        in
        let* () = assertEqual x.et y.et ~pp in
        let e = build_texpr ~line_no e x.et in
        let* () = add_constraint (HasBitArithmetic (e, x, y)) in
        return x.et )
  | EBinOpPre BMax ->
      let pp = pp_bin "max" x y in
      let* () = assertEqual x.et y.et ~pp in
      let* () = assertEqual x.et (Type.intOrNat ()) ~pp in
      return x.et
  | EBinOpPre BMin ->
      let pp = pp_bin "min" x y in
      let* () = assertEqual x.et y.et ~pp in
      let* () = assertEqual x.et (Type.intOrNat ()) ~pp in
      return x.et
  | EContains ->
      let* () = add_constraint (HasContains (x, y, line_no)) in
      return Type.bool
  | ECons ->
      let x, l = (x, y) in
      let* () =
        assertEqual l.et (Type.list x.et) ~pp:(fun () ->
            [`Text "cannot cons"; `Exprs [x; l]; `Line line_no])
      in
      return l.et
  | EGetOpt ->
      let* r = fresh_tvar in
      let pp () =
        [ `Text "getOpt expects map with values of type"
        ; `Type r
        ; `Br
        ; `Line line_no ]
      in
      let* () =
        assertEqual
          x.et
          (Type.map ~big:(Unknown.unknown ()) ~tkey:y.et ~tvalue:r)
          ~pp
      in
      return (Type.option r)
  | EAdd_seconds ->
      let t, s = (x, y) in
      let pp () =
        [`Text "sp.add_seconds("; `Exprs [t; s]; `Text ")"; `Line line_no]
      in
      let* () = assertEqual Type.timestamp t.et ~pp in
      let* () = assertEqual (Type.int ()) s.et ~pp in
      return Type.timestamp
  | ECallLambda ->
      let lambda, parameter = (x, y) in
      let* t = fresh_tvar in
      let* () =
        assertEqual lambda.et (Type.lambda parameter.et t) ~pp:(fun () ->
            [ `Expr lambda
            ; `Text "does not apply to"
            ; `Expr parameter
            ; `Line line_no ])
      in
      return t
  | EApplyLambda ->
      let lambda, parameter = (x, y) in
      let* t1 = fresh_tvar in
      let* t2 = fresh_tvar in
      let* () =
        assertEqual
          lambda.et
          (Type.lambda (Type.pair parameter.et t1) t2)
          ~pp:(fun () ->
            [ `Expr lambda
            ; `Text "does not apply to"
            ; `Expr parameter
            ; `Line line_no ])
      in
      return (Type.lambda t1 t2)
  | ETicket ->
      let content, amount = (x, y) in
      let* () =
        assertEqual amount.et (Type.nat ()) ~pp:(fun () ->
            [`Expr amount; `Text "is not a valid ticket amount"; `Line line_no])
      in
      return (Type.ticket content.et)
  | ESplitTicket ->
      let ticket, decomposition = (x, y) in
      let* t = fresh_tvar in
      let ticket_t = Type.ticket t in
      let* () =
        assertEqual ticket.et ticket_t ~pp:(fun () ->
            [`Expr ticket; `Text "is not a ticket"; `Line line_no])
      in
      let* () =
        assertEqual
          decomposition.et
          (Type.pair (Type.nat ()) (Type.nat ()))
          ~pp:(fun () ->
            [ `Expr decomposition
            ; `Text "is not a ticket decomposition (nat * nat) for"
            ; `Expr ticket
            ; `Line line_no ])
      in
      return (Type.option (Type.pair ticket_t ticket_t))

let check_prim3 ~line_no p x y z =
  let assertEqual = assertEqual ~line_no in
  let add_constraint = add_constraint ~line_no in
  match p with
  | ERange ->
      let a, b, step = (x, y, z) in
      let pp () =
        [ `Text "Range takes integers, it cannot be applied to"
        ; `Exprs [a; b; step]
        ; `Br
        ; `Line line_no ]
      in
      let* () = assertEqual a.et b.et ~pp in
      let* () = assertEqual a.et step.et ~pp in
      let* () = add_constraint (IsInt (a.et, pp)) in
      return (Type.list a.et)
  | ESplit_tokens ->
      let mutez, quantity, total = (x, y, z) in
      let pp () =
        [ `Text "sp.split_tokens("
        ; `Exprs [mutez; quantity; total]
        ; `Text ")"
        ; `Line line_no ]
      in
      let* () = assertEqual Type.token mutez.et ~pp in
      let* () = assertEqual (Type.nat ()) quantity.et ~pp in
      let* () = assertEqual (Type.nat ()) total.et ~pp in
      return Type.token
  | EUpdate_map ->
      let map, key, value = (x, y, z) in
      let* tvalue = fresh_tvar in
      let pp () =
        [ `Expr value
        ; `Text "is of type"
        ; `Type value.et
        ; `Text ", but should be an option."
        ; `Line line_no ]
      in
      let* () = assertEqual value.et (Type.option tvalue) ~pp in
      let pp () =
        [ `Text "Map is of type"
        ; `Type map.et
        ; `Text ", but used as map from"
        ; `Type key.et
        ; `Text " to "
        ; `Type tvalue
        ; `Line line_no ]
      in
      let big = Unknown.unknown () in
      let mt = Type.map ~big ~tkey:key.et ~tvalue in
      let* () = assertEqual map.et mt ~pp in
      return map.et
  | EGet_and_update ->
      let map, key, value = (x, y, z) in
      let* tvalue = fresh_tvar in
      let pp () =
        [ `Expr value
        ; `Text "is of type"
        ; `Type value.et
        ; `Text ", but should be an option."
        ; `Line line_no ]
      in
      let* () = assertEqual value.et (Type.option tvalue) ~pp in
      let pp () =
        [ `Text "Map is of type"
        ; `Type map.et
        ; `Text ", but used as map from"
        ; `Type key.et
        ; `Text " to "
        ; `Type tvalue
        ; `Line line_no ]
      in
      let big = Unknown.unknown () in
      let mt = Type.map ~big ~tkey:key.et ~tvalue in
      let* () = assertEqual map.et mt ~pp in
      return (Type.pair value.et map.et)
  | EIf ->
      let cond, a, b = (x, y, z) in
      let* () =
        assertEqual a.et b.et ~pp:(fun () ->
            [`Text "incompatible result types in if-then-else"; `Line line_no])
      in
      let* () =
        assertEqual cond.et Type.bool ~pp:(fun () ->
            [`Text "non-boolean condition in if-then-else"; `Line line_no])
      in
      return a.et
  | ETest_ticket ->
      let ticketer, content, amount = (x, y, z) in
      let* () =
        assertEqual ticketer.et Type.address ~pp:(fun () ->
            [ `Expr ticketer
            ; `Text "is not a valid ticketer (it should be an address)"
            ; `Line line_no ])
      in
      let* () =
        assertEqual amount.et (Type.nat ()) ~pp:(fun () ->
            [`Expr amount; `Text "is not a valid ticket amount"; `Line line_no])
      in
      return (Type.ticket content.et)

let check_lambda ~line_no {id; name; body; clean_stack} =
  scoped_no_occs
    (let* () = if clean_stack then set_vars String.Map.empty else return () in
     let* tParams = fresh_tvar in
     let* tResult = fresh_tvar in
     let* () = add_var ~line_no (Format.sprintf "lambda %d" id) tParams in
     let* body = body in
     let* () =
       assertEqual ~line_no body.ct tResult ~pp:(fun () ->
           [ `Text "lambda result type"
           ; `Type body.ct
           ; `Text "is not"
           ; `Type tResult
           ; `Line line_no ])
     in
     return
       {id; name; tParams = T tParams; body; tResult = T tResult; clean_stack})

let check_expr_f line_no (ee : (texpr t, tcommand t, Type.t, untyped) expr_f) =
  let assertEqual = assertEqual ~line_no in
  let add_constraint = add_constraint ~line_no in
  let check_var = check_var ~line_no in
  match ee with
  | ELambda {id; name; tParams; body; tResult; clean_stack} ->
      let* l =
        check_lambda ~line_no {id; name; tParams; body; tResult; clean_stack}
      in
      return
        (build_texpr
           ~line_no
           (ELambda l)
           (Type.lambda (get_extra l.tParams) (get_extra l.tResult)))
  | ECreate_contract {contract_template; baker; balance; storage} ->
      let* config = get_config in
      let* baker = baker in
      let* balance = balance in
      let* storage = storage in
      let* () =
        assertEqual
          baker.et
          (Type.option (Type.baker_type_of_protocol config.protocol))
          ~pp:(fun () ->
            [ `Expr baker
            ; `Text "is not a valid optional baker in sp.contract"
            ; `Line line_no ])
      in
      let* () =
        assertEqual balance.et Type.token ~pp:(fun () ->
            [ `Expr balance
            ; `Text "is not a valid amount in sp.create_contract"
            ; `Line line_no ])
      in
      let* c = check_contract_f ~line_no contract_template in
      let* () =
        assertEqual storage.et (get_extra c.derived).tstorage ~pp:(fun () ->
            [ `Text "sp.create_contract: incompatible storage type"
            ; `Line line_no ])
      in
      let e =
        ECreate_contract {contract_template = c; baker; balance; storage}
      in
      let et =
        Type.record_default_layout
          Config.Comb
          [("operation", Type.operation); ("address", Type.address)]
      in
      return (build_texpr ~line_no e et)
  | e ->
      (* Expressions that don't modify the context. *)
      let* e = sequence_expr_f e in
      let e =
        match e with
        | ECreate_contract _ | ELambda _ ->
            assert false (* Already treated above. *)
        | ( EMPrim0 _ | EMPrim1 _ | EMPrim1_fail _ | EMPrim2 _ | EMPrim3 _
          | EPrim0 _ | EPrim1 _ | EPrim2 _ | EPrim3 _ | EOpenVariant _ | EItem _
          | ESaplingVerifyUpdate _ | EMichelson _ | EMapFunction _
          | ELambdaParams _ | EContract _ | ESlice _ | EMake_signature _
          | ETransfer _ | EMatch _ | ETuple _ | ERecord _ | EList _ | EMap _
          | ESet _ | EHasEntryPoint _ ) as e ->
            e
      in
      let* et =
        match e with
        | ELambda _ | ECreate_contract _ -> assert false
        | EList xs ->
            let* t = fresh_tvar in
            let item x =
              assertEqual x.et t ~pp:(fun () ->
                  [ `Text "bad type for list item"
                  ; `Expr x
                  ; `Text "is not a"
                  ; `Type t
                  ; `Line line_no ])
            in
            let* () = iter_list item xs in
            return (Type.list t)
        | EMPrim0 p -> check_mprim0 ~line_no p
        | EMPrim1 (p, x) -> check_mprim1 ~line_no p x
        | EMPrim1_fail _ -> assert false
        | EMPrim2 (p, x1, x2) -> check_mprim2 ~line_no p x1 x2
        | EMPrim3 (p, x1, x2, x3) -> check_mprim3 ~line_no p x1 x2 x3
        | EPrim0 p -> check_prim0 ~line_no p
        | EPrim1 (p, x) -> check_prim1 ~line_no p x
        | EPrim2 (p, x, y) -> check_prim2 ~line_no e p x y
        | EPrim3 (p, x, y, z) -> check_prim3 ~line_no p x y z
        | EOpenVariant (name, x, _missing_message) ->
            let* et =
              match name with
              | "None" -> return Type.unit
              | _ -> fresh_tvar
            in
            let tv =
              match name with
              | "Some" | "None" -> Type.option et
              | _ -> Type.uvariant name et
            in
            let* () =
              assertEqual x.et tv ~pp:(fun () ->
                  [ `Text "Variant error"
                  ; `Expr x
                  ; `Text "has no constructor"
                  ; `Text name
                  ; `Br
                  ; `Line line_no ])
            in
            return et
        | ELambdaParams {id} ->
            check_var "Lambda" (Format.sprintf "lambda %d" id)
        | ESlice {offset; length; buffer} ->
            let pp expr () = [`Text "sp.slice"; `Expr expr; `Line line_no] in
            let* () = assertEqual offset.et (Type.nat ()) ~pp:(pp offset) in
            let* () = assertEqual length.et (Type.nat ()) ~pp:(pp length) in
            let* () = add_constraint (HasSlice buffer) in
            return (Type.option buffer.et)
        | EItem {items; key; default_value} ->
            let* et = fresh_tvar in
            let* () = add_constraint (HasGetItem (items, key, et)) in
            let* () =
              match default_value with
              | None -> return ()
              | Some e ->
                  assertEqual e.et et ~pp:(fun () ->
                      [ `Expr items
                      ; `Text "bad type for default value."
                      ; `Expr e
                      ; `Br
                      ; `Line line_no ])
            in
            return et
        | ERecord entries ->
            let layout = Unknown.unknown () in
            let row = List.map (map_snd (fun {et} -> et)) entries in
            return (Type.record_or_unit layout row)
        | EMap (big, entries) ->
            let* tkey = fresh_tvar in
            let* tvalue = fresh_tvar in
            let item (k, v) =
              let* () =
                assertEqual k.et tkey ~pp:(fun () ->
                    [ `Text "bad type for map key"
                    ; `Expr k
                    ; `Text "is not a"
                    ; `Type tkey
                    ; `Br
                    ; `Line line_no ])
              in
              let* () =
                assertEqual v.et tvalue ~pp:(fun () ->
                    [ `Text "bad type for map value"
                    ; `Expr v
                    ; `Text "is not a"
                    ; `Type tvalue
                    ; `Br
                    ; `Line line_no ])
              in
              return ()
            in
            let* () = iter_list item entries in
            return (Type.map ~big:(Unknown.value big) ~tkey ~tvalue)
        | ESet entries ->
            let* telement = fresh_tvar in
            let check k =
              assertEqual k.et telement ~pp:(fun () ->
                  [ `Text "bad type for set element"
                  ; `Expr k
                  ; `Text "is not an element of"
                  ; `Type (Type.set ~telement)
                  ; `Br
                  ; `Line line_no ])
            in
            let* () = iter_list check entries in
            return (Type.set ~telement)
        | ETransfer {arg; amount; destination} ->
            let pp () =
              [ `Text "transfer("
              ; `Exprs [arg; amount; destination]
              ; `Text ")"
              ; `Br
              ; `Line line_no ]
            in
            let* () = assertEqual destination.et (Type.contract arg.et) ~pp in
            let* () = assertEqual amount.et Type.token ~pp in
            return Type.operation
        | EMake_signature {secret_key; message} ->
            let pp e () = [`Text "sp.make_signature"; `Expr e; `Line line_no] in
            let* () =
              assertEqual Type.secret_key secret_key.et ~pp:(pp secret_key)
            in
            let* () = assertEqual Type.bytes message.et ~pp:(pp message) in
            return Type.signature
        | EContract {arg_type; address} ->
            let* () =
              assertEqual address.et Type.address ~pp:(fun () ->
                  [`Expr address])
            in
            return (Type.option (Type.contract arg_type))
        | EMapFunction {l; f} as e ->
            let* s = fresh_tvar in
            let* t = fresh_tvar in
            let* target = fresh_tvar in
            let* () =
              assertEqual f.et (Type.lambda s t) ~pp:(fun () ->
                  [`Text "map"; `Expr f; `Line line_no])
            in
            let e = build_texpr ~line_no e target in
            let* () = add_constraint (HasMap (e, l, f)) in
            return target
        | EMichelson (michelson, exprs) ->
            let pp () =
              [ `Text "sp.michelson"
              ; `Text michelson.name
              ; `Exprs exprs
              ; `Line line_no ]
            in
            let rec matchTypes types name tin out =
              match (types, tin) with
              | _, [] -> return (out @ types)
              | [], _ ->
                  raise
                    (SmartExcept
                       [`Text "Empty stack at instruction"; `Rec (pp ())])
              | tt1 :: types, tt2 :: tin ->
                  let* () = assertEqual tt1 tt2 ~pp in
                  matchTypes types name tin out
            in
            let* typesOut =
              let* types = return (List.map (fun e -> e.et) exprs) in
              matchTypes
                types
                michelson.name
                michelson.typesIn
                michelson.typesOut
            in
            let err message =
              raise (SmartExcept [`Text message; `Rec (pp ()); `Line line_no])
            in
            ( match typesOut with
            | [] -> err "Empty output types"
            | [t] -> return t
            | _ -> err "Too many output types" )
        | ETuple es -> return (Type.tuple (List.map (fun {et} -> et) es))
        | EMatch (scrutinee, clauses) ->
            let* result_t = fresh_tvar in
            let* row =
              clauses
              |> map_list (fun (constructor, rhs) ->
                     let* arg_t = fresh_tvar in
                     let* () =
                       assertEqual
                         rhs.et
                         (Type.lambda arg_t result_t)
                         ~pp:(fun () ->
                           [ `Text "incompatible result types in match"
                           ; `Line line_no ])
                     in
                     return (constructor, arg_t))
            in
            let* () =
              assertEqual
                scrutinee.et
                (Type.variant (Unknown.unknown ()) row)
                ~pp:(fun () -> [`Text "incoherent scrutinee type"])
            in
            return result_t
        | ESaplingVerifyUpdate {state; transaction} ->
            let* () =
              assertEqual
                transaction.et
                (Type.sapling_transaction None)
                ~pp:(fun () ->
                  [ `Expr transaction
                  ; `Text "is expected to be a sapling transaction "
                  ; `Line line_no ])
            in
            let* () =
              assertEqual state.et (Type.sapling_state None) ~pp:(fun () ->
                  [ `Expr state
                  ; `Text "is expected to be a sapling state "
                  ; `Line line_no ])
            in
            let* () = add_constraint (SaplingVerify (state, transaction)) in
            return (Type.option (Type.pair (Type.int ()) state.et))
        | EHasEntryPoint name ->
            let* tparameter_lazy =
              check_var "Self parameter (lazy)" "__parameter_lazy__"
            in
            let* () =
              let* t_ep = fresh_tvar in
              assertEqual
                (Type.uvariant name t_ep)
                tparameter_lazy
                ~pp:(fun () ->
                  [`Text "sp.has_entry_point: incompatible type"; `Line line_no])
            in
            return Type.bool
      in
      return (build_texpr ~line_no e et)

let check = {f_expr = check_expr_f; f_command = check_command_f; f_type = id}

let check_command x = cata_command check x

let check_expr = cata_expr check

let check_contract ({contract} : contract) =
  let c = map_contract_f check_expr check_command id id contract in
  let* tcontract = check_contract_f ~line_no:[] c in
  return {tcontract}

let check_action = function
  | New_contract {id; contract; line_no; accept_unknown_types; show} ->
      let* ({tcontract = c} as contract) = check_contract {contract} in
      let {tparameter; tparameter_full} = get_extra c.derived in
      let* () =
        add_var ~line_no (string_of_contract_id id) (Type.contract tparameter)
      in
      let* () =
        add_var
          ~line_no
          (string_of_contract_id id ^ "_full")
          (Type.contract tparameter_full)
      in
      let* () =
        add_var
          ~line_no
          (string_of_contract_id id ^ "_storage")
          (get_extra contract.tcontract.derived).tstorage
      in
      return
        (New_contract
           { id
           ; contract = contract.tcontract
           ; line_no
           ; accept_unknown_types
           ; show })
  | Compute {id; expression; line_no} ->
      let* expression = check_expr expression in
      let* () = add_var ~line_no (string_of_scenario_var id) expression.et in
      return (Compute {id; expression; line_no})
  | Simulation {id; line_no} -> return (Simulation {id; line_no})
  | Message
      { id
      ; valid
      ; exception_
      ; params
      ; line_no
      ; title
      ; messageClass
      ; source
      ; sender
      ; chain_id
      ; time
      ; amount
      ; level
      ; voting_powers
      ; message
      ; show
      ; export } ->
      let check_expr = check_expr in
      let* valid = check_expr valid in
      let* exception_ = map_option check_expr exception_ in
      let* params = check_expr params in
      let* voting_powers = check_expr voting_powers in
      let check_aa = function
        | Address a ->
            let* a = check_expr a in
            return (Address a)
        | Account a -> return (Account a)
      in
      let* source = map_option check_aa source in
      let* sender = map_option check_aa sender in
      let* chain_id = map_option check_expr chain_id in
      let* time = check_expr time in
      let* amount = check_expr amount in
      let* level = check_expr level in
      let id' = string_of_contract_id id in
      let* c =
        match id with
        | C_dynamic _ -> check_var ~line_no "contract id" id'
        | _ -> check_var ~line_no "contract id" (id' ^ "_full")
      in
      let* entry = fresh_tvar in
      let* () =
        let pp () =
          [ `Text id'
          ; `Text "is of type"
          ; `Type c
          ; `Text "which lacks entry point"
          ; `Text message
          ; `Text "with argument"
          ; `Type entry
          ; `Line line_no ]
        in
        assertEqual ~line_no ~pp c (Type.contract (Type.uvariant message entry))
      in
      let pp () =
        [ `Text "entry point expects parameter"
        ; `Br
        ; `Type entry
        ; `Br
        ; `Text "but got"
        ; `Br
        ; `Type params.et
        ; `Line line_no ]
      in
      let* () = assertEqual ~line_no params.et entry ~pp in
      return
        (Message
           { id
           ; valid
           ; exception_
           ; params
           ; line_no
           ; title
           ; messageClass
           ; source
           ; sender
           ; chain_id
           ; time
           ; amount
           ; level
           ; voting_powers
           ; message
           ; show
           ; export })
  | ScenarioError {message} -> return (ScenarioError {message})
  | Html {tag; inner; line_no} -> return (Html {tag; inner; line_no})
  | Verify {condition; line_no} ->
      let* condition = check_expr condition in
      let pp () = [`Text "not a boolean expression"; `Line line_no] in
      let* () = assertEqual ~line_no condition.et Type.bool ~pp in
      return (Verify {condition; line_no})
  | Show {expression; html; stripStrings; compile; line_no} ->
      let* expression = check_expr expression in
      return (Show {expression; html; stripStrings; compile; line_no})
  | Exception es -> return (Exception es)
  | Add_flag flag ->
      let* config = get_config in
      Basics.check_initial_flag ~line_no:flag.line_no flag.flag;
      let* () = set_config (Config.apply_flag config flag.flag) in
      return (Add_flag flag)
  | Set_delegate {id; line_no; baker} ->
      let* baker = check_expr baker in
      let id' = string_of_contract_id id in
      let* _ = check_var ~line_no "contract id" id' in
      let pp () = [`Text "scenario set_delegate"; `Type baker.et] in
      let* config = get_config in
      let* () =
        assertEqual
          ~line_no:[]
          baker.et
          (Type.option (Type.baker_type_of_protocol config.protocol))
          ~pp
      in
      return (Set_delegate {id; line_no; baker})
  | DynamicContract {id; model_id; line_no} ->
      let id' = string_of_dynamic_id id in
      let model_id' = string_of_contract_id model_id in
      let* tcontract = check_var ~line_no "contract id" model_id' in
      let* tstorage =
        check_var ~line_no "contract id" (model_id' ^ "_storage")
      in
      let* () = add_var ~line_no id' tcontract in
      let* () = add_var ~line_no (id' ^ "_storage") tstorage in
      return (DynamicContract {id; model_id; line_no})

let default_if condition ~line_no t =
  let open Type in
  match Type.getRepr t with
  | TUnknown {contents = UUnknown _} as t when condition ->
      let* () =
        assertEqual ~line_no (F t) Type.unit ~pp:(fun () -> [`Text "defaulter"])
      in
      return Type.unit
  | t -> return (F t)

let is_storage = function
  | {e = EPrim0 (ELocal "__storage__"); et = _} -> true
  | _ -> false

let rec default_contract {tcontract = c} =
  let ({ tparameter
       ; tparameter_lazy
       ; tparameter_non_lazy
       ; tparameter_full
       ; tstorage }
        : _ contract_derived) =
    get_extra c.derived
  in
  let uses_storage =
    exists_contract ~exclude_create_contract:true is_storage (fun _ -> false) c
  in
  let* tstorage = default_if (not uses_storage) ~line_no:[] tstorage in
  let* tparameter = default_if (c.entry_points = []) ~line_no:[] tparameter in
  let* entry_points = map_list default_entry_point c.entry_points in
  let* global_variables =
    let f (n, e) =
      let* e = cata_texpr default_alg e in
      return (n, e)
    in
    map_list f c.global_variables
  in
  let* () = solve_constraints in
  let c =
    { c with
      entry_points
    ; global_variables
    ; derived =
        T
          { tparameter
          ; tparameter_lazy
          ; tparameter_non_lazy
          ; tparameter_full
          ; tstorage } }
  in
  match (c.storage, Type.getRepr tstorage) with
  | None, Type.T0 T_unit ->
      let storage =
        Some (build_texpr ~line_no:[] (EPrim0 (ECst Literal.unit)) Type.unit)
      in
      return {tcontract = {c with storage}}
  | _ -> return {tcontract = c}

and default_entry_point (ep : _ entry_point) =
  let* body = cata_tcommand default_alg ep.body in
  return {ep with body}

and default_alg =
  let f_texpr line_no et e =
    let* e = sequence_expr_f e in
    match e with
    | ECreate_contract {contract_template = ct; baker; balance; storage} ->
        let* ct = default_contract {tcontract = ct} in
        return
          (build_texpr
             ~line_no
             (ECreate_contract
                {contract_template = ct.tcontract; baker; balance; storage})
             et)
    | e -> return (build_texpr ~line_no e et)
  in
  let f_tcommand line_no ct = function
    | CBind (x, c1, c2) ->
        let* c1 = c1 in
        let* c2 = c2 in
        return (build_tcommand ~line_no (CBind (x, c1, c2)) ct)
    | c ->
        let* c = sequence_command_f c in
        return (build_tcommand ~line_no c ct)
  in
  {f_texpr; f_tcommand; f_ttype = (fun t -> t)}

let mapM_action_contract f = function
  | New_contract ({contract} as c) ->
      let* y = f {tcontract = contract} in
      return (New_contract {c with contract = y.tcontract})
  | ( Compute _ | Simulation _ | Message _ | ScenarioError _ | Html _ | Verify _
    | Show _ | Exception _ | Set_delegate _ | DynamicContract _ | Add_flag _ )
    as c ->
      return c

let default_action = mapM_action_contract default_contract

(** {1 Non-monadic interface for the outside world} *)

let run_with config f vars x =
  run
    (f x)
    { tvar_counter = 0
    ; vars
    ; constraints = []
    ; inside_contract = false
    ; config
    ; warnings = [] }

let exec_with config f vars x = fst (run_with config f vars x)

let check_lambda config vars =
  exec_with
    config
    (fun {id; name; body; clean_stack} ->
      let body = check_command body in
      let e = {id; name; body; clean_stack; tParams = U; tResult = U} in
      let* e = check_lambda ~line_no:[] e in
      let* () = solve_constraints in
      return e)
    (mk_env vars)

let check_expr config vars =
  exec_with
    config
    (fun e ->
      let* e = check_expr e in
      let* () = solve_constraints in
      return e)
    (mk_env vars)

let check_command config vars =
  exec_with
    config
    (fun c ->
      let* c = check_command c in
      let* () = solve_constraints in
      return c)
    (mk_env vars)

let check_contract config =
  exec_with
    config
    (fun c ->
      let* c = check_contract c in
      let* () = solve_constraints in
      let* c = default_contract c in
      let* () = solve_constraints in
      return c)
    empty_env

let check_scenario config s =
  let x, {warnings} =
    run_with
      config
      (fun s ->
        let* actions = Action.map_list check_action s.actions in
        let* () = solve_constraints in
        let* actions = Action.map_list default_action actions in
        return {s with actions})
      empty_env
      s
  in
  (x, warnings)

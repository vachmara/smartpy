(* Copyright 2019-2021 Smart Chain Arena LLC. *)

(** {1 Commands} *)

open Basics
open Untyped

(**

Commands represent actions than append in a smart contract.
They can carry expressions that perform computations.

Commands are built by calling the functions defined in this module
 or by using the SmartML ppx syntax.

{2 Direct Commands}
Commands can be built directly by using the functions in this module.
This is a complex and tedious process that should only be used by the
more adventurous developers.

{2 PPX Syntax}
This is the intended syntax for most circumstances.
This is both very convenient and powerful.

{3 Examples, Commands in a Entry Points}

{[
  let%entry_point double self () =
  self.data.storedValue <- self.data.storedValue * 2;
]}

{[
  let%entry_point divide self (params : divide_params) =
    verify (params.divisor > 5) "params.divisor is too small";
    self.data.storedValue <- self.data.storedValue / params.divisor
]}

{2 Types}
Commands in this module are untyped, which means that they do not carry
 any type information. They are typed, by {!SmartML}, in a later stage, typically in a {!Scenario}.

 *)

(** The type of commands. *)
type t = command

val ifte : line_no:line_no -> expr -> t -> t -> t
(** In a SmartML ppx context,
{[ if _condition_ then _then_consequence_ else _else_consequence_ ]}
 *)

val ifteSome : line_no:line_no -> Expr.t -> t -> t -> t

val mk_match : line_no:line_no -> expr -> (string * string * t) list -> t
(** In a SmartML ppx context,
{[ match _expression_ with
   | _constr_ _var -> ...
   | ... ]}
 *)

val sp_failwith : line_no:line_no -> expr -> t
(** In a SmartML ppx context,
{[ failwith _expression_ ]}

As in Michelson, {!failwith} can be applied to an expression of any type.
 *)

val never : line_no:line_no -> expr -> t

val verify : line_no:line_no -> expr -> expr option -> t
(** In a SmartML ppx context,
{[ verify _condition_ _error_message_ ]}

As for {!failwith}, the [_error_message_] is an expression of any type.
 *)

val forGroup : line_no:line_no -> string -> expr -> t -> t

val whileLoop : line_no:line_no -> expr -> t -> t

val delItem : line_no:line_no -> expr -> expr -> t

val updateSet : line_no:line_no -> expr -> expr -> bool -> t

val set : line_no:line_no -> expr -> expr -> t

val defineLocal : line_no:line_no -> string -> expr -> t

val bind : line_no:line_no -> string option -> t -> t -> t

val seq : line_no:line_no -> t list -> t

val setType : line_no:line_no -> expr -> Type.t -> t

val result : line_no:line_no -> expr -> t

val mk_match_cons : line_no:line_no -> expr -> string -> t -> t -> t

val mk_match_product : line_no:line_no -> expr -> pattern -> t -> t

val mk_modify_product : line_no:line_no -> expr -> pattern -> t -> t

val comment : line_no:line_no -> string -> t

val set_result_type : line_no:line_no -> t -> Type.t -> t

val set_type : line_no:line_no -> expr -> Type.t -> t

val trace : line_no:line_no -> expr -> t

val set_entry_point : line_no:line_no -> string -> expr -> t

(* Copyright 2019-2021 Smart Chain Arena LLC. *)
open Basics
open Untyped

val replace_expr : (contract_id, expr) Hashtbl.t -> expr -> expr

val replace_command : (contract_id, expr) Hashtbl.t -> command -> command

val replace_contract : (contract_id, expr) Hashtbl.t -> contract -> contract

(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Utils.Misc
open Basics

type loaded_scenario =
  { scenario : tscenario
  ; scenario_state : Basics.scenario_state
  ; warnings : smart_except list list }

let action_of_json ~primitives ~env x =
  let module M = (val json_getter x : JsonGetter) in
  let open M in
  let import_expr_string s =
    Import.import_expr env (Parsexp.Single.parse_string_exn (string s))
  in
  let _import_type_string s =
    Import.import_type env (Parsexp.Single.parse_string_exn (string s))
  in
  let import_contract_id_string s =
    Import.import_contract_id (Parsexp.Single.parse_string_exn (string s))
  in
  let parse_line_no s =
    Import.import_line_no (Parsexp.Single.parse_string_exn (string s))
  in
  match string "action" with
  | "newContract" ->
      let contract =
        Import.import_contract
          env
          (Parsexp.Single.parse_string_exn (string "export"))
      in
      let id = import_contract_id_string "id" in
      let line_no = parse_line_no "line_no" in
      New_contract
        { id
        ; contract = contract.contract
        ; line_no
        ; accept_unknown_types = bool "accept_unknown_types"
        ; show = bool "show" }
  | "compute" ->
      let expression = import_expr_string "expression" in
      Compute {id = int "id"; expression; line_no = parse_line_no "line_no"}
  | "simulation" ->
      Simulation
        {id = import_contract_id_string "id"; line_no = parse_line_no "line_no"}
  | "message" ->
      let amount = import_expr_string "amount" in
      let of_seed id =
        match string id with
        | "none" -> None
        | seed ->
            if Base.String.is_prefix seed ~prefix:"seed:"
            then
              let module P = (val primitives : Primitives.Primitives) in
              Some
                (Account
                   (P.Crypto.account_of_seed
                      (String.sub seed 5 (String.length seed - 5))))
            else Some (Address (import_expr_string id))
      in
      let sender = of_seed "sender" in
      let source = of_seed "source" in
      let chain_id =
        if string "chain_id" = ""
        then None
        else Some (import_expr_string "chain_id")
      in
      let params = import_expr_string "params" in
      let line_no = parse_line_no "line_no" in
      let message = string "message" in
      let id = import_contract_id_string "id" in
      let exception_ =
        match string "exception" with
        | "None" -> None
        | _ -> Some (import_expr_string "exception")
      in
      Message
        { id
        ; valid = import_expr_string "valid"
        ; exception_
        ; params
        ; line_no
        ; title = string "title"
        ; messageClass = string "messageClass"
        ; sender
        ; source
        ; chain_id
        ; time = import_expr_string "time"
        ; amount
        ; level = import_expr_string "level"
        ; voting_powers = import_expr_string "voting_powers"
        ; message
        ; show = bool "show"
        ; export = bool "export" }
  | "error" -> ScenarioError {message = string "message"}
  | "html" ->
      Html
        { tag = string "tag"
        ; inner = string "inner"
        ; line_no = parse_line_no "line_no" }
  | "verify" ->
      Verify
        { condition = import_expr_string "condition"
        ; line_no = parse_line_no "line_no" }
  | "show" ->
      Show
        { expression = import_expr_string "expression"
        ; html = bool "html"
        ; stripStrings = bool "stripStrings"
        ; compile = bool "compile"
        ; line_no = parse_line_no "line_no" }
  | "dynamic_contract" ->
      DynamicContract
        { id =
            ( match import_contract_id_string "dynamic_id" with
            | C_dynamic dyn -> dyn
            | _ -> assert false )
        ; model_id = import_contract_id_string "model_id"
        ; line_no = parse_line_no "line_no" }
  | "flag" ->
      let flags = string_list "flag" in
      ( match Config.parse_flag flags with
      | None ->
          raise
            (SmartExcept
               [ `Text "Flag parse errors"
               ; `Text
                   (String.concat
                      "; "
                      (List.map (fun s -> Printf.sprintf "%S" s) flags))
               ; `Line (parse_line_no "line_no") ])
      | Some flag -> Add_flag {flag; line_no = parse_line_no "line_no"} )
  | action -> failwith ("Unknown action: '" ^ action ^ "'")

let rec apply_first_flags config = function
  | Add_flag {flag} :: rest ->
      apply_first_flags (Config.apply_flag config flag) rest
  | rest -> (config, rest)

let check_close_scenario config s =
  let s, warnings = Checker.check_scenario config s in
  let scenario = Closer.close_scenario ~config s in
  let scenario_state = Basics.scenario_state config in
  {scenario; scenario_state; warnings}

let load_from_string ~primitives config j =
  let module Printer = (val Printer.get config : Printer.Printer) in
  let j_actions = Yojson.Basic.Util.member "scenario" j in
  let shortname = Yojson.Basic.Util.(to_string (member "shortname" j)) in
  let env = Import.init_env () in
  let kind = {kind = Yojson.Basic.Util.(to_string (member "kind" j))} in
  let actions = ref [] in
  ( try
      List.iter
        (fun action ->
          actions := action_of_json ~primitives ~env action :: !actions)
        (Yojson.Basic.Util.to_list j_actions)
    with
  | SmartExcept exn -> actions := Exception exn :: !actions
  | exn ->
      actions :=
        Exception [`Text (Printer.exception_to_string false exn)] :: !actions );
  (* TODO *)
  let actions = List.rev !actions in
  let config, actions = apply_first_flags config actions in
  let s = {shortname; actions; flags = []; kind} in
  check_close_scenario config s

let check_close_scenario config s = check_close_scenario config s

let scenarios = Hashtbl.create 5

let register ?(flags = []) ~group ~name ~kind actions =
  let scenario = {shortname = name; actions; flags; kind = {kind}} in
  let scenario config = check_close_scenario config scenario in

  let l =
    match Hashtbl.find_opt scenarios group with
    | None -> []
    | Some l -> l
  in
  let l = l @ [scenario] in
  Hashtbl.add scenarios group l

let get group =
  Utils.Option.value ~default:[] (Hashtbl.find_opt scenarios group)

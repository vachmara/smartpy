(* Copyright 2019-2021 Smart Chain Arena LLC. *)

let intType isNat =
  match Unknown.get isNat with
  | None -> `Unknown
  | Some true -> `Nat
  | Some false -> `Int

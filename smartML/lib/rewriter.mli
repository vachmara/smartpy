(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Basics.Untyped

val embellish : config:Config.t -> command -> command

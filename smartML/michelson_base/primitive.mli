(* Copyright 2019-2021 Smart Chain Arena LLC. *)

type 'ty prim0 =
  | Sender
  | Source
  | Amount
  | Balance
  | Now
  | Self                of string option
  | Self_address
  | Chain_id
  | Total_voting_power
  | Sapling_empty_state of {memo : int}
  | Unit_
  | None_               of 'ty
  | Nil                 of 'ty
  | Empty_set           of 'ty
  | Empty_map           of 'ty * 'ty
  | Empty_bigmap        of 'ty * 'ty
[@@deriving eq, ord, show, map, fold]

type 'ty prim1 =
  | Car
  | Cdr
  | Left             of string option * string option * 'ty
  | Right            of string option * string option * 'ty
  | Some_
  | Eq
  | Abs
  | Neg
  | Int
  | IsNat
  | Neq
  | Le
  | Lt
  | Ge
  | Gt
  | Not
  | Concat1
  | Size
  | Address
  | Implicit_account
  | Contract         of string option * 'ty
  | Pack
  | Unpack           of 'ty
  | Hash_key
  | Blake2b
  | Sha256
  | Sha512
  | Keccak
  | Sha3
  | Set_delegate
  | Read_ticket
  | Join_tickets
  | Pairing_check
  | Voting_power
  | Getn             of int
  | Cast             of 'ty
  | Rename           of string option
[@@deriving eq, ord, show, map]

type prim1_fail =
  | Failwith
  | Never
[@@deriving eq, ord, show]

type prim2 =
  | Pair                  of string option * string option
  | Add
  | Mul
  | Sub
  | Lsr
  | Lsl
  | Xor
  | Ediv
  | And
  | Or
  | Cons
  | Compare
  | Concat2
  | Get
  | Mem
  | Exec
  | Apply
  | Sapling_verify_update
  | Ticket
  | Split_ticket
  | Updaten               of int
[@@deriving eq, ord, show]

type prim3 =
  | Slice
  | Update
  | Get_and_update
  | Transfer_tokens
  | Check_signature
[@@deriving eq, ord, show]

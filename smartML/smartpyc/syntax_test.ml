open Smartml

module MyContract = struct
  type t = {storedValue : int} (* [@@deriving smartml]*)

  type replace_params = {value : int}

  type divide_params = {divisor : int}

  let gen_t_replace_params =
    Type.record_default_layout Config.Tree [("value", Type.nat ())]

  let gen_t_divide_params =
    Type.record_default_layout Config.Tree [("divisor", Type.nat ())]

  let%entry_point replace self (params : replace_params) =
    self.data.storedValue <- params.value

  let%entry_point double self () =
    self.data.storedValue <- self.data.storedValue * 2;
    ()

  let%entry_point divide self (params : divide_params) =
    verify (params.divisor > 5) "params.divisor too small";
    verify (params.divisor > 6) 42;
    self.data.storedValue <- self.data.storedValue / params.divisor

  let%entry_point misc self params =
    set_type (params.divisor : int);
    verify (params.divisor > 5) "params.divisor too small";
    verify (params.divisor > 6) 42;
    begin
      match params.bar with
      | A x -> failwith (1 + x)
      | B x -> self.data.storedValue <- x
      | C n -> never n
    end;
    if 2 + params.foo < 42 then failwith params;
    if 1 + params.foo < 14
    then if 2 + params.foo < 42 then failwith params else failwith "abc";
    while self.data.storedValue < 15 do
      self.data.storedValue <- 2 * self.data.storedValue
    done;
    List.iter
      (fun x -> self.data.storedValue <- self.data.storedValue + x)
      params.l

  let another =
    Basics.build_entry_point
      ~name:"another"
      ~originate:false
      [%command self.data.storedValue <- self.data.storedValue * 42]

  let another_with_parameter =
    Basics.build_entry_point
      ~name:"another_with_parameter"
      ~originate:false
      ~tparameter:(Type.nat ())
      [%command self.data.storedValue <- self.data.storedValue * params]

  let init value =
    let storage = [%expr {storedValue = [%e value]}] in
    Basics.build_contract
      ~storage
      [replace; double; divide; misc; another; another_with_parameter]
end

let () =
  Scenario.register
    ~group:"StoreValue"
    ~name:"two_contracts"
    ~kind:"test"
    [%actions
      register_contract (MyContract.init [%expr 123456]);
      register_contract (MyContract.init [%expr 1])]

let () =
  let cst = [%expr 15] in
  Scenario.register
    ~group:"StoreValue"
    ~name:"my_test"
    ~kind:"test"
    [%actions
      let c1 = register_contract (MyContract.init [%expr 42]) in
      h1 "Store Value";
      h2 "Foo";
      verify (c1.data.storedValue = 42);
      call c1.replace {value = abs (11 + (12 / -6) - (11 % 2))};
      verify (c1.data.storedValue = 8);
      call c1.replace {value = 12};
      call c1.replace {value = 13};
      call c1.replace {value = [%e cst]};
      p "Some computation";
      show (-c1.data.storedValue * 12);
      call c1.replace {value = 25};
      call c1.double ();
      call c1.divide {divisor = 2} ~valid:false;
      verify (c1.data.storedValue = 50);
      call c1.divide {divisor = 7};
      verify (c1.data.storedValue <> 9)]

(*
 *   let%var x = c1.data.storedValue * 12 in
 *)

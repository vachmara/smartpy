(* Copyright 2019-2021 Smart Chain Arena LLC. *)

val explore :
     config:Smartml.Config.t
  -> address:string
  -> json:string
  -> operations:string
  -> unit

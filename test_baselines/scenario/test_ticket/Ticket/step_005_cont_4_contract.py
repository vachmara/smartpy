import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(m = sp.TMap(sp.TInt, sp.TTicket(sp.TInt)), x = sp.TInt).layout(("m", "x")))

  @sp.entry_point
  def ep1(self):
    with sp.match_record(self.data, "data") as data:
      match_pair_test_ticket_76_fst, match_pair_test_ticket_76_snd = sp.match_tuple(sp.get_and_update(data.m, 42, sp.none), "match_pair_test_ticket_76_fst", "match_pair_test_ticket_76_snd")
      data.m = match_pair_test_ticket_76_snd
      data.x = 0

  @sp.entry_point
  def ep2(self, params):
    with sp.match_record(self.data, "data") as data:
      ticket_83 = sp.local("ticket_83", sp.ticket('a', 1))
      ticket_84 = sp.local("ticket_84", sp.ticket('b', 2))
      sp.transfer((ticket_83.value, ticket_84.value), sp.tez(0), params)
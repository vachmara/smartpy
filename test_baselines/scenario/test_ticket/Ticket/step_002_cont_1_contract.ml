open Smartml

module Contract = struct

  let%entry_point run self () =
    with sp.modify(self.data, "t") as "t":
      ticket_test_ticket_38_data, ticket_test_ticket_38_copy = sp.match_tuple(read_ticket_raw t.value, "ticket_test_ticket_38_data", "ticket_test_ticket_38_copy")
      ticket_test_ticket_38_ticketer, ticket_test_ticket_38_content, ticket_test_ticket_38_amount = sp.match_tuple(ticket_test_ticket_38_data, "ticket_test_ticket_38_ticketer", "ticket_test_ticket_38_content", "ticket_test_ticket_38_amount")
      verify (ticket_test_ticket_38_content = "abc");
      ticket1_test_ticket_40, ticket2_test_ticket_40 = sp.match_tuple(split_ticket_raw ticket_test_ticket_38_copy (ticket_test_ticket_38_amount // 2, ticket_test_ticket_38_amount // 2).open_some(), "ticket1_test_ticket_40", "ticket2_test_ticket_40")
      join_tickets_raw (ticket2_test_ticket_40, ticket1_test_ticket_40).open_some()

  let init storage =
    Basics.build_contract
      ~tstorage:string ticket
      [run]
end
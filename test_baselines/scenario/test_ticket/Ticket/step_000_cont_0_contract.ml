open Smartml

module Contract = struct

  let%entry_point auto_call self () =
    ticket_10 = sp.local("ticket_10", ticket 1 43);
    sp.transfer(ticket_10.value, sp.tez(0), sp.self_entry_point('run'))

  let%entry_point run self params =
    set_type (params : int ticket);
    ticket_test_ticket_15_data, ticket_test_ticket_15_copy = sp.match_tuple(read_ticket_raw params, "ticket_test_ticket_15_data", "ticket_test_ticket_15_copy")
    ticket_test_ticket_15_ticketer, ticket_test_ticket_15_content, ticket_test_ticket_15_amount = sp.match_tuple(ticket_test_ticket_15_data, "ticket_test_ticket_15_ticketer", "ticket_test_ticket_15_content", "ticket_test_ticket_15_amount")
    ticket_16 = sp.local("ticket_16", ticket "abc" 42);
    self.data.y <- some ticket_16.value;
    ticket1_test_ticket_17, ticket2_test_ticket_17 = sp.match_tuple(split_ticket_raw ticket_test_ticket_15_copy (ticket_test_ticket_15_amount // 3, as_nat (ticket_test_ticket_15_amount - (ticket_test_ticket_15_amount // 3))).open_some(), "ticket1_test_ticket_17", "ticket2_test_ticket_17")
    self.data.x <- some join_tickets_raw (ticket2_test_ticket_17, ticket1_test_ticket_17).open_some()

  let%entry_point run2 self params =
    set_type (params : sp.TRecord(t = int ticket, x = int).layout(("t", "x")));
    x_test_ticket_23, t_test_ticket_23 = sp.match_record(params, "x", "t")
    verify (x_test_ticket_23 = 42);
    ticket_test_ticket_25_data, ticket_test_ticket_25_copy = sp.match_tuple(read_ticket_raw t_test_ticket_23, "ticket_test_ticket_25_data", "ticket_test_ticket_25_copy")
    ticket_test_ticket_25_ticketer, ticket_test_ticket_25_content, ticket_test_ticket_25_amount = sp.match_tuple(ticket_test_ticket_25_data, "ticket_test_ticket_25_ticketer", "ticket_test_ticket_25_content", "ticket_test_ticket_25_amount")
    ticket_26 = sp.local("ticket_26", ticket "abc" 42);
    self.data.y <- some ticket_26.value;
    ticket1_test_ticket_27, ticket2_test_ticket_27 = sp.match_tuple(split_ticket_raw ticket_test_ticket_25_copy (ticket_test_ticket_25_amount // 3, as_nat (ticket_test_ticket_25_amount - (ticket_test_ticket_25_amount // 3))).open_some(), "ticket1_test_ticket_27", "ticket2_test_ticket_27")
    self.data.x <- some join_tickets_raw (ticket2_test_ticket_27, ticket1_test_ticket_27).open_some()

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(x = int ticket option, y = string ticket option).layout(("x", "y"))
      ~storage:[%expr
                 {x = sp.none,
                  y = sp.none}]
      [auto_call; run; run2]
end
open Smartml

module Contract = struct

  let%entry_point entry_point_1 self params =
    self.data.fr <- some (mul (fst ediv params sp.mutez(1).open_some()) bls12_381_fr "0x01")

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(fr = bls12_381_fr option).layout("fr")
      ~storage:[%expr
                 {fr = sp.none}]
      [entry_point_1]
end
open Smartml

module Contract = struct

  let%entry_point setCurrentValue self params =
    verify sp.check_signature(self.data.bossPublicKey, params.userSignature, pack {c = self.data.counter; n = params.newValue; o = self.data.currentValue});
    self.data.currentValue <- params.newValue;
    self.data.counter += 1

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(bossPublicKey = key, counter = intOrNat, currentValue = string).layout(("bossPublicKey", ("counter", "currentValue")))
      ~storage:[%expr
                 {bossPublicKey = sp.key('edpkuvNy6TuQ2z8o9wnoaTtTXkzQk7nhegCHfxBc4ecsd4qG71KYNG'),
                  counter = 0,
                  currentValue = 'Hello World'}]
      [setCurrentValue]
end
open Smartml

module Contract = struct

  let%entry_point mul self params =
    self.data.g1 <- some (mul (fst params) (snd params))

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(g1 = bls12_381_g1 option).layout("g1")
      ~storage:[%expr
                 {g1 = sp.none}]
      [mul]
end
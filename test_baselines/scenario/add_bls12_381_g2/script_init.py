import smartpy as sp

class Add__Bls12_381_g2(sp.Contract):
    def __init__(self):
        self.init(g2 = sp.none)

    """
    ADD: Add two curve points or field elements.

    :: bls12_381_g2 : bls12_381_g2 : 'S -> bls12_381_g2 : 'S
    """
    @sp.entry_point
    def add(self, pair):
        self.data.g2 = sp.some(sp.fst(pair) + sp.snd(pair));

@sp.add_test(name = "Add__BLS12_381_g2")
def test():
    c1 = Add__Bls12_381_g2();

    scenario = sp.test_scenario()
    scenario += c1

    c1.add(
        sp.pair(
            sp.bls12_381_g2("0x13e02b6052719f607dacd3a088274f65596bd0d09920b61ab5da61bbdc7f5049334cf11213945d57e5ac7d055d042b7e024aa2b2f08f0a91260805272dc51051c6e47ad4fa403b02b4510b647ae3d1770bac0326a805bbefd48056c8c121bdb80606c4a02ea734cc32acd2b02bc28b99cb3e287e85a763af267492ab572e99ab3f370d275cec1da1aaa9075ff05f79be0ce5d527727d6e118cc9cdc6da2e351aadfd9baa8cbdd3a76d429a695160d12c923ac9cc3baca289e193548608b82801"),
            sp.bls12_381_g2("0x13e02b6052719f607dacd3a088274f65596bd0d09920b61ab5da61bbdc7f5049334cf11213945d57e5ac7d055d042b7e024aa2b2f08f0a91260805272dc51051c6e47ad4fa403b02b4510b647ae3d1770bac0326a805bbefd48056c8c121bdb80606c4a02ea734cc32acd2b02bc28b99cb3e287e85a763af267492ab572e99ab3f370d275cec1da1aaa9075ff05f79be0ce5d527727d6e118cc9cdc6da2e351aadfd9baa8cbdd3a76d429a695160d12c923ac9cc3baca289e193548608b82801")
        )
    );

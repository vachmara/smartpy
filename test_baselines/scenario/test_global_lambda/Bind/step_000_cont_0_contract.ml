open Smartml

module Contract = struct

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(x = intOrNat, y = intOrNat).layout(("x", "y"))
      ~storage:[%expr
                 {x = 12,
                  y = 0}]
      []
end
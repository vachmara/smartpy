open Smartml

module Contract = struct

  let%entry_point create1 self () =
    create_contract_create_contract_32 = sp.local("create_contract_create_contract_32", create contract ...);
    operations().push(create_contract_create_contract_32.value.operation);
    self.data.x <- some create_contract_create_contract_32.value.address

  let%entry_point create2 self () =
    create_contract_create_contract_37 = sp.local("create_contract_create_contract_37", create contract ...);
    operations().push(create_contract_create_contract_37.value.operation);
    create_contract_create_contract_38 = sp.local("create_contract_create_contract_38", create contract ...);
    operations().push(create_contract_create_contract_38.value.operation)

  let%entry_point create3 self () =
    create_contract_create_contract_42 = sp.local("create_contract_create_contract_42", create contract ...);
    operations().push(create_contract_create_contract_42.value.operation);
    self.data.x <- some create_contract_create_contract_42.value.address

  let%entry_point create4 self params =
    sp.for x in params:
      create_contract_create_contract_48 = sp.local("create_contract_create_contract_48", create contract ...);
      operations().push(create_contract_create_contract_48.value.operation)

  let%entry_point create5 self () =
    create_contract_create_contract_52 = sp.local("create_contract_create_contract_52", create contract ...);
    operations().push(create_contract_create_contract_52.value.operation);
    self.data.x <- some create_contract_create_contract_52.value.address

  let%entry_point create_op self () =
    operation_create_contract_59, address_create_contract_59 = sp.match_record(self.data.l(42), "operation", "address")
    operations().push(operation_create_contract_59);
    self.data.x <- some address_create_contract_59

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(l = sp.TLambda(int, sp.TRecord(address = address, operation = operation).layout(("operation", "address"))), x = address option).layout(("l", "x"))
      ~storage:[%expr
                 {l = lambda(sp.TLambda(int, sp.TRecord(address = address, operation = operation).layout(("operation", "address")))),
                  x = sp.none}]
      [create1; create2; create3; create4; create5; create_op]
end
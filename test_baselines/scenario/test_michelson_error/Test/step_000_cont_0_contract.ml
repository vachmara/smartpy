open Smartml

module Contract = struct

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(x = sp.TUnknown() list).layout("x")
      ~storage:[%expr
                 {x = sp.list([])}]
      []
end
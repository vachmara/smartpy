open Smartml

module Contract = struct

  let%entry_point a self () =
    self.data.steps += ".a";
    sp.transfer((), sp.tez(0), sp.self_entry_point('aa'))

  let%entry_point aa self () =
    self.data.steps += ".aa"

  let%entry_point b self () =
    self.data.steps += ".b";
    if self.data.steps = "check.a.b" then
      self.data.conclusion <- "BFS"
    else
      self.data.conclusion <- "DFS"

  let%entry_point check self () =
    self.data.steps <- "check";
    self.data.conclusion <- "";
    sp.transfer((), sp.tez(0), sp.self_entry_point('a'));
    sp.transfer((), sp.tez(0), sp.self_entry_point('b'))

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(conclusion = string, steps = string).layout(("conclusion", "steps"))
      ~storage:[%expr
                 {conclusion = '',
                  steps = ''}]
      [a; aa; b; check]
end
import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(a = sp.TNat, b = sp.TInt, c = sp.TBool, d = sp.TString).layout(("a", ("b", ("c", "d")))))
    self.init(a = 0,
              b = 1,
              c = True,
              d = 'abc')

  @sp.entry_point
  def ep1(self):
    with sp.match_record(self.data, "modify_record_test_modify_93") as modify_record_test_modify_93:
      sp.verify((abs(modify_record_test_modify_93.b)) == (modify_record_test_modify_93.a + 1))
      modify_record_test_modify_93.d = 'xyz'

  @sp.entry_point
  def ep2(self):
    with sp.match_record(self.data, "modify_record_test_modify_99") as modify_record_test_modify_99:
      modify_record_test_modify_99.d = 'abc'
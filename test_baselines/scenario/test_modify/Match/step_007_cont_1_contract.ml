open Smartml

module Contract = struct

  let%entry_point ep1 self () =
    with sp.match_record(self.data, "modify_record_test_modify_93") as modify_record_test_modify_93:
      verify ((abs modify_record_test_modify_93.b) = (modify_record_test_modify_93.a + 1));
      modify_record_test_modify_93.d <- "xyz"

  let%entry_point ep2 self () =
    with sp.match_record(self.data, "modify_record_test_modify_99") as modify_record_test_modify_99:
      modify_record_test_modify_99.d <- "abc"

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(a = nat, b = int, c = bool, d = string).layout(("a", ("b", ("c", "d"))))
      ~storage:[%expr
                 {a = 0,
                  b = 1,
                  c = True,
                  d = 'abc'}]
      [ep1; ep2]
end
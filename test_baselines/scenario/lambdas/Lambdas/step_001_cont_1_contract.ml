open Smartml

module Contract = struct

  let%entry_point abs_test self params =
    self.data.abcd <- self.abs(params)

  let%entry_point comp_test self () =
    self.data.abcd <- self.comp({f = sp.build_lambda(lambda lparams_7: lparams_7 + 3); x = 2})

  let%entry_point f self () =
    toto = sp.local("toto", sp.build_lambda(lambda lparams_8: (fst lparams_8) + (snd lparams_8)));
    titi = sp.local("titi", toto.value.apply(5));
    self.data.value <- titi.value(8)

  let%entry_point flambda self () =
    self.data.value <- self.flam(self.flam(15)) + self.square_root(12345)

  let%entry_point h self () =
    def f9(lparams_9):
      verify (lparams_9 >= 0);
      y = sp.local("y", lparams_9);
      sp.while (y.value * y.value) > lparams_9:
        y.value <- ((lparams_9 // y.value) + y.value) // 2;
      verify (((y.value * y.value) <= lparams_9) & (lparams_9 < ((y.value + 1) * (y.value + 1))));
      y.value
    self.data.fff <- some sp.build_lambda(f9)

  let%entry_point hh self params =
    self.data.value <- self.data.fff.open_some()(params)

  let%entry_point i self () =
    def f10(lparams_10):
      verify (lparams_10 >= 0)
    ch1 = sp.local("ch1", sp.build_lambda(f10));
    def f11(lparams_11):
      verify (lparams_11 >= 0);
      lparams_11 - 2
    ch2 = sp.local("ch2", sp.build_lambda(f11));
    def f12(lparams_12):
      verify (lparams_12 >= 0);
      True
    ch3 = sp.local("ch3", sp.build_lambda(f12));
    def f13(lparams_13):
      def f14(lparams_14):
        verify (lparams_14 >= 0);
        False
      ch3b = sp.local("ch3b", sp.build_lambda(f14));
      verify (lparams_13 >= 0);
      3 * lparams_13
    ch4 = sp.local("ch4", sp.build_lambda(f13));
    self.data.value <- ch4.value(12);
    y2 = sp.local("y2", self.not_pure({in_param = (); in_storage = self.data}));
    self.data <- y2.value.storage;
    sp.for op in y2.value.operations.rev():
      operations().push(op);
    verify (y2.value.result = self.data.value)

  let%entry_point operation_creation self () =
    def f15(lparams_15):
      __operations__ = sp.local("__operations__", sp.list([]), operation list);
      create_contract_lambdas_101 = sp.local("create_contract_lambdas_101", create contract ...);
      operations().push(create_contract_lambdas_101.value.operation);
      create_contract_lambdas_102 = sp.local("create_contract_lambdas_102", create contract ...);
      operations().push(create_contract_lambdas_102.value.operation);
      operations()
    f = sp.local("f", sp.build_lambda(f15));
    sp.for op in f.value(12345001):
      operations().push(op);
    sp.for op in f.value(12345002):
      operations().push(op)

  let%entry_point operation_creation_result self () =
    def f16(lparams_16):
      __operations__ = sp.local("__operations__", sp.list([]), operation list);
      create_contract_lambdas_110 = sp.local("create_contract_lambdas_110", create contract ...);
      operations().push(create_contract_lambdas_110.value.operation);
      let%var __s3 = 4 in
      (operations(), __s3.value)
    f = sp.local("f", sp.build_lambda(f16));
    x = sp.local("x", f.value(12345001));
    y = sp.local("y", f.value(12345002));
    sp.for op in fst x.value:
      operations().push(op);
    sp.for op in fst y.value:
      operations().push(op);
    sum = sp.local("sum", (snd x.value) + (snd y.value))

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(abcd = int, f = sp.TLambda(intOrNat, intOrNat), fff = sp.TLambda(nat, nat) option, ggg = intOrNat option, value = nat).layout(("abcd", ("f", ("fff", ("ggg", "value")))))
      ~storage:[%expr
                 {abcd = 0,
                  f = lambda(sp.TLambda(intOrNat, intOrNat)),
                  fff = sp.none,
                  ggg = sp.some(42),
                  value = 0}]
      [abs_test; comp_test; f; flambda; h; hh; i; operation_creation; operation_creation_result]
end
open Smartml

module Contract = struct

  let%entry_point ep1 self () =
    self.data.xy <- (snd self.data.xy, fst self.data.xy)

  let%entry_point ep2 self () =
    self.data.xy <- (snd (fst (self.data, ())).xy, fst (fst (self.data, ())).xy)

  let%entry_point ep3 self () =
    self.data.xy <- (fst self.data.xy, fst self.data.xy)

  let%entry_point ep4 self () =
    self.data.xy <- (snd self.data.xy, snd self.data.xy)

  let%entry_point ep5 self params =
    set_type (params : nat);
    with (ediv params 2).match_cases() as arg:
      with arg.match('None') as None:
        self.data.xy <- (0, 0)
      with arg.match('Some') as Some:
        self.data.xy <- (1, 1)


  let%entry_point ep6 self params =
    set_type (params : nat);
    with (ediv params 2).match_cases() as arg:
      with arg.match('Some') as Some:
        self.data.xy <- (1, 1)
      with arg.match('None') as None:
        self.data.xy <- (0, 0)


  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(xy = (intOrNat * intOrNat)).layout("xy")
      ~storage:[%expr
                 {xy = (0, 0)}]
      [ep1; ep2; ep3; ep4; ep5; ep6]
end
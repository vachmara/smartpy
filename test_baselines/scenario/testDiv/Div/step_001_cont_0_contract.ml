open Smartml

module Contract = struct

  let%entry_point test self params =
    self.data.a <- ediv 1 0;
    self.data.b <- ediv -1 0;
    self.data.c <- ediv 1 12;
    self.data.d <- ediv -1 12;
    self.data.e <- ediv -1 -12;
    self.data.f <- ediv 15 12;
    self.data.g <- ediv -15 12;
    self.data.h <- ediv -15 -12;
    self.data.i <- ediv sp.tez(2) sp.mutez(100);
    self.data.j <- ediv sp.tez(2) sp.mutez(101);
    self.data.k <- ediv sp.tez(2) sp.tez(100);
    self.data.l <- ediv sp.tez(2) 15;
    self.data.m <- ediv amount (params : nat)

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(a = (nat * nat) option, b = (int * nat) option, c = (nat * nat) option, d = (int * nat) option, e = (int * nat) option, f = (nat * nat) option, g = (int * nat) option, h = (int * nat) option, i = (nat * mutez) option, j = (nat * mutez) option, k = (nat * mutez) option, l = (mutez * mutez) option, m = (mutez * mutez) option).layout(("a", ("b", ("c", ("d", ("e", ("f", ("g", ("h", ("i", ("j", ("k", ("l", "m")))))))))))))
      ~storage:[%expr
                 {a = sp.none,
                  b = sp.none,
                  c = sp.none,
                  d = sp.none,
                  e = sp.none,
                  f = sp.none,
                  g = sp.none,
                  h = sp.none,
                  i = sp.none,
                  j = sp.none,
                  k = sp.none,
                  l = sp.none,
                  m = sp.none}]
      [test]
end
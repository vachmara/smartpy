open Smartml

module Contract = struct

  let%entry_point entry_point_1 self params =
    let x =
      with params.match_cases() as arg:
        with arg.match('A') as A:
          A + 12
        with arg.match('B') as B:
          never B

    in
    self.data.x <- x.value

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(x = intOrNat).layout("x")
      ~storage:[%expr
                 {x = 0}]
      [entry_point_1]
end
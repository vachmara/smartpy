open Smartml

module Contract = struct

  let%entry_point mul self params =
    self.data.g2 <- some (mul (fst params) (snd params))

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(g2 = bls12_381_g2 option).layout("g2")
      ~storage:[%expr
                 {g2 = sp.none}]
      [mul]
end
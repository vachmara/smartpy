open Smartml

module Contract = struct

  let%entry_point delegate self params =
    sp.set_delegate(params)

  let init storage =
    Basics.build_contract
      ~tstorage:unit
      ~storage:[%expr sp.unit]
      [delegate]
end
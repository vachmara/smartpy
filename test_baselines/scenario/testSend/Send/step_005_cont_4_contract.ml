open Smartml

module Contract = struct

  let%entry_point ep self params =
    if params = True then
      sp.send(sp.address('tz1M9CMEtsXm3QxA7FmMU2Qh7xzsuGXVbcDr'), sp.tez(1))
    else
      verify (params = False)

  let init storage =
    Basics.build_contract
      ~tstorage:unit
      ~storage:[%expr sp.unit]
      [ep]
end
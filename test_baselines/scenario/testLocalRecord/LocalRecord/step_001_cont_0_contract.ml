open Smartml

module Contract = struct

  let%entry_point ep self () =
    x = sp.local("x", {a = 1; b = 2});
    x.value.a <- 15

  let%entry_point ep2 self params =
    set_type (params : (int * nat))

  let init storage =
    Basics.build_contract
      ~tstorage:unit
      ~storage:[%expr sp.unit]
      [ep; ep2]
end
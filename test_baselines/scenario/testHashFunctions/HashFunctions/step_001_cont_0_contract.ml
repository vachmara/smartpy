open Smartml

module Contract = struct

  let%entry_point new_key self params =
    self.data.tz1 <- hash_key params

  let%entry_point new_value self params =
    self.data.v <- params;
    self.data.b2b <- blake2b params;
    self.data.s256 <- sha256 params;
    self.data.s512 <- sha512 params;
    self.data.sha3 <- sha3 params;
    self.data.keccak <- keccak params

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(b2b = bytes, keccak = bytes, s256 = bytes, s512 = bytes, sha3 = bytes, tz1 = keyHash, v = bytes).layout(("b2b", ("keccak", ("s256", ("s512", ("sha3", ("tz1", "v")))))))
      ~storage:[%expr
                 {b2b = sp.bytes('0x'),
                  keccak = sp.bytes('0x'),
                  s256 = sp.bytes('0x'),
                  s512 = sp.bytes('0x'),
                  sha3 = sp.bytes('0x'),
                  tz1 = sp.key_hash('tz1M9CMEtsXm3QxA7FmMU2Qh7xzsuGXVbcDr'),
                  v = sp.bytes('0x')}]
      [new_key; new_value]
end
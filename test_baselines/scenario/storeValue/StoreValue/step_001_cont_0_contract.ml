open Smartml

module Contract = struct

  let%entry_point divide self params =
    verify (params.divisor > 5);
    self.data.storedValue //= params.divisor

  let%entry_point double self () =
    self.data.storedValue *= 2

  let%entry_point replace self params =
    self.data.storedValue <- params.value

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(storedValue = nat).layout("storedValue")
      ~storage:[%expr
                 {storedValue = 12}]
      [divide; double; replace]
end
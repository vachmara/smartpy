open Smartml

module Contract = struct

  let%entry_point pop self () =
    verify (self.data.fif.first < self.data.fif.last);
    del self.data.fif.saved[self.data.fif.first];
    self.data.fif.first += 1

  let%entry_point push self params =
    set_type (params : intOrNat);
    self.data.fif.last += 1;
    self.data.fif.saved[self.data.fif.last] <- params

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(fif = sp.TRecord(first = int, last = int, saved = map(int, intOrNat)).layout(("first", ("last", "saved")))).layout("fif")
      ~storage:[%expr
                 {fif = {first = 0; last = -1; saved = {}}}]
      [pop; push]
end
import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(first = sp.TInt, last = sp.TInt, saved = sp.TMap(sp.TInt, sp.TIntOrNat)).layout(("first", ("last", "saved"))))
    self.init(first = 0,
              last = -1,
              saved = {})

  @sp.entry_point
  def pop(self):
    sp.verify(self.data.first < self.data.last)
    del self.data.saved[self.data.first]
    self.data.first += 1

  @sp.entry_point
  def push(self, params):
    self.data.last += 1
    self.data.saved[self.data.last] = params
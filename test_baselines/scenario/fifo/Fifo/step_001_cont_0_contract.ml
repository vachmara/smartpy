open Smartml

module Contract = struct

  let%entry_point pop self () =
    verify (self.data.first < self.data.last);
    del self.data.saved[self.data.first];
    self.data.first += 1

  let%entry_point push self params =
    self.data.last += 1;
    self.data.saved[self.data.last] <- params

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(first = int, last = int, saved = map(int, intOrNat)).layout(("first", ("last", "saved")))
      ~storage:[%expr
                 {first = 0,
                  last = -1,
                  saved = {}}]
      [pop; push]
end
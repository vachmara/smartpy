import smartpy as sp

class TestAddressComparison(sp.Contract):
    @sp.entry_point
    def test(self, x):
        sp.verify(sp.utils.mutez_to_nat(sp.amount) == x)
        sp.verify(sp.utils.nat_to_mutez(x) == sp.amount)

@sp.add_test(name = "Test")
def test():
    scenario = sp.test_scenario()
    c1 = TestAddressComparison()

    scenario.register(c1)

    scenario += c1.test(10).run(amount = sp.mutez(10))
    scenario += c1.test(10000000).run(amount = sp.tez(10))

import smartpy as sp

class MyContract(sp.Contract):
    def __init__(self):
        self.init(value = 0, s = '', l = sp.lambda_michelson("DUP; PUSH int 2; ADD; MUL;", sp.TNat, sp.TInt))

    @sp.entry_point
    def add(self):
        self.data.value = abs(sp.michelson("ADD", [sp.TInt, sp.TInt], [sp.TInt])(15, 16))

    @sp.entry_point
    def concat1(self):
        concat = sp.michelson("CONCAT", [sp.TList(sp.TString)], [sp.TString])
        self.data.s = concat(["a", "b", "c"])

    @sp.entry_point
    def concat2(self):
        concat = sp.michelson("CONCAT", [sp.TString, sp.TString], [sp.TString])
        self.data.s = concat("a", "b")

    @sp.entry_point
    def seq(self):
        self.data.value = abs(sp.michelson("DIP {SWAP}; ADD; MUL; DUP; MUL;", [sp.TInt, sp.TInt, sp.TInt], [sp.TInt])(15, 16, 17))

@sp.add_test(name = "Inline Michelson")
def test():
    scenario = sp.test_scenario()
    scenario.h1("Inline Michelson")
    c1 = MyContract()
    scenario += c1

sp.add_compilation_target("inlineMichelson", MyContract())

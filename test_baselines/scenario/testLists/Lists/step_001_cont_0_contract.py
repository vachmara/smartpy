import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(a = sp.TOption(sp.TRecord(l = sp.TList(sp.TIntOrNat), lr = sp.TList(sp.TIntOrNat), mi = sp.TList(sp.TRecord(key = sp.TString, value = sp.TPair(sp.TString, sp.TBool)).layout(("key", "value"))), mir = sp.TList(sp.TRecord(key = sp.TString, value = sp.TPair(sp.TString, sp.TBool)).layout(("key", "value"))), mk = sp.TList(sp.TString), mkr = sp.TList(sp.TString), mv = sp.TList(sp.TPair(sp.TString, sp.TBool)), mvr = sp.TList(sp.TPair(sp.TString, sp.TBool)), s = sp.TList(sp.TIntOrNat), sr = sp.TList(sp.TIntOrNat)).layout(("l", ("lr", ("mi", ("mir", ("mk", ("mkr", ("mv", ("mvr", ("s", "sr"))))))))))), b = sp.TIntOrNat, c = sp.TString, d = sp.TIntOrNat, e = sp.TString, f = sp.TList(sp.TIntOrNat), g = sp.TList(sp.TIntOrNat), head = sp.TString, tail = sp.TList(sp.TString)).layout(("a", ("b", ("c", ("d", ("e", ("f", ("g", ("head", "tail"))))))))))
    self.init(a = sp.none,
              b = 0,
              c = '',
              d = 0,
              e = '',
              f = sp.list([]),
              g = sp.list([]),
              head = 'no head',
              tail = sp.list(['no tail']))

  @sp.entry_point
  def test(self, params):
    self.data.a = sp.some(sp.record(l = params.l, lr = params.l.rev(), mi = params.m.items(), mir = params.m.rev_items(), mk = params.m.keys(), mkr = params.m.rev_keys(), mv = params.m.values(), mvr = params.m.rev_values(), s = params.s.elements(), sr = params.s.rev_elements()))
    self.data.b = sp.sum(params.l)
    self.data.c = sp.concat(params.m.keys())
    self.data.d = sp.sum(params.s.rev_elements())
    self.data.e = ''
    sp.for x in params.m.values():
      sp.if sp.snd(x):
        self.data.e += sp.fst(x)
    sp.for i in sp.range(0, 5):
      self.data.f.push(i * i)
    self.data.g = sp.range(1, 12)

  @sp.entry_point
  def test_match(self, params):
    with sp.match_cons(params) as match_cons_52:
      self.data.head = match_cons_52.head
      self.data.tail = match_cons_52.tail
    else:
      self.data.head = 'abc'

  @sp.entry_point
  def test_match2(self, params):
    with sp.match_cons(params) as match_cons_60:
      with sp.match_cons(match_cons_60.tail) as match_cons_61:
        self.data.head = match_cons_60.head + match_cons_61.head
        self.data.tail = match_cons_61.tail
      else:
        pass
    else:
      self.data.head = 'abc'
open Smartml

module Contract = struct

  let%entry_point ep self () =
    self.data.x <- 1

  let%entry_point ep2 self () =
    self.data.x <- 2

  let%entry_point ep3 self () =
    self.data.y <- unbound self.data.x

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(x = sp.TBounded([1, 2, 3], t=int), y = int).layout(("x", "y"))
      ~storage:[%expr
                 {x = 1,
                  y = 0}]
      [ep; ep2; ep3]
end
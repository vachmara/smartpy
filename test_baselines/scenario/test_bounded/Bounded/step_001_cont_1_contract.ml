open Smartml

module Contract = struct

  let%entry_point ep self () =
    self.data.x <- "ghi"

  let%entry_point ep2 self params =
    set_type (params : sp.TBounded(['abc', 'def', 'ghi', 'jkl'], t=string));
    self.data.x <- params

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(x = sp.TBounded(['abc', 'def', 'ghi', 'jkl'], t=string)).layout("x")
      ~storage:[%expr
                 {x = 'abc'}]
      [ep; ep2]
end
open Smartml

module Contract = struct

  let%entry_point ep1 self () =
    with self.data.x.match('A') as A:
      self.data.x <- variant('B', -2);
      self.data.r <- A;
    self.data.x <- variant('C', 3)

  let%entry_point ep3 self () =
    with self.data.z.match('Left') as Left:
      self.data.r <- Left

  let%entry_point ep4 self () =
    with self.data.x.match('A') as a1:
      with self.data.z.match('Right') as a2:
        self.data.r <- a1 + a2

  let%entry_point ep5 self () =
    if self.data.x.is_variant('Toto') then
      self.data.r <- 42

  let%entry_point ep6 self () =
    self.data.s <- self.data.x.open_variant('Toto', message = "no toto")

  let%entry_point ep7 self params =
    self.data.x <- params

  let%entry_point ep8 self params =
    with params.x.match_cases() as arg:
      with arg.match('A') as A:
        self.data.x <- params.x
      with arg.match('B') as B:
        self.data.y <- some ((12 + B) + params.y)


  let%entry_point ep9 self params =
    set_type (params.other : int);
    with params.z.match('A') as A:
      self.data.x <- params.z

  let%entry_point options self () =
    if self.data.y.is_some() then
      self.data.r <- 44 + self.data.y.open_some(message = "Not a some!");
      self.data.y <- none
    else
      self.data.r <- 3;
      self.data.y <- some 12

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(r = int, s = sp.TRecord(x = intOrNat, y = intOrNat).layout(("x", "y")), x = sp.TVariant(A = int, B = int, C = intOrNat, Toto = sp.TRecord(x = intOrNat, y = intOrNat).layout(("x", "y"))).layout((("A", "B"), ("C", "Toto"))), y = int option, z = sp.TVariant(Left = int, Right = int).layout(("Left", "Right"))).layout(("r", ("s", ("x", ("y", "z")))))
      ~storage:[%expr
                 {r = 0,
                  s = {x = 0; y = 1},
                  x = A(-1),
                  y = sp.some(-42),
                  z = Left(-10)}]
      [ep1; ep3; ep4; ep5; ep6; ep7; ep8; ep9; options]
end
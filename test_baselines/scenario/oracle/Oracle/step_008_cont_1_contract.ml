open Smartml

module Contract = struct

  let%entry_point configure self params =
    verify (self.data.admin = sender) "Privileged operation";
    self.data <- params

  let%entry_point request_tokens self params =
    set_type (params : sp.TSet(address));
    sp.transfer(sp.list([{from_ = self_address; txs = params.elements().map(sp.build_lambda(lambda lparams_1: {to_ = lparams_1; token_id = 0; amount = self.data.max_amount}))}]), sp.tez(0), sp.contract(sp.TRecord(from_ = address, txs = sp.TRecord(amount = nat, to_ = address, token_id = nat).layout(("to_", ("token_id", "amount"))) list).layout(("from_", "txs")) list, self.data.token, entry_point='transfer').open_some(message = "Incompatible token interface"))

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(active = bool, admin = address, max_amount = nat, token = address).layout(("active", ("admin", ("max_amount", "token"))))
      ~storage:[%expr
                 {active = True,
                  admin = sp.address('tz0FakeAdministrator'),
                  max_amount = 10,
                  token = sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1')}]
      [configure; request_tokens]
end
open Smartml

module Contract = struct

  let%entry_point cancel_request self params =
    with sp.match_record(self.data, "data") as data:
      verify ((sender = data.setup.escrow) | (sender = data.setup.admin)) "OracleSenderIsNotEscrowOrAdmin";
      match_pair_oracle_237_fst, match_pair_oracle_237_snd = sp.match_tuple(sp.get_and_update(data.reverse_requests, {client = params.client; client_request_id = params.client_request_id}, none), "match_pair_oracle_237_fst", "match_pair_oracle_237_snd")
      data.reverse_requests <- match_pair_oracle_237_snd;
      del data.requests[match_pair_oracle_237_fst.open_some(message = "OracleRequestUnknown")]

  let%entry_point create_request self params =
    set_type (params : sp.TRecord(amount = nat, request = sp.TRecord(cancel_timeout = timestamp, client_request_id = nat, fulfill_timeout = timestamp, job_id = bytes, oracle = address, parameters = sp.TVariant(bytes = bytes, int = int, string = string).layout(("bytes", ("int", "string"))) option, tag = string, target = address).layout(("cancel_timeout", ("client_request_id", ("fulfill_timeout", ("job_id", ("oracle", ("parameters", ("tag", "target")))))))) ticket).layout(("amount", "request")));
    amount_oracle_181, request_oracle_181 = sp.match_record(params, "amount", "request")
    with sp.match_record(self.data, "data") as data:
      verify (sender = data.setup.escrow) "OracleNotEscrow";
      verify data.setup.active "OracleInactive";
      ticket_oracle_186_data, ticket_oracle_186_copy = sp.match_tuple(read_ticket_raw request_oracle_181, "ticket_oracle_186_data", "ticket_oracle_186_copy")
      ticket_oracle_186_ticketer, ticket_oracle_186_content, ticket_oracle_186_amount = sp.match_tuple(ticket_oracle_186_data, "ticket_oracle_186_ticketer", "ticket_oracle_186_content", "ticket_oracle_186_amount")
      verify (data.setup.min_amount <= amount_oracle_181) "OracleAmountBelowMin";
      verify ((add_seconds now (data.setup.min_cancel_timeout * 60)) <= ticket_oracle_186_content.cancel_timeout) "OracleTimeoutBelowMinTimeout";
      verify ((add_seconds now (data.setup.min_fulfill_timeout * 60)) <= ticket_oracle_186_content.fulfill_timeout) "OracleTimeoutBelowMinTimeout";
      compute_oracle_192 = sp.local("compute_oracle_192", {client = ticket_oracle_186_ticketer; client_request_id = ticket_oracle_186_content.client_request_id});
      verify (not (data.reverse_requests.contains(compute_oracle_192.value))) "OracleRequestKeyAlreadyKnown";
      data.reverse_requests[compute_oracle_192.value] <- data.next_id;
      data.requests[data.next_id] <- ticket_oracle_186_copy;
      data.next_id += 1

  let%entry_point fulfill_request self params =
    with sp.match_record(self.data, "modify_record_oracle_209") as modify_record_oracle_209:
      verify (sender = modify_record_oracle_209.setup.admin) "OracleNotAdmin";
      match_pair_oracle_212_fst, match_pair_oracle_212_snd = sp.match_tuple(sp.get_and_update(modify_record_oracle_209.requests, params.request_id, none), "match_pair_oracle_212_fst", "match_pair_oracle_212_snd")
      modify_record_oracle_209.requests <- match_pair_oracle_212_snd;
      ticket_oracle_214_data, ticket_oracle_214_copy = sp.match_tuple(read_ticket_raw match_pair_oracle_212_fst.open_some(message = "OracleRequestUnknown"), "ticket_oracle_214_data", "ticket_oracle_214_copy")
      ticket_oracle_214_ticketer, ticket_oracle_214_content, ticket_oracle_214_amount = sp.match_tuple(ticket_oracle_214_data, "ticket_oracle_214_ticketer", "ticket_oracle_214_content", "ticket_oracle_214_amount")
      if params.force then
        ();
      ticket_221 = sp.local("ticket_221", ticket {client = ticket_oracle_214_ticketer; client_request_id = ticket_oracle_214_content.client_request_id; result = params.result; tag = "OracleResult"} 1);
      sp.transfer({request = ticket_oracle_214_copy; result = ticket_221.value}, sp.tez(0), sp.contract(sp.TRecord(request = sp.TRecord(cancel_timeout = timestamp, client_request_id = nat, fulfill_timeout = timestamp, job_id = bytes, oracle = address, parameters = sp.TVariant(bytes = bytes, int = int, string = string).layout(("bytes", ("int", "string"))) option, tag = string, target = address).layout(("cancel_timeout", ("client_request_id", ("fulfill_timeout", ("job_id", ("oracle", ("parameters", ("tag", "target")))))))) ticket, result = sp.TRecord(client = address, client_request_id = nat, result = sp.TVariant(bytes = bytes, int = int, string = string).layout(("bytes", ("int", "string"))), tag = string).layout(("client", ("client_request_id", ("result", "tag")))) ticket).layout(("request", "result")), modify_record_oracle_209.setup.escrow, entry_point='fulfill_request').open_some());
      del modify_record_oracle_209.reverse_requests[{client = ticket_oracle_214_ticketer; client_request_id = ticket_oracle_214_content.client_request_id}]

  let%entry_point setup self params =
    with sp.match_record(self.data, "modify_record_oracle_200") as modify_record_oracle_200:
      verify (sender = modify_record_oracle_200.setup.admin) "OracleNotAdmin";
      modify_record_oracle_200.setup <- params

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(next_id = nat, requests = bigMap(nat, sp.TRecord(cancel_timeout = timestamp, client_request_id = nat, fulfill_timeout = timestamp, job_id = bytes, oracle = address, parameters = sp.TVariant(bytes = bytes, int = int, string = string).layout(("bytes", ("int", "string"))) option, tag = string, target = address).layout(("cancel_timeout", ("client_request_id", ("fulfill_timeout", ("job_id", ("oracle", ("parameters", ("tag", "target")))))))) ticket), reverse_requests = bigMap(sp.TRecord(client = address, client_request_id = nat).layout(("client", "client_request_id")), nat), setup = sp.TRecord(active = bool, admin = address, escrow = address, min_amount = nat, min_cancel_timeout = int, min_fulfill_timeout = int).layout(("active", ("admin", ("escrow", ("min_amount", ("min_cancel_timeout", "min_fulfill_timeout"))))))).layout(("next_id", ("requests", ("reverse_requests", "setup"))))
      ~storage:[%expr
                 {next_id = 0,
                  requests = {},
                  reverse_requests = {},
                  setup = {active = True; admin = sp.address('tz0FakeOracle1'); escrow = sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1'); min_amount = 0; min_cancel_timeout = 5; min_fulfill_timeout = 5}}]
      [cancel_request; create_request; fulfill_request; setup]
end
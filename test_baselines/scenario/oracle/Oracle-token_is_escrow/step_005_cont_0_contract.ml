open Smartml

module Contract = struct

  let%entry_point balance_of self params =
    verify (not self.data.paused) "FA2_PAUSED";
    set_type (params : sp.TRecord(callback = sp.TRecord(balance = nat, request = sp.TRecord(owner = address, token_id = nat).layout(("owner", "token_id"))).layout(("request", "balance")) list contract, requests = sp.TRecord(owner = address, token_id = nat).layout(("owner", "token_id")) list).layout(("requests", "callback")));
    def f0(lparams_0):
      verify (self.data.token_metadata.contains(lparams_0.token_id)) "FA2_TOKEN_UNDEFINED";
      if self.data.ledger.contains((lparams_0.owner : address)) then
        {request = {owner = (lparams_0.owner : address); token_id = (lparams_0.token_id : nat)}; balance = self.data.ledger[(lparams_0.owner : address)].balance}
      else
        {request = {owner = (lparams_0.owner : address); token_id = (lparams_0.token_id : nat)}; balance = 0}
    responses = sp.local("responses", params.requests.map(sp.build_lambda(f0)));
    sp.transfer(responses.value, sp.tez(0), (params.callback : sp.TRecord(balance = nat, request = sp.TRecord(owner = address, token_id = nat).layout(("owner", "token_id"))).layout(("request", "balance")) list contract))

  let%entry_point cancel_request self params =
    compute_oracle_304 = sp.local("compute_oracle_304", {client = sender; client_request_id = params.client_request_id});
    verify (self.data.locked.contains(compute_oracle_304.value)) "EscrowRequestIdUnknownForClient";
    verify (now >= self.data.locked[compute_oracle_304.value].cancel_timeout) "EscrowCantCancelBeforeTimeout";
    sp.transfer(sp.list([{from_ = self_address; txs = sp.list([{to_ = sender; token_id = 0; amount = self.data.locked[compute_oracle_304.value].amount}])}]), sp.tez(0), sp.self_entry_point('transfer'));
    del self.data.locked[compute_oracle_304.value];
    if not params.force then
      sp.transfer(compute_oracle_304.value, sp.tez(0), sp.contract(sp.TRecord(client = address, client_request_id = nat).layout(("client", "client_request_id")), params.oracle, entry_point='cancel_request').open_some(message = "EscrowOracleNotFound"))

  let%entry_point force_fulfill_request self params =
    set_type (params : sp.TRecord(request = sp.TRecord(cancel_timeout = timestamp, client_request_id = nat, fulfill_timeout = timestamp, job_id = bytes, oracle = address, parameters = sp.TVariant(bytes = bytes, int = int, string = string).layout(("bytes", ("int", "string"))) option, tag = string, target = address).layout(("cancel_timeout", ("client_request_id", ("fulfill_timeout", ("job_id", ("oracle", ("parameters", ("tag", "target")))))))) ticket, result = sp.TRecord(client = address, client_request_id = nat, result = sp.TVariant(bytes = bytes, int = int, string = string).layout(("bytes", ("int", "string"))), tag = string).layout(("client", ("client_request_id", ("result", "tag")))) ticket).layout(("request", "result")));
    request_oracle_324, result_oracle_324 = sp.match_record(params, "request", "result")
    ticket_oracle_325_data, ticket_oracle_325_copy = sp.match_tuple(read_ticket_raw request_oracle_324, "ticket_oracle_325_data", "ticket_oracle_325_copy")
    ticket_oracle_325_ticketer, ticket_oracle_325_content, ticket_oracle_325_amount = sp.match_tuple(ticket_oracle_325_data, "ticket_oracle_325_ticketer", "ticket_oracle_325_content", "ticket_oracle_325_amount")
    ticket_oracle_326_data, ticket_oracle_326_copy = sp.match_tuple(read_ticket_raw result_oracle_324, "ticket_oracle_326_data", "ticket_oracle_326_copy")
    ticket_oracle_326_ticketer, ticket_oracle_326_content, ticket_oracle_326_amount = sp.match_tuple(ticket_oracle_326_data, "ticket_oracle_326_ticketer", "ticket_oracle_326_content", "ticket_oracle_326_amount")
    compute_oracle_330 = sp.local("compute_oracle_330", {client = ticket_oracle_325_ticketer; client_request_id = ticket_oracle_325_content.client_request_id});
    verify (self.data.locked.contains(compute_oracle_330.value)) "EscrowRequestUnknown";
    verify (ticket_oracle_325_content.fulfill_timeout >= now) "EscrowCantFulfillAfterTimeout";
    verify (ticket_oracle_325_content.tag = "OracleRequest") "TicketExpectedTag:OracleRequest";
    verify (ticket_oracle_326_content.tag = "OracleResult") "TicketExpectedTag:OracleResult";
    verify (ticket_oracle_325_ticketer = ticket_oracle_326_content.client) "TicketClientNotMatch";
    verify (ticket_oracle_326_ticketer = ticket_oracle_325_content.oracle) "TicketOracleNotMatch";
    verify (ticket_oracle_325_content.client_request_id = ticket_oracle_326_content.client_request_id) "TicketClientRequestIdNotMatch";
    verify (sender = ticket_oracle_326_ticketer) "EscrowSenderAndTicketerNotMatch";
    sp.transfer(sp.list([{from_ = self_address; txs = sp.list([{to_ = ticket_oracle_325_content.oracle; token_id = 0; amount = self.data.locked[compute_oracle_330.value].amount}])}]), sp.tez(0), sp.self_entry_point('transfer'));
    del self.data.locked[compute_oracle_330.value]

  let%entry_point fulfill_request self params =
    set_type (params : sp.TRecord(request = sp.TRecord(cancel_timeout = timestamp, client_request_id = nat, fulfill_timeout = timestamp, job_id = bytes, oracle = address, parameters = sp.TVariant(bytes = bytes, int = int, string = string).layout(("bytes", ("int", "string"))) option, tag = string, target = address).layout(("cancel_timeout", ("client_request_id", ("fulfill_timeout", ("job_id", ("oracle", ("parameters", ("tag", "target")))))))) ticket, result = sp.TRecord(client = address, client_request_id = nat, result = sp.TVariant(bytes = bytes, int = int, string = string).layout(("bytes", ("int", "string"))), tag = string).layout(("client", ("client_request_id", ("result", "tag")))) ticket).layout(("request", "result")));
    request_oracle_324, result_oracle_324 = sp.match_record(params, "request", "result")
    ticket_oracle_325_data, ticket_oracle_325_copy = sp.match_tuple(read_ticket_raw request_oracle_324, "ticket_oracle_325_data", "ticket_oracle_325_copy")
    ticket_oracle_325_ticketer, ticket_oracle_325_content, ticket_oracle_325_amount = sp.match_tuple(ticket_oracle_325_data, "ticket_oracle_325_ticketer", "ticket_oracle_325_content", "ticket_oracle_325_amount")
    ticket_oracle_326_data, ticket_oracle_326_copy = sp.match_tuple(read_ticket_raw result_oracle_324, "ticket_oracle_326_data", "ticket_oracle_326_copy")
    ticket_oracle_326_ticketer, ticket_oracle_326_content, ticket_oracle_326_amount = sp.match_tuple(ticket_oracle_326_data, "ticket_oracle_326_ticketer", "ticket_oracle_326_content", "ticket_oracle_326_amount")
    compute_oracle_330 = sp.local("compute_oracle_330", {client = ticket_oracle_325_ticketer; client_request_id = ticket_oracle_325_content.client_request_id});
    verify (self.data.locked.contains(compute_oracle_330.value)) "EscrowRequestUnknown";
    verify (ticket_oracle_325_content.fulfill_timeout >= now) "EscrowCantFulfillAfterTimeout";
    verify (ticket_oracle_325_content.tag = "OracleRequest") "TicketExpectedTag:OracleRequest";
    verify (ticket_oracle_326_content.tag = "OracleResult") "TicketExpectedTag:OracleResult";
    verify (ticket_oracle_325_ticketer = ticket_oracle_326_content.client) "TicketClientNotMatch";
    verify (ticket_oracle_326_ticketer = ticket_oracle_325_content.oracle) "TicketOracleNotMatch";
    verify (ticket_oracle_325_content.client_request_id = ticket_oracle_326_content.client_request_id) "TicketClientRequestIdNotMatch";
    verify (sender = ticket_oracle_326_ticketer) "EscrowSenderAndTicketerNotMatch";
    sp.transfer(sp.list([{from_ = self_address; txs = sp.list([{to_ = ticket_oracle_325_content.oracle; token_id = 0; amount = self.data.locked[compute_oracle_330.value].amount}])}]), sp.tez(0), sp.self_entry_point('transfer'));
    del self.data.locked[compute_oracle_330.value];
    sp.transfer({request = ticket_oracle_325_copy; result = ticket_oracle_326_copy}, sp.tez(0), sp.contract(sp.TRecord(request = sp.TRecord(cancel_timeout = timestamp, client_request_id = nat, fulfill_timeout = timestamp, job_id = bytes, oracle = address, parameters = sp.TVariant(bytes = bytes, int = int, string = string).layout(("bytes", ("int", "string"))) option, tag = string, target = address).layout(("cancel_timeout", ("client_request_id", ("fulfill_timeout", ("job_id", ("oracle", ("parameters", ("tag", "target")))))))) ticket, result = sp.TRecord(client = address, client_request_id = nat, result = sp.TVariant(bytes = bytes, int = int, string = string).layout(("bytes", ("int", "string"))), tag = string).layout(("client", ("client_request_id", ("result", "tag")))) ticket).layout(("request", "result")), ticket_oracle_325_content.target).open_some(message = "EscrowTargetNotFound"))

  let%entry_point mint self params =
    verify (sender = self.data.administrator) "FA2_NOT_ADMIN";
    verify (params.token_id = 0) "single-asset: token-id <> 0";
    verify (self.data.all_tokens = params.token_id) "Token-IDs should be consecutive";
    self.data.all_tokens <- max self.data.all_tokens (params.token_id + 1);
    if self.data.ledger.contains((params.address : address)) then
      self.data.ledger[(params.address : address)].balance += params.amount
    else
      self.data.ledger[(params.address : address)] <- {balance = params.amount};
    if self.data.token_metadata.contains(params.token_id) then
      ()
    else
      self.data.token_metadata[params.token_id] <- {token_id = params.token_id; token_info = params.metadata};
      self.data.total_supply[params.token_id] <- params.amount

  let%entry_point send_request self params =
    set_type (params : sp.TRecord(amount = nat, request = sp.TRecord(cancel_timeout = timestamp, client_request_id = nat, fulfill_timeout = timestamp, job_id = bytes, oracle = address, parameters = sp.TVariant(bytes = bytes, int = int, string = string).layout(("bytes", ("int", "string"))) option, tag = string, target = address).layout(("cancel_timeout", ("client_request_id", ("fulfill_timeout", ("job_id", ("oracle", ("parameters", ("tag", "target")))))))) ticket).layout(("amount", "request")));
    amount_oracle_273, request_oracle_273 = sp.match_record(params, "amount", "request")
    ticket_oracle_276_data, ticket_oracle_276_copy = sp.match_tuple(read_ticket_raw request_oracle_273, "ticket_oracle_276_data", "ticket_oracle_276_copy")
    ticket_oracle_276_ticketer, ticket_oracle_276_content, ticket_oracle_276_amount = sp.match_tuple(ticket_oracle_276_data, "ticket_oracle_276_ticketer", "ticket_oracle_276_content", "ticket_oracle_276_amount")
    verify (sender = ticket_oracle_276_ticketer) "EscrowSenderAndTicketerNotMatch";
    sp.transfer(sp.list([{from_ = sender; txs = sp.list([{to_ = self_address; token_id = 0; amount = amount_oracle_273}])}]), sp.tez(0), sp.self_entry_point('transfer'));
    verify (ticket_oracle_276_content.tag = "OracleRequest") "TicketExpectedTag:OracleRequest";
    compute_oracle_292 = sp.local("compute_oracle_292", {client = ticket_oracle_276_ticketer; client_request_id = ticket_oracle_276_content.client_request_id});
    verify (not (self.data.locked.contains(compute_oracle_292.value))) "EscrowRequestIdAlreadyKnownForClient";
    self.data.locked[compute_oracle_292.value] <- {amount = amount_oracle_273; cancel_timeout = ticket_oracle_276_content.cancel_timeout};
    sp.transfer({amount = amount_oracle_273; request = ticket_oracle_276_copy}, sp.tez(0), sp.contract(sp.TRecord(amount = nat, request = sp.TRecord(cancel_timeout = timestamp, client_request_id = nat, fulfill_timeout = timestamp, job_id = bytes, oracle = address, parameters = sp.TVariant(bytes = bytes, int = int, string = string).layout(("bytes", ("int", "string"))) option, tag = string, target = address).layout(("cancel_timeout", ("client_request_id", ("fulfill_timeout", ("job_id", ("oracle", ("parameters", ("tag", "target")))))))) ticket).layout(("amount", "request")), ticket_oracle_276_content.oracle, entry_point='create_request').open_some(message = "EscrowOracleNotFound"))

  let%entry_point set_administrator self params =
    verify (sender = self.data.administrator) "FA2_NOT_ADMIN";
    self.data.administrator <- params

  let%entry_point set_metadata self params =
    verify (sender = self.data.administrator) "FA2_NOT_ADMIN";
    self.data.metadata[params.k] <- params.v

  let%entry_point set_pause self params =
    verify (sender = self.data.administrator) "FA2_NOT_ADMIN";
    self.data.paused <- params

  let%entry_point transfer self params =
    verify (not self.data.paused) "FA2_PAUSED";
    set_type (params : sp.TRecord(from_ = address, txs = sp.TRecord(amount = nat, to_ = address, token_id = nat).layout(("to_", ("token_id", "amount"))) list).layout(("from_", "txs")) list);
    sp.for transfer in params:
      sp.for tx in transfer.txs:
        verify (tx.token_id = 0) "single-asset: token-id <> 0";
        verify ((((sender = self.data.administrator) | (transfer.from_ = sender)) | (self.data.operators.contains(({owner = transfer.from_; operator = sender; token_id = tx.token_id} : sp.TRecord(operator = address, owner = address, token_id = nat).layout(("owner", ("operator", "token_id"))))))) | (sender = self_address)) "FA2_NOT_OPERATOR";
        verify (self.data.token_metadata.contains(tx.token_id)) "FA2_TOKEN_UNDEFINED";
        if tx.amount > 0 then
          verify (self.data.ledger[(transfer.from_ : address)].balance >= tx.amount) "FA2_INSUFFICIENT_BALANCE";
          self.data.ledger[(transfer.from_ : address)].balance <- as_nat (self.data.ledger[(transfer.from_ : address)].balance - tx.amount);
          if self.data.ledger.contains((tx.to_ : address)) then
            self.data.ledger[(tx.to_ : address)].balance += tx.amount
          else
            self.data.ledger[(tx.to_ : address)] <- {balance = tx.amount}

  let%entry_point update_operators self params =
    set_type (params : sp.TVariant(add_operator = sp.TRecord(operator = address, owner = address, token_id = nat).layout(("owner", ("operator", "token_id"))), remove_operator = sp.TRecord(operator = address, owner = address, token_id = nat).layout(("owner", ("operator", "token_id")))).layout(("add_operator", "remove_operator")) list);
    sp.for update in params:
      with update.match_cases() as arg:
        with arg.match('add_operator') as add_operator:
          verify ((add_operator.owner = sender) | (sender = self.data.administrator)) "FA2_NOT_ADMIN_OR_OPERATOR";
          self.data.operators[({owner = add_operator.owner; operator = add_operator.operator; token_id = add_operator.token_id} : sp.TRecord(operator = address, owner = address, token_id = nat).layout(("owner", ("operator", "token_id"))))] <- ()
        with arg.match('remove_operator') as remove_operator:
          verify ((remove_operator.owner = sender) | (sender = self.data.administrator)) "FA2_NOT_ADMIN_OR_OPERATOR";
          del self.data.operators[({owner = remove_operator.owner; operator = remove_operator.operator; token_id = remove_operator.token_id} : sp.TRecord(operator = address, owner = address, token_id = nat).layout(("owner", ("operator", "token_id"))))]


  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(administrator = address, all_tokens = nat, ledger = bigMap(address, sp.TRecord(balance = nat).layout("balance")), locked = bigMap(sp.TRecord(client = address, client_request_id = nat).layout(("client", "client_request_id")), sp.TRecord(amount = nat, cancel_timeout = timestamp).layout(("amount", "cancel_timeout"))), metadata = bigMap(string, bytes), operators = bigMap(sp.TRecord(operator = address, owner = address, token_id = nat).layout(("owner", ("operator", "token_id"))), unit), paused = bool, token_metadata = bigMap(nat, sp.TRecord(token_id = nat, token_info = map(string, bytes)).layout(("token_id", "token_info"))), total_supply = bigMap(nat, nat)).layout(("administrator", ("all_tokens", ("ledger", ("locked", ("metadata", ("operators", ("paused", ("token_metadata", "total_supply")))))))))
      ~storage:[%expr
                 {administrator = sp.address('tz0FakeAdministrator'),
                  all_tokens = 0,
                  ledger = {},
                  locked = {},
                  metadata = {'' : sp.bytes('0x')},
                  operators = {},
                  paused = False,
                  token_metadata = {},
                  total_supply = {}}]
      [balance_of; cancel_request; force_fulfill_request; fulfill_request; mint; send_request; set_administrator; set_metadata; set_pause; transfer; update_operators]
end
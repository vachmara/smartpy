open Smartml

module Contract = struct

  let%entry_point cancel_request self params =
    compute_oracle_304 = sp.local("compute_oracle_304", {client = sender; client_request_id = params.client_request_id});
    verify (self.data.locked.contains(compute_oracle_304.value)) "EscrowRequestIdUnknownForClient";
    verify (now >= self.data.locked[compute_oracle_304.value].cancel_timeout) "EscrowCantCancelBeforeTimeout";
    sp.transfer(sp.list([{from_ = self_address; txs = sp.list([{to_ = sender; token_id = self.data.token_id; amount = self.data.locked[compute_oracle_304.value].amount}])}]), sp.tez(0), sp.contract(sp.TRecord(from_ = address, txs = sp.TRecord(amount = nat, to_ = address, token_id = nat).layout(("to_", ("token_id", "amount"))) list).layout(("from_", "txs")) list, self.data.token, entry_point='transfer').open_some());
    del self.data.locked[compute_oracle_304.value];
    if not params.force then
      sp.transfer(compute_oracle_304.value, sp.tez(0), sp.contract(sp.TRecord(client = address, client_request_id = nat).layout(("client", "client_request_id")), params.oracle, entry_point='cancel_request').open_some(message = "EscrowOracleNotFound"))

  let%entry_point force_fulfill_request self params =
    set_type (params : sp.TRecord(request = sp.TRecord(cancel_timeout = timestamp, client_request_id = nat, fulfill_timeout = timestamp, job_id = bytes, oracle = address, parameters = sp.TVariant(bytes = bytes, int = int, string = string).layout(("bytes", ("int", "string"))) option, tag = string, target = address).layout(("cancel_timeout", ("client_request_id", ("fulfill_timeout", ("job_id", ("oracle", ("parameters", ("tag", "target")))))))) ticket, result = sp.TRecord(client = address, client_request_id = nat, result = sp.TVariant(bytes = bytes, int = int, string = string).layout(("bytes", ("int", "string"))), tag = string).layout(("client", ("client_request_id", ("result", "tag")))) ticket).layout(("request", "result")));
    request_oracle_324, result_oracle_324 = sp.match_record(params, "request", "result")
    ticket_oracle_325_data, ticket_oracle_325_copy = sp.match_tuple(read_ticket_raw request_oracle_324, "ticket_oracle_325_data", "ticket_oracle_325_copy")
    ticket_oracle_325_ticketer, ticket_oracle_325_content, ticket_oracle_325_amount = sp.match_tuple(ticket_oracle_325_data, "ticket_oracle_325_ticketer", "ticket_oracle_325_content", "ticket_oracle_325_amount")
    ticket_oracle_326_data, ticket_oracle_326_copy = sp.match_tuple(read_ticket_raw result_oracle_324, "ticket_oracle_326_data", "ticket_oracle_326_copy")
    ticket_oracle_326_ticketer, ticket_oracle_326_content, ticket_oracle_326_amount = sp.match_tuple(ticket_oracle_326_data, "ticket_oracle_326_ticketer", "ticket_oracle_326_content", "ticket_oracle_326_amount")
    compute_oracle_330 = sp.local("compute_oracle_330", {client = ticket_oracle_325_ticketer; client_request_id = ticket_oracle_325_content.client_request_id});
    verify (self.data.locked.contains(compute_oracle_330.value)) "EscrowRequestUnknown";
    verify (ticket_oracle_325_content.fulfill_timeout >= now) "EscrowCantFulfillAfterTimeout";
    verify (ticket_oracle_325_content.tag = "OracleRequest") "TicketExpectedTag:OracleRequest";
    verify (ticket_oracle_326_content.tag = "OracleResult") "TicketExpectedTag:OracleResult";
    verify (ticket_oracle_325_ticketer = ticket_oracle_326_content.client) "TicketClientNotMatch";
    verify (ticket_oracle_326_ticketer = ticket_oracle_325_content.oracle) "TicketOracleNotMatch";
    verify (ticket_oracle_325_content.client_request_id = ticket_oracle_326_content.client_request_id) "TicketClientRequestIdNotMatch";
    verify (sender = ticket_oracle_326_ticketer) "EscrowSenderAndTicketerNotMatch";
    sp.transfer(sp.list([{from_ = self_address; txs = sp.list([{to_ = ticket_oracle_325_content.oracle; token_id = self.data.token_id; amount = self.data.locked[compute_oracle_330.value].amount}])}]), sp.tez(0), sp.contract(sp.TRecord(from_ = address, txs = sp.TRecord(amount = nat, to_ = address, token_id = nat).layout(("to_", ("token_id", "amount"))) list).layout(("from_", "txs")) list, self.data.token, entry_point='transfer').open_some());
    del self.data.locked[compute_oracle_330.value]

  let%entry_point fulfill_request self params =
    set_type (params : sp.TRecord(request = sp.TRecord(cancel_timeout = timestamp, client_request_id = nat, fulfill_timeout = timestamp, job_id = bytes, oracle = address, parameters = sp.TVariant(bytes = bytes, int = int, string = string).layout(("bytes", ("int", "string"))) option, tag = string, target = address).layout(("cancel_timeout", ("client_request_id", ("fulfill_timeout", ("job_id", ("oracle", ("parameters", ("tag", "target")))))))) ticket, result = sp.TRecord(client = address, client_request_id = nat, result = sp.TVariant(bytes = bytes, int = int, string = string).layout(("bytes", ("int", "string"))), tag = string).layout(("client", ("client_request_id", ("result", "tag")))) ticket).layout(("request", "result")));
    request_oracle_324, result_oracle_324 = sp.match_record(params, "request", "result")
    ticket_oracle_325_data, ticket_oracle_325_copy = sp.match_tuple(read_ticket_raw request_oracle_324, "ticket_oracle_325_data", "ticket_oracle_325_copy")
    ticket_oracle_325_ticketer, ticket_oracle_325_content, ticket_oracle_325_amount = sp.match_tuple(ticket_oracle_325_data, "ticket_oracle_325_ticketer", "ticket_oracle_325_content", "ticket_oracle_325_amount")
    ticket_oracle_326_data, ticket_oracle_326_copy = sp.match_tuple(read_ticket_raw result_oracle_324, "ticket_oracle_326_data", "ticket_oracle_326_copy")
    ticket_oracle_326_ticketer, ticket_oracle_326_content, ticket_oracle_326_amount = sp.match_tuple(ticket_oracle_326_data, "ticket_oracle_326_ticketer", "ticket_oracle_326_content", "ticket_oracle_326_amount")
    compute_oracle_330 = sp.local("compute_oracle_330", {client = ticket_oracle_325_ticketer; client_request_id = ticket_oracle_325_content.client_request_id});
    verify (self.data.locked.contains(compute_oracle_330.value)) "EscrowRequestUnknown";
    verify (ticket_oracle_325_content.fulfill_timeout >= now) "EscrowCantFulfillAfterTimeout";
    verify (ticket_oracle_325_content.tag = "OracleRequest") "TicketExpectedTag:OracleRequest";
    verify (ticket_oracle_326_content.tag = "OracleResult") "TicketExpectedTag:OracleResult";
    verify (ticket_oracle_325_ticketer = ticket_oracle_326_content.client) "TicketClientNotMatch";
    verify (ticket_oracle_326_ticketer = ticket_oracle_325_content.oracle) "TicketOracleNotMatch";
    verify (ticket_oracle_325_content.client_request_id = ticket_oracle_326_content.client_request_id) "TicketClientRequestIdNotMatch";
    verify (sender = ticket_oracle_326_ticketer) "EscrowSenderAndTicketerNotMatch";
    sp.transfer(sp.list([{from_ = self_address; txs = sp.list([{to_ = ticket_oracle_325_content.oracle; token_id = self.data.token_id; amount = self.data.locked[compute_oracle_330.value].amount}])}]), sp.tez(0), sp.contract(sp.TRecord(from_ = address, txs = sp.TRecord(amount = nat, to_ = address, token_id = nat).layout(("to_", ("token_id", "amount"))) list).layout(("from_", "txs")) list, self.data.token, entry_point='transfer').open_some());
    del self.data.locked[compute_oracle_330.value];
    sp.transfer({request = ticket_oracle_325_copy; result = ticket_oracle_326_copy}, sp.tez(0), sp.contract(sp.TRecord(request = sp.TRecord(cancel_timeout = timestamp, client_request_id = nat, fulfill_timeout = timestamp, job_id = bytes, oracle = address, parameters = sp.TVariant(bytes = bytes, int = int, string = string).layout(("bytes", ("int", "string"))) option, tag = string, target = address).layout(("cancel_timeout", ("client_request_id", ("fulfill_timeout", ("job_id", ("oracle", ("parameters", ("tag", "target")))))))) ticket, result = sp.TRecord(client = address, client_request_id = nat, result = sp.TVariant(bytes = bytes, int = int, string = string).layout(("bytes", ("int", "string"))), tag = string).layout(("client", ("client_request_id", ("result", "tag")))) ticket).layout(("request", "result")), ticket_oracle_325_content.target).open_some(message = "EscrowTargetNotFound"))

  let%entry_point send_request self params =
    set_type (params : sp.TRecord(amount = nat, request = sp.TRecord(cancel_timeout = timestamp, client_request_id = nat, fulfill_timeout = timestamp, job_id = bytes, oracle = address, parameters = sp.TVariant(bytes = bytes, int = int, string = string).layout(("bytes", ("int", "string"))) option, tag = string, target = address).layout(("cancel_timeout", ("client_request_id", ("fulfill_timeout", ("job_id", ("oracle", ("parameters", ("tag", "target")))))))) ticket, sender = address).layout(("amount", ("request", "sender"))));
    amount_oracle_270, request_oracle_270, sender_oracle_270 = sp.match_record(params, "amount", "request", "sender")
    ticket_oracle_276_data, ticket_oracle_276_copy = sp.match_tuple(read_ticket_raw request_oracle_270, "ticket_oracle_276_data", "ticket_oracle_276_copy")
    ticket_oracle_276_ticketer, ticket_oracle_276_content, ticket_oracle_276_amount = sp.match_tuple(ticket_oracle_276_data, "ticket_oracle_276_ticketer", "ticket_oracle_276_content", "ticket_oracle_276_amount")
    verify (sender = self.data.token) "EscrowSenderNotToken";
    verify (sender_oracle_270 = ticket_oracle_276_ticketer) "EscrowSenderAndTicketerNotMatch";
    verify (ticket_oracle_276_content.tag = "OracleRequest") "TicketExpectedTag:OracleRequest";
    compute_oracle_292 = sp.local("compute_oracle_292", {client = ticket_oracle_276_ticketer; client_request_id = ticket_oracle_276_content.client_request_id});
    verify (not (self.data.locked.contains(compute_oracle_292.value))) "EscrowRequestIdAlreadyKnownForClient";
    self.data.locked[compute_oracle_292.value] <- {amount = amount_oracle_270; cancel_timeout = ticket_oracle_276_content.cancel_timeout};
    sp.transfer({amount = amount_oracle_270; request = ticket_oracle_276_copy}, sp.tez(0), sp.contract(sp.TRecord(amount = nat, request = sp.TRecord(cancel_timeout = timestamp, client_request_id = nat, fulfill_timeout = timestamp, job_id = bytes, oracle = address, parameters = sp.TVariant(bytes = bytes, int = int, string = string).layout(("bytes", ("int", "string"))) option, tag = string, target = address).layout(("cancel_timeout", ("client_request_id", ("fulfill_timeout", ("job_id", ("oracle", ("parameters", ("tag", "target")))))))) ticket).layout(("amount", "request")), ticket_oracle_276_content.oracle, entry_point='create_request').open_some(message = "EscrowOracleNotFound"))

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(locked = bigMap(sp.TRecord(client = address, client_request_id = nat).layout(("client", "client_request_id")), sp.TRecord(amount = nat, cancel_timeout = timestamp).layout(("amount", "cancel_timeout"))), token = address, token_id = nat).layout(("locked", ("token", "token_id")))
      ~storage:[%expr
                 {locked = {},
                  token = sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1'),
                  token_id = 0}]
      [cancel_request; force_fulfill_request; fulfill_request; send_request]
end
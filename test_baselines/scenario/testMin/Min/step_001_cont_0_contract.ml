open Smartml

module Contract = struct

  let%entry_point ep1 self () =
    a = sp.local("a", 0);
    b = sp.local("b", 0);
    self.data.r <- min a.value b.value

  let%entry_point ep2 self () =
    a = sp.local("a", 0);
    b = sp.local("b", 0);
    self.data.r <- max a.value b.value

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(r = intOrNat).layout("r")
      ~storage:[%expr
                 {r = 0}]
      [ep1; ep2]
end
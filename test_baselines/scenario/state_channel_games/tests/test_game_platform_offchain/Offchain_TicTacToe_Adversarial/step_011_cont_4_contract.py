import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(result = sp.TOption(sp.TRecord(messages = sp.TBytes, new_state = sp.TBytes, outcome = sp.TOption(sp.TString)).layout(("messages", ("new_state", "outcome"))))).layout("result"))
    self.init(result = sp.none)

  @sp.entry_point
  def compute(self, params):
    sp.verify(~ params.params.game.settled, 'Platform_GameSettled')
    sp.set_type(params.params.game.constants.players_addr, sp.TMap(sp.TInt, sp.TAddress))
    sp.verify(params.params.sender == params.params.game.constants.players_addr[params.params.game.meta.current_player], 'Platform_GameWrongPlayer')
    sp.verify(params.params.game.meta.move_nb == params.params.move_nb, 'Platform_Wrongmove_nb')
    sp.verify(params.params.game.meta.outcome == sp.none, 'Platform_GameNotRunning')
    __s8 = sp.local("__s8", params.data.models[params.params.game.constants.model_id].apply_(sp.record(meta = params.params.game.meta, move = params.params.move_data, state = params.params.game.state)))
    self.data.result = sp.some(__s8.value)
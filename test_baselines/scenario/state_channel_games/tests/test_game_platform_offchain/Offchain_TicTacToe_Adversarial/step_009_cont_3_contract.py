import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(result = sp.TOption(sp.TString)).layout("result"))
    self.init(result = sp.none)

  @sp.entry_point
  def compute(self, params):
    game = sp.local("game", params.params.game)
    sp.set_type(game.value.constants.players_addr, sp.TMap(sp.TInt, sp.TAddress))
    sp.verify(~ game.value.settled, 'Platform_GameSettled')
    sp.set_type(params.params.move_nb, sp.TNat)
    sp.set_type(params.params.move_data, sp.TBytes)
    sp.set_type(game.value.constants.players_addr, sp.TMap(sp.TInt, sp.TAddress))
    sp.verify(params.params.sender == game.value.constants.players_addr[game.value.meta.current_player], 'Platform_GameWrongPlayer')
    sp.verify(game.value.meta.move_nb == params.params.move_nb, 'Platform_Wrongmove_nb')
    sp.verify(game.value.meta.outcome == sp.none, 'Platform_GameNotRunning')
    compute_game_platform_305 = sp.local("compute_game_platform_305", params.data.models[game.value.constants.model_id].apply_(sp.record(meta = game.value.meta, move = params.params.move_data, state = game.value.state)))
    game.value.meta = sp.record(current_player = 3 - game.value.meta.current_player, move_nb = game.value.meta.move_nb + 1, outcome = sp.eif(compute_game_platform_305.value.outcome.is_some(), sp.some(variant('game_finished', compute_game_platform_305.value.outcome.open_some())), sp.none))
    game.value.state = compute_game_platform_305.value.new_state
    sp.verify(params.params.new_state == game.value.state)
    sp.verify(params.params.new_meta == game.value.meta)
    sp.verify(sp.check_signature(params.data.channels[game.value.constants.channel_id].players[params.params.sender].pk, params.params.signature, sp.pack(('New State', sp.set_type_expr(params.params.game_id, sp.TBytes), sp.set_type_expr(game.value.meta, sp.TRecord(current_player = sp.TInt, move_nb = sp.TNat, outcome = sp.TOption(sp.TVariant(game_finished = sp.TString, player_double_played = sp.TInt, player_inactive = sp.TInt).layout(("game_finished", ("player_double_played", "player_inactive"))))).layout(("current_player", ("move_nb", "outcome")))), sp.set_type_expr(game.value.state, sp.TBytes)))), 'Platform_badSig')
    __s7 = sp.local("__s7", 'OK')
    self.data.result = sp.some(__s7.value)
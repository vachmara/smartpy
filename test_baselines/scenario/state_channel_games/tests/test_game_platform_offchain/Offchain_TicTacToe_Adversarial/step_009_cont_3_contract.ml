open Smartml

module Contract = struct

  let%entry_point compute self params =
    game = sp.local("game", params.params.game);
    set_type (game.value.constants.players_addr : map(int, address));
    verify (not game.value.settled) "Platform_GameSettled";
    set_type (params.params.move_nb : nat);
    set_type (params.params.move_data : bytes);
    set_type (game.value.constants.players_addr : map(int, address));
    verify (params.params.sender = game.value.constants.players_addr[game.value.meta.current_player]) "Platform_GameWrongPlayer";
    verify (game.value.meta.move_nb = params.params.move_nb) "Platform_Wrongmove_nb";
    verify (game.value.meta.outcome = none) "Platform_GameNotRunning";
    compute_game_platform_305 = sp.local("compute_game_platform_305", params.data.models[game.value.constants.model_id].apply_({meta = game.value.meta; move = params.params.move_data; state = game.value.state}));
    game.value.meta <- {current_player = 3 - game.value.meta.current_player; move_nb = game.value.meta.move_nb + 1; outcome = sp.eif(compute_game_platform_305.value.outcome.is_some(), some variant('game_finished', compute_game_platform_305.value.outcome.open_some()), none)};
    game.value.state <- compute_game_platform_305.value.new_state;
    verify (params.params.new_state = game.value.state);
    verify (params.params.new_meta = game.value.meta);
    verify sp.check_signature(params.data.channels[game.value.constants.channel_id].players[params.params.sender].pk, params.params.signature, pack ("New State", (params.params.game_id : bytes), (game.value.meta : sp.TRecord(current_player = int, move_nb = nat, outcome = sp.TVariant(game_finished = string, player_double_played = int, player_inactive = int).layout(("game_finished", ("player_double_played", "player_inactive"))) option).layout(("current_player", ("move_nb", "outcome")))), (game.value.state : bytes))) "Platform_badSig";
    let%var __s7 = "OK" in
    self.data.result <- some __s7.value

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(result = string option).layout("result")
      ~storage:[%expr
                 {result = sp.none}]
      [compute]
end
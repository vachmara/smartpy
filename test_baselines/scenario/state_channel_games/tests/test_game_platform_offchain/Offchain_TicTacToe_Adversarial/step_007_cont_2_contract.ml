open Smartml

module Contract = struct

  let%entry_point compute self params =
    verify (params.params.game.meta.outcome = none) "Platform_NotRunning";
    set_type (params.params.game.constants.players_addr : map(int, address));
    game = sp.local("game", params.params.game);
    verify (not game.value.settled) "Platform_GameSettled";
    set_type (params.params.move_nb : nat);
    set_type (params.params.move_data : bytes);
    set_type (game.value.constants.players_addr : map(int, address));
    verify (params.params.sender = game.value.constants.players_addr[game.value.meta.current_player]) "Platform_GameWrongPlayer";
    verify (game.value.meta.move_nb = params.params.move_nb) "Platform_Wrongmove_nb";
    verify (game.value.meta.outcome = none) "Platform_GameNotRunning";
    compute_game_platform_305 = sp.local("compute_game_platform_305", params.data.models[game.value.constants.model_id].apply_({meta = game.value.meta; move = params.params.move_data; state = game.value.state}));
    game.value.meta <- {current_player = 3 - game.value.meta.current_player; move_nb = game.value.meta.move_nb + 1; outcome = sp.eif(compute_game_platform_305.value.outcome.is_some(), some variant('game_finished', compute_game_platform_305.value.outcome.open_some()), none)};
    game.value.state <- compute_game_platform_305.value.new_state;
    let%var __s6 = game.value in
    self.data.result <- some __s6.value

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(result = sp.TRecord(addr_players = map(address, int), constants = sp.TRecord(bonds = map(int, map(nat, nat)), channel_id = bytes, game_nonce = string, model_id = bytes, play_delay = int, players_addr = map(int, address), settlements = map(sp.TVariant(game_finished = string, player_double_played = int, player_inactive = int).layout(("game_finished", ("player_double_played", "player_inactive"))), sp.TRecord(bonds = map(nat, nat), receiver = int, sender = int).layout(("bonds", ("receiver", "sender"))) list)).layout(("bonds", ("channel_id", ("game_nonce", ("model_id", ("play_delay", ("players_addr", "settlements"))))))), meta = sp.TRecord(current_player = int, move_nb = nat, outcome = sp.TVariant(game_finished = string, player_double_played = int, player_inactive = int).layout(("game_finished", ("player_double_played", "player_inactive"))) option).layout(("current_player", ("move_nb", "outcome"))), settled = bool, state = bytes, timeouts = map(int, timestamp)).layout(("addr_players", ("constants", ("meta", ("settled", ("state", "timeouts")))))) option).layout("result")
      ~storage:[%expr
                 {result = sp.none}]
      [compute]
end
open Smartml

module Contract = struct

  let%entry_point compute self params =
    verify (not params.params.game.settled) "Platform_GameSettled";
    set_type (params.params.game.constants.players_addr : map(int, address));
    verify (params.params.sender = params.params.game.constants.players_addr[params.params.game.meta.current_player]) "Platform_GameWrongPlayer";
    verify (params.params.game.meta.move_nb = params.params.move_nb) "Platform_Wrongmove_nb";
    verify (params.params.game.meta.outcome = none) "Platform_GameNotRunning";
    let%var __s8 = params.data.models[params.params.game.constants.model_id].apply_({meta = params.params.game.meta; move = params.params.move_data; state = params.params.game.state}) in
    self.data.result <- some __s8.value

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(result = sp.TRecord(messages = bytes, new_state = bytes, outcome = string option).layout(("messages", ("new_state", "outcome"))) option).layout("result")
      ~storage:[%expr
                 {result = sp.none}]
      [compute]
end
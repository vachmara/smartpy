import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(result = sp.TOption(sp.TRecord(addr_players = sp.TMap(sp.TAddress, sp.TInt), constants = sp.TRecord(bonds = sp.TMap(sp.TInt, sp.TMap(sp.TNat, sp.TNat)), channel_id = sp.TBytes, game_nonce = sp.TString, model_id = sp.TBytes, play_delay = sp.TInt, players_addr = sp.TMap(sp.TInt, sp.TAddress), settlements = sp.TMap(sp.TVariant(game_finished = sp.TString, player_double_played = sp.TInt, player_inactive = sp.TInt).layout(("game_finished", ("player_double_played", "player_inactive"))), sp.TList(sp.TRecord(bonds = sp.TMap(sp.TNat, sp.TNat), receiver = sp.TInt, sender = sp.TInt).layout(("bonds", ("receiver", "sender")))))).layout(("bonds", ("channel_id", ("game_nonce", ("model_id", ("play_delay", ("players_addr", "settlements"))))))), meta = sp.TRecord(current_player = sp.TInt, move_nb = sp.TNat, outcome = sp.TOption(sp.TVariant(game_finished = sp.TString, player_double_played = sp.TInt, player_inactive = sp.TInt).layout(("game_finished", ("player_double_played", "player_inactive"))))).layout(("current_player", ("move_nb", "outcome"))), settled = sp.TBool, state = sp.TBytes, timeouts = sp.TMap(sp.TInt, sp.TTimestamp)).layout(("addr_players", ("constants", ("meta", ("settled", ("state", "timeouts")))))))).layout("result"))
    self.init(result = sp.none)

  @sp.entry_point
  def compute(self, params):
    sp.set_type(params.params.constants.players_addr, sp.TMap(sp.TInt, sp.TAddress))
    sp.set_type(params.params.constants, sp.TRecord(bonds = sp.TMap(sp.TInt, sp.TMap(sp.TNat, sp.TNat)), channel_id = sp.TBytes, game_nonce = sp.TString, model_id = sp.TBytes, play_delay = sp.TInt, players_addr = sp.TMap(sp.TInt, sp.TAddress), settlements = sp.TMap(sp.TVariant(game_finished = sp.TString, player_double_played = sp.TInt, player_inactive = sp.TInt).layout(("game_finished", ("player_double_played", "player_inactive"))), sp.TList(sp.TRecord(bonds = sp.TMap(sp.TNat, sp.TNat), receiver = sp.TInt, sender = sp.TInt).layout(("bonds", ("receiver", "sender")))))).layout(("bonds", ("channel_id", ("game_nonce", ("model_id", ("play_delay", ("players_addr", "settlements"))))))))
    sp.set_type(params.params.params, sp.TBytes)
    sp.set_type(params.params.signatures, sp.TMap(sp.TKey, sp.TSignature))
    sp.verify(~ (params.data.games.contains(sp.blake2b(sp.pack(params.params.constants)))), 'Platform_GameAlreadyExists')
    sp.verify(params.data.channels.contains(params.params.constants.channel_id), ('Platform_ChannelNotFound: ', params.params.constants.channel_id))
    channel = sp.local("channel", params.data.channels[params.params.constants.channel_id])
    sp.verify(~ channel.value.closed, 'Platform_ChannelClosed')
    addr_players = sp.local("addr_players", {})
    sp.for addr_player in params.params.constants.players_addr.items():
      addr_players.value[addr_player.value] = addr_player.key
      sp.verify(channel.value.players.contains(addr_player.value), 'Platform_GamePlayerNotInChannel')
    sp.for player in channel.value.players.values():
      sp.verify(params.params.signatures.contains(player.pk), 'Platform_MissingSig')
      sp.verify(sp.check_signature(player.pk, params.params.signatures[player.pk], sp.pack(('New Game', sp.set_type_expr(params.params.constants, sp.TRecord(bonds = sp.TMap(sp.TInt, sp.TMap(sp.TNat, sp.TNat)), channel_id = sp.TBytes, game_nonce = sp.TString, model_id = sp.TBytes, play_delay = sp.TInt, players_addr = sp.TMap(sp.TInt, sp.TAddress), settlements = sp.TMap(sp.TVariant(game_finished = sp.TString, player_double_played = sp.TInt, player_inactive = sp.TInt).layout(("game_finished", ("player_double_played", "player_inactive"))), sp.TList(sp.TRecord(bonds = sp.TMap(sp.TNat, sp.TNat), receiver = sp.TInt, sender = sp.TInt).layout(("bonds", ("receiver", "sender")))))).layout(("bonds", ("channel_id", ("game_nonce", ("model_id", ("play_delay", ("players_addr", "settlements")))))))), sp.set_type_expr(params.params.params, sp.TBytes)))))
    __s1 = sp.local("__s1", sp.record(addr_players = addr_players.value, constants = params.params.constants, meta = sp.record(current_player = 1, move_nb = 0, outcome = sp.set_type_expr(sp.none, sp.TOption(sp.TVariant(game_finished = sp.TString, player_double_played = sp.TInt, player_inactive = sp.TInt).layout(("game_finished", ("player_double_played", "player_inactive")))))), settled = False, state = params.data.models[params.params.constants.model_id].init(params.params.params), timeouts = sp.set_type_expr({}, sp.TMap(sp.TInt, sp.TTimestamp))))
    self.data.result = sp.some(__s1.value)
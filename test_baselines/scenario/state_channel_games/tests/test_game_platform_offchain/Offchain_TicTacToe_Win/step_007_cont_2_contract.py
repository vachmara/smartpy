import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(result = sp.TOption(sp.TRecord(addr_players = sp.TMap(sp.TAddress, sp.TInt), constants = sp.TRecord(bonds = sp.TMap(sp.TInt, sp.TMap(sp.TNat, sp.TNat)), channel_id = sp.TBytes, game_nonce = sp.TString, model_id = sp.TBytes, play_delay = sp.TInt, players_addr = sp.TMap(sp.TInt, sp.TAddress), settlements = sp.TMap(sp.TVariant(game_finished = sp.TString, player_double_played = sp.TInt, player_inactive = sp.TInt).layout(("game_finished", ("player_double_played", "player_inactive"))), sp.TList(sp.TRecord(bonds = sp.TMap(sp.TNat, sp.TNat), receiver = sp.TInt, sender = sp.TInt).layout(("bonds", ("receiver", "sender")))))).layout(("bonds", ("channel_id", ("game_nonce", ("model_id", ("play_delay", ("players_addr", "settlements"))))))), meta = sp.TRecord(current_player = sp.TInt, move_nb = sp.TNat, outcome = sp.TOption(sp.TVariant(game_finished = sp.TString, player_double_played = sp.TInt, player_inactive = sp.TInt).layout(("game_finished", ("player_double_played", "player_inactive"))))).layout(("current_player", ("move_nb", "outcome"))), settled = sp.TBool, state = sp.TBytes, timeouts = sp.TMap(sp.TInt, sp.TTimestamp)).layout(("addr_players", ("constants", ("meta", ("settled", ("state", "timeouts")))))))).layout("result"))
    self.init(result = sp.none)

  @sp.entry_point
  def compute(self, params):
    sp.verify(params.params.game.meta.outcome == sp.none, 'Platform_NotRunning')
    sp.set_type(params.params.game.constants.players_addr, sp.TMap(sp.TInt, sp.TAddress))
    game = sp.local("game", params.params.game)
    sp.verify(~ game.value.settled, 'Platform_GameSettled')
    sp.set_type(params.params.move_nb, sp.TNat)
    sp.set_type(params.params.move_data, sp.TBytes)
    sp.set_type(game.value.constants.players_addr, sp.TMap(sp.TInt, sp.TAddress))
    sp.verify(params.params.sender == game.value.constants.players_addr[game.value.meta.current_player], 'Platform_GameWrongPlayer')
    sp.verify(game.value.meta.move_nb == params.params.move_nb, 'Platform_Wrongmove_nb')
    sp.verify(game.value.meta.outcome == sp.none, 'Platform_GameNotRunning')
    compute_game_platform_305 = sp.local("compute_game_platform_305", params.data.models[game.value.constants.model_id].apply_(sp.record(meta = game.value.meta, move = params.params.move_data, state = game.value.state)))
    game.value.meta = sp.record(current_player = 3 - game.value.meta.current_player, move_nb = game.value.meta.move_nb + 1, outcome = sp.eif(compute_game_platform_305.value.outcome.is_some(), sp.some(variant('game_finished', compute_game_platform_305.value.outcome.open_some())), sp.none))
    game.value.state = compute_game_platform_305.value.new_state
    __s2 = sp.local("__s2", game.value)
    self.data.result = sp.some(__s2.value)
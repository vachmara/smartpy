open Smartml

module Contract = struct

  let%entry_point compute self params =
    set_type (params.params.constants.players_addr : map(int, address));
    set_type (params.params.constants : sp.TRecord(bonds = map(int, map(nat, nat)), channel_id = bytes, game_nonce = string, model_id = bytes, play_delay = int, players_addr = map(int, address), settlements = map(sp.TVariant(game_finished = string, player_double_played = int, player_inactive = int).layout(("game_finished", ("player_double_played", "player_inactive"))), sp.TRecord(bonds = map(nat, nat), receiver = int, sender = int).layout(("bonds", ("receiver", "sender"))) list)).layout(("bonds", ("channel_id", ("game_nonce", ("model_id", ("play_delay", ("players_addr", "settlements"))))))));
    set_type (params.params.params : bytes);
    set_type (params.params.signatures : map(key, signature));
    verify (not (params.data.games.contains(blake2b (pack params.params.constants)))) "Platform_GameAlreadyExists";
    verify (params.data.channels.contains(params.params.constants.channel_id)) ("Platform_ChannelNotFound: ", params.params.constants.channel_id);
    channel = sp.local("channel", params.data.channels[params.params.constants.channel_id]);
    verify (not channel.value.closed) "Platform_ChannelClosed";
    addr_players = sp.local("addr_players", {});
    sp.for addr_player in params.params.constants.players_addr.items():
      addr_players.value[addr_player.value] <- addr_player.key;
      verify (channel.value.players.contains(addr_player.value)) "Platform_GamePlayerNotInChannel";
    sp.for player in channel.value.players.values():
      verify (params.params.signatures.contains(player.pk)) "Platform_MissingSig";
      verify sp.check_signature(player.pk, params.params.signatures[player.pk], pack ("New Game", (params.params.constants : sp.TRecord(bonds = map(int, map(nat, nat)), channel_id = bytes, game_nonce = string, model_id = bytes, play_delay = int, players_addr = map(int, address), settlements = map(sp.TVariant(game_finished = string, player_double_played = int, player_inactive = int).layout(("game_finished", ("player_double_played", "player_inactive"))), sp.TRecord(bonds = map(nat, nat), receiver = int, sender = int).layout(("bonds", ("receiver", "sender"))) list)).layout(("bonds", ("channel_id", ("game_nonce", ("model_id", ("play_delay", ("players_addr", "settlements")))))))), (params.params.params : bytes)));
    let%var __s1 = {addr_players = addr_players.value; constants = params.params.constants; meta = {current_player = 1; move_nb = 0; outcome = (none : sp.TVariant(game_finished = string, player_double_played = int, player_inactive = int).layout(("game_finished", ("player_double_played", "player_inactive"))) option)}; settled = False; state = params.data.models[params.params.constants.model_id].init(params.params.params); timeouts = ({} : map(int, timestamp))} in
    self.data.result <- some __s1.value

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(result = sp.TRecord(addr_players = map(address, int), constants = sp.TRecord(bonds = map(int, map(nat, nat)), channel_id = bytes, game_nonce = string, model_id = bytes, play_delay = int, players_addr = map(int, address), settlements = map(sp.TVariant(game_finished = string, player_double_played = int, player_inactive = int).layout(("game_finished", ("player_double_played", "player_inactive"))), sp.TRecord(bonds = map(nat, nat), receiver = int, sender = int).layout(("bonds", ("receiver", "sender"))) list)).layout(("bonds", ("channel_id", ("game_nonce", ("model_id", ("play_delay", ("players_addr", "settlements"))))))), meta = sp.TRecord(current_player = int, move_nb = nat, outcome = sp.TVariant(game_finished = string, player_double_played = int, player_inactive = int).layout(("game_finished", ("player_double_played", "player_inactive"))) option).layout(("current_player", ("move_nb", "outcome"))), settled = bool, state = bytes, timeouts = map(int, timestamp)).layout(("addr_players", ("constants", ("meta", ("settled", ("state", "timeouts")))))) option).layout("result")
      ~storage:[%expr
                 {result = sp.none}]
      [compute]
end
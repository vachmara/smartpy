open Smartml

module Contract = struct

  let%entry_point bank_to_bonds self params =
    verify (self.data.bank.contains(sender)) "Platform_NotEnoughToken";
    verify (self.data.channels.contains(params.channel_id)) ("Platform_ChannelNotFound: ", params.channel_id);
    channel = sp.local("channel", self.data.channels[params.channel_id]);
    verify (not channel.value.closed) "Platform_ChannelClosed";
    verify (channel.value.players.contains(params.receiver)) "Platform_ReceiverNotInChannel";
    sender = sp.local("sender", self.data.bank[sender]);
    to = sp.local("to", channel.value.players[params.receiver]);
    sp.for token in params.tokens.items():
      if token.value > 0 then
        verify (sender.value.contains(token.key)) "Platform_NotEnoughToken";
        sender.value[token.key] <- sp.as_nat(sender.value[token.key] - token.value, message = "Platform_NotEnoughToken");
        if to.value.bonds.contains(token.key) then
          to.value.bonds[token.key] += token.value
        else
          to.value.bonds[token.key] <- token.value;
    self.data.channels[params.channel_id].players[params.receiver] <- to.value;
    self.data.bank[sender] <- sender.value

  let%entry_point cancel_new_game self params =
    verify (self.data.games.contains(params)) ("Platform_GameNotFound: ", params);
    game = sp.local("game", self.data.games[params]);
    verify (sender = game.value.constants.players_addr[1]) "Platform_NotGameCreator";
    del self.data.games[params]

  let%entry_point double_signed self params =
    verify (self.data.games.contains(params.game_id)) ("Platform_GameNotFound: ", params.game_id);
    game = sp.local("game", self.data.games[params.game_id]);
    verify (game.value.meta.outcome = none) "Platform_NotRunning";
    verify (params.new_meta.move_nb = game.value.meta.move_nb) "Platform_NotSameMove";
    verify (not ((params.new_meta = game.value.meta) & (params.new_state = game.value.state))) "Platform_NotDifferentStates";
    compute_game_platform_415 = sp.local("compute_game_platform_415", pack ("New State", (params.game_id : bytes), (params.new_meta : sp.TRecord(current_player = int, move_nb = nat, outcome = sp.TVariant(game_finished = string, player_double_played = int, player_inactive = int).layout(("game_finished", ("player_double_played", "player_inactive"))) option).layout(("current_player", ("move_nb", "outcome")))), (params.new_state : bytes)));
    set_type (game.value.constants.players_addr : map(int, address));
    verify sp.check_signature(self.data.channels[game.value.constants.channel_id].players[game.value.constants.players_addr[3 - game.value.meta.current_player]].pk, params.sig, compute_game_platform_415.value) "Platform_badSig";
    game.value.meta.outcome <- some variant('player_double_played', game.value.meta.current_player);
    self.data.games[params.game_id] <- game.value

  let%entry_point double_signed_offchain self params =
    set_type (params.new_meta1 : sp.TRecord(current_player = int, move_nb = nat, outcome = sp.TVariant(game_finished = string, player_double_played = int, player_inactive = int).layout(("game_finished", ("player_double_played", "player_inactive"))) option).layout(("current_player", ("move_nb", "outcome"))));
    set_type (params.new_meta2 : sp.TRecord(current_player = int, move_nb = nat, outcome = sp.TVariant(game_finished = string, player_double_played = int, player_inactive = int).layout(("game_finished", ("player_double_played", "player_inactive"))) option).layout(("current_player", ("move_nb", "outcome"))));
    set_type (params.new_state1 : bytes);
    set_type (params.new_state2 : bytes);
    verify (self.data.games.contains(params.game_id)) ("Platform_GameNotFound: ", params.game_id);
    game = sp.local("game", self.data.games[params.game_id]);
    verify (game.value.meta.outcome = none) "Platform_NotRunning";
    verify (params.new_meta1.move_nb = params.new_meta2.move_nb);
    verify (not ((params.new_meta1 = params.new_meta2) & (params.new_state1 = params.new_state2))) "Platform_NotSameStates";
    compute_game_platform_431 = sp.local("compute_game_platform_431", pack ("New State", (params.game_id : bytes), (params.new_meta1 : sp.TRecord(current_player = int, move_nb = nat, outcome = sp.TVariant(game_finished = string, player_double_played = int, player_inactive = int).layout(("game_finished", ("player_double_played", "player_inactive"))) option).layout(("current_player", ("move_nb", "outcome")))), (params.new_state1 : bytes)));
    compute_game_platform_432 = sp.local("compute_game_platform_432", pack ("New State", (params.game_id : bytes), (params.new_meta2 : sp.TRecord(current_player = int, move_nb = nat, outcome = sp.TVariant(game_finished = string, player_double_played = int, player_inactive = int).layout(("game_finished", ("player_double_played", "player_inactive"))) option).layout(("current_player", ("move_nb", "outcome")))), (params.new_state2 : bytes)));
    set_type (game.value.constants.players_addr : map(int, address));
    verify sp.check_signature(self.data.channels[game.value.constants.channel_id].players[game.value.constants.players_addr[params.player]].pk, params.sig1, compute_game_platform_431.value) "Platform_badSig";
    set_type (game.value.constants.players_addr : map(int, address));
    verify sp.check_signature(self.data.channels[game.value.constants.channel_id].players[game.value.constants.players_addr[params.player]].pk, params.sig2, compute_game_platform_432.value) "Platform_badSig";
    game.value.meta.outcome <- some variant('player_double_played', params.player);
    self.data.games[params.game_id] <- game.value

  let%entry_point finalise_withdraw self params =
    verify (self.data.channels.contains(params.channel_id)) ("Platform_ChannelNotFound: ", params.channel_id);
    channel = sp.local("channel", self.data.channels[params.channel_id]);
    verify (not channel.value.closed) "Platform_ChannelClosed";
    verify (channel.value.players.contains(sender)) "Platform_SenderNotInChannel";
    player = sp.local("player", channel.value.players[sender]);
    verify player.value.withdraw.is_some() "Platform_NoWithdrawOpened";
    verify (player.value.withdraw_id = params.withdraw_id) "Platform_WithdrawIdMismatch";
    withdraw = sp.local("withdraw", player.value.withdraw.open_some());
    verify (now >= withdraw.value.timeout) "Platform_ChallengeDelayNotOver";
    sp.for token in withdraw.value.challenge_tokens.items():
      if (token.value > 0) & (player.value.bonds.contains(token.key)) then
        verify ((player.value.bonds[token.key] - withdraw.value.tokens[token.key]) > token.value) "Platform_TokenChallenged";
    bank = sp.local("bank", {});
    if self.data.bank.contains(sender) then
      bank.value <- self.data.bank[sender];
    sp.for token in withdraw.value.tokens.items():
      if token.value > 0 then
        verify (player.value.bonds.contains(token.key)) "Platform_NotEnoughTokens";
        player.value.bonds[token.key] <- sp.as_nat(player.value.bonds[token.key] - token.value, message = "Platform_NotEnoughTokens");
        if bank.value.contains(token.key) then
          bank.value[token.key] += token.value
        else
          bank.value[token.key] <- token.value;
    self.data.bank[sender] <- bank.value;
    player.value.withdraw <- none;
    channel.value.players[sender] <- player.value;
    self.data.channels[params.channel_id] <- channel.value

  let%entry_point new_channel self params =
    verify (not (self.data.channels.contains(blake2b (pack (self_address, params.players, params.nonce))))) "Platform_ChannelAlreadyExists";
    players_map = sp.local("players_map", {});
    sp.for player in params.players.items():
      players_map.value[player.key] <- {bonds = {}; pk = player.value; withdraw = none; withdraw_id = 0};
    self.data.channels[blake2b (pack (self_address, params.players, params.nonce))] <- {closed = False; nonce = params.nonce; players = players_map.value; withdraw_delay = params.withdraw_delay}

  let%entry_point new_game self params =
    set_type (params.constants : sp.TRecord(bonds = map(int, map(nat, nat)), channel_id = bytes, game_nonce = string, model_id = bytes, play_delay = int, players_addr = map(int, address), settlements = map(sp.TVariant(game_finished = string, player_double_played = int, player_inactive = int).layout(("game_finished", ("player_double_played", "player_inactive"))), sp.TRecord(bonds = map(nat, nat), receiver = int, sender = int).layout(("bonds", ("receiver", "sender"))) list)).layout(("bonds", ("channel_id", ("game_nonce", ("model_id", ("play_delay", ("players_addr", "settlements"))))))));
    set_type (params.params : bytes);
    set_type (params.signatures : map(key, signature));
    verify (not (self.data.games.contains(blake2b (pack params.constants)))) "Platform_GameAlreadyExists";
    verify (self.data.channels.contains(params.constants.channel_id)) ("Platform_ChannelNotFound: ", params.constants.channel_id);
    channel = sp.local("channel", self.data.channels[params.constants.channel_id]);
    verify (not channel.value.closed) "Platform_ChannelClosed";
    addr_players = sp.local("addr_players", {});
    sp.for addr_player in params.constants.players_addr.items():
      addr_players.value[addr_player.value] <- addr_player.key;
      verify (channel.value.players.contains(addr_player.value)) "Platform_GamePlayerNotInChannel";
    sp.for player in channel.value.players.values():
      verify (params.signatures.contains(player.pk)) "Platform_MissingSig";
      verify sp.check_signature(player.pk, params.signatures[player.pk], pack ("New Game", (params.constants : sp.TRecord(bonds = map(int, map(nat, nat)), channel_id = bytes, game_nonce = string, model_id = bytes, play_delay = int, players_addr = map(int, address), settlements = map(sp.TVariant(game_finished = string, player_double_played = int, player_inactive = int).layout(("game_finished", ("player_double_played", "player_inactive"))), sp.TRecord(bonds = map(nat, nat), receiver = int, sender = int).layout(("bonds", ("receiver", "sender"))) list)).layout(("bonds", ("channel_id", ("game_nonce", ("model_id", ("play_delay", ("players_addr", "settlements")))))))), (params.params : bytes)));
    self.data.games[blake2b (pack params.constants)] <- {addr_players = addr_players.value; constants = params.constants; meta = {current_player = 1; move_nb = 0; outcome = (none : sp.TVariant(game_finished = string, player_double_played = int, player_inactive = int).layout(("game_finished", ("player_double_played", "player_inactive"))) option)}; settled = False; state = self.data.models[params.constants.model_id].init(params.params); timeouts = ({} : map(int, timestamp))}

  let%entry_point new_model self params =
    self.data.models[blake2b (pack params.model)] <- params.model

  let%entry_point play self params =
    verify (self.data.games.contains(params.game_id)) ("Platform_GameNotFound: ", params.game_id);
    game = sp.local("game", self.data.games[params.game_id]);
    verify (game.value.meta.outcome = none) "Platform_NotRunning";
    set_type (params.move_nb : nat);
    set_type (params.move_data : bytes);
    set_type (game.value.constants.players_addr : map(int, address));
    verify (sender = game.value.constants.players_addr[game.value.meta.current_player]) "Platform_GameWrongPlayer";
    verify (game.value.meta.move_nb = params.move_nb) "Platform_Wrongmove_nb";
    verify (game.value.meta.outcome = none) "Platform_GameNotRunning";
    compute_game_platform_305 = sp.local("compute_game_platform_305", self.data.models[game.value.constants.model_id].apply_({meta = game.value.meta; move = params.move_data; state = game.value.state}));
    game.value.meta <- {current_player = 3 - game.value.meta.current_player; move_nb = game.value.meta.move_nb + 1; outcome = sp.eif(compute_game_platform_305.value.outcome.is_some(), some variant('game_finished', compute_game_platform_305.value.outcome.open_some()), none)};
    game.value.state <- compute_game_platform_305.value.new_state;
    if game.value.timeouts.contains(game.value.meta.current_player) then
      game.value.timeouts[game.value.meta.current_player] <- add_seconds now game.value.constants.play_delay;
    self.data.games[params.game_id] <- game.value

  let%entry_point push_bonds self params =
    verify (self.data.channels.contains(params.channel_id)) ("Platform_ChannelNotFound: ", params.channel_id);
    channel = sp.local("channel", self.data.channels[params.channel_id]);
    verify (not channel.value.closed) "Platform_ChannelClosed";
    verify (channel.value.players.contains(params.player_addr)) "Platform_NotChannelPlayer";
    player = sp.local("player", channel.value.players[params.player_addr]);
    sp.for token in params.bonds.keys():
      if player.value.bonds.contains(token) then
        player.value.bonds[token] += params.bonds[token]
      else
        player.value.bonds[token] <- params.bonds[token];
    self.data.channels[params.channel_id].players[params.player_addr] <- player.value

  let%entry_point set_outcome self params =
    set_type (params.signatures : map(key, signature));
    verify (self.data.games.contains(params.game_id)) ("Platform_GameNotFound: ", params.game_id);
    game = sp.local("game", self.data.games[params.game_id]);
    verify (game.value.meta.outcome = none) "Platform_NotRunning";
    compute_game_platform_324 = sp.local("compute_game_platform_324", pack ("New Outcome", (params.game_id : bytes), (params.outcome : string)));
    players = sp.local("players", self.data.channels[game.value.constants.channel_id].players);
    sp.for player in players.value.values():
      verify (params.signatures.contains(player.pk)) "Platform_MissingSig";
      verify sp.check_signature(player.pk, params.signatures[player.pk], compute_game_platform_324.value) "Platform_badSig";
    game.value.meta.outcome <- some variant('game_finished', params.outcome);
    self.data.games[params.game_id] <- game.value

  let%entry_point settle self params =
    verify (self.data.games.contains(params)) ("Platform_GameNotFound: ", params);
    game = sp.local("game", self.data.games[params]);
    verify (not game.value.settled) "Plateform_GameSettled";
    if game.value.constants.settlements.contains(game.value.meta.outcome.open_some(message = "Plateform_GameIsntOver")) then
      compute_game_platform_361 = sp.local("compute_game_platform_361", game.value.constants.settlements[game.value.meta.outcome.open_some()]);
      verify (self.data.channels.contains(game.value.constants.channel_id)) ("Platform_ChannelNotFound: ", game.value.constants.channel_id);
      channel = sp.local("channel", self.data.channels[game.value.constants.channel_id]);
      verify (not channel.value.closed) "Platform_ChannelClosed";
      sp.for transfer in compute_game_platform_361.value:
        compute_game_platform_212 = sp.local("compute_game_platform_212", game.value.constants.players_addr[transfer.sender]);
        compute_game_platform_213 = sp.local("compute_game_platform_213", game.value.constants.players_addr[transfer.receiver]);
        if not (compute_game_platform_212.value = compute_game_platform_213.value) then
          sender = sp.local("sender", channel.value.players[compute_game_platform_212.value]);
          receiver = sp.local("receiver", channel.value.players[compute_game_platform_213.value]);
          sp.for bond in transfer.bonds.items():
            if not (bond.value = 0) then
              verify (sender.value.bonds.contains(bond.key)) ("Platform_NotEnoughToken:", {amount = bond.value; sender = compute_game_platform_212.value; token = bond.key});
              sender.value.bonds[bond.key] <- sp.as_nat(sender.value.bonds[bond.key] - bond.value, message = ("Platform_NotEnoughToken:", {amount = bond.value; sender = compute_game_platform_212.value; token = bond.key}));
              if sender.value.bonds[bond.key] = 0 then
                del sender.value.bonds[bond.key];
              if receiver.value.bonds.contains(bond.key) then
                receiver.value.bonds[bond.key] += bond.value
              else
                receiver.value.bonds[bond.key] <- bond.value;
          channel.value.players[compute_game_platform_212.value] <- sender.value;
          channel.value.players[compute_game_platform_213.value] <- receiver.value;
      self.data.channels[game.value.constants.channel_id].players <- channel.value.players;
    game.value.settled <- True;
    self.data.games[params] <- game.value

  let%entry_point starved self params =
    verify (self.data.games.contains(params.game_id)) ("Platform_GameNotFound: ", params.game_id);
    game = sp.local("game", self.data.games[params.game_id]);
    verify (game.value.meta.outcome = none) "Platform_NotRunning";
    verify (game.value.timeouts.contains(params.player_id)) "Platform_NotTimeoutSetup";
    verify (now > game.value.timeouts[params.player_id]) "Platform_NotTimedOut";
    game.value.meta.outcome <- some variant('player_inactive', params.player_id);
    self.data.games[params.game_id] <- game.value

  let%entry_point starving self params =
    verify (self.data.games.contains(params.game_id)) ("Platform_GameNotFound: ", params.game_id);
    game = sp.local("game", self.data.games[params.game_id]);
    verify (game.value.meta.outcome = none) "Platform_NotRunning";
    if params.flag then
      game.value.timeouts[3 - game.value.addr_players[sender]] <- add_seconds now game.value.constants.play_delay
    else
      del game.value.timeouts[3 - game.value.addr_players[sender]];
    self.data.games[params.game_id] <- game.value

  let%entry_point update_state self params =
    set_type (params.signatures : map(key, signature));
    verify (self.data.games.contains(params.game_id)) ("Platform_GameNotFound: ", params.game_id);
    game = sp.local("game", self.data.games[params.game_id]);
    verify (game.value.meta.outcome = none) "Platform_NotRunning";
    verify (params.new_meta.move_nb > game.value.meta.move_nb);
    compute_game_platform_379 = sp.local("compute_game_platform_379", pack ("New State", (params.game_id : bytes), (params.new_meta : sp.TRecord(current_player = int, move_nb = nat, outcome = sp.TVariant(game_finished = string, player_double_played = int, player_inactive = int).layout(("game_finished", ("player_double_played", "player_inactive"))) option).layout(("current_player", ("move_nb", "outcome")))), (params.new_state : bytes)));
    players = sp.local("players", self.data.channels[game.value.constants.channel_id].players);
    sp.for player in players.value.values():
      verify sp.check_signature(player.pk, params.signatures[player.pk], compute_game_platform_379.value) "Platform_badSig";
    game.value.meta <- params.new_meta;
    game.value.state <- params.new_state;
    if game.value.timeouts.contains(game.value.meta.current_player) then
      game.value.timeouts[game.value.meta.current_player] <- add_seconds now game.value.constants.play_delay;
    self.data.games[params.game_id] <- game.value

  let%entry_point withdraw_challenge self params =
    set_type (params.game_ids : sp.TSet(bytes));
    verify (self.data.channels.contains(params.channel_id)) ("Platform_ChannelNotFound: ", params.channel_id);
    channel = sp.local("channel", self.data.channels[params.channel_id]);
    verify (not channel.value.closed) "Platform_ChannelClosed";
    verify (channel.value.players.contains(sender)) "Platform_SenderNotInChannel";
    verify (channel.value.players.contains(params.withdrawer)) "Platform_WithdrawerNotInChannel";
    verify channel.value.players[params.withdrawer].withdraw.is_some() "Platform_NoWithdrawRequestOpened";
    verify (channel.value.players[params.withdrawer].withdraw_id = params.withdraw_id) "Platform_WithdrawIdMismatch";
    withdraw = sp.local("withdraw", channel.value.players[params.withdrawer].withdraw.open_some());
    sp.for game_id in params.game_ids.elements():
      verify (self.data.games.contains(game_id)) ("Platform_GameNotFound: ", game_id);
      game = sp.local("game", self.data.games[game_id]);
      verify (game.value.constants.channel_id = params.channel_id) "Platform_ChannelIdMismatch";
      if game.value.settled then
        verify (withdraw.value.challenge.contains(game_id)) "Platform_GameSettled";
        withdraw.value.challenge.remove(game_id);
        set_type (withdraw.value.challenge_tokens : map(nat, int));
        set_type (game.value.constants.bonds[game.value.addr_players[params.withdrawer]] : map(nat, nat));
        sp.for token in game.value.constants.bonds[game.value.addr_players[params.withdrawer]].items():
          if withdraw.value.challenge_tokens.contains(token.key) then
            compute_game_platform_141 = sp.local("compute_game_platform_141", withdraw.value.challenge_tokens[token.key] - (to_int game.value.constants.bonds[game.value.addr_players[params.withdrawer]][token.key]));
            if compute_game_platform_141.value = 0 then
              del withdraw.value.challenge_tokens[token.key]
            else
              withdraw.value.challenge_tokens[token.key] <- compute_game_platform_141.value
          else
            withdraw.value.challenge_tokens[token.key] <- 0 - (to_int game.value.constants.bonds[game.value.addr_players[params.withdrawer]][token.key])
      else
        if not (withdraw.value.challenge.contains(game_id)) then
          withdraw.value.challenge.add(game_id);
          set_type (withdraw.value.challenge_tokens : map(nat, int));
          set_type (game.value.constants.bonds[game.value.addr_players[params.withdrawer]] : map(nat, nat));
          sp.for token in game.value.constants.bonds[game.value.addr_players[params.withdrawer]].items():
            if withdraw.value.challenge_tokens.contains(token.key) then
              withdraw.value.challenge_tokens[token.key] += to_int game.value.constants.bonds[game.value.addr_players[params.withdrawer]][token.key]
            else
              if not ((to_int game.value.constants.bonds[game.value.addr_players[params.withdrawer]][token.key]) = 0) then
                withdraw.value.challenge_tokens[token.key] <- to_int game.value.constants.bonds[game.value.addr_players[params.withdrawer]][token.key];
    if not (params.withdrawer = sender) then
      withdraw.value.timeout <- now;
    channel.value.players[params.withdrawer].withdraw <- some withdraw.value;
    self.data.channels[params.channel_id] <- channel.value

  let%entry_point withdraw_request self params =
    verify (self.data.channels.contains(params.channel_id)) ("Platform_ChannelNotFound: ", params.channel_id);
    channel = sp.local("channel", self.data.channels[params.channel_id]);
    verify (not channel.value.closed) "Platform_ChannelClosed";
    verify (channel.value.players.contains(sender)) "Platform_SenderNotInChannel";
    player = sp.local("player", channel.value.players[sender]);
    player.value.withdraw_id += 1;
    player.value.withdraw <- some {challenge = sp.set([]); challenge_tokens = {}; timeout = add_seconds now channel.value.withdraw_delay; tokens = params.tokens};
    channel.value.players[sender] <- player.value;
    self.data.channels[params.channel_id] <- channel.value

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(bank = bigMap(address, map(nat, nat)), channels = bigMap(bytes, sp.TRecord(closed = bool, nonce = string, players = map(address, sp.TRecord(bonds = map(nat, nat), pk = key, withdraw = sp.TRecord(challenge = sp.TSet(bytes), challenge_tokens = map(nat, int), timeout = timestamp, tokens = map(nat, nat)).layout(("challenge", ("challenge_tokens", ("timeout", "tokens")))) option, withdraw_id = nat).layout(("bonds", ("pk", ("withdraw", "withdraw_id"))))), withdraw_delay = int).layout(("closed", ("nonce", ("players", "withdraw_delay"))))), games = bigMap(bytes, sp.TRecord(addr_players = map(address, int), constants = sp.TRecord(bonds = map(int, map(nat, nat)), channel_id = bytes, game_nonce = string, model_id = bytes, play_delay = int, players_addr = map(int, address), settlements = map(sp.TVariant(game_finished = string, player_double_played = int, player_inactive = int).layout(("game_finished", ("player_double_played", "player_inactive"))), sp.TRecord(bonds = map(nat, nat), receiver = int, sender = int).layout(("bonds", ("receiver", "sender"))) list)).layout(("bonds", ("channel_id", ("game_nonce", ("model_id", ("play_delay", ("players_addr", "settlements"))))))), meta = sp.TRecord(current_player = int, move_nb = nat, outcome = sp.TVariant(game_finished = string, player_double_played = int, player_inactive = int).layout(("game_finished", ("player_double_played", "player_inactive"))) option).layout(("current_player", ("move_nb", "outcome"))), settled = bool, state = bytes, timeouts = map(int, timestamp)).layout(("addr_players", ("constants", ("meta", ("settled", ("state", "timeouts"))))))), models = bigMap(bytes, sp.TRecord(apply_ = sp.TLambda(sp.TRecord(meta = sp.TRecord(current_player = int, move_nb = nat, outcome = sp.TVariant(game_finished = string, player_double_played = int, player_inactive = int).layout(("game_finished", ("player_double_played", "player_inactive"))) option).layout(("current_player", ("move_nb", "outcome"))), move = bytes, state = bytes).layout(("meta", ("move", "state"))), sp.TRecord(messages = bytes, new_state = bytes, outcome = string option).layout(("messages", ("new_state", "outcome")))), init = sp.TLambda(bytes, bytes)).layout(("apply_", "init")))).layout(("bank", ("channels", ("games", "models"))))
      ~storage:[%expr
                 {bank = {},
                  channels = {},
                  games = {},
                  models = {}}]
      [bank_to_bonds; cancel_new_game; double_signed; double_signed_offchain; finalise_withdraw; new_channel; new_game; new_model; play; push_bonds; set_outcome; settle; starved; starving; update_state; withdraw_challenge; withdraw_request]
end
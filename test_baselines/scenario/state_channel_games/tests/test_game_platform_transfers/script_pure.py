import smartpy as sp

gp           = sp.io.import_template("state_channel_games/game_platform.py")
Transfer     = sp.io.import_template("state_channel_games/models/transfer.py").Transfer
model_wrap   = sp.io.import_template("state_channel_games/model_wrap.py").model_wrap

def transfer_settlements(p1_to_p2 = {}, p2_to_p1 = {}, p1_to_p1 = {}, p2_to_p2 = {}):
    transfers = []
    if len(p1_to_p2) > 0:
        transfers.append(gp.transfer(1, 2, p1_to_p2))
    if len(p2_to_p1) > 0:
        transfers.append(gp.transfer(2, 1, p2_to_p1))
    if len(p1_to_p1) > 0:
        transfers.append(gp.transfer(1, 1, p1_to_p1))
    if len(p2_to_p2) > 0:
        transfers.append(gp.transfer(2, 2, p2_to_p2))
    settlements = sp.map({
        sp.variant("player_double_played", 1)      : transfers,
        sp.variant("player_double_played", 2)      : transfers,
        sp.variant("player_inactive",      1)      : transfers,
        sp.variant("player_inactive",      2)      : transfers,
        sp.variant("game_finished", "transfered")  : transfers
    })
    bonds = {1: p1_to_p2, 2: p2_to_p1}
    sp.set_type_expr(bonds, gp.types.t_game_bonds)
    sp.set_type_expr(settlements, gp.types.t_constants)
    return settlements, bonds

def make_signatures(p1, p2, x):
    sig1 = sp.make_signature(p1.secret_key, x)
    sig2 = sp.make_signature(p2.secret_key, x)
    return sp.map({p1.public_key: sig1, p2.public_key: sig2})

if "templates" not in __name__:
    player1 = sp.test_account('player1')
    player2 = sp.test_account('player2')
    players = {player1.address: player1.public_key, player2.address: player2.public_key}

    transfer = Transfer()
    play_delay = 3600 * 24

    def new_game_sigs(constants, params = sp.unit):
        new_game = gp.action_new_game(constants, sp.pack(params))
        return make_signatures(player1, player2, new_game)

    @sp.add_test(name="Simple transfers")
    def test():
        sc = sp.test_scenario()
        sc.table_of_contents()
        sc.h1("Transfers")
        sc.h2("Contract")
        c1 = gp.GamePlatform()
        sc += c1
        sc.h2("New model: Transfer")
        model = sc.compute(model_wrap(transfer))
        model_id = sc.compute(sp.blake2b(sp.pack(model)))
        c1.new_model(model = model).run(sender = player1)

        sc.h2("New channel")
        channelParams = sp.record(players = players, nonce = "Channel 1", withdraw_delay = 3600 * 24)
        channel_id = sp.blake2b(sp.pack((c1.address, channelParams.players, channelParams.nonce)))
        c1.new_channel(channelParams)

        ###########
        #  Test 1 #
        ###########
        sc.h2("Simple transfer")
        game_num = 1
        sc.h3("Player 1 will transfer 10 to Player 2")
        settlements, bonds = transfer_settlements(p1_to_p2 = {0:10})
        settlements = sc.compute(settlements)
        sc.show(settlements)
        sc.h3("Player 1 push {0: 10}")
        c1.push_bonds(player_addr = player1.address, channel_id = channel_id, bonds = {0: 10}).run(sender = player1)

        sc.h3("P1 Instanciate new game : Transfer")
        constants = sc.compute(sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:player1.address, 2:player2.address},
                              game_nonce = "game"+str(game_num), play_delay = play_delay, bonds = bonds, settlements = settlements))
        game_id = sp.blake2b(sp.pack(constants))
        c1.new_game(constants = constants, params = sp.pack(sp.unit), signatures = new_game_sigs(constants)).run(sender = player1)

        sc.h3("(Onchain) play transfer")
        c1.play(game_id = game_id, move_data = sp.pack(sp.unit), move_nb = 0).run(sender = player1)
        sc.verify(gp.finished_outcome(c1, game_id) == "transfered")

        sc.h3("Settle game")
        sc.verify( gp.player_bonds(c1, channel_id, player1.address)[0] == 10)
        sc.verify(~gp.player_bonds(c1, channel_id, player2.address).contains(0))
        c1.settle(game_id).run(sender = player1)
        sc.verify(~gp.player_bonds(c1, channel_id, player1.address).contains(0))
        sc.verify( gp.player_bonds(c1, channel_id, player2.address)[0] == 10)
        sc.p("P2 received 10 tokens 0")

        ############
        #  Test 2  #
        ############
        sc.h2("Transfer back")
        game_num += 1
        sc.h3("P1 and P2 inverted. Player 1 will transfer 10 to Player 2")
        settlements, bonds = transfer_settlements(p1_to_p2 = {0:10})
        settlements = sc.compute(settlements)
        sc.show(settlements)

        sc.h3("P2 Instanciate new game : Transfer")
        constants = sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:player2.address, 2:player1.address},
                              game_nonce = "game"+str(game_num), play_delay = play_delay, bonds = bonds, settlements = settlements)
        game_id = sp.blake2b(sp.pack(constants))
        c1.new_game(constants = constants, params = sp.pack(sp.unit), signatures = new_game_sigs(constants)).run(sender = player2)

        sc.h3("(Onchain) play transfer")
        c1.play(game_id = game_id, move_data = sp.pack(sp.unit), move_nb = 0).run(sender = player2)
        sc.verify(gp.finished_outcome(c1, game_id) == "transfered")

        sc.verify(~gp.player_bonds(c1, channel_id, player1.address).contains(0))
        sc.verify( gp.player_bonds(c1, channel_id, player2.address)[0] == 10)
        c1.settle(game_id).run(sender = player1)
        sc.verify( gp.player_bonds(c1, channel_id, player1.address)[0] == 10)
        sc.verify(~gp.player_bonds(c1, channel_id, player2.address).contains(0))
        sc.p("P2 (player 1 of previous game) received 10 tokens 0")

        ###########
        #  Test 3 #
        ###########
        sc.h2("Multiple transfers")
        game_num += 1
        sc.h3("Player 1 transfers 10 token_1 to Player 2 and 10 token_2 ")
        settlements, bonds = transfer_settlements(p1_to_p2 = {1:10, 2:10})
        settlements = sc.compute(settlements)
        sc.show(settlements)
        sc.h3("Player 1 push {1: 10, 2: 10}")
        c1.push_bonds(player_addr = player1.address, channel_id = channel_id, bonds = {1: 10, 2: 10}).run(sender = player1)

        sc.h3("P1 Instanciate new game : Transfer")
        constants = sc.compute(sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:player1.address, 2:player2.address},
                              game_nonce = "game"+str(game_num), play_delay = play_delay, bonds = bonds, settlements = settlements))
        game_id = sp.blake2b(sp.pack(constants))
        c1.new_game(constants = constants, params = sp.pack(sp.unit), signatures = new_game_sigs(constants)).run(sender = player1)

        sc.h3("(Onchain) play transfer")
        c1.play(game_id = game_id, move_data = sp.pack(sp.unit), move_nb = 0).run(sender = player1)
        sc.verify(gp.finished_outcome(c1, game_id) == "transfered")

        sc.h3("Settle game")
        sc.verify( gp.player_bonds(c1, channel_id, player1.address)[1] == 10)
        sc.verify( gp.player_bonds(c1, channel_id, player1.address)[2] == 10)
        sc.verify(~gp.player_bonds(c1, channel_id, player2.address).contains(1))
        sc.verify(~gp.player_bonds(c1, channel_id, player2.address).contains(2))
        c1.settle(game_id).run(sender = player1)
        sc.verify(~gp.player_bonds(c1, channel_id, player1.address).contains(1))
        sc.verify(~gp.player_bonds(c1, channel_id, player1.address).contains(2))
        sc.verify( gp.player_bonds(c1, channel_id, player2.address)[1] == 10)
        sc.verify( gp.player_bonds(c1, channel_id, player2.address)[2] == 10)
        sc.p("P1 received 10 token_1 and 10 token_2")

        ###########
        #  Test 4 #
        ###########
        sc.h2("Self transfer")
        game_num += 1
        sc.h3("Player 1 transfers 5 tokens to himself")
        settlements, bonds = transfer_settlements(p1_to_p1 = {0:5})
        settlements = sc.compute(settlements)
        sc.show(settlements)

        sc.h3("P1 Instanciate new game : Transfer")
        constants = sc.compute(sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:player1.address, 2:player2.address},
                              game_nonce = "game"+str(game_num), play_delay = play_delay, bonds = bonds, settlements = settlements))
        game_id = sp.blake2b(sp.pack(constants))
        c1.new_game(constants = constants, params = sp.pack(sp.unit), signatures = new_game_sigs(constants)).run(sender = player1)

        sc.h3("(Onchain) play transfer")
        c1.play(game_id = game_id, move_data = sp.pack(sp.unit), move_nb = 0).run(sender = player1)
        sc.verify(gp.finished_outcome(c1, game_id) == "transfered")

        sc.h3("Settle game")
        sc.verify(gp.player_bonds(c1, channel_id, player1.address)[0] == 10)
        c1.settle(game_id).run(sender = player1)
        sc.verify(gp.player_bonds(c1, channel_id, player1.address)[0] == 10)
        sc.p("P1 tokens didn't changed")

        ###########
        #  Test 5 #
        ###########
        sc.h2("Transfer in both direction")
        game_num += 1
        sc.h3("Player 1 transfers 5 tokens 0 to player 2, 2 tokens to himself, Player 2 transfer 4 tokens 0 to player 1")
        settlements, bonds = transfer_settlements(p1_to_p2 = {0:5}, p1_to_p1 = {0:2}, p2_to_p1 = {0: 4})
        settlements = sc.compute(settlements)
        sc.show(settlements)

        sc.h3("Player 2 push {0: 10}")
        c1.push_bonds(player_addr = player2.address, channel_id = channel_id, bonds = {0: 10}).run(sender = player2)

        sc.h3("P1 Instanciate new game : Transfer")
        constants = sc.compute(sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:player1.address, 2:player2.address},
                              game_nonce = "game"+str(game_num), play_delay = play_delay, bonds = bonds, settlements = settlements))
        game_id = sp.blake2b(sp.pack(constants))
        c1.new_game(constants = constants, params = sp.pack(sp.unit), signatures = new_game_sigs(constants)).run(sender = player1)

        sc.h3("(Onchain) play transfer")
        c1.play(game_id = game_id, move_data = sp.pack(sp.unit), move_nb = 0).run(sender = player1)
        sc.verify(gp.finished_outcome(c1, game_id) == "transfered")

        sc.h3("Player 2 settle the game")
        sc.verify(gp.player_bonds(c1, channel_id, player1.address)[0] == 10)
        sc.verify(gp.player_bonds(c1, channel_id, player2.address)[0] == 10)
        c1.settle(game_id).run(sender = player2)
        sc.verify(gp.player_bonds(c1, channel_id, player1.address)[0] == 9)
        sc.verify(gp.player_bonds(c1, channel_id, player2.address)[0] == 11)
        sc.p("P1 lost 1 token, P2 won 1 token")

        ###########
        #  Test 6 #
        ###########
        sc.h2("Simple withdraw")
        sc.h3("Player 1 request withdraw 5 tokens")
        c1.withdraw_request(channel_id = channel_id, tokens = sp.map({0: 5})).run(sender = player1)
        sc.p("Player 1 channel bonds")
        sc.show(c1.data.channels[channel_id].players[player1.address])
        sc.h3("Player 2 request leave empty challenge")
        c1.withdraw_challenge(channel_id = channel_id, withdrawer = player1.address, withdraw_id = 1, game_ids = sp.set({})).run(sender = player2)
        sc.h3("Player 1 finalize withdraw request")
        sc.verify(gp.player_bonds(c1, channel_id, player1.address)[0] == 9)
        c1.finalise_withdraw(channel_id = channel_id, withdraw_id = 1).run(sender = player1)
        sc.verify(gp.player_bonds(c1, channel_id, player1.address)[0] == 4)
        sc.verify(c1.data.bank[player1.address][0] == 5)

    @sp.add_test(name="Withdraw challenges")
    def test():
        sc = sp.test_scenario()
        sc.table_of_contents()
        sc.h1("Withdraw challenges")
        sc.h2("Contract")
        c1 = gp.GamePlatform()
        sc += c1
        sc.h2("New model: Transfer")
        model = sc.compute(model_wrap(transfer))
        model_id = sc.compute(sp.blake2b(sp.pack(model)))
        c1.new_model(model = model).run(sender = player1)

        sc.h2("New channel")
        channelParams = sp.record(players = players, nonce = "Channel 1", withdraw_delay = 3600*24)
        channel_id = sp.blake2b(sp.pack((c1.address, channelParams.players, channelParams.nonce)))
        c1.new_channel(channelParams)

        sc.h2("Player 1 push {0: 10}")
        c1.push_bonds(player_addr = player1.address, channel_id = channel_id, bonds = {0: 10}).run(sender = player1)

        sc.h2("P1 Instanciate new game : Transfer")
        settlements, bonds = transfer_settlements(p1_to_p2 = {0:5})
        settlements = sc.compute(settlements)
        constants = sc.compute(sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:player1.address, 2:player2.address},
                                game_nonce = "game", play_delay = play_delay, bonds = bonds, settlements = settlements))
        game_id = sp.blake2b(sp.pack(constants))
        c1.new_game(constants = constants, params = sp.pack(sp.unit), signatures = new_game_sigs(constants)).run(sender = player1)

        ###########
        #  Test 1 #
        ###########
        sc.h2("Challenged withdraw")
        sc.h3("Player 1 request withdraw 8 tokens")
        c1.withdraw_request(channel_id = channel_id, tokens = sp.map({0: 8})).run(sender = player1)
        sc.h3("Player 2 challenge by pushing a game_id")
        c1.withdraw_challenge(channel_id = channel_id, withdrawer = player1.address, withdraw_id = 1, game_ids = sp.set({game_id})).run(sender = player2)
        sc.h3("Player 1 can't finalize withdraw")
        c1.finalise_withdraw(channel_id = channel_id, withdraw_id = 1).run(sender = player1, valid = False, exception = "Platform_TokenChallenged")
        sc.h3("Player 1 proposes an abort outcome for running transfer and Player 2 accepts")
        proposed_outcome = gp.action_new_outcome(game_id, "aborted")
        outcome_sigs = make_signatures(player1, player2, proposed_outcome)
        c1.set_outcome(game_id = game_id, outcome = "aborted", signatures = outcome_sigs).run(sender = player1)
        c1.settle(game_id).run(sender = player1)
        sc.h3("Player 1 resolve challenge")
        c1.withdraw_challenge(channel_id = channel_id, withdrawer = player1.address, withdraw_id = 1, game_ids = sp.set({game_id})).run(sender = player1)
        sc.h3("Player 1 finalize withdraw request")
        c1.finalise_withdraw(channel_id = channel_id, withdraw_id = 1).run(sender = player1)

        ###########
        #  Test 2 #
        ###########
        sc.h2("Multi challenged withdraw")
        sc.p("3 running transfers: 1 with low money, 2 with high money.<br/>\
              Player 1 resolves game 1 but it's not sufficient to withdraw and then resolve game 2, that become sufficient to withdraw")
        sc.h2("Player 1 push {1: 100}")
        c1.push_bonds(player_addr = player1.address, channel_id = channel_id, bonds = {1: 100}).run(sender = player1)
        sc.h2("Instanciate new transfers games")
        sc.h3("Game 1: 5 tokens 1")
        settlements, bonds = transfer_settlements(p1_to_p2 = {1:5})
        settlements = sc.compute(settlements)
        constants = sc.compute(sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:player1.address, 2:player2.address},
                                game_nonce = "game_1", play_delay = play_delay, bonds = bonds, settlements = settlements))
        game_id_1 = sp.blake2b(sp.pack(constants))
        c1.new_game(constants = constants, params = sp.pack(sp.unit), signatures = new_game_sigs(constants)).run(sender = player1)
        sc.h3("Game 2: 40 tokens 1")
        settlements, bonds = transfer_settlements(p1_to_p2 = {1:40})
        settlements = sc.compute(settlements)
        constants = sc.compute(sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:player1.address, 2:player2.address},
                                game_nonce = "game_2", play_delay = play_delay, bonds = bonds, settlements = settlements))
        game_id_2 = sp.blake2b(sp.pack(constants))
        c1.new_game(constants = constants, params = sp.pack(sp.unit), signatures = new_game_sigs(constants)).run(sender = player1)
        sc.h3("Game 3: 40 tokens 1")
        constants = sc.compute(sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:player1.address, 2:player2.address},
                                game_nonce = "game_3", play_delay = play_delay, bonds = bonds, settlements = settlements))
        game_id_3 = sp.blake2b(sp.pack(constants))
        c1.new_game(constants = constants, params = sp.pack(sp.unit), signatures = new_game_sigs(constants)).run(sender = player1)

        sc.h3("Player 1 request withdraw 50 tokens 1")
        c1.withdraw_request(channel_id = channel_id, tokens = sp.map({1: 50})).run(sender = player1)
        sc.h3("Player 2 challenge by pushing 3 game ids")
        c1.withdraw_challenge(channel_id = channel_id, withdrawer = player1.address, withdraw_id = 2, game_ids = sp.set([game_id_1, game_id_2, game_id_3])).run(sender = player2)
        sc.h3("Player 1 can't finalize withdraw")
        c1.finalise_withdraw(channel_id = channel_id, withdraw_id = 2).run(sender = player1, valid = False, exception = "Platform_TokenChallenged")

        sc.h3("Player 1 proposes an abort outcome for game_id_1 running transfer and Player 2 accepts")
        proposed_outcome = gp.action_new_outcome(game_id_1, "aborted")
        outcome_sigs = make_signatures(player1, player2, proposed_outcome)
        c1.set_outcome(game_id = game_id_1, outcome = "aborted", signatures = outcome_sigs).run(sender = player1)
        c1.settle(game_id_1).run(sender = player1)
        sc.h3("Player 1 tries to resolve challenge but still doesn't have resolved enough challenge")
        c1.withdraw_challenge(channel_id = channel_id, withdrawer = player1.address, withdraw_id = 2, game_ids = sp.set({game_id_1})).run(sender = player1)
        c1.finalise_withdraw(channel_id = channel_id, withdraw_id = 2).run(sender = player1, valid = False, exception = "Platform_TokenChallenged")

        sc.h3("Player 1 proposes an abort outcome for game_id_2 running transfer and Player 2 accepts")
        proposed_outcome = gp.action_new_outcome(game_id_2, "aborted")
        outcome_sigs = make_signatures(player1, player2, proposed_outcome)
        c1.set_outcome(game_id = game_id_2, outcome = "aborted", signatures = outcome_sigs).run(sender = player1)
        c1.settle(game_id_2).run(sender = player1)
        sc.h3("Player 1 resolves challenge")
        c1.withdraw_challenge(channel_id = channel_id, withdrawer = player1.address, withdraw_id = 2, game_ids = sp.set({game_id_2})).run(sender = player1)
        sc.h3("Player 1 finalizes withdraw request")
        c1.finalise_withdraw(channel_id = channel_id, withdraw_id = 2).run(sender = player1)

    @sp.add_test(name="Withdraw timeout and not enough tokens")
    def test():
        sc = sp.test_scenario()
        sc.table_of_contents()
        sc.h1("Expected transfer errors")
        sc.h2("Contract")
        c1 = gp.GamePlatform()
        sc += c1
        sc.h2("New model: Transfer")
        model = sc.compute(model_wrap(transfer))
        c1.new_model(model = model).run(sender = player1)

        sc.h2("New channel")
        channelParams = sp.record(players = players, nonce = "Channel 1", withdraw_delay = 3600*24)
        channel_id = sp.blake2b(sp.pack((c1.address, channelParams.players, channelParams.nonce)))
        c1.new_channel(channelParams)

        sc.h2("Player 1 push {0: 10}")
        c1.push_bonds(player_addr = player1.address, channel_id = channel_id, bonds = {0: 10}).run(sender = player1)

        sc.h2("Not enough tokens")
        sc.h3("Player 1 request withdraw 50 tokens")
        c1.withdraw_request(channel_id = channel_id, tokens = sp.map({0: 50})).run(sender = player1)
        sc.h3("Player 1 fails to withdraw 50 tokens")
        c1.finalise_withdraw(channel_id = channel_id, withdraw_id = 1).run(sender = player1, now = sp.timestamp(3600 * 24 + 1), valid = False, exception = "Platform_NotEnoughTokens")

        sc.h3("Player 1 request withdraw 5 tokens")
        c1.withdraw_request(channel_id = channel_id, tokens = sp.map({0: 5})).run(sender = player1)
        sc.h3("Player 1 can't finalize before timeout")
        c1.finalise_withdraw(channel_id = channel_id, withdraw_id = 2).run(sender = player1, valid = False, exception = "Platform_ChallengeDelayNotOver")
        sc.h3("Player 1 finalize after timeout")
        c1.finalise_withdraw(channel_id = channel_id, withdraw_id = 2).run(sender = player1, now = sp.timestamp(3600 * 24 * 2 + 2))
        sc.verify(gp.player_bonds(c1, channel_id, player1.address)[0] == 5)
        sc.verify(c1.data.bank[player1.address][0] == 5)

    @sp.add_test(name="Settled game Challenge")
    def test():
        sc = sp.test_scenario()
        sc.table_of_contents()
        sc.h1("Settled game Challenge")
        sc.h2("Contract")
        c1 = gp.GamePlatform()
        sc += c1
        sc.h2("New model: Transfer")
        model = sc.compute(model_wrap(transfer))
        model_id = sc.compute(sp.blake2b(sp.pack(model)))
        c1.new_model(model = model).run(sender = player1)

        sc.h2("New channel")
        channelParams = sp.record(players = players, nonce = "Channel 1", withdraw_delay = 3600*24)
        channel_id = sp.blake2b(sp.pack((c1.address, channelParams.players, channelParams.nonce)))
        c1.new_channel(channelParams)

        sc.h2("Player 1 and 2 push {0: 10}")
        c1.push_bonds(player_addr = player1.address, channel_id = channel_id, bonds = {0: 10}).run(sender = player1)

        sc.h2("P1 Instanciate new game : Transfer")
        settlements, bonds = transfer_settlements(p1_to_p2 = {0:100})
        settlements = sc.compute(settlements)
        constants = sc.compute(sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:player1.address, 2:player2.address},
                                game_nonce = "game", play_delay = play_delay, bonds = bonds, settlements = settlements))
        game_id = sp.blake2b(sp.pack(constants))
        c1.new_game(constants = constants, params = sp.pack(sp.unit), signatures = new_game_sigs(constants)).run(sender = player1)

        sc.h3("Player 1 proposes an abort outcome for game_id running transfer and Player 2 accepts")
        proposed_outcome = gp.action_new_outcome(game_id, "aborted")
        outcome_sigs = make_signatures(player1, player2, proposed_outcome)
        c1.set_outcome(game_id = game_id, outcome = "aborted", signatures = outcome_sigs).run(sender = player1)
        c1.settle(game_id).run(sender = player1)

        sc.h2("Player 2 tries to challenge with a settled game")
        sc.h3("Player 1 request withdraw 5 tokens")
        c1.withdraw_request(channel_id = channel_id, tokens = sp.map({0: 5})).run(sender = player1)
        sc.h3("Player 2 fails to push settled game id as a challenge")
        c1.withdraw_challenge(channel_id = channel_id, withdrawer = player1.address,
            withdraw_id = 1, game_ids = sp.set({game_id})).run(sender = player2, valid = False, exception = "Platform_GameSettled")

    @sp.add_test(name="Expected errors")
    def test():
        sc = sp.test_scenario()
        sc.table_of_contents()
        sc.h1("Expected transfer errors")
        sc.h2("Contract")
        c1 = gp.GamePlatform()
        sc += c1
        sc.h2("New model: Transfer")
        model = sc.compute(model_wrap(transfer))
        model_id = sc.compute(sp.blake2b(sp.pack(model)))
        c1.new_model(model = model).run(sender = player1)

        sc.h2("New channel")
        channelParams = sp.record(players = players, nonce = "Channel 1", withdraw_delay = 3600*24)
        channel_id = sp.blake2b(sp.pack((c1.address, channelParams.players, channelParams.nonce)))
        c1.new_channel(channelParams)

        sc.h3("P1 Instanciate new game : Transfer")
        settlements, bonds = transfer_settlements(p1_to_p2 = {0:10})
        settlements = sc.compute(settlements)
        constants = sc.compute(sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:player1.address, 2:player2.address},
                                game_nonce = "game1", play_delay = play_delay, bonds = bonds, settlements = settlements))
        game_id = sp.blake2b(sp.pack(constants))
        c1.new_game(constants = constants, params = sp.pack(sp.unit), signatures = new_game_sigs(constants)).run(sender = player1)

        sc.h3("Player 1 plays")
        c1.play(game_id = game_id, move_data = sp.pack(sp.unit), move_nb = 0).run(sender = player1)

        sc.h2("(fail) Settle: Don't own token")
        error_message = sp.pair("Platform_NotEnoughToken:", sp.record(sender = player1.address, token = 0, amount = 10))
        sp.set_type_expr(error_message, sp.TPair(sp.TString, sp.TRecord(sender = sp.TAddress, token = sp.TNat, amount = sp.TNat)))
        c1.settle(game_id).run(sender = player1, valid = False, exception = error_message)

        sc.h2("Push 5 tokens 0")
        c1.push_bonds(player_addr = player1.address, channel_id = channel_id, bonds = {0: 5})

        sc.h2("fail) Settle: No enough tokens")
        error_message = sp.pair("Platform_NotEnoughToken:", sp.record(sender = player1.address, token = 0, amount = 10))
        sp.set_type_expr(error_message, sp.TPair(sp.TString, sp.TRecord(sender = sp.TAddress, token = sp.TNat, amount = sp.TNat, bond = gp.types.t_token_map)))
        c1.settle(game_id).run(sender = player1, valid = False, exception = error_message)

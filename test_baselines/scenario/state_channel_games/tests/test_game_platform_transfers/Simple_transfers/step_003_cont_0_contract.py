import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(bank = sp.TBigMap(sp.TAddress, sp.TMap(sp.TNat, sp.TNat)), channels = sp.TBigMap(sp.TBytes, sp.TRecord(closed = sp.TBool, nonce = sp.TString, players = sp.TMap(sp.TAddress, sp.TRecord(bonds = sp.TMap(sp.TNat, sp.TNat), pk = sp.TKey, withdraw = sp.TOption(sp.TRecord(challenge = sp.TSet(sp.TBytes), challenge_tokens = sp.TMap(sp.TNat, sp.TInt), timeout = sp.TTimestamp, tokens = sp.TMap(sp.TNat, sp.TNat)).layout(("challenge", ("challenge_tokens", ("timeout", "tokens"))))), withdraw_id = sp.TNat).layout(("bonds", ("pk", ("withdraw", "withdraw_id"))))), withdraw_delay = sp.TInt).layout(("closed", ("nonce", ("players", "withdraw_delay"))))), games = sp.TBigMap(sp.TBytes, sp.TRecord(addr_players = sp.TMap(sp.TAddress, sp.TInt), constants = sp.TRecord(bonds = sp.TMap(sp.TInt, sp.TMap(sp.TNat, sp.TNat)), channel_id = sp.TBytes, game_nonce = sp.TString, model_id = sp.TBytes, play_delay = sp.TInt, players_addr = sp.TMap(sp.TInt, sp.TAddress), settlements = sp.TMap(sp.TVariant(game_finished = sp.TString, player_double_played = sp.TInt, player_inactive = sp.TInt).layout(("game_finished", ("player_double_played", "player_inactive"))), sp.TList(sp.TRecord(bonds = sp.TMap(sp.TNat, sp.TNat), receiver = sp.TInt, sender = sp.TInt).layout(("bonds", ("receiver", "sender")))))).layout(("bonds", ("channel_id", ("game_nonce", ("model_id", ("play_delay", ("players_addr", "settlements"))))))), meta = sp.TRecord(current_player = sp.TInt, move_nb = sp.TNat, outcome = sp.TOption(sp.TVariant(game_finished = sp.TString, player_double_played = sp.TInt, player_inactive = sp.TInt).layout(("game_finished", ("player_double_played", "player_inactive"))))).layout(("current_player", ("move_nb", "outcome"))), settled = sp.TBool, state = sp.TBytes, timeouts = sp.TMap(sp.TInt, sp.TTimestamp)).layout(("addr_players", ("constants", ("meta", ("settled", ("state", "timeouts"))))))), models = sp.TBigMap(sp.TBytes, sp.TRecord(apply_ = sp.TLambda(sp.TRecord(meta = sp.TRecord(current_player = sp.TInt, move_nb = sp.TNat, outcome = sp.TOption(sp.TVariant(game_finished = sp.TString, player_double_played = sp.TInt, player_inactive = sp.TInt).layout(("game_finished", ("player_double_played", "player_inactive"))))).layout(("current_player", ("move_nb", "outcome"))), move = sp.TBytes, state = sp.TBytes).layout(("meta", ("move", "state"))), sp.TRecord(messages = sp.TBytes, new_state = sp.TBytes, outcome = sp.TOption(sp.TString)).layout(("messages", ("new_state", "outcome")))), init = sp.TLambda(sp.TBytes, sp.TBytes)).layout(("apply_", "init")))).layout(("bank", ("channels", ("games", "models")))))
    self.init(bank = {},
              channels = {},
              games = {},
              models = {})

  @sp.entry_point
  def bank_to_bonds(self, params):
    sp.verify(self.data.bank.contains(sp.sender), 'Platform_NotEnoughToken')
    sp.verify(self.data.channels.contains(params.channel_id), ('Platform_ChannelNotFound: ', params.channel_id))
    channel = sp.local("channel", self.data.channels[params.channel_id])
    sp.verify(~ channel.value.closed, 'Platform_ChannelClosed')
    sp.verify(channel.value.players.contains(params.receiver), 'Platform_ReceiverNotInChannel')
    sender = sp.local("sender", self.data.bank[sp.sender])
    to = sp.local("to", channel.value.players[params.receiver])
    sp.for token in params.tokens.items():
      sp.if token.value > 0:
        sp.verify(sender.value.contains(token.key), 'Platform_NotEnoughToken')
        sender.value[token.key] = sp.as_nat(sender.value[token.key] - token.value, message = 'Platform_NotEnoughToken')
        sp.if to.value.bonds.contains(token.key):
          to.value.bonds[token.key] += token.value
        sp.else:
          to.value.bonds[token.key] = token.value
    self.data.channels[params.channel_id].players[params.receiver] = to.value
    self.data.bank[sp.sender] = sender.value

  @sp.entry_point
  def cancel_new_game(self, params):
    sp.verify(self.data.games.contains(params), ('Platform_GameNotFound: ', params))
    game = sp.local("game", self.data.games[params])
    sp.verify(sp.sender == game.value.constants.players_addr[1], 'Platform_NotGameCreator')
    del self.data.games[params]

  @sp.entry_point
  def double_signed(self, params):
    sp.verify(self.data.games.contains(params.game_id), ('Platform_GameNotFound: ', params.game_id))
    game = sp.local("game", self.data.games[params.game_id])
    sp.verify(game.value.meta.outcome == sp.none, 'Platform_NotRunning')
    sp.verify(params.new_meta.move_nb == game.value.meta.move_nb, 'Platform_NotSameMove')
    sp.verify(~ ((params.new_meta == game.value.meta) & (params.new_state == game.value.state)), 'Platform_NotDifferentStates')
    compute_game_platform_415 = sp.local("compute_game_platform_415", sp.pack(('New State', sp.set_type_expr(params.game_id, sp.TBytes), sp.set_type_expr(params.new_meta, sp.TRecord(current_player = sp.TInt, move_nb = sp.TNat, outcome = sp.TOption(sp.TVariant(game_finished = sp.TString, player_double_played = sp.TInt, player_inactive = sp.TInt).layout(("game_finished", ("player_double_played", "player_inactive"))))).layout(("current_player", ("move_nb", "outcome")))), sp.set_type_expr(params.new_state, sp.TBytes))))
    sp.set_type(game.value.constants.players_addr, sp.TMap(sp.TInt, sp.TAddress))
    sp.verify(sp.check_signature(self.data.channels[game.value.constants.channel_id].players[game.value.constants.players_addr[3 - game.value.meta.current_player]].pk, params.sig, compute_game_platform_415.value), 'Platform_badSig')
    game.value.meta.outcome = sp.some(variant('player_double_played', game.value.meta.current_player))
    self.data.games[params.game_id] = game.value

  @sp.entry_point
  def double_signed_offchain(self, params):
    sp.set_type(params.new_meta1, sp.TRecord(current_player = sp.TInt, move_nb = sp.TNat, outcome = sp.TOption(sp.TVariant(game_finished = sp.TString, player_double_played = sp.TInt, player_inactive = sp.TInt).layout(("game_finished", ("player_double_played", "player_inactive"))))).layout(("current_player", ("move_nb", "outcome"))))
    sp.set_type(params.new_meta2, sp.TRecord(current_player = sp.TInt, move_nb = sp.TNat, outcome = sp.TOption(sp.TVariant(game_finished = sp.TString, player_double_played = sp.TInt, player_inactive = sp.TInt).layout(("game_finished", ("player_double_played", "player_inactive"))))).layout(("current_player", ("move_nb", "outcome"))))
    sp.set_type(params.new_state1, sp.TBytes)
    sp.set_type(params.new_state2, sp.TBytes)
    sp.verify(self.data.games.contains(params.game_id), ('Platform_GameNotFound: ', params.game_id))
    game = sp.local("game", self.data.games[params.game_id])
    sp.verify(game.value.meta.outcome == sp.none, 'Platform_NotRunning')
    sp.verify(params.new_meta1.move_nb == params.new_meta2.move_nb)
    sp.verify(~ ((params.new_meta1 == params.new_meta2) & (params.new_state1 == params.new_state2)), 'Platform_NotSameStates')
    compute_game_platform_431 = sp.local("compute_game_platform_431", sp.pack(('New State', sp.set_type_expr(params.game_id, sp.TBytes), sp.set_type_expr(params.new_meta1, sp.TRecord(current_player = sp.TInt, move_nb = sp.TNat, outcome = sp.TOption(sp.TVariant(game_finished = sp.TString, player_double_played = sp.TInt, player_inactive = sp.TInt).layout(("game_finished", ("player_double_played", "player_inactive"))))).layout(("current_player", ("move_nb", "outcome")))), sp.set_type_expr(params.new_state1, sp.TBytes))))
    compute_game_platform_432 = sp.local("compute_game_platform_432", sp.pack(('New State', sp.set_type_expr(params.game_id, sp.TBytes), sp.set_type_expr(params.new_meta2, sp.TRecord(current_player = sp.TInt, move_nb = sp.TNat, outcome = sp.TOption(sp.TVariant(game_finished = sp.TString, player_double_played = sp.TInt, player_inactive = sp.TInt).layout(("game_finished", ("player_double_played", "player_inactive"))))).layout(("current_player", ("move_nb", "outcome")))), sp.set_type_expr(params.new_state2, sp.TBytes))))
    sp.set_type(game.value.constants.players_addr, sp.TMap(sp.TInt, sp.TAddress))
    sp.verify(sp.check_signature(self.data.channels[game.value.constants.channel_id].players[game.value.constants.players_addr[params.player]].pk, params.sig1, compute_game_platform_431.value), 'Platform_badSig')
    sp.set_type(game.value.constants.players_addr, sp.TMap(sp.TInt, sp.TAddress))
    sp.verify(sp.check_signature(self.data.channels[game.value.constants.channel_id].players[game.value.constants.players_addr[params.player]].pk, params.sig2, compute_game_platform_432.value), 'Platform_badSig')
    game.value.meta.outcome = sp.some(variant('player_double_played', params.player))
    self.data.games[params.game_id] = game.value

  @sp.entry_point
  def finalise_withdraw(self, params):
    sp.verify(self.data.channels.contains(params.channel_id), ('Platform_ChannelNotFound: ', params.channel_id))
    channel = sp.local("channel", self.data.channels[params.channel_id])
    sp.verify(~ channel.value.closed, 'Platform_ChannelClosed')
    sp.verify(channel.value.players.contains(sp.sender), 'Platform_SenderNotInChannel')
    player = sp.local("player", channel.value.players[sp.sender])
    sp.verify(player.value.withdraw.is_some(), 'Platform_NoWithdrawOpened')
    sp.verify(player.value.withdraw_id == params.withdraw_id, 'Platform_WithdrawIdMismatch')
    withdraw = sp.local("withdraw", player.value.withdraw.open_some())
    sp.verify(sp.now >= withdraw.value.timeout, 'Platform_ChallengeDelayNotOver')
    sp.for token in withdraw.value.challenge_tokens.items():
      sp.if (token.value > 0) & (player.value.bonds.contains(token.key)):
        sp.verify((player.value.bonds[token.key] - withdraw.value.tokens[token.key]) > token.value, 'Platform_TokenChallenged')
    bank = sp.local("bank", {})
    sp.if self.data.bank.contains(sp.sender):
      bank.value = self.data.bank[sp.sender]
    sp.for token in withdraw.value.tokens.items():
      sp.if token.value > 0:
        sp.verify(player.value.bonds.contains(token.key), 'Platform_NotEnoughTokens')
        player.value.bonds[token.key] = sp.as_nat(player.value.bonds[token.key] - token.value, message = 'Platform_NotEnoughTokens')
        sp.if bank.value.contains(token.key):
          bank.value[token.key] += token.value
        sp.else:
          bank.value[token.key] = token.value
    self.data.bank[sp.sender] = bank.value
    player.value.withdraw = sp.none
    channel.value.players[sp.sender] = player.value
    self.data.channels[params.channel_id] = channel.value

  @sp.entry_point
  def new_channel(self, params):
    sp.verify(~ (self.data.channels.contains(sp.blake2b(sp.pack((sp.self_address, params.players, params.nonce))))), 'Platform_ChannelAlreadyExists')
    players_map = sp.local("players_map", {})
    sp.for player in params.players.items():
      players_map.value[player.key] = sp.record(bonds = {}, pk = player.value, withdraw = sp.none, withdraw_id = 0)
    self.data.channels[sp.blake2b(sp.pack((sp.self_address, params.players, params.nonce)))] = sp.record(closed = False, nonce = params.nonce, players = players_map.value, withdraw_delay = params.withdraw_delay)

  @sp.entry_point
  def new_game(self, params):
    sp.set_type(params.constants, sp.TRecord(bonds = sp.TMap(sp.TInt, sp.TMap(sp.TNat, sp.TNat)), channel_id = sp.TBytes, game_nonce = sp.TString, model_id = sp.TBytes, play_delay = sp.TInt, players_addr = sp.TMap(sp.TInt, sp.TAddress), settlements = sp.TMap(sp.TVariant(game_finished = sp.TString, player_double_played = sp.TInt, player_inactive = sp.TInt).layout(("game_finished", ("player_double_played", "player_inactive"))), sp.TList(sp.TRecord(bonds = sp.TMap(sp.TNat, sp.TNat), receiver = sp.TInt, sender = sp.TInt).layout(("bonds", ("receiver", "sender")))))).layout(("bonds", ("channel_id", ("game_nonce", ("model_id", ("play_delay", ("players_addr", "settlements"))))))))
    sp.set_type(params.params, sp.TBytes)
    sp.set_type(params.signatures, sp.TMap(sp.TKey, sp.TSignature))
    sp.verify(~ (self.data.games.contains(sp.blake2b(sp.pack(params.constants)))), 'Platform_GameAlreadyExists')
    sp.verify(self.data.channels.contains(params.constants.channel_id), ('Platform_ChannelNotFound: ', params.constants.channel_id))
    channel = sp.local("channel", self.data.channels[params.constants.channel_id])
    sp.verify(~ channel.value.closed, 'Platform_ChannelClosed')
    addr_players = sp.local("addr_players", {})
    sp.for addr_player in params.constants.players_addr.items():
      addr_players.value[addr_player.value] = addr_player.key
      sp.verify(channel.value.players.contains(addr_player.value), 'Platform_GamePlayerNotInChannel')
    sp.for player in channel.value.players.values():
      sp.verify(params.signatures.contains(player.pk), 'Platform_MissingSig')
      sp.verify(sp.check_signature(player.pk, params.signatures[player.pk], sp.pack(('New Game', sp.set_type_expr(params.constants, sp.TRecord(bonds = sp.TMap(sp.TInt, sp.TMap(sp.TNat, sp.TNat)), channel_id = sp.TBytes, game_nonce = sp.TString, model_id = sp.TBytes, play_delay = sp.TInt, players_addr = sp.TMap(sp.TInt, sp.TAddress), settlements = sp.TMap(sp.TVariant(game_finished = sp.TString, player_double_played = sp.TInt, player_inactive = sp.TInt).layout(("game_finished", ("player_double_played", "player_inactive"))), sp.TList(sp.TRecord(bonds = sp.TMap(sp.TNat, sp.TNat), receiver = sp.TInt, sender = sp.TInt).layout(("bonds", ("receiver", "sender")))))).layout(("bonds", ("channel_id", ("game_nonce", ("model_id", ("play_delay", ("players_addr", "settlements")))))))), sp.set_type_expr(params.params, sp.TBytes)))))
    self.data.games[sp.blake2b(sp.pack(params.constants))] = sp.record(addr_players = addr_players.value, constants = params.constants, meta = sp.record(current_player = 1, move_nb = 0, outcome = sp.set_type_expr(sp.none, sp.TOption(sp.TVariant(game_finished = sp.TString, player_double_played = sp.TInt, player_inactive = sp.TInt).layout(("game_finished", ("player_double_played", "player_inactive")))))), settled = False, state = self.data.models[params.constants.model_id].init(params.params), timeouts = sp.set_type_expr({}, sp.TMap(sp.TInt, sp.TTimestamp)))

  @sp.entry_point
  def new_model(self, params):
    self.data.models[sp.blake2b(sp.pack(params.model))] = params.model

  @sp.entry_point
  def play(self, params):
    sp.verify(self.data.games.contains(params.game_id), ('Platform_GameNotFound: ', params.game_id))
    game = sp.local("game", self.data.games[params.game_id])
    sp.verify(game.value.meta.outcome == sp.none, 'Platform_NotRunning')
    sp.set_type(params.move_nb, sp.TNat)
    sp.set_type(params.move_data, sp.TBytes)
    sp.set_type(game.value.constants.players_addr, sp.TMap(sp.TInt, sp.TAddress))
    sp.verify(sp.sender == game.value.constants.players_addr[game.value.meta.current_player], 'Platform_GameWrongPlayer')
    sp.verify(game.value.meta.move_nb == params.move_nb, 'Platform_Wrongmove_nb')
    sp.verify(game.value.meta.outcome == sp.none, 'Platform_GameNotRunning')
    compute_game_platform_305 = sp.local("compute_game_platform_305", self.data.models[game.value.constants.model_id].apply_(sp.record(meta = game.value.meta, move = params.move_data, state = game.value.state)))
    game.value.meta = sp.record(current_player = 3 - game.value.meta.current_player, move_nb = game.value.meta.move_nb + 1, outcome = sp.eif(compute_game_platform_305.value.outcome.is_some(), sp.some(variant('game_finished', compute_game_platform_305.value.outcome.open_some())), sp.none))
    game.value.state = compute_game_platform_305.value.new_state
    sp.if game.value.timeouts.contains(game.value.meta.current_player):
      game.value.timeouts[game.value.meta.current_player] = sp.add_seconds(sp.now, game.value.constants.play_delay)
    self.data.games[params.game_id] = game.value

  @sp.entry_point
  def push_bonds(self, params):
    sp.verify(self.data.channels.contains(params.channel_id), ('Platform_ChannelNotFound: ', params.channel_id))
    channel = sp.local("channel", self.data.channels[params.channel_id])
    sp.verify(~ channel.value.closed, 'Platform_ChannelClosed')
    sp.verify(channel.value.players.contains(params.player_addr), 'Platform_NotChannelPlayer')
    player = sp.local("player", channel.value.players[params.player_addr])
    sp.for token in params.bonds.keys():
      sp.if player.value.bonds.contains(token):
        player.value.bonds[token] += params.bonds[token]
      sp.else:
        player.value.bonds[token] = params.bonds[token]
    self.data.channels[params.channel_id].players[params.player_addr] = player.value

  @sp.entry_point
  def set_outcome(self, params):
    sp.set_type(params.signatures, sp.TMap(sp.TKey, sp.TSignature))
    sp.verify(self.data.games.contains(params.game_id), ('Platform_GameNotFound: ', params.game_id))
    game = sp.local("game", self.data.games[params.game_id])
    sp.verify(game.value.meta.outcome == sp.none, 'Platform_NotRunning')
    compute_game_platform_324 = sp.local("compute_game_platform_324", sp.pack(('New Outcome', sp.set_type_expr(params.game_id, sp.TBytes), sp.set_type_expr(params.outcome, sp.TString))))
    players = sp.local("players", self.data.channels[game.value.constants.channel_id].players)
    sp.for player in players.value.values():
      sp.verify(params.signatures.contains(player.pk), 'Platform_MissingSig')
      sp.verify(sp.check_signature(player.pk, params.signatures[player.pk], compute_game_platform_324.value), 'Platform_badSig')
    game.value.meta.outcome = sp.some(variant('game_finished', params.outcome))
    self.data.games[params.game_id] = game.value

  @sp.entry_point
  def settle(self, params):
    sp.verify(self.data.games.contains(params), ('Platform_GameNotFound: ', params))
    game = sp.local("game", self.data.games[params])
    sp.verify(~ game.value.settled, 'Plateform_GameSettled')
    sp.if game.value.constants.settlements.contains(game.value.meta.outcome.open_some(message = 'Plateform_GameIsntOver')):
      compute_game_platform_361 = sp.local("compute_game_platform_361", game.value.constants.settlements[game.value.meta.outcome.open_some()])
      sp.verify(self.data.channels.contains(game.value.constants.channel_id), ('Platform_ChannelNotFound: ', game.value.constants.channel_id))
      channel = sp.local("channel", self.data.channels[game.value.constants.channel_id])
      sp.verify(~ channel.value.closed, 'Platform_ChannelClosed')
      sp.for transfer in compute_game_platform_361.value:
        compute_game_platform_212 = sp.local("compute_game_platform_212", game.value.constants.players_addr[transfer.sender])
        compute_game_platform_213 = sp.local("compute_game_platform_213", game.value.constants.players_addr[transfer.receiver])
        sp.if ~ (compute_game_platform_212.value == compute_game_platform_213.value):
          sender = sp.local("sender", channel.value.players[compute_game_platform_212.value])
          receiver = sp.local("receiver", channel.value.players[compute_game_platform_213.value])
          sp.for bond in transfer.bonds.items():
            sp.if ~ (bond.value == 0):
              sp.verify(sender.value.bonds.contains(bond.key), ('Platform_NotEnoughToken:', sp.record(amount = bond.value, sender = compute_game_platform_212.value, token = bond.key)))
              sender.value.bonds[bond.key] = sp.as_nat(sender.value.bonds[bond.key] - bond.value, message = ('Platform_NotEnoughToken:', sp.record(amount = bond.value, sender = compute_game_platform_212.value, token = bond.key)))
              sp.if sender.value.bonds[bond.key] == 0:
                del sender.value.bonds[bond.key]
              sp.if receiver.value.bonds.contains(bond.key):
                receiver.value.bonds[bond.key] += bond.value
              sp.else:
                receiver.value.bonds[bond.key] = bond.value
          channel.value.players[compute_game_platform_212.value] = sender.value
          channel.value.players[compute_game_platform_213.value] = receiver.value
      self.data.channels[game.value.constants.channel_id].players = channel.value.players
    game.value.settled = True
    self.data.games[params] = game.value

  @sp.entry_point
  def starved(self, params):
    sp.verify(self.data.games.contains(params.game_id), ('Platform_GameNotFound: ', params.game_id))
    game = sp.local("game", self.data.games[params.game_id])
    sp.verify(game.value.meta.outcome == sp.none, 'Platform_NotRunning')
    sp.verify(game.value.timeouts.contains(params.player_id), 'Platform_NotTimeoutSetup')
    sp.verify(sp.now > game.value.timeouts[params.player_id], 'Platform_NotTimedOut')
    game.value.meta.outcome = sp.some(variant('player_inactive', params.player_id))
    self.data.games[params.game_id] = game.value

  @sp.entry_point
  def starving(self, params):
    sp.verify(self.data.games.contains(params.game_id), ('Platform_GameNotFound: ', params.game_id))
    game = sp.local("game", self.data.games[params.game_id])
    sp.verify(game.value.meta.outcome == sp.none, 'Platform_NotRunning')
    sp.if params.flag:
      game.value.timeouts[3 - game.value.addr_players[sp.sender]] = sp.add_seconds(sp.now, game.value.constants.play_delay)
    sp.else:
      del game.value.timeouts[3 - game.value.addr_players[sp.sender]]
    self.data.games[params.game_id] = game.value

  @sp.entry_point
  def update_state(self, params):
    sp.set_type(params.signatures, sp.TMap(sp.TKey, sp.TSignature))
    sp.verify(self.data.games.contains(params.game_id), ('Platform_GameNotFound: ', params.game_id))
    game = sp.local("game", self.data.games[params.game_id])
    sp.verify(game.value.meta.outcome == sp.none, 'Platform_NotRunning')
    sp.verify(params.new_meta.move_nb > game.value.meta.move_nb)
    compute_game_platform_379 = sp.local("compute_game_platform_379", sp.pack(('New State', sp.set_type_expr(params.game_id, sp.TBytes), sp.set_type_expr(params.new_meta, sp.TRecord(current_player = sp.TInt, move_nb = sp.TNat, outcome = sp.TOption(sp.TVariant(game_finished = sp.TString, player_double_played = sp.TInt, player_inactive = sp.TInt).layout(("game_finished", ("player_double_played", "player_inactive"))))).layout(("current_player", ("move_nb", "outcome")))), sp.set_type_expr(params.new_state, sp.TBytes))))
    players = sp.local("players", self.data.channels[game.value.constants.channel_id].players)
    sp.for player in players.value.values():
      sp.verify(sp.check_signature(player.pk, params.signatures[player.pk], compute_game_platform_379.value), 'Platform_badSig')
    game.value.meta = params.new_meta
    game.value.state = params.new_state
    sp.if game.value.timeouts.contains(game.value.meta.current_player):
      game.value.timeouts[game.value.meta.current_player] = sp.add_seconds(sp.now, game.value.constants.play_delay)
    self.data.games[params.game_id] = game.value

  @sp.entry_point
  def withdraw_challenge(self, params):
    sp.set_type(params.game_ids, sp.TSet(sp.TBytes))
    sp.verify(self.data.channels.contains(params.channel_id), ('Platform_ChannelNotFound: ', params.channel_id))
    channel = sp.local("channel", self.data.channels[params.channel_id])
    sp.verify(~ channel.value.closed, 'Platform_ChannelClosed')
    sp.verify(channel.value.players.contains(sp.sender), 'Platform_SenderNotInChannel')
    sp.verify(channel.value.players.contains(params.withdrawer), 'Platform_WithdrawerNotInChannel')
    sp.verify(channel.value.players[params.withdrawer].withdraw.is_some(), 'Platform_NoWithdrawRequestOpened')
    sp.verify(channel.value.players[params.withdrawer].withdraw_id == params.withdraw_id, 'Platform_WithdrawIdMismatch')
    withdraw = sp.local("withdraw", channel.value.players[params.withdrawer].withdraw.open_some())
    sp.for game_id in params.game_ids.elements():
      sp.verify(self.data.games.contains(game_id), ('Platform_GameNotFound: ', game_id))
      game = sp.local("game", self.data.games[game_id])
      sp.verify(game.value.constants.channel_id == params.channel_id, 'Platform_ChannelIdMismatch')
      sp.if game.value.settled:
        sp.verify(withdraw.value.challenge.contains(game_id), 'Platform_GameSettled')
        withdraw.value.challenge.remove(game_id)
        sp.set_type(withdraw.value.challenge_tokens, sp.TMap(sp.TNat, sp.TInt))
        sp.set_type(game.value.constants.bonds[game.value.addr_players[params.withdrawer]], sp.TMap(sp.TNat, sp.TNat))
        sp.for token in game.value.constants.bonds[game.value.addr_players[params.withdrawer]].items():
          sp.if withdraw.value.challenge_tokens.contains(token.key):
            compute_game_platform_141 = sp.local("compute_game_platform_141", withdraw.value.challenge_tokens[token.key] - sp.to_int(game.value.constants.bonds[game.value.addr_players[params.withdrawer]][token.key]))
            sp.if compute_game_platform_141.value == 0:
              del withdraw.value.challenge_tokens[token.key]
            sp.else:
              withdraw.value.challenge_tokens[token.key] = compute_game_platform_141.value
          sp.else:
            withdraw.value.challenge_tokens[token.key] = 0 - sp.to_int(game.value.constants.bonds[game.value.addr_players[params.withdrawer]][token.key])
      sp.else:
        sp.if ~ (withdraw.value.challenge.contains(game_id)):
          withdraw.value.challenge.add(game_id)
          sp.set_type(withdraw.value.challenge_tokens, sp.TMap(sp.TNat, sp.TInt))
          sp.set_type(game.value.constants.bonds[game.value.addr_players[params.withdrawer]], sp.TMap(sp.TNat, sp.TNat))
          sp.for token in game.value.constants.bonds[game.value.addr_players[params.withdrawer]].items():
            sp.if withdraw.value.challenge_tokens.contains(token.key):
              withdraw.value.challenge_tokens[token.key] += sp.to_int(game.value.constants.bonds[game.value.addr_players[params.withdrawer]][token.key])
            sp.else:
              sp.if ~ (sp.to_int(game.value.constants.bonds[game.value.addr_players[params.withdrawer]][token.key]) == 0):
                withdraw.value.challenge_tokens[token.key] = sp.to_int(game.value.constants.bonds[game.value.addr_players[params.withdrawer]][token.key])
    sp.if ~ (params.withdrawer == sp.sender):
      withdraw.value.timeout = sp.now
    channel.value.players[params.withdrawer].withdraw = sp.some(withdraw.value)
    self.data.channels[params.channel_id] = channel.value

  @sp.entry_point
  def withdraw_request(self, params):
    sp.verify(self.data.channels.contains(params.channel_id), ('Platform_ChannelNotFound: ', params.channel_id))
    channel = sp.local("channel", self.data.channels[params.channel_id])
    sp.verify(~ channel.value.closed, 'Platform_ChannelClosed')
    sp.verify(channel.value.players.contains(sp.sender), 'Platform_SenderNotInChannel')
    player = sp.local("player", channel.value.players[sp.sender])
    player.value.withdraw_id += 1
    player.value.withdraw = sp.some(sp.record(challenge = sp.set([]), challenge_tokens = {}, timeout = sp.add_seconds(sp.now, channel.value.withdraw_delay), tokens = params.tokens))
    channel.value.players[sp.sender] = player.value
    self.data.channels[params.channel_id] = channel.value
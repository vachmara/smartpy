import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(apply_ = sp.TLambda(sp.TRecord(meta = sp.TRecord(current_player = sp.TInt, move_nb = sp.TNat, outcome = sp.TOption(sp.TString)).layout(("current_player", ("move_nb", "outcome"))), move = sp.TRecord(i = sp.TInt, j = sp.TInt).layout(("i", "j")), state = sp.TMap(sp.TInt, sp.TMap(sp.TInt, sp.TInt))).layout(("meta", ("move", "state"))), sp.TRecord(messages = sp.TUnit, new_state = sp.TMap(sp.TInt, sp.TMap(sp.TInt, sp.TInt)), outcome = sp.TOption(sp.TString)).layout(("messages", ("new_state", "outcome")))), constants = sp.TRecord(channel_id = sp.TBytes, game_nonce = sp.TString, loser = sp.TMutez, model_id = sp.TBytes, player1 = sp.TRecord(addr = sp.TAddress, pk = sp.TKey).layout(("addr", "pk")), player2 = sp.TRecord(addr = sp.TAddress, pk = sp.TKey).layout(("addr", "pk")), winner = sp.TMutez).layout(("channel_id", ("game_nonce", ("loser", ("model_id", ("player1", ("player2", "winner"))))))), meta = sp.TRecord(current_player = sp.TInt, move_nb = sp.TNat, outcome = sp.TOption(sp.TString)).layout(("current_player", ("move_nb", "outcome"))), state = sp.TMap(sp.TInt, sp.TMap(sp.TInt, sp.TInt))).layout(("apply_", ("constants", ("meta", "state")))))
    self.init(apply_ = lambda(sp.TLambda(sp.TRecord(meta = sp.TRecord(current_player = sp.TInt, move_nb = sp.TNat, outcome = sp.TOption(sp.TString)).layout(("current_player", ("move_nb", "outcome"))), move = sp.TRecord(i = sp.TInt, j = sp.TInt).layout(("i", "j")), state = sp.TMap(sp.TInt, sp.TMap(sp.TInt, sp.TInt))).layout(("meta", ("move", "state"))), sp.TRecord(messages = sp.TUnit, new_state = sp.TMap(sp.TInt, sp.TMap(sp.TInt, sp.TInt)), outcome = sp.TOption(sp.TString)).layout(("messages", ("new_state", "outcome"))))),
              constants = sp.record(channel_id = sp.bytes('0x01'), game_nonce = '', loser = sp.tez(0), model_id = sp.bytes('0x'), player1 = sp.record(addr = sp.address('tz0Fakeplayer1'), pk = sp.key('edpkFakeplayer1')), player2 = sp.record(addr = sp.address('tz0Fakeplayer2'), pk = sp.key('edpkFakeplayer2')), winner = sp.tez(0)),
              meta = sp.record(current_player = 1, move_nb = 0, outcome = sp.none),
              state = {0 : {0 : 0, 1 : 0, 2 : 0}, 1 : {0 : 0, 1 : 0, 2 : 0}, 2 : {0 : 0, 1 : 0, 2 : 0}})

  @sp.entry_point
  def play(self, params):
    sp.set_type(params.move_nb, sp.TNat)
    sp.set_type(params.move_data, sp.TRecord(i = sp.TInt, j = sp.TInt).layout(("i", "j")))
    sp.if self.data.meta.current_player == 1:
      sp.verify(sp.sender == self.data.constants.player1.addr, 'Game_WrongPlayer')
    sp.else:
      sp.verify(sp.sender == self.data.constants.player2.addr, 'Game_WrongPlayer')
    sp.verify(self.data.meta.outcome.is_variant('None'))
    sp.verify(self.data.meta.move_nb == params.move_nb)
    compute_game_tester_40 = sp.local("compute_game_tester_40", self.data.apply_(sp.record(meta = self.data.meta, move = params.move_data, state = self.data.state)))
    self.data.meta.outcome = compute_game_tester_40.value.outcome
    self.data.meta.move_nb += 1
    self.data.meta.current_player = 3 - self.data.meta.current_player
    self.data.state = compute_game_tester_40.value.new_state
open Smartml

module Contract = struct

  let%entry_point play self params =
    set_type (params.move_nb : nat);
    set_type (params.move_data : unit);
    if self.data.meta.current_player = 1 then
      verify (sender = self.data.constants.player1.addr) "Game_WrongPlayer"
    else
      verify (sender = self.data.constants.player2.addr) "Game_WrongPlayer";
    verify self.data.meta.outcome.is_variant('None');
    verify (self.data.meta.move_nb = params.move_nb);
    compute_game_tester_40 = sp.local("compute_game_tester_40", self.data.apply_({meta = self.data.meta; move = params.move_data; state = self.data.state}));
    self.data.meta.outcome <- compute_game_tester_40.value.outcome;
    self.data.meta.move_nb += 1;
    self.data.meta.current_player <- 3 - self.data.meta.current_player;
    self.data.state <- compute_game_tester_40.value.new_state

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(apply_ = sp.TLambda(sp.TRecord(meta = sp.TRecord(current_player = int, move_nb = nat, outcome = string option).layout(("current_player", ("move_nb", "outcome"))), move = unit, state = unit).layout(("meta", ("move", "state"))), sp.TRecord(messages = unit, new_state = unit, outcome = string option).layout(("messages", ("new_state", "outcome")))), constants = sp.TRecord(channel_id = bytes, game_nonce = string, loser = mutez, model_id = bytes, player1 = sp.TRecord(addr = address, pk = key).layout(("addr", "pk")), player2 = sp.TRecord(addr = address, pk = key).layout(("addr", "pk")), winner = mutez).layout(("channel_id", ("game_nonce", ("loser", ("model_id", ("player1", ("player2", "winner"))))))), meta = sp.TRecord(current_player = int, move_nb = nat, outcome = string option).layout(("current_player", ("move_nb", "outcome"))), state = unit).layout(("apply_", ("constants", ("meta", "state"))))
      ~storage:[%expr
                 {apply_ = lambda(sp.TLambda(sp.TRecord(meta = sp.TRecord(current_player = int, move_nb = nat, outcome = string option).layout(("current_player", ("move_nb", "outcome"))), move = unit, state = unit).layout(("meta", ("move", "state"))), sp.TRecord(messages = unit, new_state = unit, outcome = string option).layout(("messages", ("new_state", "outcome"))))),
                  constants = {channel_id = sp.bytes('0x01'); game_nonce = ''; loser = sp.tez(0); model_id = sp.bytes('0x'); player1 = {addr = sp.address('tz0Fakeplayer1'); pk = sp.key('edpkFakeplayer1')}; player2 = {addr = sp.address('tz0Fakeplayer2'); pk = sp.key('edpkFakeplayer2')}; winner = sp.tez(0)},
                  meta = {current_player = 1; move_nb = 0; outcome = sp.none},
                  state = sp.unit}]
      [play]
end
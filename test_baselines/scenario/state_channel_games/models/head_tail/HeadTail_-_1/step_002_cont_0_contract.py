import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(apply_ = sp.TLambda(sp.TRecord(meta = sp.TRecord(current_player = sp.TInt, move_nb = sp.TNat, outcome = sp.TOption(sp.TString)).layout(("current_player", ("move_nb", "outcome"))), move = sp.TNat, state = sp.TRecord(hash1 = sp.TBytes, hash2 = sp.TBytes, secret1 = sp.TOption(sp.TNat), secret2 = sp.TOption(sp.TNat)).layout(("hash1", ("hash2", ("secret1", "secret2"))))).layout(("meta", ("move", "state"))), sp.TRecord(messages = sp.TUnit, new_state = sp.TRecord(hash1 = sp.TBytes, hash2 = sp.TBytes, secret1 = sp.TOption(sp.TNat), secret2 = sp.TOption(sp.TNat)).layout(("hash1", ("hash2", ("secret1", "secret2")))), outcome = sp.TOption(sp.TString)).layout(("messages", ("new_state", "outcome")))), constants = sp.TRecord(channel_id = sp.TBytes, game_nonce = sp.TString, loser = sp.TMutez, model_id = sp.TBytes, player1 = sp.TRecord(addr = sp.TAddress, pk = sp.TKey).layout(("addr", "pk")), player2 = sp.TRecord(addr = sp.TAddress, pk = sp.TKey).layout(("addr", "pk")), winner = sp.TMutez).layout(("channel_id", ("game_nonce", ("loser", ("model_id", ("player1", ("player2", "winner"))))))), meta = sp.TRecord(current_player = sp.TInt, move_nb = sp.TNat, outcome = sp.TOption(sp.TString)).layout(("current_player", ("move_nb", "outcome"))), state = sp.TRecord(hash1 = sp.TBytes, hash2 = sp.TBytes, secret1 = sp.TOption(sp.TNat), secret2 = sp.TOption(sp.TNat)).layout(("hash1", ("hash2", ("secret1", "secret2"))))).layout(("apply_", ("constants", ("meta", "state")))))
    self.init(apply_ = lambda(sp.TLambda(sp.TRecord(meta = sp.TRecord(current_player = sp.TInt, move_nb = sp.TNat, outcome = sp.TOption(sp.TString)).layout(("current_player", ("move_nb", "outcome"))), move = sp.TNat, state = sp.TRecord(hash1 = sp.TBytes, hash2 = sp.TBytes, secret1 = sp.TOption(sp.TNat), secret2 = sp.TOption(sp.TNat)).layout(("hash1", ("hash2", ("secret1", "secret2"))))).layout(("meta", ("move", "state"))), sp.TRecord(messages = sp.TUnit, new_state = sp.TRecord(hash1 = sp.TBytes, hash2 = sp.TBytes, secret1 = sp.TOption(sp.TNat), secret2 = sp.TOption(sp.TNat)).layout(("hash1", ("hash2", ("secret1", "secret2")))), outcome = sp.TOption(sp.TString)).layout(("messages", ("new_state", "outcome"))))),
              constants = sp.record(channel_id = sp.bytes('0x01'), game_nonce = '', loser = sp.tez(0), model_id = sp.bytes('0x'), player1 = sp.record(addr = sp.address('tz0Fakeplayer1'), pk = sp.key('edpkFakeplayer1')), player2 = sp.record(addr = sp.address('tz0Fakeplayer2'), pk = sp.key('edpkFakeplayer2')), winner = sp.tez(0)),
              meta = sp.record(current_player = 1, move_nb = 0, outcome = sp.none),
              state = sp.record(hash1 = sp.bytes('0x64fabcca0db8d1beeaed6aa9a387fbed25054c08783d649f886a87873d47b00f'), hash2 = sp.bytes('0x649aa69ef8e3629c646e26a505d57962409eb84e452417b57e8a77b37c8f8e76'), secret1 = sp.none, secret2 = sp.none))

  @sp.entry_point
  def play(self, params):
    sp.set_type(params.move_nb, sp.TNat)
    sp.set_type(params.move_data, sp.TNat)
    sp.if self.data.meta.current_player == 1:
      sp.verify(sp.sender == self.data.constants.player1.addr, 'Game_WrongPlayer')
    sp.else:
      sp.verify(sp.sender == self.data.constants.player2.addr, 'Game_WrongPlayer')
    sp.verify(self.data.meta.outcome.is_variant('None'))
    sp.verify(self.data.meta.move_nb == params.move_nb)
    compute_game_tester_40 = sp.local("compute_game_tester_40", self.data.apply_(sp.record(meta = self.data.meta, move = params.move_data, state = self.data.state)))
    self.data.meta.outcome = compute_game_tester_40.value.outcome
    self.data.meta.move_nb += 1
    self.data.meta.current_player = 3 - self.data.meta.current_player
    self.data.state = compute_game_tester_40.value.new_state
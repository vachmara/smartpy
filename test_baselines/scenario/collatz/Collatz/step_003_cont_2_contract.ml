open Smartml

module Contract = struct

  let%entry_point reset self () =
    self.data.counter <- 0

  let%entry_point run self params =
    if params > 1 then
      self.data.counter += 1;
      if (params % 2) = 0 then
        sp.transfer({k = sp.self_entry_point('run'); x = params}, sp.tez(0), sp.contract(sp.TRecord(k = nat contract, x = nat).layout(("k", "x")), self.data.onEven).open_some())
      else
        sp.transfer({k = sp.self_entry_point('run'); x = params}, sp.tez(0), sp.contract(sp.TRecord(k = nat contract, x = nat).layout(("k", "x")), self.data.onOdd).open_some())

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(counter = intOrNat, onEven = address, onOdd = address).layout(("counter", ("onEven", "onOdd")))
      ~storage:[%expr
                 {counter = 0,
                  onEven = sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1'),
                  onOdd = sp.address('KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF')}]
      [reset; run]
end
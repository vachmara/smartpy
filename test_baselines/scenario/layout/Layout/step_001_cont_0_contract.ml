open Smartml

module Contract = struct

  let%entry_point run_record self params =
    set_type (params : sp.TRecord(a = int, b = string, c = bool).layout(("b", ("a", "c"))))

  let%entry_point run_record_2 self params =
    set_type (params : sp.TRecord(a = int, b = string, c = bool, d = nat).layout(("a", ("b", ("c", "d")))))

  let%entry_point run_type_record self params =
    set_type (params : sp.TRecord(a = int, b = string).layout(("b", "a")))

  let%entry_point run_type_variant self params =
    set_type (params : sp.TVariant(d = string, e = int).layout(("e", "d")))

  let%entry_point run_variant self params =
    set_type (params : sp.TVariant(a = int, b = string, c = bool).layout(("b", ("a", "c"))))

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(a = intOrNat, b = nat).layout(("b", "a"))
      ~storage:[%expr
                 {a = 1,
                  b = 12}]
      [run_record; run_record_2; run_type_record; run_type_variant; run_variant]
end
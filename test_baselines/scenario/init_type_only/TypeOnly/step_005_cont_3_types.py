import smartpy as sp

tstorage = sp.TRecord(a = sp.TIntOrNat, b = sp.TBool).layout(("a", "b"))
tparameter = sp.TVariant(f = sp.TIntOrNat).layout("f")
tglobals = { }
tviews = { }

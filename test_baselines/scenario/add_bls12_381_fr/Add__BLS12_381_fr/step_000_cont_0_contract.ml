open Smartml

module Contract = struct

  let%entry_point add self params =
    self.data.fr <- some ((fst params) + (snd params))

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(fr = bls12_381_fr option).layout("fr")
      ~storage:[%expr
                 {fr = sp.none}]
      [add]
end
open Smartml

module Contract = struct

  let%entry_point administrate self params =
    verify (sender = self.data.admin) "Aggregator_NotAdmin";
    set_type (params : sp.TVariant(changeActive = bool, changeAdmin = address, changeOracles = sp.TRecord(added = (address * sp.TRecord(adminAddress = address, endingRound = nat option, startingRound = nat).layout(("adminAddress", ("endingRound", "startingRound")))) list, removed = address list).layout(("added", "removed")), updateFutureRounds = sp.TRecord(maxSubmissions = nat, minSubmissions = nat, oraclePayment = nat, restartDelay = nat, timeout = nat).layout(("maxSubmissions", ("minSubmissions", ("oraclePayment", ("restartDelay", "timeout")))))).layout((("changeActive", "changeAdmin"), ("changeOracles", "updateFutureRounds"))) list);
    sp.for action in params:
      with action.match_cases() as arg:
        with arg.match('changeActive') as changeActive:
          self.data.active <- changeActive
        with arg.match('changeAdmin') as changeAdmin:
          self.data.admin <- changeAdmin
        with arg.match('updateFutureRounds') as updateFutureRounds:
          verify (sender = self.data.admin) "Aggregator_NotAdmin";
          compute_price_feed_672 = sp.local("compute_price_feed_672", len self.data.oracles);
          verify (updateFutureRounds.maxSubmissions >= updateFutureRounds.minSubmissions) "Aggregator_MaxInferiorToMin";
          verify (compute_price_feed_672.value >= updateFutureRounds.maxSubmissions) "Aggregator_MaxExceedActive";
          verify ((compute_price_feed_672.value = 0) | (compute_price_feed_672.value > updateFutureRounds.restartDelay)) "Aggregator_DelayExceedTotal";
          verify ((compute_price_feed_672.value = 0) | (updateFutureRounds.minSubmissions > 0)) "Aggregator_MinSubmissionsTooLow";
          verify (self.data.recordedFunds.available >= ((updateFutureRounds.oraclePayment * compute_price_feed_672.value) * 2)) "Aggregator_InsufficientFundsForPayment";
          self.data.restartDelay <- updateFutureRounds.restartDelay;
          self.data.minSubmissions <- updateFutureRounds.minSubmissions;
          self.data.maxSubmissions <- updateFutureRounds.maxSubmissions;
          self.data.timeout <- updateFutureRounds.timeout;
          self.data.oraclePayment <- updateFutureRounds.oraclePayment
        with arg.match('changeOracles') as changeOracles:
          verify (sender = self.data.admin) "Aggregator_NotAdmin";
          sp.for oracleAddress in changeOracles.removed:
            del self.data.oracles[oracleAddress];
          sp.for oracle in changeOracles.added:
            match_pair_price_feed_696_fst, match_pair_price_feed_696_snd = sp.match_tuple(oracle, "match_pair_price_feed_696_fst", "match_pair_price_feed_696_snd")
            set_type (match_pair_price_feed_696_snd.endingRound : nat option);
            endingRound = sp.local("endingRound", 4294967295);
            if match_pair_price_feed_696_snd.endingRound.is_some() then
              endingRound.value <- match_pair_price_feed_696_snd.endingRound.open_some();
            self.data.oracles[match_pair_price_feed_696_fst] <- {adminAddress = match_pair_price_feed_696_snd.adminAddress; endingRound = endingRound.value; lastStartedRound = 0; startingRound = match_pair_price_feed_696_snd.startingRound; withdrawable = 0};
            if (self.data.reportingRoundId <> 0) & (match_pair_price_feed_696_snd.startingRound <= self.data.reportingRoundId) then
              self.data.reportingRoundDetails.activeOracles.add(match_pair_price_feed_696_fst)


  let%entry_point decimals self params =
    sp.transfer(self.data.decimals, sp.tez(0), params)

  let%entry_point forceBalanceUpdate self () =
    sp.transfer({requests = sp.list([{owner = self_address; token_id = 0}]); callback = sp.self_entry_point('updateAvailableFunds')}, sp.tez(0), sp.contract(sp.TRecord(callback = sp.TRecord(balance = nat, request = sp.TRecord(owner = address, token_id = nat).layout(("owner", "token_id"))).layout(("request", "balance")) list contract, requests = sp.TRecord(owner = address, token_id = nat).layout(("owner", "token_id")) list).layout(("requests", "callback")), self.data.linkToken, entry_point='balance_of').open_some(message = "Aggregator_InvalidTokenkInterface"))

  let%entry_point latestRoundData self params =
    sp.transfer(self.data.rounds[self.data.latestRoundId], sp.tez(0), params)

  let%entry_point submit self params =
    match_pair_price_feed_622_fst, match_pair_price_feed_622_snd = sp.match_tuple(params, "match_pair_price_feed_622_fst", "match_pair_price_feed_622_snd")
    verify self.data.active;
    verify (self.data.oracles.contains(sender)) "Aggregator_NotOracle";
    verify (self.data.oracles[sender].startingRound <= match_pair_price_feed_622_fst) "Aggregator_NotYetEnabledOracle";
    verify (self.data.oracles[sender].endingRound > match_pair_price_feed_622_fst) "Aggregator_NotLongerAllowedOracle";
    verify ((((match_pair_price_feed_622_fst + 1) = self.data.reportingRoundId) | (match_pair_price_feed_622_fst = self.data.reportingRoundId)) | (match_pair_price_feed_622_fst = (self.data.reportingRoundId + 1))) "Aggregator_InvalidRound";
    if (match_pair_price_feed_622_fst + 1) = self.data.reportingRoundId then
      verify (not (self.data.previousRoundDetails.submissions.contains(sender))) "Aggregator_SubmittedInCurrent";
      verify ((len self.data.previousRoundDetails.submissions) < self.data.previousRoundDetails.maxSubmissions) "Aggregator_RoundMaxSubmissionExceed";
      self.data.previousRoundDetails.submissions[sender] <- match_pair_price_feed_622_snd;
      if (len self.data.previousRoundDetails.submissions) >= self.data.previousRoundDetails.minSubmissions then
        self.data.rounds[as_nat (self.data.reportingRoundId - 1)].answer <- self.median(self.data.previousRoundDetails.submissions.values());
        self.data.rounds[as_nat (self.data.reportingRoundId - 1)].updatedAt <- now;
        self.data.rounds[as_nat (self.data.reportingRoundId - 1)].answeredInRound <- self.data.reportingRoundId
    else
      if match_pair_price_feed_622_fst = self.data.reportingRoundId then
        verify (not (self.data.reportingRoundDetails.submissions.contains(sender))) "Aggregator_AlreadySubmittedForThisRound";
        verify ((len self.data.reportingRoundDetails.submissions) < self.data.reportingRoundDetails.maxSubmissions) "Aggregator_RoundMaxSubmissionExceed";
        self.data.reportingRoundDetails.submissions[sender] <- match_pair_price_feed_622_snd;
        if (len self.data.reportingRoundDetails.submissions) >= self.data.reportingRoundDetails.minSubmissions then
          self.data.rounds[self.data.reportingRoundId].answer <- self.median(self.data.reportingRoundDetails.submissions.values());
          self.data.rounds[self.data.reportingRoundId].updatedAt <- now;
          self.data.rounds[self.data.reportingRoundId].answeredInRound <- self.data.reportingRoundId;
          self.data.latestRoundId <- self.data.reportingRoundId
      else
        if self.data.reportingRoundId > 0 then
          verify ((self.data.oracles[sender].lastStartedRound = 0) | ((self.data.reportingRoundId + 1) > (self.data.oracles[sender].lastStartedRound + self.data.restartDelay))) "Aggregator_WaitBeforeInit";
          verify ((now > (add_seconds self.data.rounds[self.data.reportingRoundId].startedAt ((to_int self.data.timeout) * 60))) | (self.data.rounds[self.data.reportingRoundId].answeredInRound = self.data.reportingRoundId)) "Aggregator_PreviousRoundNotOver";
        answer = sp.local("answer", 0);
        answeredInRound = sp.local("answeredInRound", 0);
        if self.data.minSubmissions = 1 then
          answer.value <- match_pair_price_feed_622_snd;
          answeredInRound.value <- self.data.reportingRoundId + 1;
        self.data.rounds[self.data.reportingRoundId + 1] <- {answer = answer.value; answeredInRound = answeredInRound.value; roundId = self.data.reportingRoundId + 1; startedAt = now; updatedAt = now};
        self.data.previousRoundDetails <- self.data.reportingRoundDetails;
        y2 = sp.local("y2", self.getActiveOracles({in_param = self.data.reportingRoundId + 1; in_storage = self.data}));
        self.data <- y2.value.storage;
        sp.for op in y2.value.operations.rev():
          operations().push(op);
        self.data.reportingRoundDetails <- {activeOracles = y2.value.result; maxSubmissions = self.data.maxSubmissions; minSubmissions = self.data.minSubmissions; submissions = {sender : match_pair_price_feed_622_snd}; timeout = self.data.timeout};
        self.data.oracles[sender].lastStartedRound <- self.data.reportingRoundId + 1;
        self.data.reportingRoundId += 1;
    self.data.recordedFunds.available <- sp.as_nat(self.data.recordedFunds.available - self.data.oraclePayment, message = "Aggregator_OraclePaymentUnderflow");
    self.data.recordedFunds.allocated += self.data.oraclePayment;
    self.data.oracles[sender].withdrawable += self.data.oraclePayment

  let%entry_point updateAvailableFunds self params =
    set_type (params : sp.TRecord(balance = nat, request = sp.TRecord(owner = address, token_id = nat).layout(("owner", "token_id"))).layout(("request", "balance")) list);
    verify (sender = self.data.linkToken) "Aggregator_NotLinkToken";
    balance = sp.local("balance", 0);
    sp.for resp in params:
      verify (resp.request.owner = self_address);
      balance.value <- resp.balance;
    if balance.value <> self.data.recordedFunds.available then
      self.data.recordedFunds.available <- balance.value

  let%entry_point withdrawPayment self params =
    verify (self.data.oracles[params.oracleAddress].adminAddress = sender) "Aggregator_NotOracleAdmin";
    verify (self.data.oracles[params.oracleAddress].withdrawable >= params.amount) "Aggregator_InsufficientWithdrawableFunds";
    self.data.oracles[params.oracleAddress].withdrawable <- as_nat (self.data.oracles[params.oracleAddress].withdrawable - params.amount);
    self.data.recordedFunds.allocated <- as_nat (self.data.recordedFunds.allocated - params.amount);
    sp.transfer(sp.list([{from_ = self_address; txs = sp.list([{to_ = params.recipientAddress; token_id = 0; amount = params.amount}])}]), sp.tez(0), sp.contract(sp.TRecord(from_ = address, txs = sp.TRecord(amount = nat, to_ = address, token_id = nat).layout(("to_", ("token_id", "amount"))) list).layout(("from_", "txs")) list, self.data.linkToken, entry_point='transfer').open_some(message = "Aggregator_InvalidTokenkInterface"));
    sp.transfer({requests = sp.list([{owner = self_address; token_id = 0}]); callback = sp.self_entry_point('updateAvailableFunds')}, sp.tez(0), sp.contract(sp.TRecord(callback = sp.TRecord(balance = nat, request = sp.TRecord(owner = address, token_id = nat).layout(("owner", "token_id"))).layout(("request", "balance")) list contract, requests = sp.TRecord(owner = address, token_id = nat).layout(("owner", "token_id")) list).layout(("requests", "callback")), self.data.linkToken, entry_point='balance_of').open_some(message = "Aggregator_InvalidTokenkInterface"))

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(active = bool, admin = address, decimals = nat, latestRoundId = nat, linkToken = address, maxSubmissions = nat, metadata = bigMap(string, bytes), minSubmissions = nat, oraclePayment = nat, oracles = map(address, sp.TRecord(adminAddress = address, endingRound = nat, lastStartedRound = nat, startingRound = nat, withdrawable = nat).layout(("adminAddress", ("endingRound", ("lastStartedRound", ("startingRound", "withdrawable")))))), previousRoundDetails = sp.TRecord(activeOracles = sp.TSet(address), maxSubmissions = nat, minSubmissions = nat, submissions = map(address, nat), timeout = nat).layout(("activeOracles", ("maxSubmissions", ("minSubmissions", ("submissions", "timeout"))))), recordedFunds = sp.TRecord(allocated = nat, available = nat).layout(("allocated", "available")), reportingRoundDetails = sp.TRecord(activeOracles = sp.TSet(address), maxSubmissions = nat, minSubmissions = nat, submissions = map(address, nat), timeout = nat).layout(("activeOracles", ("maxSubmissions", ("minSubmissions", ("submissions", "timeout"))))), reportingRoundId = nat, restartDelay = nat, rounds = bigMap(nat, sp.TRecord(answer = nat, answeredInRound = nat, roundId = nat, startedAt = timestamp, updatedAt = timestamp).layout(("answer", ("answeredInRound", ("roundId", ("startedAt", "updatedAt")))))), timeout = nat).layout(("active", ("admin", ("decimals", ("latestRoundId", ("linkToken", ("maxSubmissions", ("metadata", ("minSubmissions", ("oraclePayment", ("oracles", ("previousRoundDetails", ("recordedFunds", ("reportingRoundDetails", ("reportingRoundId", ("restartDelay", ("rounds", "timeout")))))))))))))))))
      ~storage:[%expr
                 {active = True,
                  admin = sp.address('tz1evBmfWZoPDN38avoRGbjJaLBJUP8AZz6a'),
                  decimals = 8,
                  latestRoundId = 0,
                  linkToken = sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1'),
                  maxSubmissions = 6,
                  metadata = {'' : sp.bytes('0x3c55524c3e')},
                  minSubmissions = 3,
                  oraclePayment = 1,
                  oracles = {sp.address('KT1LLTzYhdhxTqKu7ByJz8KaShF6qPTdx5os') : {adminAddress = sp.address('tz1evBmfWZoPDN38avoRGbjJaLBJUP8AZz6a'); endingRound = 4294967295; lastStartedRound = 0; startingRound = 0; withdrawable = 0}, sp.address('KT1LhTzYhdhxTqKu7ByJz8KaShF6qPTdx5os') : {adminAddress = sp.address('tz1evBmfWZoPDN38avoRGbjJaLBJUP8AZz6a'); endingRound = 4294967295; lastStartedRound = 0; startingRound = 0; withdrawable = 0}, sp.address('KT1P7oeoKWHx5SXt73qpEanzkr8yeEKABqko') : {adminAddress = sp.address('tz1evBmfWZoPDN38avoRGbjJaLBJUP8AZz6a'); endingRound = 4294967295; lastStartedRound = 0; startingRound = 0; withdrawable = 0}, sp.address('KT1SCkxmTqTkmc7zoAP5uMYT9rp9iqVVRgdt') : {adminAddress = sp.address('tz1evBmfWZoPDN38avoRGbjJaLBJUP8AZz6a'); endingRound = 4294967295; lastStartedRound = 0; startingRound = 0; withdrawable = 0}},
                  previousRoundDetails = {activeOracles = sp.set([]); maxSubmissions = 0; minSubmissions = 0; submissions = {}; timeout = 0},
                  recordedFunds = {allocated = 0; available = 0},
                  reportingRoundDetails = {activeOracles = sp.set([]); maxSubmissions = 0; minSubmissions = 0; submissions = {}; timeout = 0},
                  reportingRoundId = 0,
                  restartDelay = 2,
                  rounds = {},
                  timeout = 10}]
      [administrate; decimals; forceBalanceUpdate; latestRoundData; submit; updateAvailableFunds; withdrawPayment]
end
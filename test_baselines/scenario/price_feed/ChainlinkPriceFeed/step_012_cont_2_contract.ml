open Smartml

module Contract = struct

  let%entry_point administrate self params =
    verify (sender = self.data.admin) "Proxy_NotAdmin";
    set_type (params : sp.TVariant(changeActive = bool, changeAdmin = address, changeAggregator = address).layout(("changeActive", ("changeAdmin", "changeAggregator"))) list);
    sp.for action in params:
      with action.match_cases() as arg:
        with arg.match('changeActive') as changeActive:
          self.data.active <- changeActive
        with arg.match('changeAdmin') as changeAdmin:
          self.data.admin <- changeAdmin
        with arg.match('changeAggregator') as changeAggregator:
          self.data.aggregator <- some changeAggregator


  let%entry_point decimals self params =
    sp.transfer(params, sp.tez(0), sp.contract((unit * address), self.data.aggregator.open_some(message = "Proxy_AggregatorNotConfigured"), entry_point='decimals').open_some(message = "Proxy_InvalidParametersInDecimalsView"))

  let%entry_point description self params =
    sp.transfer(params, sp.tez(0), sp.contract((unit * address), self.data.aggregator.open_some(message = "Proxy_AggregatorNotConfigured"), entry_point='description').open_some(message = "Proxy_InvalidParametersInDescriptionView"))

  let%entry_point latestRoundData self params =
    sp.transfer(params, sp.tez(0), sp.contract((unit * address), self.data.aggregator.open_some(message = "Proxy_AggregatorNotConfigured"), entry_point='latestRoundData').open_some(message = "Proxy_InvalidParametersInLatestRoundDataView"))

  let%entry_point version self params =
    sp.transfer(params, sp.tez(0), sp.contract((unit * address), self.data.aggregator.open_some(message = "Proxy_AggregatorNotConfigured"), entry_point='version').open_some(message = "Proxy_InvalidParametersInVersionView"))

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(active = bool, admin = address, aggregator = address option).layout(("active", ("admin", "aggregator")))
      ~storage:[%expr
                 {active = True,
                  admin = sp.address('tz1evBmfWZoPDN38avoRGbjJaLBJUP8AZz6a'),
                  aggregator = sp.some(sp.address('KT1CfuSjCcunNQ5qCCro2Kc74uivnor9d8ba%latestRoundData'))}]
      [administrate; decimals; description; latestRoundData; version]
end
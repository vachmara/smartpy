open Smartml

module Contract = struct

  let%entry_point fifo self params =
    set_type (params : sp.TVariant(Left = unit, Right = int).layout(("Left", "Right")));
    if params.is_variant('Left') then
      verify (self.data.Left.Left < self.data.Left.Right);
      del self.data.Right[self.data.Left.Left];
      self.data.Left.Left <- 1 + self.data.Left.Left
    else
      self.data.Left.Right <- 1 + self.data.Left.Right;
      self.data.Right[self.data.Left.Right] <- params.open_variant('Right')

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(Left = sp.TRecord(Left = int, Right = int).layout(("Left", "Right")), Right = map(int, int)).layout(("Left", "Right"))
      ~storage:[%expr
                 {Left = {Left = 0; Right = -1},
                  Right = {}}]
      [fifo]
end
open Smartml

module Contract = struct

  let%entry_point approve self params =
    set_type (params : sp.TRecord(spender = address, value = nat).layout(("spender", "value")));
    if not (self.data.balances.contains(sender)) then
      self.data.balances[sender] <- {approvals = {}; balance = 0};
    verify (not self.data.paused) "FA1.2_Paused";
    verify ((self.data.balances[sender].approvals.get(params.spender, default_value = 0) = 0) | (params.value = 0)) "FA1.2_UnsafeAllowanceChange";
    self.data.balances[sender].approvals[params.spender] <- params.value

  let%entry_point burn self params =
    set_type (params : sp.TRecord(address = address, value = nat).layout(("address", "value")));
    verify (sender = self.data.administrator) "FA1.2_NotAdmin";
    verify (self.data.balances[params.address].balance >= params.value) "FA1.2_InsufficientBalance";
    self.data.balances[params.address].balance <- as_nat (self.data.balances[params.address].balance - params.value);
    self.data.totalSupply <- as_nat (self.data.totalSupply - params.value)

  let%entry_point getAdministrator self params =
    set_type (fst params : unit);
    let%var __s1 = self.data.administrator in
    set_type (snd params : address contract);
    sp.transfer(__s1.value, sp.tez(0), snd params)

  let%entry_point getAllowance self params =
    let __s2 =
      if self.data.balances.contains((fst params).owner) then
        self.data.balances[(fst params).owner].approvals.get((fst params).spender, default_value = 0)
      else
        0
    in
    set_type (snd params : nat contract);
    sp.transfer(__s2.value, sp.tez(0), snd params)

  let%entry_point getBalance self params =
    let __s3 =
      if self.data.balances.contains(fst params) then
        self.data.balances[fst params].balance
      else
        0
    in
    set_type (snd params : nat contract);
    sp.transfer(__s3.value, sp.tez(0), snd params)

  let%entry_point getTotalSupply self params =
    set_type (fst params : unit);
    let%var __s4 = self.data.totalSupply in
    set_type (snd params : nat contract);
    sp.transfer(__s4.value, sp.tez(0), snd params)

  let%entry_point mint self params =
    set_type (params : sp.TRecord(address = address, value = nat).layout(("address", "value")));
    verify (sender = self.data.administrator) "FA1.2_NotAdmin";
    if not (self.data.balances.contains(params.address)) then
      self.data.balances[params.address] <- {approvals = {}; balance = 0};
    self.data.balances[params.address].balance += params.value;
    self.data.totalSupply += params.value

  let%entry_point setAdministrator self params =
    set_type (params : address);
    verify (sender = self.data.administrator) "FA1.2_NotAdmin";
    self.data.administrator <- params

  let%entry_point setPause self params =
    set_type (params : bool);
    verify (sender = self.data.administrator) "FA1.2_NotAdmin";
    self.data.paused <- params

  let%entry_point transfer self params =
    set_type (params : sp.TRecord(from_ = address, to_ = address, value = nat).layout(("from_ as from", ("to_ as to", "value"))));
    verify ((sender = self.data.administrator) | ((not self.data.paused) & ((params.from_ = sender) | (self.data.balances[params.from_].approvals[sender] >= params.value)))) "FA1.2_NotAllowed";
    if not (self.data.balances.contains(params.from_)) then
      self.data.balances[params.from_] <- {approvals = {}; balance = 0};
    if not (self.data.balances.contains(params.to_)) then
      self.data.balances[params.to_] <- {approvals = {}; balance = 0};
    verify (self.data.balances[params.from_].balance >= params.value) "FA1.2_InsufficientBalance";
    self.data.balances[params.from_].balance <- as_nat (self.data.balances[params.from_].balance - params.value);
    self.data.balances[params.to_].balance += params.value;
    if (params.from_ <> sender) & (not (sender = self.data.administrator)) then
      self.data.balances[params.from_].approvals[sender] <- as_nat (self.data.balances[params.from_].approvals[sender] - params.value)

  let%entry_point update_metadata self params =
    verify (sender = self.data.administrator) "FA1.2_NotAdmin";
    self.data.metadata[params.key] <- params.value

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(administrator = address, balances = bigMap(address, sp.TRecord(approvals = map(address, nat), balance = nat).layout(("approvals", "balance"))), metadata = bigMap(string, bytes), paused = bool, token_metadata = bigMap(nat, sp.TRecord(token_id = nat, token_info = map(string, bytes)).layout(("token_id", "token_info"))), totalSupply = nat).layout(("administrator", ("balances", ("metadata", ("paused", ("token_metadata", "totalSupply"))))))
      ~storage:[%expr
                 {administrator = sp.address('tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w'),
                  balances = {},
                  metadata = {'' : sp.bytes('0x697066733a2f2f516d616941556a3146464e4759547538724c426a633365654e3963534b7761463845474d424e446d687a504e4664')},
                  paused = False,
                  token_metadata = {0 : {token_id = 0; token_info = {'decimals' : sp.bytes('0x3138'), 'icon' : sp.bytes('0x68747470733a2f2f736d61727470792e696f2f7374617469632f696d672f6c6f676f2d6f6e6c792e737667'), 'name' : sp.bytes('0x4d7920477265617420546f6b656e'), 'symbol' : sp.bytes('0x4d4754')}}},
                  totalSupply = 0}]
      [approve; burn; getAdministrator; getAllowance; getBalance; getTotalSupply; mint; setAdministrator; setPause; transfer; update_metadata]
end
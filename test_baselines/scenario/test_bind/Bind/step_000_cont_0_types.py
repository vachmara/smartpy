import smartpy as sp

tstorage = sp.TRecord(x = sp.TIntOrNat, y = sp.TIntOrNat).layout(("x", "y"))
tparameter = sp.TVariant(call_f = sp.TIntOrNat).layout("call_f")
tglobals = { }
tviews = { }

import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(counter = sp.TInt).layout("counter"))
    self.init(counter = 0)

  @sp.entry_point
  def compute(self, params):
    sp.transfer(params, sp.tez(0), sp.self_entry_point('run'))

  @sp.entry_point
  def reset(self):
    self.data.counter = 0

  @sp.entry_point
  def run(self, params):
    sp.verify((self.data.counter * params) != 9)
    sp.if params > 1:
      self.data.counter += 1
      sp.transfer(params - 1, sp.tez(0), sp.self_entry_point('run'))
open Smartml

module Contract = struct

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(x = string).layout("x")
      ~storage:[%expr
                 {x = 'abc'}]
      []
end
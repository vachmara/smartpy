import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TUnit)
    self.init()

  @sp.entry_point
  def test(self, params):
    sp.verify((sp.slice(sp.pack(sp.set_type_expr(sp.fst(params), sp.TAddress)), 6, 22)) == (sp.slice(sp.pack(sp.set_type_expr(sp.snd(params), sp.TAddress)), 6, 22)))
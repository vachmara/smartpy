open Smartml

module Contract = struct

  let%entry_point run self () =
    verify ((pack baker_hash "SG1jfZeHRzeWAM1T4zrwunEyUpwWc82D4tbv") = bytes "0x050a00000014dd577f8a5cd09a4a1e62241676ffaf4bc0c5b87f")

  let init storage =
    Basics.build_contract
      ~tstorage:unit
      ~storage:[%expr sp.unit]
      [run]
end
import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(x = sp.TIntOrNat).layout("x"))
    self.init(x = 0)

  @sp.entry_point
  def entry_point_1(self, params):
    match_pair_test_imported_9_fst, match_pair_test_imported_9_snd = sp.match_tuple(params, "match_pair_test_imported_9_fst", "match_pair_test_imported_9_snd")
    match_pair_test_imported_10_fst, match_pair_test_imported_10_snd = sp.match_tuple(match_pair_test_imported_9_snd, "match_pair_test_imported_10_fst", "match_pair_test_imported_10_snd")
    self.data.x += (match_pair_test_imported_9_fst * match_pair_test_imported_10_fst) * match_pair_test_imported_10_snd
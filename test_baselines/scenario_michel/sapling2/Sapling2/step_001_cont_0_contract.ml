open Smartml

module Contract = struct

  let%entry_point handle self params =
    sp.for operation in params:
      result = sp.local("result", sp.sapling_verify_update(self.data.ledger, operation.transaction).open_some());
      self.data.ledger <- snd result.value;
      amount = sp.local("amount", fst result.value);
      amount_tez = sp.local("amount_tez", sp.mutez(abs amount.value));
      if amount.value > 0 then
        sp.transfer((), amount_tez.value, implicit_account operation.key.open_some())
      else
        verify (not operation.key.is_some());
        verify (amount = amount_tez.value)

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(ledger = 8 saplingState).layout("ledger")
      ~storage:[%expr
                 {ledger = []}]
      [handle]
end
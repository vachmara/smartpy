open Smartml

module Contract = struct

  let%entry_point ep1 self params =
    match_pair_test_match_10_fst, match_pair_test_match_10_snd = sp.match_tuple(params, "match_pair_test_match_10_fst", "match_pair_test_match_10_snd")
    verify (match_pair_test_match_10_fst = "x");
    verify (match_pair_test_match_10_snd = 2)

  let%entry_point ep2 self params =
    my_x, my_y, my_z = sp.match_tuple(params, "my_x", "my_y", "my_z")
    verify (my_x = "x");
    verify (my_y = 2);
    verify my_z

  let%entry_point ep3 self params =
    set_type (params : sp.TRecord(x = string, y = int, z = bool).layout(("x", ("y", "z"))));
    x_test_match_24, z_test_match_24 = sp.match_record(params, "x", "z")
    verify (x_test_match_24 = "x");
    verify z_test_match_24

  let%entry_point ep4 self params =
    set_type (params.x01 : int);
    set_type (params.x02 : key);
    set_type (params.x03 : string);
    set_type (params.x04 : timestamp);
    set_type (params.x05 : bytes);
    set_type (params.x06 : address);
    set_type (params.x07 : bool);
    set_type (params.x08 : keyHash);
    set_type (params.x09 : signature);
    set_type (params.x10 : mutez);
    x07_test_match_40, x03_test_match_40 = sp.match_record(params, "x07", "x03")
    verify (x03_test_match_40 = "x");
    verify x07_test_match_40

  let%entry_point ep5 self params =
    a, b, c, d = sp.match_tuple(params, "a", "b", "c", "d")
    verify (((a * b) + (c * d)) = 12)

  let%entry_point ep6 self params =
    a, b, c, d = sp.match_tuple(params, "a", "b", "c", "d")
    set_type (c : int);
    verify (((a * b) + d) = 12)

  let init storage =
    Basics.build_contract
      ~tstorage:unit
      ~storage:[%expr sp.unit]
      [ep1; ep2; ep3; ep4; ep5; ep6]
end
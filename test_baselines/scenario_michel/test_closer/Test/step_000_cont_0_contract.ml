open Smartml

module Contract = struct

  let%entry_point test_none self params =
    verify params.is_variant('None')

  let%entry_point test_some self params =
    verify params.is_some()

  let init storage =
    Basics.build_contract
      ~tstorage:unit
      ~storage:[%expr sp.unit]
      [test_none; test_some]
end
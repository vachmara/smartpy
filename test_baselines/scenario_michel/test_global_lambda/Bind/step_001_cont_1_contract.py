import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(x = sp.TOption(sp.TIntOrNat)).layout("x"))
    self.init(x = sp.none)

  @sp.entry_point
  def check(self, params):
    __s1 = sp.local("__s1", params.params * 2)
    sp.verify(__s1.value == params.result)

  @sp.entry_point
  def test(self, params):
    __s2 = sp.local("__s2", params * 2)
    self.data.x = sp.some(__s2.value)
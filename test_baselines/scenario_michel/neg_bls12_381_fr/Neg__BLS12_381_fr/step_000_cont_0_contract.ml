open Smartml

module Contract = struct

  let%entry_point negate self params =
    set_type (params.fr : bls12_381_fr);
    self.data.fr <- some (- params.fr)

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(fr = bls12_381_fr option).layout("fr")
      ~storage:[%expr
                 {fr = sp.none}]
      [negate]
end
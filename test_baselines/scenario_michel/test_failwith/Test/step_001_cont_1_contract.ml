open Smartml

module Contract = struct

  let%entry_point ep self params =
    if params = 0 then
      failwith "zero"
    else
      failwith "non-zero"

  let init storage =
    Basics.build_contract
      ~tstorage:unit
      ~storage:[%expr sp.unit]
      [ep]
end
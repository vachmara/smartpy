import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(f = sp.TOption(sp.TLambda(sp.TIntOrNat, sp.TIntOrNat))).layout("f"))
    self.init(f = sp.none)

  @sp.entry_point
  def ep(self):
    def f0(lparams_0):
      sp.if lparams_0 == 0:
        sp.failwith('zero')
      sp.else:
        sp.result(1)
    self.data.f = sp.some(sp.build_lambda(f0))
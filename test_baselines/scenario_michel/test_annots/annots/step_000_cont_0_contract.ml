open Smartml

module Contract = struct

  let%entry_point ep self () =
    verify (self.data.x.contains({a = 0; b = sender; c = 0}))

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(x = map(sp.TRecord(a = nat, b = address, c = nat).layout(("a", ("b", "c"))), unit)).layout("x")
      ~storage:[%expr
                 {x = {}}]
      [ep]
end
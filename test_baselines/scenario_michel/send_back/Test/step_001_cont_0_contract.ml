open Smartml

module Contract = struct

  let%entry_point bounce self () =
    sp.send(source, amount)

  let%entry_point bounce2 self () =
    sp.send(source, sp.tez(1));
    sp.send(source, amount - sp.tez(1))

  let init storage =
    Basics.build_contract
      ~tstorage:unit
      ~storage:[%expr sp.unit]
      [bounce; bounce2]
end
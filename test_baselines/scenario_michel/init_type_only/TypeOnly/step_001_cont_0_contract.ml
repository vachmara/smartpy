open Smartml

module Contract = struct

  let%entry_point f self params =
    self.data.a += params

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(a = int, b = bool).layout(("a", "b"))
      [f]
end
import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TUnit)
    self.init()

  @sp.entry_point
  def run(self):
    sp.verify(sp.pack(sp.baker_hash('SG1jfZeHRzeWAM1T4zrwunEyUpwWc82D4tbv')) == sp.bytes('0x050a00000014dd577f8a5cd09a4a1e62241676ffaf4bc0c5b87f'))
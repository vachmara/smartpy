Comment...
 h1: Escrow
Creating contract
 => test_baselines/scenario_michel/escrow/Escrow/step_001_cont_0_pre_michelson.michel 168
 -> (Pair 0 (Pair 0 (Pair "tz0FakeBob" (Pair "1970-01-01T00:02:03Z" (Pair 4000000 (Pair 50000000 (Pair 0xc2e588e23a6c8b8192da64af45b7b603ac420aefd57cc1570682350154e9c04e "tz0FakeAlice")))))))
 => test_baselines/scenario_michel/escrow/Escrow/step_001_cont_0_storage.tz 1
 => test_baselines/scenario_michel/escrow/Escrow/step_001_cont_0_storage.json 37
 => test_baselines/scenario_michel/escrow/Escrow/step_001_cont_0_sizes.csv 2
 => test_baselines/scenario_michel/escrow/Escrow/step_001_cont_0_storage.py 1
 => test_baselines/scenario_michel/escrow/Escrow/step_001_cont_0_types.py 7
 => test_baselines/scenario_michel/escrow/Escrow/step_001_cont_0_contract.tz 213
 => test_baselines/scenario_michel/escrow/Escrow/step_001_cont_0_contract.json 305
 => test_baselines/scenario_michel/escrow/Escrow/step_001_cont_0_contract.py 42
 => test_baselines/scenario_michel/escrow/Escrow/step_001_cont_0_contract.ml 43
 => test_baselines/scenario_michel/escrow/Escrow/step_002_cont_0_params.py 1
 => test_baselines/scenario_michel/escrow/Escrow/step_002_cont_0_params.tz 1
 => test_baselines/scenario_michel/escrow/Escrow/step_002_cont_0_params.json 1
Executing addBalanceOwner(sp.record())...
 -> (Pair 0 (Pair 50000000 (Pair "tz0FakeBob" (Pair "1970-01-01T00:02:03Z" (Pair 4000000 (Pair 50000000 (Pair 0xc2e588e23a6c8b8192da64af45b7b603ac420aefd57cc1570682350154e9c04e "tz0FakeAlice")))))))
 => test_baselines/scenario_michel/escrow/Escrow/step_003_cont_0_params.py 1
 => test_baselines/scenario_michel/escrow/Escrow/step_003_cont_0_params.tz 1
 => test_baselines/scenario_michel/escrow/Escrow/step_003_cont_0_params.json 1
Executing addBalanceCounterparty(sp.record())...
 -> (Pair 4000000 (Pair 50000000 (Pair "tz0FakeBob" (Pair "1970-01-01T00:02:03Z" (Pair 4000000 (Pair 50000000 (Pair 0xc2e588e23a6c8b8192da64af45b7b603ac420aefd57cc1570682350154e9c04e "tz0FakeAlice")))))))
Comment...
 h3: Erronous secret
 => test_baselines/scenario_michel/escrow/Escrow/step_005_cont_0_params.py 1
 => test_baselines/scenario_michel/escrow/Escrow/step_005_cont_0_params.tz 1
 => test_baselines/scenario_michel/escrow/Escrow/step_005_cont_0_params.json 1
Executing claimCounterparty(sp.record(secret = sp.bytes('0x01223343')))...
 -> --- Expected failure in transaction --- Wrong condition: (self.data.hashedSecret == sp.blake2b(params.secret) : sp.TBool) (python/templates/escrow.py, line 37)
 (python/templates/escrow.py, line 37)
Comment...
 h3: Correct secret
 => test_baselines/scenario_michel/escrow/Escrow/step_007_cont_0_params.py 1
 => test_baselines/scenario_michel/escrow/Escrow/step_007_cont_0_params.tz 1
 => test_baselines/scenario_michel/escrow/Escrow/step_007_cont_0_params.json 1
Executing claimCounterparty(sp.record(secret = sp.bytes('0x01223344')))...
 -> (Pair 0 (Pair 0 (Pair "tz0FakeBob" (Pair "1970-01-01T00:02:03Z" (Pair 4000000 (Pair 50000000 (Pair 0xc2e588e23a6c8b8192da64af45b7b603ac420aefd57cc1570682350154e9c04e "tz0FakeAlice")))))))
  + Transfer
     params: sp.unit
     amount: sp.tez(54)
     to:     sp.contract(sp.TUnit, sp.address('tz0FakeBob')).open_some()

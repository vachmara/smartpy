import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TUnit)
    self.init()

  @sp.entry_point
  def run(self):
    sp.for test in sp.list([('+', sp.build_lambda(lambda lparams_0: sp.fst(lparams_0) + sp.snd(lparams_0)), sp.list([(11, 22, 33), (11, 28, 39), (11, -47, -36), (11, -2, 9), (123, 22, 145), (123, 28, 151), (123, -47, 76), (123, -2, 121), (-15, 22, 7), (-15, 28, 13), (-15, -47, -62), (-15, -2, -17)])), ('-', sp.build_lambda(lambda lparams_1: (0 + sp.fst(lparams_1)) - sp.snd(lparams_1)), sp.list([(11, 22, -11), (11, 28, -17), (11, -47, 58), (11, -2, 13), (123, 22, 101), (123, 28, 95), (123, -47, 170), (123, -2, 125), (-15, 22, -37), (-15, 28, -43), (-15, -47, 32), (-15, -2, -13)])), ('*', sp.build_lambda(lambda lparams_2: sp.fst(lparams_2) * sp.snd(lparams_2)), sp.list([(11, 22, 242), (11, 28, 308), (11, -47, -517), (11, -2, -22), (123, 22, 2706), (123, 28, 3444), (123, -47, -5781), (123, -2, -246), (-15, 22, -330), (-15, 28, -420), (-15, -47, 705), (-15, -2, 30)]))]):
      name, loc_op, loc_tests = sp.match_tuple(test, "name", "loc_op", "loc_tests")
      sp.for test_ in loc_tests:
        x, y, res = sp.match_tuple(test_, "x", "y", "res")
        z = sp.local("z", loc_op((x, y)))
        sp.verify(z.value == res, (name, (x, (y, z.value, res))))
    sp.for test in sp.list([('%', sp.build_lambda(lambda lparams_3: sp.fst(lparams_3) % sp.snd(lparams_3)), sp.list([(11, 22, 11), (11, 28, 11), (123, 22, 13), (123, 28, 11)])), ('//', sp.build_lambda(lambda lparams_4: sp.fst(lparams_4) // sp.snd(lparams_4)), sp.list([(11, 22, 0), (11, 28, 0), (123, 22, 5), (123, 28, 4)]))]):
      name, loc_op, loc_tests = sp.match_tuple(test, "name", "loc_op", "loc_tests")
      sp.for test_ in loc_tests:
        x, y, res = sp.match_tuple(test_, "x", "y", "res")
        z = sp.local("z", loc_op((x, y)))
        sp.verify(z.value == res, (name, (x, (y, z.value, res))))
    sp.for test in sp.list([('&', sp.build_lambda(lambda lparams_5: sp.fst(lparams_5) & sp.snd(lparams_5)), sp.list([(True, True, True), (True, False, False), (False, True, False), (False, False, False)])), ('|', sp.build_lambda(lambda lparams_6: sp.fst(lparams_6) | sp.snd(lparams_6)), sp.list([(True, True, True), (True, False, True), (False, True, True), (False, False, False)]))]):
      name, loc_op, loc_tests = sp.match_tuple(test, "name", "loc_op", "loc_tests")
      sp.for test_ in loc_tests:
        x, y, res = sp.match_tuple(test_, "x", "y", "res")
        z = sp.local("z", loc_op((x, y)))
        sp.verify(z.value == res, (name, (x, (y, z.value, res))))
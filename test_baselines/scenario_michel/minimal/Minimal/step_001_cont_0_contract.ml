open Smartml

module Contract = struct

  let%entry_point entry_point_1 self () =
    ()

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(x = intOrNat).layout("x")
      ~storage:[%expr
                 {x = 12}]
      [entry_point_1]
end
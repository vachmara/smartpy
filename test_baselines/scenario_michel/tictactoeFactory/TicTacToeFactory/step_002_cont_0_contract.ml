open Smartml

module Contract = struct

  let%entry_point build self params =
    verify ((not self.data.paused) | (sender = self.data.admin));
    verify (not (self.data.boards.contains(params.game)));
    self.data.boards[params.game] <- {deck = ({0 : {0 : 0, 1 : 0, 2 : 0}, 1 : {0 : 0, 1 : 0, 2 : 0}, 2 : {0 : 0, 1 : 0, 2 : 0}} : map(intOrNat, map(intOrNat, int))); draw = False; metaData = {}; nbMoves = 0; nextPlayer = 1; player1 = params.player1; player2 = params.player2; winner = 0}

  let%entry_point deleteGame self params =
    verify (sender = self.data.admin);
    del self.data.boards[params.game]

  let%entry_point play self params =
    verify (not self.data.paused);
    verify ((self.data.boards[params.game].winner = 0) & (not self.data.boards[params.game].draw));
    verify ((params.i >= 0) & (params.i < 3));
    verify ((params.j >= 0) & (params.j < 3));
    verify (params.move = self.data.boards[params.game].nextPlayer);
    verify (self.data.boards[params.game].deck[params.i][params.j] = 0);
    if params.move = 1 then
      verify (sender = self.data.boards[params.game].player1)
    else
      verify (sender = self.data.boards[params.game].player2);
    self.data.boards[params.game].nextPlayer <- 3 - self.data.boards[params.game].nextPlayer;
    self.data.boards[params.game].deck[params.i][params.j] <- params.move;
    self.data.boards[params.game].nbMoves += 1;
    if ((self.data.boards[params.game].deck[params.i][0] <> 0) & (self.data.boards[params.game].deck[params.i][0] = self.data.boards[params.game].deck[params.i][1])) & (self.data.boards[params.game].deck[params.i][0] = self.data.boards[params.game].deck[params.i][2]) then
      self.data.boards[params.game].winner <- self.data.boards[params.game].deck[params.i][0];
    if ((self.data.boards[params.game].deck[0][params.j] <> 0) & (self.data.boards[params.game].deck[0][params.j] = self.data.boards[params.game].deck[1][params.j])) & (self.data.boards[params.game].deck[0][params.j] = self.data.boards[params.game].deck[2][params.j]) then
      self.data.boards[params.game].winner <- self.data.boards[params.game].deck[0][params.j];
    if ((self.data.boards[params.game].deck[0][0] <> 0) & (self.data.boards[params.game].deck[0][0] = self.data.boards[params.game].deck[1][1])) & (self.data.boards[params.game].deck[0][0] = self.data.boards[params.game].deck[2][2]) then
      self.data.boards[params.game].winner <- self.data.boards[params.game].deck[0][0];
    if ((self.data.boards[params.game].deck[0][2] <> 0) & (self.data.boards[params.game].deck[0][2] = self.data.boards[params.game].deck[1][1])) & (self.data.boards[params.game].deck[0][2] = self.data.boards[params.game].deck[2][0]) then
      self.data.boards[params.game].winner <- self.data.boards[params.game].deck[0][2];
    if (self.data.boards[params.game].nbMoves = 9) & (self.data.boards[params.game].winner = 0) then
      self.data.boards[params.game].draw <- True

  let%entry_point setGameMetaData self params =
    verify ((sender = self.data.admin) | (sender = self.data.boards[params.game].player1));
    set_type (params.name : string);
    set_type (params.value : string);
    self.data.boards[params.game].metaData[params.name] <- params.value

  let%entry_point setMetaData self params =
    verify (sender = self.data.admin);
    set_type (params.name : string);
    set_type (params.value : string);
    self.data.metaData[params.name] <- params.value

  let%entry_point setPause self params =
    verify (sender = self.data.admin);
    self.data.paused <- params

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(admin = address, boards = bigMap(string, sp.TRecord(deck = map(intOrNat, map(intOrNat, int)), draw = bool, metaData = map(string, string), nbMoves = intOrNat, nextPlayer = int, player1 = address, player2 = address, winner = int).layout(("deck", ("draw", ("metaData", ("nbMoves", ("nextPlayer", ("player1", ("player2", "winner"))))))))), metaData = map(string, string), paused = bool).layout(("admin", ("boards", ("metaData", "paused"))))
      ~storage:[%expr
                 {admin = sp.address('tz0FakeAdmin'),
                  boards = {},
                  metaData = {},
                  paused = False}]
      [build; deleteGame; play; setGameMetaData; setMetaData; setPause]
end
import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(logic = sp.TLambda(sp.TBytes, sp.TNat), value = sp.TNat).layout(("logic", "value")))
    self.init(logic = lambda(sp.TLambda(sp.TBytes, sp.TNat)),
              value = 0)

  @sp.entry_point
  def calc(self, params):
    self.data.value = self.data.logic(params)

  @sp.entry_point
  def updateLogic(self, params):
    self.data.logic = params
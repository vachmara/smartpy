import smartpy as sp

tstorage = sp.TRecord(logic = sp.TLambda(sp.TBytes, sp.TNat), value = sp.TNat).layout(("logic", "value"))
tparameter = sp.TVariant(calc = sp.TBytes, updateLogic = sp.TLambda(sp.TBytes, sp.TNat)).layout(("calc", "updateLogic"))
tglobals = { }
tviews = { }

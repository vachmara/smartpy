open Smartml

module Contract = struct

  let%entry_point mul self params =
    self.data.fr <- some (mul (fst params) (snd params))

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(fr = bls12_381_fr option).layout("fr")
      ~storage:[%expr
                 {fr = sp.none}]
      [mul]
end
open Smartml

module Contract = struct

  let%entry_point compute self params =
    self.data.counter <- 0;
    self.data.steps <- sp.list([]);
    sp.transfer(params, sp.tez(0), sp.self_entry_point('run'))

  let%entry_point run self params =
    self.data.steps.push(params);
    if params > 1 then
      sp.transfer(params - 2, sp.tez(0), sp.self_entry_point('run'));
      sp.transfer(params - 1, sp.tez(0), sp.self_entry_point('run'))
    else
      self.data.counter += 1

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(counter = intOrNat, steps = int list).layout(("counter", "steps"))
      ~storage:[%expr
                 {counter = 0,
                  steps = sp.list([])}]
      [compute; run]
end
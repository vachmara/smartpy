open Smartml

module Contract = struct

  let%entry_point pairing_check self params =
    self.data.check <- some (pairing_check params)

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(check = bool option).layout("check")
      ~storage:[%expr
                 {check = sp.none}]
      [pairing_check]
end
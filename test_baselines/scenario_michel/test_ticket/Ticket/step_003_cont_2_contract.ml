open Smartml

module Contract = struct

  let%entry_point run self () =
    with sp.match_record(self.data, "data") as data:
      ticket_test_ticket_52_data, ticket_test_ticket_52_copy = sp.match_tuple(read_ticket_raw data.t, "ticket_test_ticket_52_data", "ticket_test_ticket_52_copy")
      ticket_test_ticket_52_ticketer, ticket_test_ticket_52_content, ticket_test_ticket_52_amount = sp.match_tuple(ticket_test_ticket_52_data, "ticket_test_ticket_52_ticketer", "ticket_test_ticket_52_content", "ticket_test_ticket_52_amount")
      verify (ticket_test_ticket_52_content = 42);
      ticket1_test_ticket_54, ticket2_test_ticket_54 = sp.match_tuple(split_ticket_raw ticket_test_ticket_52_copy (ticket_test_ticket_52_amount // 2, ticket_test_ticket_52_amount // 2).open_some(), "ticket1_test_ticket_54", "ticket2_test_ticket_54")
      data.t <- join_tickets_raw (ticket2_test_ticket_54, ticket1_test_ticket_54).open_some()

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(t = nat ticket, x = int).layout(("t", "x"))
      [run]
end
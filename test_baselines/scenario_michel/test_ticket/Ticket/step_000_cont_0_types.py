import smartpy as sp

tstorage = sp.TRecord(x = sp.TOption(sp.TTicket(sp.TInt)), y = sp.TOption(sp.TTicket(sp.TString))).layout(("x", "y"))
tparameter = sp.TVariant(auto_call = sp.TUnit, run = sp.TTicket(sp.TInt), run2 = sp.TRecord(t = sp.TTicket(sp.TInt), x = sp.TInt).layout(("t", "x"))).layout(("auto_call", ("run", "run2")))
tglobals = { }
tviews = { }

open Smartml

module Contract = struct

  let%entry_point ep1 self () =
    with sp.match_record(self.data, "data") as data:
      match_pair_test_ticket_76_fst, match_pair_test_ticket_76_snd = sp.match_tuple(sp.get_and_update(data.m, 42, none), "match_pair_test_ticket_76_fst", "match_pair_test_ticket_76_snd")
      data.m <- match_pair_test_ticket_76_snd;
      data.x <- 0

  let%entry_point ep2 self params =
    with sp.match_record(self.data, "data") as data:
      ticket_83 = sp.local("ticket_83", ticket "a" 1);
      ticket_84 = sp.local("ticket_84", ticket "b" 2);
      sp.transfer((ticket_83.value, ticket_84.value), sp.tez(0), params)

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(m = map(int, int ticket), x = int).layout(("m", "x"))
      [ep1; ep2]
end
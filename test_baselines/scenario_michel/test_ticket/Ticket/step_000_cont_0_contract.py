import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(x = sp.TOption(sp.TTicket(sp.TInt)), y = sp.TOption(sp.TTicket(sp.TString))).layout(("x", "y")))
    self.init(x = sp.none,
              y = sp.none)

  @sp.entry_point
  def auto_call(self):
    ticket_10 = sp.local("ticket_10", sp.ticket(1, 43))
    sp.transfer(ticket_10.value, sp.tez(0), sp.self_entry_point('run'))

  @sp.entry_point
  def run(self, params):
    sp.set_type(params, sp.TTicket(sp.TInt))
    ticket_test_ticket_15_data, ticket_test_ticket_15_copy = sp.match_tuple(sp.read_ticket_raw(params), "ticket_test_ticket_15_data", "ticket_test_ticket_15_copy")
    ticket_test_ticket_15_ticketer, ticket_test_ticket_15_content, ticket_test_ticket_15_amount = sp.match_tuple(ticket_test_ticket_15_data, "ticket_test_ticket_15_ticketer", "ticket_test_ticket_15_content", "ticket_test_ticket_15_amount")
    ticket_16 = sp.local("ticket_16", sp.ticket('abc', 42))
    self.data.y = sp.some(ticket_16.value)
    ticket1_test_ticket_17, ticket2_test_ticket_17 = sp.match_tuple(sp.split_ticket_raw(ticket_test_ticket_15_copy, (ticket_test_ticket_15_amount // 3, sp.as_nat(ticket_test_ticket_15_amount - (ticket_test_ticket_15_amount // 3)))).open_some(), "ticket1_test_ticket_17", "ticket2_test_ticket_17")
    self.data.x = sp.some(sp.join_tickets_raw((ticket2_test_ticket_17, ticket1_test_ticket_17)).open_some())

  @sp.entry_point
  def run2(self, params):
    sp.set_type(params, sp.TRecord(t = sp.TTicket(sp.TInt), x = sp.TInt).layout(("t", "x")))
    x_test_ticket_23, t_test_ticket_23 = sp.match_record(params, "x", "t")
    sp.verify(x_test_ticket_23 == 42)
    ticket_test_ticket_25_data, ticket_test_ticket_25_copy = sp.match_tuple(sp.read_ticket_raw(t_test_ticket_23), "ticket_test_ticket_25_data", "ticket_test_ticket_25_copy")
    ticket_test_ticket_25_ticketer, ticket_test_ticket_25_content, ticket_test_ticket_25_amount = sp.match_tuple(ticket_test_ticket_25_data, "ticket_test_ticket_25_ticketer", "ticket_test_ticket_25_content", "ticket_test_ticket_25_amount")
    ticket_26 = sp.local("ticket_26", sp.ticket('abc', 42))
    self.data.y = sp.some(ticket_26.value)
    ticket1_test_ticket_27, ticket2_test_ticket_27 = sp.match_tuple(sp.split_ticket_raw(ticket_test_ticket_25_copy, (ticket_test_ticket_25_amount // 3, sp.as_nat(ticket_test_ticket_25_amount - (ticket_test_ticket_25_amount // 3)))).open_some(), "ticket1_test_ticket_27", "ticket2_test_ticket_27")
    self.data.x = sp.some(sp.join_tickets_raw((ticket2_test_ticket_27, ticket1_test_ticket_27)).open_some())
open Smartml

module Contract = struct

  let%entry_point entry_point_1 self params =
    match_pair_test_imported_9_fst, match_pair_test_imported_9_snd = sp.match_tuple(params, "match_pair_test_imported_9_fst", "match_pair_test_imported_9_snd")
    match_pair_test_imported_10_fst, match_pair_test_imported_10_snd = sp.match_tuple(match_pair_test_imported_9_snd, "match_pair_test_imported_10_fst", "match_pair_test_imported_10_snd")
    self.data.x += (match_pair_test_imported_9_fst * match_pair_test_imported_10_fst) * match_pair_test_imported_10_snd

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(x = intOrNat).layout("x")
      ~storage:[%expr
                 {x = 0}]
      [entry_point_1]
end
Comment...
 h1: Nim
Comment...
 h2: Contract
Creating contract
 => test_baselines/scenario_michel/state_channel_games/models/nim/Nim_-_1/step_002_cont_0_pre_michelson.michel 115
 -> (Pair { DUP; GET 4; DUP; GET 3; DUP 3; GET 3; CAR; MEM; IF {} { PUSH string "unknown heap"; FAILWITH }; DUP; GET 3; DUP 3; GET 3; CAR; GET; IF_SOME {} { PUSH int 29; FAILWITH }; SWAP; DUP; DUG 2; CAR; IF_SOME { DROP; SWAP; DUP; DUG 2; CAR; IF_SOME {} { PUSH int 31; FAILWITH }; DUP 4; GET 3; CDR; COMPARE; LE } { PUSH bool True }; IF {} { PUSH string "bound exceeded"; FAILWITH }; PUSH int 1; DUP 4; GET 3; CDR; COMPARE; GE; IF {} { PUSH string "must take at least one"; FAILWITH }; DUP 3; GET 3; CDR; COMPARE; LE; IF {} { PUSH string "heap too small"; FAILWITH }; DUP; GET 3; DUP; DUP 4; GET 3; CAR; DUP; DUG 2; GET; IF_SOME {} { PUSH int 34; FAILWITH }; DUP 5; GET 3; CDR; SWAP; SUB; SOME; SWAP; UPDATE; UPDATE 3; PUSH (option string) (Some "player_1_won"); PUSH int 1; DIG 3; CAR; CAR; COMPARE; EQ; DUP 3; GET 4; COMPARE; EQ; IF { DROP; PUSH (option string) (Some "player_2_won") } {}; SWAP; DUP; DUG 2; GET 3; ITER { CDR; PUSH int 0; COMPARE; LT; IF { DROP; NONE string } {} }; SWAP; UNIT; PAIR 3 } (Pair (Pair 0x01 (Pair "" (Pair 0 (Pair 0x (Pair (Pair "tz0Fakeplayer1" "edpkFakeplayer1") (Pair (Pair "tz0Fakeplayer2" "edpkFakeplayer2") 0)))))) (Pair (Pair 1 (Pair 0 None)) (Pair None (Pair {Elt 0 3; Elt 1 4; Elt 2 5; Elt 3 6; Elt 4 7} True)))))
 => test_baselines/scenario_michel/state_channel_games/models/nim/Nim_-_1/step_002_cont_0_storage.tz 1
 => test_baselines/scenario_michel/state_channel_games/models/nim/Nim_-_1/step_002_cont_0_storage.json 180
 => test_baselines/scenario_michel/state_channel_games/models/nim/Nim_-_1/step_002_cont_0_sizes.csv 2
 => test_baselines/scenario_michel/state_channel_games/models/nim/Nim_-_1/step_002_cont_0_storage.py 1
 => test_baselines/scenario_michel/state_channel_games/models/nim/Nim_-_1/step_002_cont_0_types.py 7
 => test_baselines/scenario_michel/state_channel_games/models/nim/Nim_-_1/step_002_cont_0_contract.tz 127
 => test_baselines/scenario_michel/state_channel_games/models/nim/Nim_-_1/step_002_cont_0_contract.json 348
 => test_baselines/scenario_michel/state_channel_games/models/nim/Nim_-_1/step_002_cont_0_contract.py 25
 => test_baselines/scenario_michel/state_channel_games/models/nim/Nim_-_1/step_002_cont_0_contract.ml 29
Comment...
 h2: A sequence of interactions with a winner

import smartpy as sp

GameTester = sp.io.import_template("state_channel_games/game_tester.py").GameTester

class Nim:
    def __init__(self):
        self.t_init_input = sp.TRecord(
            numberOfHeaps = sp.TInt,
            bound         = sp.TOption(sp.TInt),
            misere        = sp.TBool)
        self.t_game_state = sp.TRecord(
            heaps  = sp.TMap(sp.TInt, sp.TInt),
            bound  = sp.TOption(sp.TInt),
            misere = sp.TBool)
        self.t_move_data = sp.TRecord(heapId = sp.TInt, size = sp.TInt)
        self.t_messages = sp.TUnit

    def init(self, config):
        heaps = sp.local('heaps', {})
        with sp.for_('i', sp.range(0, config.numberOfHeaps)) as i:
            heaps.value[i] = 3 + i
        sp.result(sp.record(heaps = heaps.value, bound = config.bound, misere = config.misere))


    def apply_(self, meta, state, move):
        s  = sp.local('s', state)

        sp.verify(s.value.heaps.contains(move.heapId), "unknown heap")
        heap = sp.local('heap', s.value.heaps[move.heapId])

        sp.verify(s.value.bound.is_none() | (move.size <= s.value.bound.open_some()), "bound exceeded")
        sp.verify(move.size >= 1, "must take at least one")
        sp.verify(move.size <= heap.value, "heap too small")
        s.value.heaps[move.heapId] -= move.size

        outcome = sp.local('outcome', sp.some("player_1_won"))
        with sp.if_(s.value.misere == (meta.current_player == 1)):
            outcome.value = sp.some("player_2_won")

        with sp.for_('x', s.value.heaps.values()) as x:
            with sp.if_(x > 0):
                outcome.value = sp.none

        sp.result(sp.record(
            new_state = s.value,
            outcome   = outcome.value,
            messages  = sp.unit
        ))

# Tests
if "templates" not in __name__:

    player1 = sp.test_account('player1')
    player2 = sp.test_account('player2')
    p1 = sp.record(addr = player1.address, pk = player1.public_key)
    p2 = sp.record(addr = player2.address, pk = player2.public_key)

    @sp.add_test(name="Nim - 1")
    def test():
        scenario = sp.test_scenario()
        scenario.h1("Nim")
        scenario.h2("Contract")

        params = sp.record(
            numberOfHeaps = 5,
            bound         = sp.none,
            misere        = True)

        c1 = GameTester(Nim(), p1, p2, params)
        scenario += c1
        scenario.h2("A sequence of interactions with a winner")
        # TODO

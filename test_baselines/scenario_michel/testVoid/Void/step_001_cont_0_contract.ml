open Smartml

module Contract = struct

  let init storage =
    Basics.build_contract
      ~tstorage:unit
      ~storage:[%expr sp.unit]
      []
end
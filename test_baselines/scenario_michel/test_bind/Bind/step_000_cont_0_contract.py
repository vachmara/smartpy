import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(x = sp.TIntOrNat, y = sp.TIntOrNat).layout(("x", "y")))
    self.init(x = 12,
              y = 0)

  @sp.entry_point
  def call_f(self, params):
    __s1 = sp.local("__s1", self.data.x + params)
    self.data.x = __s1.value
    __s2 = sp.local("__s2", self.data.x + 5)
    self.data.x = __s2.value
    __s3 = sp.local("__s3", self.data.x + 32)
    self.data.x = __s3.value
open Smartml

module Contract = struct

  let%entry_point add self () =
    self.data.value <- abs sp.michelson("ADD")(15, 16)

  let%entry_point concat1 self () =
    self.data.s <- sp.michelson("CONCAT")(sp.list(["a", "b", "c"]))

  let%entry_point concat2 self () =
    self.data.s <- sp.michelson("CONCAT")("a", "b")

  let%entry_point seq self () =
    self.data.value <- abs sp.michelson("DIP {SWAP}; ADD; MUL; DUP; MUL;")(15, 16, 17)

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(l = sp.TLambda(nat, int), s = string, value = nat).layout(("l", ("s", "value")))
      ~storage:[%expr
                 {l = lambda(sp.TLambda(nat, int)),
                  s = '',
                  value = 0}]
      [add; concat1; concat2; seq]
end
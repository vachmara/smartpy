open Smartml

module Contract = struct

  let%entry_point ep self () =
    self.data.out <- now > (add_seconds now 1);
    verify (((add_seconds now 12) - now) = 12);
    verify ((now - (add_seconds now 12)) = -12);
    verify ((now - (add_seconds now 12)) = -12);
    self.data.next <- add_seconds now 86400

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(next = timestamp, out = bool).layout(("next", "out"))
      ~storage:[%expr
                 {next = sp.timestamp(0),
                  out = False}]
      [ep]
end
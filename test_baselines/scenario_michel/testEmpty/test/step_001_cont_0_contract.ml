open Smartml

module Contract = struct

  let%entry_point ep1 self () =
    self.data.m1 <- {"e" : "f"};
    self.data.l1 <- sp.list(["g"]);
    self.data.o1 <- some "h"

  let%entry_point ep2 self () =
    self.data.m2 <- {};
    self.data.l2 <- sp.list([])

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(l1 = string list, l2 = string list, m1 = map(string, string), m2 = map(string, string), o1 = string option).layout(("l1", ("l2", ("m1", ("m2", "o1")))))
      ~storage:[%expr
                 {l1 = sp.list([]),
                  l2 = sp.list(['c']),
                  m1 = {},
                  m2 = {'a' : 'b'},
                  o1 = sp.none}]
      [ep1; ep2]
end
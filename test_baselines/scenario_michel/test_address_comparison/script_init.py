import smartpy as sp

class TestAddressComparison(sp.Contract):
    @sp.entry_point
    def test(self, params):
        sp.verify(
            sp.utils.same_underlying_address(sp.fst(params), sp.snd(params))
        )

@sp.add_test(name = "TestAddressComparison")
def test():
    scenario = sp.test_scenario()
    c1 = TestAddressComparison()

    scenario.register(c1)

    scenario += c1.test((
        sp.address("KT1WD5PV1i1HQTFhNUxVGNjRda63trNyshwU"),
        sp.address("KT1WD5PV1i1HQTFhNUxVGNjRda63trNyshwU")
    ))

    scenario += c1.test((
        sp.address("KT1WD5PV1i1HQTFhNUxVGNjRda63trNyshwU"),
        sp.address("KT1WD5PV1i1HQTFhNUxVGNjRda63trNyshwU%a")
    ))

    scenario += c1.test((
        sp.address("KT1WD5PV1i1HQTFhNUxVGNjRda63trNyshwU"),
        sp.address("KT1WD5PV1i1HQTFhNUxVGNjRda63trNyshwU%")
    ))

    scenario += c1.test((
        sp.address("KT1WD5PV1i1HQTFhNUxVGNjRda63trNyshwU"),
        sp.address("tz1Z6Uk4qfdAJLJuCdGzL8aheqedW8sBQv2T")
    )).run(valid = False)

    scenario += c1.test((
        sp.address("KT1WD5PV1i1HQTFhNUxVGNjRda63trNyshwU"),
        sp.address("KT1AYAtnyeZKifkjv5ooKXsKuWWbpECMgoUC")
    )).run(valid = False)

    scenario += c1.test((
        sp.address("KT1WD5PV1i1HQTFhNUxVGNjRda63trNyshwU"),
        sp.address("KT1AYAtnyeZKifkjv5ooKXsKuWWbpECMgoUC%")
    )).run(valid = False)

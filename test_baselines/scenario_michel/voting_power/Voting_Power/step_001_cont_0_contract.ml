open Smartml

module Contract = struct

  let%entry_point validate self params =
    self.data.a <- voting_power (hash_key params);
    self.data.b <- total_voting_power

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(a = nat, b = nat).layout(("a", "b"))
      ~storage:[%expr
                 {a = 0,
                  b = 0}]
      [validate]
end
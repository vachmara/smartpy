open Smartml

module Contract = struct

  let%entry_point ep1 self params =
    set_type (params : unit)

  let%entry_point ep2 self params =
    set_type (params : timestamp contract)

  let init storage =
    Basics.build_contract
      ~tstorage:unit
      ~storage:[%expr sp.unit]
      [ep1; ep2]
end
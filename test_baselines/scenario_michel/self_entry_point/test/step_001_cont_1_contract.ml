open Smartml

module Contract = struct

  let%entry_point ep3 self params =
    set_type (params : bool);
    set_type (sp.self_entry_point('ep4') : timestamp contract);
    sp.transfer(sp.self_entry_point('ep4'), sp.tez(0), sp.contract(timestamp contract, self.data.a, entry_point='ep2').open_some())

  let%entry_point ep4 self params =
    set_type (params : timestamp)

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(a = address).layout("a")
      ~storage:[%expr
                 {a = sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1')}]
      [ep3; ep4]
end
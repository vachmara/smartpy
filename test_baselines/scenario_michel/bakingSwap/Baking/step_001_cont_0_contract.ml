open Smartml

module Contract = struct

  let%entry_point collateralize self params =
    verify (sender = self.data.admin);
    self.data.collateral += params

  let%entry_point delegate self params =
    verify (sender = self.data.admin);
    verify (amount = sp.tez(0));
    sp.set_delegate(params)

  let%entry_point deposit self params =
    verify (self.data.rate >= params.rate);
    verify (self.data.duration <= params.duration);
    verify (not (self.data.ledger.contains(sender)));
    self.data.collateral -= sp.split_tokens(amount, self.data.rate, 10000);
    verify (self.data.collateral >= sp.tez(0));
    self.data.ledger[sender] <- {amount = amount + sp.split_tokens(amount, self.data.rate, 10000); due = add_seconds now ((self.data.duration * 24) * 3600)}

  let%entry_point setOffer self params =
    verify (sender = self.data.admin);
    self.data.rate <- params.rate;
    self.data.duration <- params.duration

  let%entry_point uncollateralize self params =
    verify (sender = self.data.admin);
    self.data.collateral -= params;
    verify (self.data.collateral >= sp.tez(0))

  let%entry_point withdraw self () =
    verify (now >= self.data.ledger[sender].due);
    sp.send(sender, self.data.ledger[sender].amount);
    del self.data.ledger[sender]

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(admin = address, collateral = mutez, duration = int, ledger = map(address, sp.TRecord(amount = mutez, due = timestamp).layout(("amount", "due"))), rate = nat).layout(("admin", ("collateral", ("duration", ("ledger", "rate")))))
      ~storage:[%expr
                 {admin = sp.address('tz1SfRoaCkrBkXqTzhz67QYVPJAU9Y2g48kq'),
                  collateral = sp.tez(0),
                  duration = 365,
                  ledger = {},
                  rate = 700}]
      [collateralize; delegate; deposit; setOffer; uncollateralize; withdraw]
end
open Smartml

module Contract = struct

  let%entry_point ep self params =
    self.data.x <- some params

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(x = intOrNat option).layout("x")
      ~storage:[%expr
                 {x = sp.none}]
      [ep]
end
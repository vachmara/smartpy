open Smartml

module Contract = struct

  let%entry_point ep1 self () =
    set_type (self.data.x2 : sp.TRecord(a = nat, b = int, c = bool, d = string).layout(("a", ("b", ("c", "d")))));
    set_type (self.data.x3 : sp.TRecord(a = nat, b = int, c = bool, d = string).layout(((("a", "b"), "c"), "d")));
    set_type (self.data.x4 : sp.TRecord(a = nat, b = int, c = bool, d = string).layout((("a", "b"), ("c", "d"))));
    with sp.match_record(self.data.x1, "modify_record_test_modify_21") as modify_record_test_modify_21:
      verify ((abs modify_record_test_modify_21.b) = (modify_record_test_modify_21.a + 1));
    with sp.match_record(self.data.x2, "modify_record_test_modify_23") as modify_record_test_modify_23:
      verify ((abs modify_record_test_modify_23.b) = (modify_record_test_modify_23.a + 1));
      modify_record_test_modify_23.d <- "xyz";
    with sp.match_record(self.data.x3, "modify_record_test_modify_26") as modify_record_test_modify_26:
      verify ((abs modify_record_test_modify_26.b) = (modify_record_test_modify_26.a + 1));
      modify_record_test_modify_26.d <- "xyz";
    with sp.match_record(self.data.x4, "modify_record_test_modify_29") as modify_record_test_modify_29:
      verify ((abs modify_record_test_modify_29.b) = (modify_record_test_modify_29.a + 1));
      modify_record_test_modify_29.d <- "xyz"

  let%entry_point ep2 self () =
    with sp.match_record(self.data.x1, "modify_record_test_modify_35") as modify_record_test_modify_35:
      verify ((abs modify_record_test_modify_35.b) = (modify_record_test_modify_35.a + 1));
      modify_record_test_modify_35.d <- "xyz"

  let%entry_point ep3 self () =
    with sp.match_tuple(self.data.y, "a", "b", "c", "d") as a, b, c, d:
      verify ((abs b.value) = (a.value + 1));
      d.value <- "xyz";
      (a.value, b.value, c.value, d.value)

  let%entry_point ep4 self () =
    with sp.match_record(self.data.x1, "modify_record_test_modify_49") as modify_record_test_modify_49:
      ()

  let%entry_point ep5 self params =
    with sp.match_record(self.data.x1, "modify_record_test_modify_61") as modify_record_test_modify_61:
      sp.send(params, sp.tez(0));
      modify_record_test_modify_61.d <- "xyz";
    self.data.x1.a += 5

  let%entry_point ep6 self () =
    with sp.match_record(self.data.z, "modify_record_test_modify_68") as modify_record_test_modify_68:
      with sp.match_record(modify_record_test_modify_68.d, "modify_record_test_modify_69") as modify_record_test_modify_69:
        modify_record_test_modify_68.b <- 100;
        modify_record_test_modify_69.e <- 2;
        modify_record_test_modify_69.f <- "y"

  let%entry_point ep7 self () =
    verify (self.data.z.d.e = 2);
    with sp.match_record(self.data.z.d, "modify_record_test_modify_77") as modify_record_test_modify_77:
      verify (modify_record_test_modify_77.e = 2);
      modify_record_test_modify_77.e <- 3;
      modify_record_test_modify_77.f <- "z";
      verify (modify_record_test_modify_77.e = 3);
      modify_record_test_modify_77.e <- 4;
      verify (modify_record_test_modify_77.e = 4);
    verify (self.data.z.d.e = 4)

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(x1 = sp.TRecord(a = nat, b = int, c = bool, d = string).layout(("a", ("b", ("c", "d")))), x2 = sp.TRecord(a = nat, b = int, c = bool, d = string).layout(("a", ("b", ("c", "d")))), x3 = sp.TRecord(a = nat, b = int, c = bool, d = string).layout(((("a", "b"), "c"), "d")), x4 = sp.TRecord(a = nat, b = int, c = bool, d = string).layout((("a", "b"), ("c", "d"))), y = sp.TTuple(nat, int, bool, string), z = sp.TRecord(a = nat, b = int, c = bool, d = sp.TRecord(e = intOrNat, f = string).layout(("e", "f"))).layout(("a", ("b", ("c", "d"))))).layout(("x1", ("x2", ("x3", ("x4", ("y", "z"))))))
      ~storage:[%expr
                 {x1 = {a = 0; b = 1; c = True; d = 'abc'},
                  x2 = {a = 0; b = 1; c = True; d = 'abc'},
                  x3 = {a = 0; b = 1; c = True; d = 'abc'},
                  x4 = {a = 0; b = 1; c = True; d = 'abc'},
                  y = (0, 1, True, 'abc'),
                  z = {a = 0; b = 1; c = True; d = {e = 1; f = 'x'}}}]
      [ep1; ep2; ep3; ep4; ep5; ep6; ep7]
end
open Smartml

module Contract = struct

  let%entry_point administrate self params =
    verify (sender = self.data.admin) "NOT ADMIN";
    set_type (params : sp.TVariant(setActive = bool, setAdmin = address).layout(("setActive", "setAdmin")) list);
    sp.for action in params:
      with action.match_cases() as arg:
        with arg.match('setActive') as setActive:
          self.data.active <- setActive
        with arg.match('setAdmin') as setAdmin:
          self.data.admin <- setAdmin


  let%entry_point setValue self params =
    verify self.data.active "NOT ACTIVE";
    self.data.value <- params

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(active = bool, admin = address, value = int option).layout(("active", ("admin", "value")))
      ~storage:[%expr
                 {active = False,
                  admin = sp.address('tz1UyQDepgtUBnWjyzzonqeDwaiWoQzRKSP5'),
                  value = sp.none}]
      [administrate; setValue]
end
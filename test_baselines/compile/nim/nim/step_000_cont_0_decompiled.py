import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(claimed = sp.TBool, deck = sp.TMap(sp.TInt, sp.TInt), nextPlayer = sp.TInt, size = sp.TInt, winner = sp.TInt).layout((("claimed", "deck"), ("nextPlayer", ("size", "winner")))))

  @sp.entry_point
  def claim(self, params):
    sp.set_type(params, sp.TInt)
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: let [r217] =
          iter [ __storage.deck; 0 ]
          step s161; s162 ->
            [ Add(Cdr(s161), s162) ]
        let x239 =
          if Eq(Compare(r217, 0))
          then
            if __storage.claimed
            then
              Failwith("WrongCondition: ~ self.data.claimed")
            else
              let x198 =
                Pair
                  ( Pair(True, __storage.deck)
                  , Pair
                      ( __storage.nextPlayer
                      , Pair(__storage.size, __storage.winner)
                      )
                  )
              let x202 =
                let [x252; x253] = Unpair(2, x198)
                Pair
                  ( x252
                  , let [x256; x257] = Unpair(2, x253)
                    Pair
                      ( x256
                      , let [x258; x259] = Unpair(2, x257)
                        Pair(x258, let _ = x259
                                   Car(Cdr(x198)))
                      )
                  )
              if Eq(Compare(Cdr(Cdr(Cdr(x202))), l4))
              then
                x202
              else
                Failwith("WrongCondition: params.winner == self.data.winner")
          else
            Failwith("WrongCondition: sp.sum(self.data.deck.values()) == 0")
        Pair(Nil<operation>, record_of_tree(..., x239))]')
    sp.failwith(sp.build_lambda(f-1)(sp.unit))

  @sp.entry_point
  def remove(self, params):
    sp.set_type(params, sp.TPair(sp.TInt, sp.TInt))
    sp.verify(sp.fst(params) >= 0, 'WrongCondition: params.cell >= 0')
    sp.verify(sp.fst(params) < self.data.size, 'WrongCondition: params.cell < self.data.size')
    sp.verify(sp.snd(params) >= 1, 'WrongCondition: params.k >= 1')
    sp.verify(sp.snd(params) <= 2, 'WrongCondition: params.k <= 2')
    with sp.some(self.data.deck[sp.fst(params)]).match_cases() as arg:
      with arg.match('Some') as s78:
        sp.verify(sp.snd(params) <= s78, 'WrongCondition: params.k <= self.data.deck[params.cell]')
        x105 = sp.local("x105", self.data.deck)
        with sp.some(x105.value[sp.fst(params)]).match_cases() as arg:
          with arg.match('Some') as s119:
            x146 = sp.local("x146", ((self.data.claimed, sp.update_map(x105.value, sp.fst(params), sp.some(s119 - sp.snd(params)))), (self.data.nextPlayer, (self.data.size, self.data.winner))))
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: Unpair(2, x146)]')
            sp.failwith(sp.build_lambda(f-1)(sp.unit))
          with arg.match('None') as _r12:
            sp.failwith(23)

      with arg.match('None') as _r2:
        sp.failwith(22)


@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()

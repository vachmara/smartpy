import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(l = sp.TLambda(sp.TNat, sp.TInt), s = sp.TString, value = sp.TNat).layout(("l", ("s", "value"))))

  @sp.entry_point
  def add(self, params):
    sp.set_type(params, sp.TUnit)
    self.data = sp.record(l = self.data.l, s = self.data.s, value = 31)

  @sp.entry_point
  def concat1(self, params):
    sp.set_type(params, sp.TUnit)
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: ["a"; "b"; "c"]#list(string)]')
    sp.failwith(sp.build_lambda(f-1)(sp.unit))

  @sp.entry_point
  def concat2(self, params):
    sp.set_type(params, sp.TUnit)
    def f-1(lparams_-1):
      sp.failwith('[Error: TODO prim2: Concat2]')
    self.data = sp.record(l = self.data.l, s = sp.build_lambda(f-1)(sp.unit), value = self.data.value)

  @sp.entry_point
  def seq(self, params):
    sp.set_type(params, sp.TUnit)
    self.data = sp.record(l = self.data.l, s = self.data.s, value = 262144)

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()

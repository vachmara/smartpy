import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(x = sp.TNat, y = sp.TString, z = sp.TNat).layout(("x", ("y", "z"))))

  @sp.entry_point
  def ep0(self, params):
    sp.set_type(params, sp.TVariant(Left = sp.TUnit, Right = sp.TUnit).layout(("Left", "Right")))
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: (fun (x2 : { nat ; (nat ; (string ; nat)) }) : { list(operation) ; (nat ; (nat ; (string ; nat))) } ->
match Contract("tz0Fakealice"#address, unit) with
| Some s16 ->
    let x30 = Mul(Car(x2), 1000000#mutez)
    match Contract("tz0Fakebob"#address, unit) with
    | Some s36 ->
        let x55 =
          let [x287; x288] = Unpair(2, Cdr(x2))
          let _ = x287
          Pair(Add(1#nat, Car(Cdr(x2))), x288)
        Pair
          ( Cons
              ( Transfer_tokens(Unit, 2000000#mutez, s36)
              , Cons(Transfer_tokens(Unit, x30, s16), Nil<operation>)
              )
          , Pair(Mul(Car(x2), Car(x55)), x55)
          )
    | None _ -> Failwith(10)
    end
| None _ -> Failwith(9)
end)]')
    sp.failwith(sp.build_lambda(f-1)(sp.unit))

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()

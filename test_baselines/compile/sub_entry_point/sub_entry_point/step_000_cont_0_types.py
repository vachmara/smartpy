import smartpy as sp

tstorage = sp.TRecord(x = sp.TNat, y = sp.TString, z = sp.TNat).layout(("x", ("y", "z")))
tparameter = sp.TVariant(f = sp.TUnit, g = sp.TUnit).layout(("f", "g"))
tglobals = { "a": sp.TLambda(sp.TRecord(in_param = sp.TNat, in_storage = sp.TRecord(x = sp.TNat, y = sp.TString, z = sp.TNat).layout(("x", ("y", "z")))).layout(("in_param", "in_storage")), sp.TRecord(operations = sp.TList(sp.TOperation), result = sp.TNat, storage = sp.TRecord(x = sp.TNat, y = sp.TString, z = sp.TNat).layout(("x", ("y", "z")))).layout(("operations", ("result", "storage")))) }
tviews = { }

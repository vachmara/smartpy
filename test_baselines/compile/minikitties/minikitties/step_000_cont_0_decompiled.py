import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(creator = sp.TAddress, kitties = sp.TMap(sp.TInt, sp.TPair(sp.TPair(sp.TPair(sp.TTimestamp, sp.TMutez), sp.TPair(sp.TInt, sp.TTimestamp)), sp.TPair(sp.TPair(sp.TBool, sp.TInt), sp.TPair(sp.TAddress, sp.TMutez))))).layout(("creator", "kitties")))

  @sp.entry_point
  def ep0(self, params):
    sp.set_type(params, sp.TVariant(Left = sp.TVariant(Left = sp.TPair(sp.TPair(sp.TMutez, sp.TInt), sp.TPair(sp.TInt, sp.TInt)), Right = sp.TPair(sp.TPair(sp.TPair(sp.TTimestamp, sp.TMutez), sp.TPair(sp.TInt, sp.TTimestamp)), sp.TPair(sp.TPair(sp.TBool, sp.TInt), sp.TPair(sp.TAddress, sp.TMutez)))).layout(("Left", "Right")), Right = sp.TVariant(Left = sp.TPair(sp.TInt, sp.TMutez), Right = sp.TVariant(Left = sp.TPair(sp.TMutez, sp.TPair(sp.TInt, sp.TMutez)), Right = sp.TPair(sp.TMutez, sp.TPair(sp.TInt, sp.TMutez))).layout(("Left", "Right"))).layout(("Left", "Right"))).layout(("Left", "Right")))
    with params.match_cases() as arg:
      with arg.match('Left') as l4:
        with l4.match_cases() as arg:
          with arg.match('Left') as l604:
            sp.verify((sp.fst(sp.snd(l604)) + (- sp.snd(sp.snd(l604)))) != 0, 'WrongCondition: params.parent1 != params.parent2')
            with sp.some(self.data.kitties[sp.fst(sp.snd(l604))]).match_cases() as arg:
              with arg.match('Some') as s673:
                sp.verify(sp.fst(sp.fst(sp.fst(s673))) < sp.now, 'WrongCondition: self.data.kitties[params.parent1].auction < sp.now')
                with sp.some(self.data.kitties[sp.fst(sp.snd(l604))]).match_cases() as arg:
                  with arg.match('Some') as s703:
                    sp.verify(sp.snd(sp.snd(sp.fst(s703))) < sp.now, 'WrongCondition: self.data.kitties[params.parent1].hatching < sp.now')
                    with sp.some(self.data.kitties[sp.snd(sp.snd(l604))]).match_cases() as arg:
                      with arg.match('Some') as s732:
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: let [s857; s858; s859] =
          if Neq(Compare(Car(Cdr(Cdr(s732))), Sender))
          then
            match Get(Cdr(Cdr(l604)), __storage.kitties) with
            | Some s753 ->
                if Lt(Compare(0#mutez, Cdr(Car(Car(s753)))))
                then
                  match Get(Cdr(Cdr(l604)), __storage.kitties) with
                  | Some s791 ->
                      if Lt(Compare(Cdr(Car(Car(s791))), Car(Car(l604))))
                      then
                        if Eq(Compare(Amount, Car(Car(l604))))
                        then
                          match Get(Cdr(Cdr(l604)), __storage.kitties) with
                          | Some s834 ->
                              match Contract(Car(Cdr(Cdr(s834))), unit) with
                              | Some s842 ->
                                  [ Cons
                                      ( Transfer_tokens(Unit, Car(Car(l604)), s842)
                                      , Nil<operation>
                                      )
                                  ; l604
                                  ; Pair(__storage.creator, __storage.kitties)
                                  ]
                              | None _ -> Failwith(53)
                              end
                          | None _ -> Failwith(65)
                          end
                        else
                          Failwith("WrongCondition: sp.amount == params.borrowPrice")
                      else
                        Failwith("WrongCondition: self.data.kitties[params.parent2].borrowPrice < params.borrowPrice")
                  | None _ -> Failwith(65)
                  end
                else
                  Failwith("WrongCondition: sp.tez(0) < self.data.kitties[params.parent2].borrowPrice")
            | None _ -> Failwith(65)
            end
          else
            [ Nil<operation>
            ; l604
            ; Pair(__storage.creator, __storage.kitties)
            ]
        match Get(Cdr(Cdr(s858)), Cdr(s859)) with
        | Some s874 ->
            if Lt(Compare(Car(Car(Car(s874))), Now))
            then
              match Get(Cdr(Cdr(s858)), Cdr(s859)) with
              | Some s904 ->
                  if Lt(Compare(Cdr(Cdr(Car(s904))), Now))
                  then
                    let x935 = Car(Cdr(s858))
                    match Get(x935, Cdr(s859)) with
                    | Some s940 ->
                        let x971 =
                          let [x1213; x1214] = Unpair(2, s859)
                          Pair
                            ( x1213
                            , let _ = x1214
                              Update(x935, Some_(Pair
                                                   ( Pair
                                                       ( Car(Car(s940))
                                                       , Pair
                                                           ( Car(Cdr(Car(s940)))
                                                           , Add(100, Now)
                                                           )
                                                       )
                                                   , Cdr(s940)
                                                   )), Cdr(s859))
                            )
                        let x983 = Cdr(Cdr(s858))
                        match Get(x983, Cdr(x971)) with
                        | Some s988 ->
                            let x1019 =
                              let [x1211; x1212] = Unpair(2, x971)
                              Pair
                                ( x1211
                                , let _ = x1212
                                  Update(x983, Some_(Pair
                                                       ( Pair
                                                           ( Car(Car(s988))
                                                           , Pair
                                                               ( Car(Cdr(Car(s988)))
                                                               , Add(100, Now)
                                                               )
                                                           )
                                                       , Cdr(s988)
                                                       )), Cdr(x971))
                                )
                            match Get(Car(Cdr(s858)), Cdr(x1019)) with
                            | Some s1063 ->
                                let x1076 = Car(Cdr(Car(s1063)))
                                match Get(Cdr(Cdr(s858)), Cdr(x1019)) with
                                | Some s1099 ->
                                    let s1104 = Add(100, Now)
                                    let x1113 = Car(Cdr(Car(s1099)))
                                    let [s1135; s1136; s1137; s1138; s1139;
                                          s1140; s1141] =
                                      if Le(Compare(x1113, x1076))
                                      then
                                        [ x1076
                                        ; s1104
                                        ; Pair
                                            ( Pair(False, Cdr(Car(s858)))
                                            , Pair(Sender, 0#mutez)
                                            )
                                        ; Cdr(x1019)
                                        ; x1019
                                        ; s857
                                        ; s858
                                        ]
                                      else
                                        [ x1113
                                        ; s1104
                                        ; Pair
                                            ( Pair(False, Cdr(Car(s858)))
                                            , Pair(Sender, 0#mutez)
                                            )
                                        ; Cdr(x1019)
                                        ; x1019
                                        ; s857
                                        ; s858
                                        ]
                                    [ s1140
                                    ; let [x1209; x1210] = Unpair(2, s1139)
                                      Pair
                                        ( x1209
                                        , let _ = x1210
                                          Update(Cdr(Car(s1141)), Some_(
                                          Pair
                                            ( Pair
                                                ( Pair
                                                    ( "1970-01-01T00:00:00Z"#timestamp
                                                    , 0#mutez
                                                    )
                                                , Pair(Add(1, s1135), s1136)
                                                )
                                            , s1137
                                            )), s1138)
                                        )
                                    ]
                                | None _ -> Failwith(65)
                                end
                            | None _ -> Failwith(64)
                            end
                        | None _ -> Failwith(71)
                        end
                    | None _ -> Failwith(70)
                    end
                  else
                    Failwith("WrongCondition: self.data.kitties[params.parent2].hatching < sp.now")
              | None _ -> Failwith(65)
              end
            else
              Failwith("WrongCondition: self.data.kitties[params.parent2].auction < sp.now")
        | None _ -> Failwith(65)
        end]')
                        sp.failwith(sp.build_lambda(f-1)(sp.unit))
                      with arg.match('None') as _r317:
                        sp.failwith(65)

                  with arg.match('None') as _r308:
                    sp.failwith(64)

              with arg.match('None') as _r299:
                sp.failwith(64)

          with arg.match('Right') as r605:
            sp.verify(self.data.creator == sp.sender, 'WrongCondition: self.data.creator == sp.sender')
            sp.verify(sp.fst(sp.fst(sp.snd(r605))), 'WrongCondition: params.kitty.isNew')
            x295 = sp.local("x295", (sp.list([]), (self.data.creator, sp.update_map(self.data.kitties, sp.snd(sp.fst(sp.snd(r605))), sp.some(r605)))))
            s1160 = sp.local("s1160", sp.fst(x295.value))
            s1161 = sp.local("s1161", sp.snd(x295.value))
            x351 = sp.local("x351", (s1160.value, s1161.value))
            s1162 = sp.local("s1162", sp.fst(x351.value))
            s1163 = sp.local("s1163", sp.snd(x351.value))
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: record_of_tree(..., s1163)]')
            sp.failwith(sp.build_lambda(f-1)(sp.unit))

      with arg.match('Right') as r5:
        with r5.match_cases() as arg:
          with arg.match('Left') as l6:
            with sp.some(self.data.kitties[sp.fst(l6)]).match_cases() as arg:
              with arg.match('Some') as s266:
                sp.verify(sp.tez(0) < sp.snd(sp.snd(sp.snd(s266))), 'WrongCondition: sp.tez(0) < self.data.kitties[params.kittyId].price')
                with sp.some(self.data.kitties[sp.fst(l6)]).match_cases() as arg:
                  with arg.match('Some') as s292:
                    sp.verify(sp.snd(sp.snd(sp.snd(s292))) <= sp.snd(l6), 'WrongCondition: self.data.kitties[params.kittyId].price <= params.price')
                    sp.verify(sp.amount == sp.snd(l6), 'WrongCondition: sp.amount == params.price')
                    with sp.some(self.data.kitties[sp.fst(l6)]).match_cases() as arg:
                      with arg.match('Some') as s325:
                        with sp.contract(sp.TUnit, sp.fst(sp.snd(sp.snd(s325)))).match_cases() as arg:
                          with arg.match('Some') as s333:
                            x354 = sp.local("x354", self.data.kitties)
                            with sp.some(x354.value[sp.fst(l6)]).match_cases() as arg:
                              with arg.match('Some') as s368:
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: Unpair(2, s368)]')
                                sp.failwith(sp.build_lambda(f-1)(sp.unit))
                              with arg.match('None') as _r192:
                                sp.failwith(39)

                          with arg.match('None') as _r182:
                            sp.failwith(38)

                      with arg.match('None') as _r174:
                        sp.failwith(34)

                  with arg.match('None') as _r166:
                    sp.failwith(34)

              with arg.match('None') as _r158:
                sp.failwith(34)

          with arg.match('Right') as r7:
            with r7.match_cases() as arg:
              with arg.match('Left') as l8:
                sp.verify(sp.tez(0) <= sp.snd(sp.snd(l8)), 'WrongCondition: sp.tez(0) <= params.price')
                with sp.some(self.data.kitties[sp.fst(sp.snd(l8))]).match_cases() as arg:
                  with arg.match('Some') as s147:
                    sp.verify(sp.fst(sp.fst(sp.fst(s147))) < sp.now, 'WrongCondition: self.data.kitties[params.kittyId].auction < sp.now')
                    with sp.some(self.data.kitties[sp.fst(sp.snd(l8))]).match_cases() as arg:
                      with arg.match('Some') as s177:
                        sp.verify(sp.snd(sp.snd(sp.fst(s177))) < sp.now, 'WrongCondition: self.data.kitties[params.kittyId].hatching < sp.now')
                        x199 = sp.local("x199", self.data.kitties)
                        x208 = sp.local("x208", sp.fst(sp.snd(l8)))
                        with sp.some(x199.value[x208.value]).match_cases() as arg:
                          with arg.match('Some') as s213:
                            x154 = sp.local("x154", (sp.list([]), (self.data.creator, sp.update_map(x199.value, x208.value, sp.some((((sp.fst(sp.fst(sp.fst(s213))), sp.snd(sp.snd(l8))), sp.snd(sp.fst(s213))), sp.snd(s213)))))))
                            s247 = sp.local("s247", sp.fst(x154.value))
                            s248 = sp.local("s248", sp.snd(x154.value))
                            x351 = sp.local("x351", (s247.value, s248.value))
                            s1162 = sp.local("s1162", sp.fst(x351.value))
                            s1163 = sp.local("s1163", sp.snd(x351.value))
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: record_of_tree(..., s1163)]')
                            sp.failwith(sp.build_lambda(f-1)(sp.unit))
                          with arg.match('None') as _r106:
                            sp.failwith(30)

                      with arg.match('None') as _r92:
                        sp.failwith(29)

                  with arg.match('None') as _r83:
                    sp.failwith(29)

              with arg.match('Right') as r9:
                sp.verify(sp.tez(0) <= sp.snd(sp.snd(r9)), 'WrongCondition: sp.tez(0) <= params.price')
                with sp.some(self.data.kitties[sp.fst(sp.snd(r9))]).match_cases() as arg:
                  with arg.match('Some') as s34:
                    sp.verify(sp.fst(sp.fst(sp.fst(s34))) < sp.now, 'WrongCondition: self.data.kitties[params.kittyId].auction < sp.now')
                    with sp.some(self.data.kitties[sp.fst(sp.snd(r9))]).match_cases() as arg:
                      with arg.match('Some') as s64:
                        sp.verify(sp.snd(sp.snd(sp.fst(s64))) < sp.now, 'WrongCondition: self.data.kitties[params.kittyId].hatching < sp.now')
                        x86 = sp.local("x86", self.data.kitties)
                        x95 = sp.local("x95", sp.fst(sp.snd(r9)))
                        with sp.some(x86.value[x95.value]).match_cases() as arg:
                          with arg.match('Some') as s100:
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: Unpair(2, s100)]')
                            sp.failwith(sp.build_lambda(f-1)(sp.unit))
                          with arg.match('None') as _r33:
                            sp.failwith(24)

                      with arg.match('None') as _r19:
                        sp.failwith(23)

                  with arg.match('None') as _r10:
                    sp.failwith(23)





@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()

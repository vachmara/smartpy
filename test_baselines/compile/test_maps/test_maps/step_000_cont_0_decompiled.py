import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(m = sp.TMap(sp.TInt, sp.TString), x = sp.TString, y = sp.TOption(sp.TString), z = sp.TOption(sp.TString)).layout((("m", "x"), ("y", "z"))))

  @sp.entry_point
  def test_get_and_update(self, params):
    sp.set_type(params, sp.TUnit)
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: record_of_tree(..., let [x141; _] =
                              Get_and_update(1, Some_("one"), __storage.m)
                            Pair
                              ( Pair(__storage.m, __storage.x)
                              , Pair(__storage.y, x141)
                              ))]')
    sp.failwith(sp.build_lambda(f-1)(sp.unit))

  @sp.entry_point
  def test_map_get(self, params):
    sp.set_type(params, sp.TMap(sp.TInt, sp.TString))
    with sp.some(params[12]).match_cases() as arg:
      with arg.match('Some') as s121:
        x17 = sp.local("x17", ((self.data.m, s121), (self.data.y, self.data.z)))
        x187 = sp.local("x187", x17.value)
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: record_of_tree(..., x187)]')
        sp.failwith(sp.build_lambda(f-1)(sp.unit))
      with arg.match('None') as _r3:
        sp.failwith(12)


  @sp.entry_point
  def test_map_get2(self, params):
    sp.set_type(params, sp.TMap(sp.TInt, sp.TString))
    with sp.some(params[12]).match_cases() as arg:
      with arg.match('Some') as s99:
        x35 = sp.local("x35", ((self.data.m, s99), (self.data.y, self.data.z)))
        x185 = sp.local("x185", x35.value)
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: record_of_tree(..., x185)]')
        sp.failwith(sp.build_lambda(f-1)(sp.unit))
      with arg.match('None') as _r21:
        sp.failwith(17)


  @sp.entry_point
  def test_map_get_default_values(self, params):
    sp.set_type(params, sp.TMap(sp.TInt, sp.TString))
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: record_of_tree(..., let x76 = __storage.m
                            let [s82; s83; s84] =
                              match Get(12, r69) with
                              | Some s80 ->
                                  [ s80
                                  ; x76
                                  ; Pair(__storage.y, __storage.z)
                                  ]
                              | None _ ->
                                  [ "abc"
                                  ; x76
                                  ; Pair(__storage.y, __storage.z)
                                  ]
                              end
                            Pair(Pair(s83, s82), s84))]')
    sp.failwith(sp.build_lambda(f-1)(sp.unit))

  @sp.entry_point
  def test_map_get_missing_value(self, params):
    sp.set_type(params, sp.TMap(sp.TInt, sp.TString))
    with sp.some(params[12]).match_cases() as arg:
      with arg.match('Some') as s55:
        x55 = sp.local("x55", ((self.data.m, s55), (self.data.y, self.data.z)))
        x181 = sp.local("x181", x55.value)
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: record_of_tree(..., x181)]')
        sp.failwith(sp.build_lambda(f-1)(sp.unit))
      with arg.match('None') as _r41:
        sp.failwith('missing 12')


  @sp.entry_point
  def test_map_get_missing_value2(self, params):
    sp.set_type(params, sp.TMap(sp.TInt, sp.TString))
    with sp.some(params[12]).match_cases() as arg:
      with arg.match('Some') as s36:
        x73 = sp.local("x73", ((self.data.m, s36), (self.data.y, self.data.z)))
        x179 = sp.local("x179", x73.value)
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: record_of_tree(..., x179)]')
        sp.failwith(sp.build_lambda(f-1)(sp.unit))
      with arg.match('None') as _r59:
        sp.failwith(1234)


  @sp.entry_point
  def test_map_get_opt(self, params):
    sp.set_type(params, sp.TMap(sp.TInt, sp.TString))
    self.data = sp.record(m = self.data.m, x = self.data.x, y = sp.some(params[12]), z = self.data.z)

  @sp.entry_point
  def test_update_map(self, params):
    sp.set_type(params, sp.TUnit)
    self.data = sp.record(m = sp.update_map(self.data.m, 1, sp.some('one')), x = self.data.x, y = self.data.y, z = self.data.z)

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()

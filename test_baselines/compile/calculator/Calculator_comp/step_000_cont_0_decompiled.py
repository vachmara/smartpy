import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TNat)

  @sp.entry_point
  def add(self, params):
    sp.set_type(params, sp.TPair(sp.TNat, sp.TNat))
    self.data = sp.fst(params) + sp.snd(params)

  @sp.entry_point
  def factorial(self, params):
    sp.set_type(params, sp.TNat)
    x198 = sp.local("x198", 1 + params)
    c11 = sp.local("c11", x198.value > 1)
    y9 = sp.local("y9", 1)
    r284 = sp.local("r284", 1)
    sp.while c11.value:
      x225 = sp.local("x225", 1 + y9.value)
      c11.value = x198.value > x225.value
      y9.value = x225.value
      r284.value = y9.value * r284.value
    self.data = r284.value

  @sp.entry_point
  def log2(self, params):
    sp.set_type(params, sp.TNat)
    c31 = sp.local("c31", 1 < params)
    y29 = sp.local("y29", params)
    r297 = sp.local("r297", 0)
    sp.while c31.value:
      with sp.ediv(y29.value, 2).match_cases() as arg:
        with arg.match('Some') as s170:
          c31.value = 1 < sp.fst(s170)
          y29.value = sp.fst(s170)
          r297.value += 1
        with arg.match('None') as _r33:
          sp.failwith(42)

    self.data = r297.value

  @sp.entry_point
  def multiply(self, params):
    sp.set_type(params, sp.TPair(sp.TNat, sp.TNat))
    self.data = sp.fst(params) * sp.snd(params)

  @sp.entry_point
  def square(self, params):
    sp.set_type(params, sp.TNat)
    self.data = params * params

  @sp.entry_point
  def squareRoot(self, params):
    sp.set_type(params, sp.TNat)
    sp.verify(params >= 0, 'WrongCondition: params >= 0')
    c65 = sp.local("c65", (params * params) > params)
    r295 = sp.local("r295", params)
    sp.while c65.value:
      with sp.ediv(params, r295.value).match_cases() as arg:
        with arg.match('Some') as s52:
          with sp.ediv(sp.fst(s52) + r295.value, 2).match_cases() as arg:
            with arg.match('Some') as s61:
              c65.value = (sp.fst(s61) * sp.fst(s61)) > params
              r295.value = sp.fst(s61)
            with arg.match('None') as _r93:
              sp.failwith(26)

        with arg.match('None') as _r87:
          sp.failwith(26)

    s128 = sp.local("s128", sp.fst(sp.eif((r295.value * r295.value) <= params, (params < ((r295.value + 1) * (1 + r295.value)), r295.value), (False, r295.value))))
    s129 = sp.local("s129", sp.snd(sp.eif((r295.value * r295.value) <= params, (params < ((r295.value + 1) * (1 + r295.value)), r295.value), (False, r295.value))))
    sp.verify(s128.value, 'WrongCondition: ((y.value * y.value) <= params) & (params < ((y.value + 1) * (y.value + 1)))')
    self.data = s129.value

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()

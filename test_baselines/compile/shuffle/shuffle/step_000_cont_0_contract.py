import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(a = sp.TRecord(x1 = sp.TInt, x2 = sp.TInt).layout(("x1", "x2")), b = sp.TRecord(x1 = sp.TInt, x2 = sp.TInt).layout(("x1", "x2"))).layout(("a", "b")))
    self.init(a = sp.record(x1 = -1, x2 = -2),
              b = sp.record(x1 = -3, x2 = -4))

  @sp.entry_point
  def swap(self):
    x = sp.local("x", self.data.a.x1)
    self.data.a.x1 = self.data.a.x2
    self.data.a.x2 = x.value
    self.data.b.x1 = self.data.b.x2
    self.data.b.x2 *= 2
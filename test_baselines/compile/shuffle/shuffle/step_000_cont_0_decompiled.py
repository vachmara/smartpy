import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(a = sp.TPair(sp.TInt, sp.TInt), b = sp.TPair(sp.TInt, sp.TInt)).layout(("a", "b")))

  @sp.entry_point
  def ep0(self, params):
    sp.set_type(params, sp.TUnit)
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: Unpair(2, let [x66; x67] = Unpair(2, __storage.a)
                  let _ = x66
                  Pair(Cdr(__storage.a), x67))]')
    sp.failwith(sp.build_lambda(f-1)(sp.unit))

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()

import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(admin = sp.TAddress, boards = sp.TBigMap(sp.TString, sp.TPair(sp.TPair(sp.TPair(sp.TMap(sp.TInt, sp.TMap(sp.TInt, sp.TInt)), sp.TBool), sp.TPair(sp.TMap(sp.TString, sp.TString), sp.TInt)), sp.TPair(sp.TPair(sp.TInt, sp.TAddress), sp.TPair(sp.TAddress, sp.TInt)))), metaData = sp.TMap(sp.TString, sp.TString), paused = sp.TBool).layout((("admin", "boards"), ("metaData", "paused"))))

  @sp.entry_point
  def build(self, params):
    sp.set_type(params, sp.TPair(sp.TString, sp.TPair(sp.TAddress, sp.TAddress)))
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: let [s2210; s2211; s2212] =
          if __storage.paused
          then
            [ Eq(Compare(Sender, __storage.admin))
            ; l184
            ; Pair
                ( Pair(__storage.admin, __storage.boards)
                , Pair(__storage.metaData, __storage.paused)
                )
            ]
          else
            [ True
            ; l184
            ; Pair
                ( Pair(__storage.admin, __storage.boards)
                , Pair(__storage.metaData, __storage.paused)
                )
            ]
        let x2307 =
          if s2210
          then
            if Mem(Car(s2211), Cdr(Car(s2212)))
            then
              Failwith("WrongCondition: ~ (self.data.boards.contains(params.game))")
            else
              Pair
                ( Pair
                    ( Car(Car(s2212))
                    , Update(Car(s2211), Some_(Pair
                                                 ( Pair
                                                     ( Pair
                                                         ( [0: [0: 0; 1: 0; 2: 0]#map(int,int); 1: [0: 0; 1: 0; 2: 0]#map(int,int); 2: [0: 0; 1: 0; 2: 0]#map(int,int)]#map(int,map(int, int))
                                                         , False
                                                         )
                                                     , Pair
                                                         ( []#map(string,string)
                                                         , 0
                                                         )
                                                     )
                                                 , Pair
                                                     ( Pair
                                                         ( 1
                                                         , Car(Cdr(s2211))
                                                         )
                                                     , Pair
                                                         ( Cdr(Cdr(s2211))
                                                         , 0
                                                         )
                                                     )
                                                 )), Cdr(Car(s2212)))
                    )
                , Cdr(s2212)
                )
          else
            Failwith("WrongCondition: (~ self.data.paused) | (sp.sender == self.data.admin)")
        Pair(Nil<operation>, record_of_tree(..., x2307))]')
    sp.failwith(sp.build_lambda(f-1)(sp.unit))

  @sp.entry_point
  def deleteGame(self, params):
    sp.set_type(params, sp.TString)
    sp.verify(sp.sender == self.data.admin, 'WrongCondition: sp.sender == self.data.admin')
    x19 = sp.local("x19", ((self.data.admin, sp.update_map(self.data.boards, params, sp.none)), (self.data.metaData, self.data.paused)))
    x2305 = sp.local("x2305", x19.value)
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: record_of_tree(..., x2305)]')
    sp.failwith(sp.build_lambda(f-1)(sp.unit))

  @sp.entry_point
  def play(self, params):
    sp.set_type(params, sp.TPair(sp.TPair(sp.TString, sp.TInt), sp.TPair(sp.TInt, sp.TInt)))
    x34 = sp.bind_block("x34"):
    with x34:
      sp.if self.data.paused:
        sp.failwith('WrongCondition: ~ self.data.paused')
      sp.else:
        with sp.some(self.data.boards[sp.fst(sp.fst(params))]).match_cases() as arg:
          with arg.match('Some') as s213:
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: let [s251; s252; s253] =
          if Eq(Compare(Cdr(Cdr(Cdr(s213))), 0))
          then
            match Get(Car(Car(r187)), __storage.boards) with
            | Some s242 ->
                [ Not(Cdr(Car(Car(s242))))
                ; r187
                ; Pair
                    ( Pair(__storage.admin, __storage.boards)
                    , Pair(__storage.metaData, __storage.paused)
                    )
                ]
            | None _ -> Failwith(51)
            end
          else
            [ False
            ; r187
            ; Pair
                ( Pair(__storage.admin, __storage.boards)
                , Pair(__storage.metaData, __storage.paused)
                )
            ]
        if s251
        then
          let [s274; s275; s276] =
            if Ge(Compare(Cdr(Car(s252)), 0))
            then
              [ Gt(Compare(3, Cdr(Car(s252)))); s252; s253 ]
            else
              [ False; s252; s253 ]
          if s274
          then
            let [s295; s296; s297] =
              if Ge(Compare(Car(Cdr(s275)), 0))
              then
                [ Gt(Compare(3, Car(Cdr(s275)))); s275; s276 ]
              else
                [ False; s275; s276 ]
            if s295
            then
              match Get(Car(Car(s296)), Cdr(Car(s297))) with
              | Some s320 ->
                  if Eq(Compare(Cdr(Cdr(s296)), Car(Car(Cdr(s320)))))
                  then
                    match Get(Car(Car(s296)), Cdr(Car(s297))) with
                    | Some s354 ->
                        match Get(Cdr(Car(s296)), Car(Car(Car(s354)))) with
                        | Some s370 ->
                            match Get(Car(Cdr(s296)), s370) with
                            | Some s382 ->
                                if Eq(Compare(s382, 0))
                                then
                                  let [s462; s463] =
                                    if Eq(Compare(1, Cdr(Cdr(s296))))
                                    then
                                      match Get
                                              ( Car(Car(s296))
                                              , Cdr(Car(s297))
                                              ) with
                                      | Some s449 ->
                                          if Eq(Compare
                                                  ( Sender
                                                  , Cdr(Car(Cdr(s449)))
                                                  ))
                                          then
                                            [ s296; s297 ]
                                          else
                                            Failwith("WrongCondition: sp.sender == self.data.boards[params.game].player1")
                                      | None _ -> Failwith(51)
                                      end
                                    else
                                      match Get
                                              ( Car(Car(s296))
                                              , Cdr(Car(s297))
                                              ) with
                                      | Some s418 ->
                                          if Eq(Compare
                                                  ( Sender
                                                  , Car(Cdr(Cdr(s418)))
                                                  ))
                                          then
                                            [ s296; s297 ]
                                          else
                                            Failwith("WrongCondition: sp.sender == self.data.boards[params.game].player2")
                                      | None _ -> Failwith(51)
                                      end
                                  match Get(Car(Car(s462)), Cdr(Car(s463))) with
                                  | Some s493 ->
                                      match Get
                                              ( Car(Car(s462))
                                              , Cdr(Car(s463))
                                              ) with
                                      | Some s533 ->
                                          let x558 =
                                            Update(Car(Car(s462)), Some_(
                                          Pair
                                            ( Car(s493)
                                            , Pair
                                                ( Pair
                                                    ( Sub
                                                        ( 3
                                                        , Car(Car(Cdr(s533)))
                                                        )
                                                    , Cdr(Car(Cdr(s493)))
                                                    )
                                                , Cdr(Cdr(s493))
                                                )
                                            )), Cdr(Car(s463)))
                                          match Get(Car(Car(s462)), x558) with
                                          | Some s573 ->
                                              match Get
                                                      ( Cdr(Car(s462))
                                                      , Car(Car(Car(s573)))
                                                      ) with
                                              | Some s608 ->
                                                  let x662 =
                                                    Update(Car(Car(s462)), Some_(
                                                  Pair
                                                    ( Pair
                                                        ( Pair
                                                            ( Update(Cdr(Car(s462)), Some_(Update(Car(Cdr(s462)), Some_(Cdr(Cdr(s462))), s608)), Car(Car(Car(s573))))
                                                            , Cdr(Car(Car(s573)))
                                                            )
                                                        , Cdr(Car(s573))
                                                        )
                                                    , Cdr(s573)
                                                    )), x558)
                                                  match Get
                                                          ( Car(Car(s462))
                                                          , x662
                                                          ) with
                                                  | Some s677 ->
                                                      let x718 =
                                                        Pair
                                                          ( Pair
                                                              ( Car(Car(s463))
                                                              , Update(Car(Car(s462)), Some_(
                                                            Pair
                                                              ( Pair
                                                                  ( Car(Car(s677))
                                                                  , Pair
                                                                    ( Car(Cdr(Car(s677)))
                                                                    , 
                                                                    Add
                                                                    ( 1
                                                                    , Cdr(Cdr(Car(s677)))
                                                                    )
                                                                    )
                                                                  )
                                                              , Cdr(s677)
                                                              )), x662)
                                                              )
                                                          , Cdr(s463)
                                                          )
                                                      match Get
                                                              ( Car(Car(s462))
                                                              , Cdr(Car(x718))
                                                              ) with
                                                      | Some s728 ->
                                                          match Get
                                                                  ( Cdr(Car(s462))
                                                                  , Car(Car(Car(s728)))
                                                                  ) with
                                                          | Some s744 ->
                                                              match Get
                                                                    ( 0
                                                                    , s744
                                                                    ) with
                                                              | Some s752 ->
                                                                  let 
                                                                  [s855;
                                                                    s856;
                                                                    s857] =
                                                                    if Neq(
                                                                    Compare
                                                                    ( s752
                                                                    , 0
                                                                    ))
                                                                    then
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s462))
                                                                    , Cdr(Car(x718))
                                                                    ) with
                                                                    | Some s780 ->
                                                                    match 
                                                                    Get
                                                                    ( Cdr(Car(s462))
                                                                    , Car(Car(Car(s780)))
                                                                    ) with
                                                                    | Some s798 ->
                                                                    match 
                                                                    Get
                                                                    ( 1
                                                                    , s798
                                                                    ) with
                                                                    | Some s805 ->
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s462))
                                                                    , Cdr(Car(x718))
                                                                    ) with
                                                                    | Some s823 ->
                                                                    match 
                                                                    Get
                                                                    ( Cdr(Car(s462))
                                                                    , Car(Car(Car(s823)))
                                                                    ) with
                                                                    | Some s839 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , s839
                                                                    ) with
                                                                    | Some s847 ->
                                                                    [ Eq(
                                                                    Compare
                                                                    ( s847
                                                                    , s805
                                                                    ))
                                                                    ; s462
                                                                    ; x718
                                                                    ]
                                                                    | None _ ->
                                                                    Failwith(72)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(64)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(51)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(72)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(64)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(51)
                                                                    end
                                                                    else
                                                                    [ False
                                                                    ; s462
                                                                    ; x718
                                                                    ]
                                                                  let 
                                                                  [s953;
                                                                    s954;
                                                                    s955] =
                                                                    if s855
                                                                    then
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s856))
                                                                    , Cdr(Car(s857))
                                                                    ) with
                                                                    | Some s878 ->
                                                                    match 
                                                                    Get
                                                                    ( Cdr(Car(s856))
                                                                    , Car(Car(Car(s878)))
                                                                    ) with
                                                                    | Some s896 ->
                                                                    match 
                                                                    Get
                                                                    ( 2
                                                                    , s896
                                                                    ) with
                                                                    | Some s903 ->
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s856))
                                                                    , Cdr(Car(s857))
                                                                    ) with
                                                                    | Some s921 ->
                                                                    match 
                                                                    Get
                                                                    ( Cdr(Car(s856))
                                                                    , Car(Car(Car(s921)))
                                                                    ) with
                                                                    | Some s937 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , s937
                                                                    ) with
                                                                    | Some s945 ->
                                                                    [ Eq(
                                                                    Compare
                                                                    ( s945
                                                                    , s903
                                                                    ))
                                                                    ; s856
                                                                    ; s857
                                                                    ]
                                                                    | None _ ->
                                                                    Failwith(72)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(64)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(51)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(72)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(64)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(51)
                                                                    end
                                                                    else
                                                                    [ False
                                                                    ; s856
                                                                    ; s857
                                                                    ]
                                                                  let 
                                                                  [s1068;
                                                                    s1069] =
                                                                    if s953
                                                                    then
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s954))
                                                                    , Cdr(Car(s955))
                                                                    ) with
                                                                    | Some s985 ->
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s954))
                                                                    , Cdr(Car(s955))
                                                                    ) with
                                                                    | Some s1014 ->
                                                                    match 
                                                                    Get
                                                                    ( Cdr(Car(s954))
                                                                    , Car(Car(Car(s1014)))
                                                                    ) with
                                                                    | Some s1037 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , s1037
                                                                    ) with
                                                                    | Some s1048 ->
                                                                    [ s954
                                                                    ; 
                                                                    Pair
                                                                    ( 
                                                                    Pair
                                                                    ( Car(Car(s955))
                                                                    , Update(Car(Car(s954)), Some_(
                                                                    let 
                                                                    [x2315;
                                                                    x2316] =
                                                                    Unpair(2, s985)
                                                                    Pair
                                                                    ( x2315
                                                                    , 
                                                                    let 
                                                                    [x2323;
                                                                    x2324] =
                                                                    Unpair(2, x2316)
                                                                    Pair
                                                                    ( x2323
                                                                    , 
                                                                    let 
                                                                    [x2331;
                                                                    x2332] =
                                                                    Unpair(2, x2324)
                                                                    Pair
                                                                    ( x2331
                                                                    , 
                                                                    let _ =
                                                                    x2332
                                                                    s1048
                                                                    )
                                                                    )
                                                                    )), Cdr(Car(s955)))
                                                                    )
                                                                    , Cdr(s955)
                                                                    )
                                                                    ]
                                                                    | None _ ->
                                                                    Failwith(73)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(64)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(51)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(73)
                                                                    end
                                                                    else
                                                                    [ s954
                                                                    ; s955
                                                                    ]
                                                                  match 
                                                                  Get
                                                                    ( Car(Car(s1068))
                                                                    , Cdr(Car(s1069))
                                                                    ) with
                                                                  | Some s1084 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , Car(Car(Car(s1084)))
                                                                    ) with
                                                                    | Some s1095 ->
                                                                    match 
                                                                    Get
                                                                    ( Car(Cdr(s1068))
                                                                    , s1095
                                                                    ) with
                                                                    | Some s1107 ->
                                                                    let 
                                                                    [s1208;
                                                                    s1209;
                                                                    s1210] =
                                                                    if Neq(
                                                                    Compare
                                                                    ( s1107
                                                                    , 0
                                                                    ))
                                                                    then
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1068))
                                                                    , Cdr(Car(s1069))
                                                                    ) with
                                                                    | Some s1135 ->
                                                                    match 
                                                                    Get
                                                                    ( 1
                                                                    , Car(Car(Car(s1135)))
                                                                    ) with
                                                                    | Some s1145 ->
                                                                    match 
                                                                    Get
                                                                    ( Car(Cdr(s1068))
                                                                    , s1145
                                                                    ) with
                                                                    | Some s1159 ->
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1068))
                                                                    , Cdr(Car(s1069))
                                                                    ) with
                                                                    | Some s1177 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , Car(Car(Car(s1177)))
                                                                    ) with
                                                                    | Some s1188 ->
                                                                    match 
                                                                    Get
                                                                    ( Car(Cdr(s1068))
                                                                    , s1188
                                                                    ) with
                                                                    | Some s1200 ->
                                                                    [ Eq(
                                                                    Compare
                                                                    ( s1200
                                                                    , s1159
                                                                    ))
                                                                    ; s1068
                                                                    ; s1069
                                                                    ]
                                                                    | None _ ->
                                                                    Failwith(65)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(65)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(51)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(65)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(65)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(51)
                                                                    end
                                                                    else
                                                                    [ False
                                                                    ; s1068
                                                                    ; s1069
                                                                    ]
                                                                    let 
                                                                    [s1304;
                                                                    s1305;
                                                                    s1306] =
                                                                    if s1208
                                                                    then
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1209))
                                                                    , Cdr(Car(s1210))
                                                                    ) with
                                                                    | Some s1231 ->
                                                                    match 
                                                                    Get
                                                                    ( 2
                                                                    , Car(Car(Car(s1231)))
                                                                    ) with
                                                                    | Some s1241 ->
                                                                    match 
                                                                    Get
                                                                    ( Car(Cdr(s1209))
                                                                    , s1241
                                                                    ) with
                                                                    | Some s1255 ->
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1209))
                                                                    , Cdr(Car(s1210))
                                                                    ) with
                                                                    | Some s1273 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , Car(Car(Car(s1273)))
                                                                    ) with
                                                                    | Some s1284 ->
                                                                    match 
                                                                    Get
                                                                    ( Car(Cdr(s1209))
                                                                    , s1284
                                                                    ) with
                                                                    | Some s1296 ->
                                                                    [ Eq(
                                                                    Compare
                                                                    ( s1296
                                                                    , s1255
                                                                    ))
                                                                    ; s1209
                                                                    ; s1210
                                                                    ]
                                                                    | None _ ->
                                                                    Failwith(65)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(65)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(51)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(65)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(65)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(51)
                                                                    end
                                                                    else
                                                                    [ False
                                                                    ; s1209
                                                                    ; s1210
                                                                    ]
                                                                    let 
                                                                    [s1418;
                                                                    s1419] =
                                                                    if s1304
                                                                    then
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1305))
                                                                    , Cdr(Car(s1306))
                                                                    ) with
                                                                    | Some s1336 ->
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1305))
                                                                    , Cdr(Car(s1306))
                                                                    ) with
                                                                    | Some s1365 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , Car(Car(Car(s1365)))
                                                                    ) with
                                                                    | Some s1379 ->
                                                                    match 
                                                                    Get
                                                                    ( Car(Cdr(s1305))
                                                                    , s1379
                                                                    ) with
                                                                    | Some s1398 ->
                                                                    [ s1305
                                                                    ; 
                                                                    Pair
                                                                    ( 
                                                                    Pair
                                                                    ( Car(Car(s1306))
                                                                    , Update(Car(Car(s1305)), Some_(
                                                                    let 
                                                                    [x2313;
                                                                    x2314] =
                                                                    Unpair(2, s1336)
                                                                    Pair
                                                                    ( x2313
                                                                    , 
                                                                    let 
                                                                    [x2321;
                                                                    x2322] =
                                                                    Unpair(2, x2314)
                                                                    Pair
                                                                    ( x2321
                                                                    , 
                                                                    let 
                                                                    [x2329;
                                                                    x2330] =
                                                                    Unpair(2, x2322)
                                                                    Pair
                                                                    ( x2329
                                                                    , 
                                                                    let _ =
                                                                    x2330
                                                                    s1398
                                                                    )
                                                                    )
                                                                    )), Cdr(Car(s1306)))
                                                                    )
                                                                    , Cdr(s1306)
                                                                    )
                                                                    ]
                                                                    | None _ ->
                                                                    Failwith(65)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(65)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(51)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(73)
                                                                    end
                                                                    else
                                                                    [ s1305
                                                                    ; s1306
                                                                    ]
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1418))
                                                                    , Cdr(Car(s1419))
                                                                    ) with
                                                                    | Some s1434 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , Car(Car(Car(s1434)))
                                                                    ) with
                                                                    | Some s1445 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , s1445
                                                                    ) with
                                                                    | Some s1453 ->
                                                                    let 
                                                                    [s1543;
                                                                    s1544;
                                                                    s1545] =
                                                                    if Neq(
                                                                    Compare
                                                                    ( s1453
                                                                    , 0
                                                                    ))
                                                                    then
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1418))
                                                                    , Cdr(Car(s1419))
                                                                    ) with
                                                                    | Some s1481 ->
                                                                    match 
                                                                    Get
                                                                    ( 1
                                                                    , Car(Car(Car(s1481)))
                                                                    ) with
                                                                    | Some s1491 ->
                                                                    match 
                                                                    Get
                                                                    ( 1
                                                                    , s1491
                                                                    ) with
                                                                    | Some s1498 ->
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1418))
                                                                    , Cdr(Car(s1419))
                                                                    ) with
                                                                    | Some s1516 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , Car(Car(Car(s1516)))
                                                                    ) with
                                                                    | Some s1527 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , s1527
                                                                    ) with
                                                                    | Some s1535 ->
                                                                    [ Eq(
                                                                    Compare
                                                                    ( s1535
                                                                    , s1498
                                                                    ))
                                                                    ; s1418
                                                                    ; s1419
                                                                    ]
                                                                    | None _ ->
                                                                    Failwith(66)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(66)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(51)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(66)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(66)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(51)
                                                                    end
                                                                    else
                                                                    [ False
                                                                    ; s1418
                                                                    ; s1419
                                                                    ]
                                                                    let 
                                                                    [s1628;
                                                                    s1629;
                                                                    s1630] =
                                                                    if s1543
                                                                    then
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1544))
                                                                    , Cdr(Car(s1545))
                                                                    ) with
                                                                    | Some s1566 ->
                                                                    match 
                                                                    Get
                                                                    ( 2
                                                                    , Car(Car(Car(s1566)))
                                                                    ) with
                                                                    | Some s1576 ->
                                                                    match 
                                                                    Get
                                                                    ( 2
                                                                    , s1576
                                                                    ) with
                                                                    | Some s1583 ->
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1544))
                                                                    , Cdr(Car(s1545))
                                                                    ) with
                                                                    | Some s1601 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , Car(Car(Car(s1601)))
                                                                    ) with
                                                                    | Some s1612 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , s1612
                                                                    ) with
                                                                    | Some s1620 ->
                                                                    [ Eq(
                                                                    Compare
                                                                    ( s1620
                                                                    , s1583
                                                                    ))
                                                                    ; s1544
                                                                    ; s1545
                                                                    ]
                                                                    | None _ ->
                                                                    Failwith(66)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(66)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(51)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(66)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(66)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(51)
                                                                    end
                                                                    else
                                                                    [ False
                                                                    ; s1544
                                                                    ; s1545
                                                                    ]
                                                                    let 
                                                                    [s1734;
                                                                    s1735] =
                                                                    if s1628
                                                                    then
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1629))
                                                                    , Cdr(Car(s1630))
                                                                    ) with
                                                                    | Some s1660 ->
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1629))
                                                                    , Cdr(Car(s1630))
                                                                    ) with
                                                                    | Some s1689 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , Car(Car(Car(s1689)))
                                                                    ) with
                                                                    | Some s1703 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , s1703
                                                                    ) with
                                                                    | Some s1714 ->
                                                                    [ s1629
                                                                    ; 
                                                                    Pair
                                                                    ( 
                                                                    Pair
                                                                    ( Car(Car(s1630))
                                                                    , Update(Car(Car(s1629)), Some_(
                                                                    let 
                                                                    [x2311;
                                                                    x2312] =
                                                                    Unpair(2, s1660)
                                                                    Pair
                                                                    ( x2311
                                                                    , 
                                                                    let 
                                                                    [x2319;
                                                                    x2320] =
                                                                    Unpair(2, x2312)
                                                                    Pair
                                                                    ( x2319
                                                                    , 
                                                                    let 
                                                                    [x2327;
                                                                    x2328] =
                                                                    Unpair(2, x2320)
                                                                    Pair
                                                                    ( x2327
                                                                    , 
                                                                    let _ =
                                                                    x2328
                                                                    s1714
                                                                    )
                                                                    )
                                                                    )), Cdr(Car(s1630)))
                                                                    )
                                                                    , Cdr(s1630)
                                                                    )
                                                                    ]
                                                                    | None _ ->
                                                                    Failwith(66)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(66)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(51)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(73)
                                                                    end
                                                                    else
                                                                    [ s1629
                                                                    ; s1630
                                                                    ]
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1734))
                                                                    , Cdr(Car(s1735))
                                                                    ) with
                                                                    | Some s1750 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , Car(Car(Car(s1750)))
                                                                    ) with
                                                                    | Some s1761 ->
                                                                    match 
                                                                    Get
                                                                    ( 2
                                                                    , s1761
                                                                    ) with
                                                                    | Some s1769 ->
                                                                    let 
                                                                    [s1859;
                                                                    s1860;
                                                                    s1861] =
                                                                    if Neq(
                                                                    Compare
                                                                    ( s1769
                                                                    , 0
                                                                    ))
                                                                    then
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1734))
                                                                    , Cdr(Car(s1735))
                                                                    ) with
                                                                    | Some s1797 ->
                                                                    match 
                                                                    Get
                                                                    ( 1
                                                                    , Car(Car(Car(s1797)))
                                                                    ) with
                                                                    | Some s1807 ->
                                                                    match 
                                                                    Get
                                                                    ( 1
                                                                    , s1807
                                                                    ) with
                                                                    | Some s1814 ->
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1734))
                                                                    , Cdr(Car(s1735))
                                                                    ) with
                                                                    | Some s1832 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , Car(Car(Car(s1832)))
                                                                    ) with
                                                                    | Some s1843 ->
                                                                    match 
                                                                    Get
                                                                    ( 2
                                                                    , s1843
                                                                    ) with
                                                                    | Some s1851 ->
                                                                    [ Eq(
                                                                    Compare
                                                                    ( s1851
                                                                    , s1814
                                                                    ))
                                                                    ; s1734
                                                                    ; s1735
                                                                    ]
                                                                    | None _ ->
                                                                    Failwith(67)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(67)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(51)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(67)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(67)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(51)
                                                                    end
                                                                    else
                                                                    [ False
                                                                    ; s1734
                                                                    ; s1735
                                                                    ]
                                                                    let 
                                                                    [s1944;
                                                                    s1945;
                                                                    s1946] =
                                                                    if s1859
                                                                    then
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1860))
                                                                    , Cdr(Car(s1861))
                                                                    ) with
                                                                    | Some s1882 ->
                                                                    match 
                                                                    Get
                                                                    ( 2
                                                                    , Car(Car(Car(s1882)))
                                                                    ) with
                                                                    | Some s1892 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , s1892
                                                                    ) with
                                                                    | Some s1899 ->
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1860))
                                                                    , Cdr(Car(s1861))
                                                                    ) with
                                                                    | Some s1917 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , Car(Car(Car(s1917)))
                                                                    ) with
                                                                    | Some s1928 ->
                                                                    match 
                                                                    Get
                                                                    ( 2
                                                                    , s1928
                                                                    ) with
                                                                    | Some s1936 ->
                                                                    [ Eq(
                                                                    Compare
                                                                    ( s1936
                                                                    , s1899
                                                                    ))
                                                                    ; s1860
                                                                    ; s1861
                                                                    ]
                                                                    | None _ ->
                                                                    Failwith(67)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(67)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(51)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(67)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(67)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(51)
                                                                    end
                                                                    else
                                                                    [ False
                                                                    ; s1860
                                                                    ; s1861
                                                                    ]
                                                                    let 
                                                                    [s2050;
                                                                    s2051] =
                                                                    if s1944
                                                                    then
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1945))
                                                                    , Cdr(Car(s1946))
                                                                    ) with
                                                                    | Some s1976 ->
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1945))
                                                                    , Cdr(Car(s1946))
                                                                    ) with
                                                                    | Some s2005 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , Car(Car(Car(s2005)))
                                                                    ) with
                                                                    | Some s2019 ->
                                                                    match 
                                                                    Get
                                                                    ( 2
                                                                    , s2019
                                                                    ) with
                                                                    | Some s2030 ->
                                                                    [ s1945
                                                                    ; 
                                                                    Pair
                                                                    ( 
                                                                    Pair
                                                                    ( Car(Car(s1946))
                                                                    , Update(Car(Car(s1945)), Some_(
                                                                    let 
                                                                    [x2309;
                                                                    x2310] =
                                                                    Unpair(2, s1976)
                                                                    Pair
                                                                    ( x2309
                                                                    , 
                                                                    let 
                                                                    [x2317;
                                                                    x2318] =
                                                                    Unpair(2, x2310)
                                                                    Pair
                                                                    ( x2317
                                                                    , 
                                                                    let 
                                                                    [x2325;
                                                                    x2326] =
                                                                    Unpair(2, x2318)
                                                                    Pair
                                                                    ( x2325
                                                                    , 
                                                                    let _ =
                                                                    x2326
                                                                    s2030
                                                                    )
                                                                    )
                                                                    )), Cdr(Car(s1946)))
                                                                    )
                                                                    , Cdr(s1946)
                                                                    )
                                                                    ]
                                                                    | None _ ->
                                                                    Failwith(67)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(67)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(51)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(73)
                                                                    end
                                                                    else
                                                                    [ s1945
                                                                    ; s1946
                                                                    ]
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s2050))
                                                                    , Cdr(Car(s2051))
                                                                    ) with
                                                                    | Some s2066 ->
                                                                    let 
                                                                    [s2100;
                                                                    s2101;
                                                                    s2102] =
                                                                    if Eq(
                                                                    Compare
                                                                    ( Cdr(Cdr(Car(s2066)))
                                                                    , 9
                                                                    ))
                                                                    then
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s2050))
                                                                    , Cdr(Car(s2051))
                                                                    ) with
                                                                    | Some s2091 ->
                                                                    [ Eq(
                                                                    Compare
                                                                    ( Cdr(Cdr(Cdr(s2091)))
                                                                    , 0
                                                                    ))
                                                                    ; s2050
                                                                    ; s2051
                                                                    ]
                                                                    | None _ ->
                                                                    Failwith(51)
                                                                    end
                                                                    else
                                                                    [ False
                                                                    ; s2050
                                                                    ; s2051
                                                                    ]
                                                                    if s2100
                                                                    then
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s2101))
                                                                    , Cdr(Car(s2102))
                                                                    ) with
                                                                    | Some s2127 ->
                                                                    Pair
                                                                    ( 
                                                                    Pair
                                                                    ( Car(Car(s2102))
                                                                    , Update(Car(Car(s2101)), Some_(
                                                                    Pair
                                                                    ( 
                                                                    Pair
                                                                    ( 
                                                                    Pair
                                                                    ( Car(Car(Car(s2127)))
                                                                    , True
                                                                    )
                                                                    , Cdr(Car(s2127))
                                                                    )
                                                                    , Cdr(s2127)
                                                                    )), Cdr(Car(s2102)))
                                                                    )
                                                                    , Cdr(s2102)
                                                                    )
                                                                    | None _ ->
                                                                    Failwith(69)
                                                                    end
                                                                    else
                                                                    s2102
                                                                    | None _ ->
                                                                    Failwith(51)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(67)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(67)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(51)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(66)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(66)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(51)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(65)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(65)
                                                                    end
                                                                  | None _ ->
                                                                    Failwith(51)
                                                                  end
                                                              | None _ ->
                                                                  Failwith(72)
                                                              end
                                                          | None _ ->
                                                              Failwith(64)
                                                          end
                                                      | None _ ->
                                                          Failwith(51)
                                                      end
                                                  | None _ -> Failwith(63)
                                                  end
                                              | None _ -> Failwith(62)
                                              end
                                          | None _ -> Failwith(62)
                                          end
                                      | None _ -> Failwith(51)
                                      end
                                  | None _ -> Failwith(61)
                                  end
                                else
                                  Failwith("WrongCondition: self.data.boards[params.game].deck[params.i][params.j] == 0")
                            | None _ -> Failwith(56)
                            end
                        | None _ -> Failwith(56)
                        end
                    | None _ -> Failwith(51)
                    end
                  else
                    Failwith("WrongCondition: params.move == self.data.boards[params.game].nextPlayer")
              | None _ -> Failwith(51)
              end
            else
              Failwith("WrongCondition: (params.j >= 0) & (params.j < 3)")
          else
            Failwith("WrongCondition: (params.i >= 0) & (params.i < 3)")
        else
          Failwith("WrongCondition: (self.data.boards[params.game].winner == 0) & (~ self.data.boards[params.game].draw)")]')
            sp.failwith(sp.build_lambda(f-1)(sp.unit))
          with arg.match('None') as _r24:
            sp.failwith(51)

    x2303 = sp.local("x2303", x34.value)
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: record_of_tree(..., x2303)]')
    sp.failwith(sp.build_lambda(f-1)(sp.unit))

  @sp.entry_point
  def setGameMetaData(self, params):
    sp.set_type(params, sp.TPair(sp.TString, sp.TPair(sp.TString, sp.TString)))
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: let [s101; s102; s103] =
          if Eq(Compare(Sender, __storage.admin))
          then
            [ True
            ; l6
            ; Pair
                ( Pair(__storage.admin, __storage.boards)
                , Pair(__storage.metaData, __storage.paused)
                )
            ]
          else
            match Get(Car(l6), __storage.boards) with
            | Some s90 ->
                [ Eq(Compare(Sender, Cdr(Car(Cdr(s90)))))
                ; l6
                ; Pair
                    ( Pair(__storage.admin, __storage.boards)
                    , Pair(__storage.metaData, __storage.paused)
                    )
                ]
            | None _ -> Failwith(42)
            end
        let x2301 =
          if s101
          then
            match Get(Car(s102), Cdr(Car(s103))) with
            | Some s130 ->
                Pair
                  ( Pair
                      ( Car(Car(s103))
                      , Update(Car(s102), Some_(Pair
                                                  ( Pair
                                                      ( Car(Car(s130))
                                                      , Pair
                                                          ( Update(Car(Cdr(s102)), Some_(Cdr(Cdr(s102))), Car(Cdr(Car(s130))))
                                                          , Cdr(Cdr(Car(s130)))
                                                          )
                                                      )
                                                  , Cdr(s130)
                                                  )), Cdr(Car(s103)))
                      )
                  , Cdr(s103)
                  )
            | None _ -> Failwith(46)
            end
          else
            Failwith("WrongCondition: (sp.sender == self.data.admin) | (sp.sender == self.data.boards[params.game].player1)")
        Pair(Nil<operation>, record_of_tree(..., x2301))]')
    sp.failwith(sp.build_lambda(f-1)(sp.unit))

  @sp.entry_point
  def setMetaData(self, params):
    sp.set_type(params, sp.TPair(sp.TString, sp.TString))
    sp.verify(sp.sender == self.data.admin, 'WrongCondition: sp.sender == self.data.admin')
    x59 = sp.local("x59", ((self.data.admin, self.data.boards), (sp.update_map(self.data.metaData, sp.fst(params), sp.some(sp.snd(params))), self.data.paused)))
    x2299 = sp.local("x2299", x59.value)
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: record_of_tree(..., x2299)]')
    sp.failwith(sp.build_lambda(f-1)(sp.unit))

  @sp.entry_point
  def setPause(self, params):
    sp.set_type(params, sp.TBool)
    sp.verify(sp.sender == self.data.admin, 'WrongCondition: sp.sender == self.data.admin')
    x76 = sp.local("x76", ((self.data.admin, self.data.boards), (self.data.metaData, params)))
    x2297 = sp.local("x2297", x76.value)
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: record_of_tree(..., x2297)]')
    sp.failwith(sp.build_lambda(f-1)(sp.unit))

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()

import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(multisigs = sp.TMap(sp.TInt, sp.TPair(sp.TPair(sp.TPair(sp.TMutez, sp.TList(sp.TPair(sp.TPair(sp.TInt, sp.TPair(sp.TBool, sp.TList(sp.TPair(sp.TBool, sp.TPair(sp.TAddress, sp.TInt))))), sp.TPair(sp.TPair(sp.TInt, sp.TInt), sp.TPair(sp.TInt, sp.TInt))))), sp.TPair(sp.TInt, sp.TString)), sp.TPair(sp.TPair(sp.TBool, sp.TAddress), sp.TPair(sp.TInt, sp.TPair(sp.TInt, sp.TInt))))), nbMultisigs = sp.TInt).layout(("multisigs", "nbMultisigs")))

  @sp.entry_point
  def ep0(self, params):
    sp.set_type(params, sp.TVariant(Left = sp.TPair(sp.TPair(sp.TPair(sp.TMutez, sp.TList(sp.TPair(sp.TPair(sp.TInt, sp.TPair(sp.TBool, sp.TList(sp.TPair(sp.TBool, sp.TPair(sp.TAddress, sp.TInt))))), sp.TPair(sp.TPair(sp.TInt, sp.TInt), sp.TPair(sp.TInt, sp.TInt))))), sp.TPair(sp.TInt, sp.TString)), sp.TPair(sp.TPair(sp.TBool, sp.TAddress), sp.TPair(sp.TInt, sp.TPair(sp.TInt, sp.TInt)))), Right = sp.TPair(sp.TInt, sp.TPair(sp.TString, sp.TAddress))).layout(("Left", "Right")))
    with params.match_cases() as arg:
      with arg.match('Left') as l4:
        x47 = sp.local("x47", (sp.list([]), (sp.update_map(self.data.multisigs, self.data.nbMultisigs, sp.some(l4)), 1 + self.data.nbMultisigs)))
        s712 = sp.local("s712", sp.fst(x47.value))
        s713 = sp.local("s713", sp.snd(x47.value))
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: record_of_tree(..., s713)]')
        sp.failwith(sp.build_lambda(f-1)(sp.unit))
      with arg.match('Right') as r5:
        sp.verify(sp.sender == sp.snd(sp.snd(r5)), 'WrongCondition: params.id == sp.sender')
        with sp.some(self.data.multisigs[sp.fst(r5)]).match_cases() as arg:
          with arg.match('Some') as s32:
            sp.verify(sp.fst(sp.snd(r5)) == sp.snd(sp.snd(sp.fst(s32))), 'WrongCondition: params.contractName == self.data.multisigs[params.contractId].name')
            with sp.some(self.data.multisigs[sp.fst(r5)]).match_cases() as arg:
              with arg.match('Some') as s64:
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: let [r626; r627; r628; r629] =
          map [ Cdr(Car(Car(s64)))
              ; Nil<operation>
              ; r5
              ; Pair(__storage.multisigs, __storage.nbMultisigs)
              ]
          step s73; s74; s75; s76 ->
            let [r601; r602; r603; r604; r605] =
              map [ Cdr(Cdr(Car(s73))); s73; s74; s75; s76 ]
              step s81; s82; s83; s84; s85 ->
                let [s596; s597; s598; s599; s600] =
                  if Eq(Compare(Car(Cdr(s81)), Cdr(Cdr(s84))))
                  then
                    if Car(s81)
                    then
                      Failwith("WrongCondition: ~ participant.hasVoted")
                    else
                      let x121 =
                        let [x915; x916] = Unpair(2, s81)
                        let _ = x915
                        Pair(True, x916)
                      let x126 =
                        let [x913; x914] = Unpair(2, s82)
                        Pair
                          ( x913
                          , let [x921; x922] = Unpair(2, x914)
                            Pair
                              ( x921
                              , let [x927; x928] = Unpair(2, x922)
                                Pair
                                  ( x927
                                  , let _ = x928
                                    Add(Cdr(Cdr(x121)), Cdr(Cdr(Cdr(s82))))
                                  )
                              )
                          )
                      let x132 =
                        let [x911; x912] = Unpair(2, x126)
                        Pair
                          ( x911
                          , let [x919; x920] = Unpair(2, x912)
                            Pair
                              ( x919
                              , let [x925; x926] = Unpair(2, x920)
                                let _ = x925
                                Pair(Add(1, Car(Cdr(Cdr(x126)))), x926)
                              )
                          )
                      let [s156; s157; s158; s159; s160; s161] =
                        if Car(Cdr(Car(x132)))
                        then
                          [ False; x121; x132; s83; s84; s85 ]
                        else
                          [ Le(Compare
                                 ( Car(Car(Cdr(x132)))
                                 , Car(Cdr(Cdr(x132)))
                                 ))
                          ; x121
                          ; x132
                          ; s83
                          ; s84
                          ; s85
                          ]
                      let [s180; s181; s182; s183; s184; s185] =
                        if s156
                        then
                          [ Le(Compare
                                 ( Cdr(Car(Cdr(s158)))
                                 , Cdr(Cdr(Cdr(s158)))
                                 ))
                          ; s157
                          ; s158
                          ; s159
                          ; s160
                          ; s161
                          ]
                        else
                          [ False; s157; s158; s159; s160; s161 ]
                      let [s591; s592; s593; s594; s595] =
                        if s180
                        then
                          match Get(Car(s184), Car(s185)) with
                          | Some s228 ->
                              let x259 =
                                let [x909; x910] = Unpair(2, s185)
                                let _ = x909
                                Pair
                                  ( Update(Car(s184), Some_(let [x907; x908] =
                                                              Unpair(2, s228)
                                                            Pair
                                                              ( x907
                                                              , let [x917;
                                                                    x918] =
                                                                  Unpair(2, x908)
                                                                Pair
                                                                  ( x917
                                                                  , let 
                                                                    [x923;
                                                                    x924] =
                                                                    Unpair(2, x918)
                                                                    Pair
                                                                    ( x923
                                                                    , 
                                                                    let 
                                                                    [x929;
                                                                    x930] =
                                                                    Unpair(2, x924)
                                                                    Pair
                                                                    ( x929
                                                                    , 
                                                                    let _ =
                                                                    x930
                                                                    Add
                                                                    ( Car(Car(s182))
                                                                    , Cdr(Cdr(Cdr(Cdr(s228))))
                                                                    )
                                                                    )
                                                                    )
                                                                  )
                                                              )), Car(s185))
                                  , x910
                                  )
                              match Get(Car(s184), Car(x259)) with
                              | Some s278 ->
                                  let x309 =
                                    let [x905; x906] = Unpair(2, x259)
                                    let _ = x905
                                    Pair
                                      ( Update(Car(s184), Some_(Pair
                                                                  ( Pair
                                                                    ( Car(Car(s278))
                                                                    , 
                                                                    Pair
                                                                    ( 
                                                                    Add
                                                                    ( 1
                                                                    , Car(Cdr(Car(s278)))
                                                                    )
                                                                    , Cdr(Cdr(Car(s278)))
                                                                    )
                                                                    )
                                                                  , Cdr(s278)
                                                                  )), Car(x259))
                                      , x906
                                      )
                                  match Get(Car(s184), Car(x309)) with
                                  | Some s325 ->
                                      let [s390; s391; s392; s393; s394; s395
                                            ] =
                                        if Car(Car(Cdr(s325)))
                                        then
                                          [ False
                                          ; s181
                                          ; Pair
                                              ( Pair
                                                  ( Car(Car(s182))
                                                  , Pair
                                                      ( True
                                                      , Cdr(Cdr(Car(s182)))
                                                      )
                                                  )
                                              , Cdr(s182)
                                              )
                                          ; s183
                                          ; s184
                                          ; x309
                                          ]
                                        else
                                          match Get(Car(s184), Car(x309)) with
                                          | Some s350 ->
                                              match Get(Car(s184), Car(x309)) with
                                              | Some s377 ->
                                                  [ Le(Compare
                                                         ( Car(Cdr(Cdr(s377)))
                                                         , Car(Cdr(Car(s350)))
                                                         ))
                                                  ; s181
                                                  ; Pair
                                                      ( Pair
                                                          ( Car(Car(s182))
                                                          , Pair
                                                              ( True
                                                              , Cdr(Cdr(Car(s182)))
                                                              )
                                                          )
                                                      , Cdr(s182)
                                                      )
                                                  ; s183
                                                  ; s184
                                                  ; x309
                                                  ]
                                              | None _ -> Failwith(24)
                                              end
                                          | None _ -> Failwith(24)
                                          end
                                      let [s450; s451; s452; s453; s454; s455
                                            ] =
                                        if s390
                                        then
                                          match Get(Car(s394), Car(s395)) with
                                          | Some s412 ->
                                              match Get(Car(s394), Car(s395)) with
                                              | Some s438 ->
                                                  [ Le(Compare
                                                         ( Car(Cdr(Cdr(Cdr(s438))))
                                                         , Cdr(Cdr(Cdr(Cdr(s412))))
                                                         ))
                                                  ; s391
                                                  ; s392
                                                  ; s393
                                                  ; s394
                                                  ; s395
                                                  ]
                                              | None _ -> Failwith(24)
                                              end
                                          | None _ -> Failwith(24)
                                          end
                                        else
                                          [ False
                                          ; s391
                                          ; s392
                                          ; s393
                                          ; s394
                                          ; s395
                                          ]
                                      let [s586; s587; s588; s589; s590] =
                                        if s450
                                        then
                                          match Get(Car(s454), Car(s455)) with
                                          | Some s481 ->
                                              let x522 =
                                                let [x903; x904] =
                                                  Unpair(2, s455)
                                                let _ = x903
                                                Pair
                                                  ( Update(Car(s454), Some_(
                                                Pair
                                                  ( Car(s481)
                                                  , Pair
                                                      ( Pair
                                                          ( True
                                                          , Cdr(Car(Cdr(s481)))
                                                          )
                                                      , Cdr(Cdr(s481))
                                                      )
                                                  )), Car(s455))
                                                  , x904
                                                  )
                                              match Get(Car(s454), Car(x522)) with
                                              | Some s532 ->
                                                  match Contract(Cdr(Car(Cdr(s532))), unit) with
                                                  | Some s543 ->
                                                      match Get
                                                              ( Car(s454)
                                                              , Car(x522)
                                                              ) with
                                                      | Some s568 ->
                                                          [ s451
                                                          ; s452
                                                          ; Cons
                                                              ( Transfer_tokens(Unit, Car(Car(Car(s568))), s543)
                                                              , s453
                                                              )
                                                          ; s454
                                                          ; x522
                                                          ]
                                                      | None _ ->
                                                          Failwith(24)
                                                      end
                                                  | None _ -> Failwith(52)
                                                  end
                                              | None _ -> Failwith(24)
                                              end
                                          | None _ -> Failwith(41)
                                          end
                                        else
                                          [ s451; s452; s453; s454; s455 ]
                                      [ s586; s587; s588; s589; s590 ]
                                  | None _ -> Failwith(24)
                                  end
                              | None _ -> Failwith(39)
                              end
                          | None _ -> Failwith(38)
                          end
                        else
                          [ s181; s182; s183; s184; s185 ]
                      [ s591; s592; s593; s594; s595 ]
                  else
                    [ s81; s82; s83; s84; s85 ]
                [ s596; s597; s598; s599; s600 ]
            [ Pair
                ( Pair(Car(Car(r602)), Pair(Car(Cdr(Car(r602))), r601))
                , Cdr(r602)
                )
            ; r603
            ; r604
            ; r605
            ]
        match Get(Car(r628), Car(r629)) with
        | Some s652 ->
            [ r627
            ; let [x901; x902] = Unpair(2, r629)
              let _ = x901
              Pair
                ( Update(Car(r628), Some_(Pair
                                            ( Pair
                                                ( Pair
                                                    ( Car(Car(Car(s652)))
                                                    , r626
                                                    )
                                                , Cdr(Car(s652))
                                                )
                                            , Cdr(s652)
                                            )), Car(r629))
                , x902
                )
            ]
        | None _ -> Failwith(28)
        end]')
                sp.failwith(sp.build_lambda(f-1)(sp.unit))
              with arg.match('None') as _r12:
                sp.failwith(24)

          with arg.match('None') as _r4:
            sp.failwith(24)



@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()

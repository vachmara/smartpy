import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(check = sp.TBool, deck = sp.TMap(sp.TInt, sp.TMap(sp.TInt, sp.TInt)), draw = sp.TBool, kings = sp.TMap(sp.TInt, sp.TPair(sp.TInt, sp.TPair(sp.TInt, sp.TBool))), nextPlayer = sp.TInt, previousPawnMove = sp.TOption(sp.TPair(sp.TPair(sp.TInt, sp.TInt), sp.TPair(sp.TInt, sp.TInt))), winner = sp.TInt).layout((("check", ("deck", "draw")), (("kings", "nextPlayer"), ("previousPawnMove", "winner")))))

  @sp.entry_point
  def ep0(self, params):
    sp.set_type(params, sp.TPair(sp.TPair(sp.TInt, sp.TInt), sp.TPair(sp.TInt, sp.TInt)))
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: let [s23; s24; s25] =
          if Eq(Compare(__storage.winner, 0))
          then
            [ Not(__storage.draw)
            ; __parameter
            ; Pair
                ( Pair(__storage.check, Pair(__storage.deck, __storage.draw))
                , Pair
                    ( Pair(__storage.kings, __storage.nextPlayer)
                    , Pair(__storage.previousPawnMove, __storage.winner)
                    )
                )
            ]
          else
            [ False
            ; __parameter
            ; Pair
                ( Pair(__storage.check, Pair(__storage.deck, __storage.draw))
                , Pair
                    ( Pair(__storage.kings, __storage.nextPlayer)
                    , Pair(__storage.previousPawnMove, __storage.winner)
                    )
                )
            ]
        if s23
        then
          let [s46; s47; s48] =
            if Ge(Compare(Car(Car(s24)), 0))
            then
              [ Gt(Compare(8, Car(Car(s24)))); s24; s25 ]
            else
              [ False; s24; s25 ]
          if s46
          then
            let [s69; s70; s71] =
              if Ge(Compare(Cdr(Car(s47)), 0))
              then
                [ Gt(Compare(8, Cdr(Car(s47)))); s47; s48 ]
              else
                [ False; s47; s48 ]
            if s69
            then
              let [s90; s91; s92] =
                if Ge(Compare(Car(Cdr(s70)), 0))
                then
                  [ Gt(Compare(8, Car(Cdr(s70)))); s70; s71 ]
                else
                  [ False; s70; s71 ]
              if s90
              then
                let [s111; s112; s113] =
                  if Ge(Compare(Cdr(Cdr(s91)), 0))
                  then
                    [ Gt(Compare(8, Cdr(Cdr(s91)))); s91; s92 ]
                  else
                    [ False; s91; s92 ]
                if s111
                then
                  match Get(Car(Car(s112)), Car(Cdr(Car(s113)))) with
                  | Some s142 ->
                      match Get(Cdr(Car(s112)), s142) with
                      | Some s157 ->
                          if Eq(Compare(Compare(s157, 0), Cdr(Car(Cdr(s113)))))
                          then
                            match Get(Car(Cdr(s112)), Car(Cdr(Car(s113)))) with
                            | Some s194 ->
                                match Get(Cdr(Cdr(s112)), s194) with
                                | Some s208 ->
                                    if Neq(Compare
                                             ( Compare(s208, 0)
                                             , Cdr(Car(Cdr(s113)))
                                             ))
                                    then
                                      match Get
                                              ( Car(Car(s112))
                                              , Car(Cdr(Car(s113)))
                                              ) with
                                      | Some s235 ->
                                          match Get(Cdr(Car(s112)), s235) with
                                          | Some s248 ->
                                              let [s456; s457] =
                                                if Eq(Compare
                                                        ( Abs(s248)
                                                        , 1#nat
                                                        ))
                                                then
                                                  let [s324; s325; s326] =
                                                    if Neq(Compare
                                                             ( Sub
                                                                 ( Cdr(Cdr(s112))
                                                                 , Cdr(Car(s112))
                                                                 )
                                                             , 0
                                                             ))
                                                    then
                                                      match Get
                                                              ( Car(Cdr(s112))
                                                              , Car(Cdr(Car(s113)))
                                                              ) with
                                                      | Some s300 ->
                                                          match Get
                                                                  ( Cdr(Cdr(s112))
                                                                  , s300
                                                                  ) with
                                                          | Some s314 ->
                                                              [ Eq(Compare
                                                                    ( 
                                                                   Compare
                                                                    ( s314
                                                                    , 0
                                                                    )
                                                                    , 0
                                                                    ))
                                                              ; s112
                                                              ; s113
                                                              ]
                                                          | None _ ->
                                                              Failwith(55)
                                                          end
                                                      | None _ ->
                                                          Failwith(55)
                                                      end
                                                    else
                                                      [ False; s112; s113 ]
                                                  let [s437; s438] =
                                                    if s324
                                                    then
                                                      if Eq(Compare
                                                              ( Abs(Sub
                                                                    ( Cdr(Cdr(s325))
                                                                    , Cdr(Car(s325))
                                                                    ))
                                                              , 1#nat
                                                              ))
                                                      then
                                                        let [s435; s436] =
                                                          match Car(Cdr(Cdr(s326))) with
                                                          | Some _ ->
                                                              match Car(Cdr(Cdr(s326))) with
                                                              | Some s369 ->
                                                                  match Car(Cdr(Cdr(s326))) with
                                                                  | Some s380 ->
                                                                    if Eq(
                                                                    Compare
                                                                    ( Car(Car(s380))
                                                                    , Car(Cdr(s369))
                                                                    ))
                                                                    then
                                                                    match Car(Cdr(Cdr(s326))) with
                                                                    | Some s409 ->
                                                                    match Car(Cdr(Cdr(s326))) with
                                                                    | Some s422 ->
                                                                    if Eq(
                                                                    Compare
                                                                    ( Cdr(Car(s422))
                                                                    , 
                                                                    Sub
                                                                    ( Cdr(Cdr(s409))
                                                                    , 
                                                                    Mul
                                                                    ( 2
                                                                    , Cdr(Car(Cdr(s326)))
                                                                    )
                                                                    )
                                                                    ))
                                                                    then
                                                                    [ s325
                                                                    ; s326
                                                                    ]
                                                                    else
                                                                    Failwith("WrongCondition: self.data.previousPawnMove.open_some().f.j == (self.data.previousPawnMove.open_some().t.j - (2 * self.data.nextPlayer))")
                                                                    | None _ ->
                                                                    Failwith(58)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(58)
                                                                    end
                                                                    else
                                                                    Failwith("WrongCondition: self.data.previousPawnMove.open_some().f.i == self.data.previousPawnMove.open_some().t.i")
                                                                  | None _ ->
                                                                    Failwith(58)
                                                                  end
                                                              | None _ ->
                                                                  Failwith(58)
                                                              end
                                                          | None _ ->
                                                              [ s325; s326 ]
                                                          end
                                                        [ s435; s436 ]
                                                      else
                                                        Failwith("WrongCondition: (abs(params.t.j - params.f.j)) == 1")
                                                    else
                                                      [ s325; s326 ]
                                                  [ s437
                                                  ; let [x2389; x2390] =
                                                      Unpair(2, s438)
                                                    Pair
                                                      ( x2389
                                                      , let [x2399; x2400] =
                                                          Unpair(2, x2390)
                                                        Pair
                                                          ( x2399
                                                          , let [x2403; x2404
                                                                  ] =
                                                              Unpair(2, x2400)
                                                            let _ = x2403
                                                            Pair
                                                              ( Some_(
                                                            Pair
                                                              ( Car(s437)
                                                              , Cdr(s437)
                                                              ))
                                                              , x2404
                                                              )
                                                          )
                                                      )
                                                  ]
                                                else
                                                  [ s112
                                                  ; let [x2387; x2388] =
                                                      Unpair(2, s113)
                                                    Pair
                                                      ( x2387
                                                      , let [x2397; x2398] =
                                                          Unpair(2, x2388)
                                                        Pair
                                                          ( x2397
                                                          , let [x2401; x2402
                                                                  ] =
                                                              Unpair(2, x2398)
                                                            let _ = x2401
                                                            Pair
                                                              ( None<{ (int ; int) ; (int ; int) }>
                                                              , x2402
                                                              )
                                                          )
                                                      )
                                                  ]
                                              match Get
                                                      ( Car(Car(s456))
                                                      , Car(Cdr(Car(s457)))
                                                      ) with
                                              | Some s472 ->
                                                  match Get
                                                          ( Cdr(Car(s456))
                                                          , s472
                                                          ) with
                                                  | Some s485 ->
                                                      let [s788; s789] =
                                                        if Eq(Compare
                                                                ( Abs(s485)
                                                                , 5#nat
                                                                ))
                                                        then
                                                          let [s533; s534;
                                                                s535] =
                                                            if Eq(Compare
                                                                    ( 
                                                                  Sub
                                                                    ( Cdr(Cdr(s456))
                                                                    , Cdr(Car(s456))
                                                                    )
                                                                    , 0
                                                                    ))
                                                            then
                                                              [ True
                                                              ; s456
                                                              ; s457
                                                              ]
                                                            else
                                                              [ Eq(Compare
                                                                    ( 
                                                                   Sub
                                                                    ( Car(Cdr(s456))
                                                                    , Car(Car(s456))
                                                                    )
                                                                    , 0
                                                                    ))
                                                              ; s456
                                                              ; s457
                                                              ]
                                                          let [s570; s571;
                                                                s572] =
                                                            if s533
                                                            then
                                                              [ True
                                                              ; s534
                                                              ; s535
                                                              ]
                                                            else
                                                              [ Eq(Compare
                                                                    ( Abs(
                                                                   Sub
                                                                    ( Cdr(Cdr(s534))
                                                                    , Cdr(Car(s534))
                                                                    ))
                                                                    , Abs(
                                                                   Sub
                                                                    ( Car(Cdr(s534))
                                                                    , Car(Car(s534))
                                                                    ))
                                                                    ))
                                                              ; s534
                                                              ; s535
                                                              ]
                                                          if s570
                                                          then
                                                            let x595 =
                                                              Abs(Sub
                                                                    ( Cdr(Cdr(s571))
                                                                    , Cdr(Car(s571))
                                                                    ))
                                                            let x618 =
                                                              Abs(Sub
                                                                    ( Car(Cdr(s571))
                                                                    , Car(Car(s571))
                                                                    ))
                                                            let [s634; s635;
                                                                  s636; s637] =
                                                              if Le(Compare
                                                                    ( x618
                                                                    , x595
                                                                    ))
                                                              then
                                                                [ x595
                                                                ; 1
                                                                ; s571
                                                                ; s572
                                                                ]
                                                              else
                                                                [ x618
                                                                ; 1
                                                                ; s571
                                                                ; s572
                                                                ]
                                                            let x646 =
                                                              Sub
                                                                ( Int(s634)
                                                                , s635
                                                                )
                                                            let [_] =
                                                              loop [ Gt(
                                                                   Compare
                                                                    ( x646
                                                                    , 1
                                                                    ))
                                                                   ; 1
                                                                   ]
                                                              step s650 ->
                                                                match 
                                                                Get
                                                                  ( Add
                                                                    ( Car(Car(s636))
                                                                    , 
                                                                    Mul
                                                                    ( s650
                                                                    , 
                                                                    Compare
                                                                    ( 
                                                                    Sub
                                                                    ( Car(Cdr(s636))
                                                                    , Car(Car(s636))
                                                                    )
                                                                    , 0
                                                                    )
                                                                    )
                                                                    )
                                                                  , Car(Cdr(Car(s637)))
                                                                  ) with
                                                                | Some s706 ->
                                                                    match 
                                                                    Get
                                                                    ( 
                                                                    Add
                                                                    ( Cdr(Car(s636))
                                                                    , 
                                                                    Mul
                                                                    ( s650
                                                                    , 
                                                                    Compare
                                                                    ( 
                                                                    Sub
                                                                    ( Cdr(Cdr(s636))
                                                                    , Cdr(Car(s636))
                                                                    )
                                                                    , 0
                                                                    )
                                                                    )
                                                                    )
                                                                    , s706
                                                                    ) with
                                                                    | Some s757 ->
                                                                    if Eq(
                                                                    Compare
                                                                    ( s757
                                                                    , 0
                                                                    ))
                                                                    then
                                                                    let x775 =
                                                                    Add
                                                                    ( 1
                                                                    , s650
                                                                    )
                                                                    [ Gt(
                                                                    Compare
                                                                    ( x646
                                                                    , x775
                                                                    ))
                                                                    ; x775
                                                                    ]
                                                                    else
                                                                    Failwith("WrongCondition: self.data.deck[params.f.i + (k * sp.sign(params.t.i - params.f.i))][params.f.j + (k * sp.sign(params.t.j - params.f.j))] == 0")
                                                                    | None _ ->
                                                                    Failwith(70)
                                                                    end
                                                                | None _ ->
                                                                    Failwith(70)
                                                                end
                                                            [ s636; s637 ]
                                                          else
                                                            Failwith("WrongCondition: (((params.t.j - params.f.j) == 0) | ((params.t.i - params.f.i) == 0)) | ((abs(params.t.j - params.f.j)) == (abs(params.t.i - params.f.i)))")
                                                        else
                                                          [ s456; s457 ]
                                                      match Get
                                                              ( Car(Car(s788))
                                                              , Car(Cdr(Car(s789)))
                                                              ) with
                                                      | Some s804 ->
                                                          match Get
                                                                  ( Cdr(Car(s788))
                                                                  , s804
                                                                  ) with
                                                          | Some s817 ->
                                                              let [s1043;
                                                                    s1044] =
                                                                if Eq(
                                                                Compare
                                                                  ( Abs(s817)
                                                                  , 6#nat
                                                                  ))
                                                                then
                                                                  let 
                                                                  [s867;
                                                                    s868;
                                                                    s869] =
                                                                    if Le(
                                                                    Compare
                                                                    ( Abs(
                                                                    Sub
                                                                    ( Cdr(Cdr(s788))
                                                                    , Cdr(Car(s788))
                                                                    ))
                                                                    , 1#nat
                                                                    ))
                                                                    then
                                                                    [ Le(
                                                                    Compare
                                                                    ( Abs(
                                                                    Sub
                                                                    ( Car(Cdr(s788))
                                                                    , Car(Car(s788))
                                                                    ))
                                                                    , 1#nat
                                                                    ))
                                                                    ; s788
                                                                    ; s789
                                                                    ]
                                                                    else
                                                                    [ False
                                                                    ; s788
                                                                    ; s789
                                                                    ]
                                                                  if s867
                                                                  then
                                                                    let x900 =
                                                                    Cdr(Car(Cdr(s869)))
                                                                    match 
                                                                    Get
                                                                    ( x900
                                                                    , Car(Car(Cdr(s869)))
                                                                    ) with
                                                                    | Some s905 ->
                                                                    match 
                                                                    Get
                                                                    ( Cdr(Car(Cdr(s869)))
                                                                    , Update(x900, Some_(
                                                                    let 
                                                                    [x2385;
                                                                    x2386] =
                                                                    Unpair(2, s905)
                                                                    Pair
                                                                    ( x2385
                                                                    , 
                                                                    let 
                                                                    [x2395;
                                                                    x2396] =
                                                                    Unpair(2, x2386)
                                                                    Pair
                                                                    ( x2395
                                                                    , 
                                                                    let _ =
                                                                    x2396
                                                                    True
                                                                    )
                                                                    )), Car(Car(Cdr(s869))))
                                                                    ) with
                                                                    | Some s955 ->
                                                                    let s959 =
                                                                    Update(x900, Some_(
                                                                    let 
                                                                    [x2383;
                                                                    x2384] =
                                                                    Unpair(2, s905)
                                                                    Pair
                                                                    ( x2383
                                                                    , 
                                                                    let 
                                                                    [x2393;
                                                                    x2394] =
                                                                    Unpair(2, x2384)
                                                                    Pair
                                                                    ( x2393
                                                                    , 
                                                                    let _ =
                                                                    x2394
                                                                    True
                                                                    )
                                                                    )), Car(Car(Cdr(s869))))
                                                                    match 
                                                                    Get
                                                                    ( Cdr(Car(Cdr(s869)))
                                                                    , Update(Cdr(Car(Cdr(s869))), Some_(
                                                                    let 
                                                                    [x2381;
                                                                    x2382] =
                                                                    Unpair(2, s955)
                                                                    let _ =
                                                                    x2381
                                                                    Pair
                                                                    ( Car(Cdr(s868))
                                                                    , x2382
                                                                    )), s959)
                                                                    ) with
                                                                    | Some s1013 ->
                                                                    [ s868
                                                                    ; 
                                                                    Pair
                                                                    ( Car(s869)
                                                                    , 
                                                                    Pair
                                                                    ( 
                                                                    Pair
                                                                    ( Update(Cdr(Car(Cdr(s869))), Some_(
                                                                    let 
                                                                    [x2379;
                                                                    x2380] =
                                                                    Unpair(2, s1013)
                                                                    Pair
                                                                    ( x2379
                                                                    , 
                                                                    let 
                                                                    [x2391;
                                                                    x2392] =
                                                                    Unpair(2, x2380)
                                                                    let _ =
                                                                    x2391
                                                                    Pair
                                                                    ( Cdr(Cdr(s868))
                                                                    , x2392
                                                                    )
                                                                    )), Update(Cdr(Car(Cdr(s869))), Some_(
                                                                    let 
                                                                    [x2377;
                                                                    x2378] =
                                                                    Unpair(2, s955)
                                                                    let _ =
                                                                    x2377
                                                                    Pair
                                                                    ( Car(Cdr(s868))
                                                                    , x2378
                                                                    )), s959))
                                                                    , Cdr(Car(Cdr(s869)))
                                                                    )
                                                                    , Cdr(Cdr(s869))
                                                                    )
                                                                    )
                                                                    ]
                                                                    | None _ ->
                                                                    Failwith(76)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(75)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(74)
                                                                    end
                                                                  else
                                                                    Failwith("WrongCondition: ((abs(params.t.j - params.f.j)) <= 1) & ((abs(params.t.i - params.f.i)) <= 1)")
                                                                else
                                                                  [ s788
                                                                  ; s789
                                                                  ]
                                                              match Get
                                                                    ( Car(Car(s1043))
                                                                    , Car(Cdr(Car(s1044)))
                                                                    ) with
                                                              | Some s1059 ->
                                                                  match 
                                                                  Get
                                                                    ( Cdr(Car(s1043))
                                                                    , s1059
                                                                    ) with
                                                                  | Some s1072 ->
                                                                    let 
                                                                    [s1338;
                                                                    s1339] =
                                                                    if Eq(
                                                                    Compare
                                                                    ( Abs(s1072)
                                                                    , 2#nat
                                                                    ))
                                                                    then
                                                                    let 
                                                                    [s1120;
                                                                    s1121;
                                                                    s1122] =
                                                                    if Eq(
                                                                    Compare
                                                                    ( 
                                                                    Sub
                                                                    ( Cdr(Cdr(s1043))
                                                                    , Cdr(Car(s1043))
                                                                    )
                                                                    , 0
                                                                    ))
                                                                    then
                                                                    [ True
                                                                    ; s1043
                                                                    ; s1044
                                                                    ]
                                                                    else
                                                                    [ Eq(
                                                                    Compare
                                                                    ( 
                                                                    Sub
                                                                    ( Car(Cdr(s1043))
                                                                    , Car(Car(s1043))
                                                                    )
                                                                    , 0
                                                                    ))
                                                                    ; s1043
                                                                    ; s1044
                                                                    ]
                                                                    if s1120
                                                                    then
                                                                    let x1145 =
                                                                    Abs(
                                                                    Sub
                                                                    ( Cdr(Cdr(s1121))
                                                                    , Cdr(Car(s1121))
                                                                    ))
                                                                    let x1168 =
                                                                    Abs(
                                                                    Sub
                                                                    ( Car(Cdr(s1121))
                                                                    , Car(Car(s1121))
                                                                    ))
                                                                    let 
                                                                    [s1184;
                                                                    s1185;
                                                                    s1186;
                                                                    s1187] =
                                                                    if Le(
                                                                    Compare
                                                                    ( x1168
                                                                    , x1145
                                                                    ))
                                                                    then
                                                                    [ x1145
                                                                    ; 1
                                                                    ; s1121
                                                                    ; s1122
                                                                    ]
                                                                    else
                                                                    [ x1168
                                                                    ; 1
                                                                    ; s1121
                                                                    ; s1122
                                                                    ]
                                                                    let x1196 =
                                                                    Sub
                                                                    ( Int(s1184)
                                                                    , s1185
                                                                    )
                                                                    let 
                                                                    [_] =
                                                                    loop 
                                                                    [ Gt(
                                                                    Compare
                                                                    ( x1196
                                                                    , 1
                                                                    ))
                                                                    ; 1
                                                                    ]
                                                                    step s1200 ->
                                                                    match 
                                                                    Get
                                                                    ( 
                                                                    Add
                                                                    ( Car(Car(s1186))
                                                                    , 
                                                                    Mul
                                                                    ( s1200
                                                                    , 
                                                                    Compare
                                                                    ( 
                                                                    Sub
                                                                    ( Car(Cdr(s1186))
                                                                    , Car(Car(s1186))
                                                                    )
                                                                    , 0
                                                                    )
                                                                    )
                                                                    )
                                                                    , Car(Cdr(Car(s1187)))
                                                                    ) with
                                                                    | Some s1256 ->
                                                                    match 
                                                                    Get
                                                                    ( 
                                                                    Add
                                                                    ( Cdr(Car(s1186))
                                                                    , 
                                                                    Mul
                                                                    ( s1200
                                                                    , 
                                                                    Compare
                                                                    ( 
                                                                    Sub
                                                                    ( Cdr(Cdr(s1186))
                                                                    , Cdr(Car(s1186))
                                                                    )
                                                                    , 0
                                                                    )
                                                                    )
                                                                    )
                                                                    , s1256
                                                                    ) with
                                                                    | Some s1307 ->
                                                                    if Eq(
                                                                    Compare
                                                                    ( s1307
                                                                    , 0
                                                                    ))
                                                                    then
                                                                    let x1325 =
                                                                    Add
                                                                    ( 1
                                                                    , s1200
                                                                    )
                                                                    [ Gt(
                                                                    Compare
                                                                    ( x1196
                                                                    , x1325
                                                                    ))
                                                                    ; x1325
                                                                    ]
                                                                    else
                                                                    Failwith("WrongCondition: self.data.deck[params.f.i + (k * sp.sign(params.t.i - params.f.i))][params.f.j + (k * sp.sign(params.t.j - params.f.j))] == 0")
                                                                    | None _ ->
                                                                    Failwith(82)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(82)
                                                                    end
                                                                    [ s1186
                                                                    ; s1187
                                                                    ]
                                                                    else
                                                                    Failwith("WrongCondition: ((params.t.j - params.f.j) == 0) | ((params.t.i - params.f.i) == 0)")
                                                                    else
                                                                    [ s1043
                                                                    ; s1044
                                                                    ]
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1338))
                                                                    , Car(Cdr(Car(s1339)))
                                                                    ) with
                                                                    | Some s1354 ->
                                                                    match 
                                                                    Get
                                                                    ( Cdr(Car(s1338))
                                                                    , s1354
                                                                    ) with
                                                                    | Some s1367 ->
                                                                    let 
                                                                    [s1624;
                                                                    s1625] =
                                                                    if Eq(
                                                                    Compare
                                                                    ( Abs(s1367)
                                                                    , 4#nat
                                                                    ))
                                                                    then
                                                                    if Eq(
                                                                    Compare
                                                                    ( Abs(
                                                                    Sub
                                                                    ( Cdr(Cdr(s1338))
                                                                    , Cdr(Car(s1338))
                                                                    ))
                                                                    , Abs(
                                                                    Sub
                                                                    ( Car(Cdr(s1338))
                                                                    , Car(Car(s1338))
                                                                    ))
                                                                    ))
                                                                    then
                                                                    let x1431 =
                                                                    Abs(
                                                                    Sub
                                                                    ( Cdr(Cdr(s1338))
                                                                    , Cdr(Car(s1338))
                                                                    ))
                                                                    let x1454 =
                                                                    Abs(
                                                                    Sub
                                                                    ( Car(Cdr(s1338))
                                                                    , Car(Car(s1338))
                                                                    ))
                                                                    let 
                                                                    [s1470;
                                                                    s1471;
                                                                    s1472;
                                                                    s1473] =
                                                                    if Le(
                                                                    Compare
                                                                    ( x1454
                                                                    , x1431
                                                                    ))
                                                                    then
                                                                    [ x1431
                                                                    ; 1
                                                                    ; s1338
                                                                    ; s1339
                                                                    ]
                                                                    else
                                                                    [ x1454
                                                                    ; 1
                                                                    ; s1338
                                                                    ; s1339
                                                                    ]
                                                                    let x1482 =
                                                                    Sub
                                                                    ( Int(s1470)
                                                                    , s1471
                                                                    )
                                                                    let 
                                                                    [_] =
                                                                    loop 
                                                                    [ Gt(
                                                                    Compare
                                                                    ( x1482
                                                                    , 1
                                                                    ))
                                                                    ; 1
                                                                    ]
                                                                    step s1486 ->
                                                                    match 
                                                                    Get
                                                                    ( 
                                                                    Add
                                                                    ( Car(Car(s1472))
                                                                    , 
                                                                    Mul
                                                                    ( s1486
                                                                    , 
                                                                    Compare
                                                                    ( 
                                                                    Sub
                                                                    ( Car(Cdr(s1472))
                                                                    , Car(Car(s1472))
                                                                    )
                                                                    , 0
                                                                    )
                                                                    )
                                                                    )
                                                                    , Car(Cdr(Car(s1473)))
                                                                    ) with
                                                                    | Some s1542 ->
                                                                    match 
                                                                    Get
                                                                    ( 
                                                                    Add
                                                                    ( Cdr(Car(s1472))
                                                                    , 
                                                                    Mul
                                                                    ( s1486
                                                                    , 
                                                                    Compare
                                                                    ( 
                                                                    Sub
                                                                    ( Cdr(Cdr(s1472))
                                                                    , Cdr(Car(s1472))
                                                                    )
                                                                    , 0
                                                                    )
                                                                    )
                                                                    )
                                                                    , s1542
                                                                    ) with
                                                                    | Some s1593 ->
                                                                    if Eq(
                                                                    Compare
                                                                    ( s1593
                                                                    , 0
                                                                    ))
                                                                    then
                                                                    let x1611 =
                                                                    Add
                                                                    ( 1
                                                                    , s1486
                                                                    )
                                                                    [ Gt(
                                                                    Compare
                                                                    ( x1482
                                                                    , x1611
                                                                    ))
                                                                    ; x1611
                                                                    ]
                                                                    else
                                                                    Failwith("WrongCondition: self.data.deck[params.f.i + (k * sp.sign(params.t.i - params.f.i))][params.f.j + (k * sp.sign(params.t.j - params.f.j))] == 0")
                                                                    | None _ ->
                                                                    Failwith(88)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(88)
                                                                    end
                                                                    [ s1472
                                                                    ; s1473
                                                                    ]
                                                                    else
                                                                    Failwith("WrongCondition: (abs(params.t.j - params.f.j)) == (abs(params.t.i - params.f.i))")
                                                                    else
                                                                    [ s1338
                                                                    ; s1339
                                                                    ]
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1624))
                                                                    , Car(Cdr(Car(s1625)))
                                                                    ) with
                                                                    | Some s1640 ->
                                                                    match 
                                                                    Get
                                                                    ( Cdr(Car(s1624))
                                                                    , s1640
                                                                    ) with
                                                                    | Some s1653 ->
                                                                    let 
                                                                    [s1704;
                                                                    s1705] =
                                                                    if Eq(
                                                                    Compare
                                                                    ( Abs(s1653)
                                                                    , 3#nat
                                                                    ))
                                                                    then
                                                                    if Eq(
                                                                    Compare
                                                                    ( Abs(
                                                                    Mul
                                                                    ( 
                                                                    Sub
                                                                    ( Cdr(Cdr(s1624))
                                                                    , Cdr(Car(s1624))
                                                                    )
                                                                    , 
                                                                    Sub
                                                                    ( Car(Cdr(s1624))
                                                                    , Car(Car(s1624))
                                                                    )
                                                                    ))
                                                                    , 2#nat
                                                                    ))
                                                                    then
                                                                    [ s1624
                                                                    ; s1625
                                                                    ]
                                                                    else
                                                                    Failwith("WrongCondition: (abs((params.t.j - params.f.j) * (params.t.i - params.f.i))) == 2")
                                                                    else
                                                                    [ s1624
                                                                    ; s1625
                                                                    ]
                                                                    let x1732 =
                                                                    Car(Cdr(s1704))
                                                                    match 
                                                                    Get
                                                                    ( x1732
                                                                    , Car(Cdr(Car(s1705)))
                                                                    ) with
                                                                    | Some s1737 ->
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1704))
                                                                    , Car(Cdr(Car(s1705)))
                                                                    ) with
                                                                    | Some s1769 ->
                                                                    match 
                                                                    Get
                                                                    ( Cdr(Car(s1704))
                                                                    , s1769
                                                                    ) with
                                                                    | Some s1791 ->
                                                                    let x1818 =
                                                                    Update(x1732, Some_(Update(Cdr(Cdr(s1704)), Some_(s1791), s1737)), Car(Cdr(Car(s1705))))
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1704))
                                                                    , x1818
                                                                    ) with
                                                                    | Some s1834 ->
                                                                    Pair
                                                                    ( Nil<operation>
                                                                    , record_of_tree(..., 
                                                                    Pair
                                                                    ( 
                                                                    Pair
                                                                    ( Car(Car(s1705))
                                                                    , 
                                                                    Pair
                                                                    ( Update(Car(Car(s1704)), Some_(Update(Cdr(Car(s1704)), Some_(0), s1834)), x1818)
                                                                    , Cdr(Cdr(Car(s1705)))
                                                                    )
                                                                    )
                                                                    , 
                                                                    Pair
                                                                    ( 
                                                                    Pair
                                                                    ( Car(Car(Cdr(s1705)))
                                                                    , Neg(Cdr(Car(Cdr(s1705))))
                                                                    )
                                                                    , Cdr(Cdr(s1705))
                                                                    )
                                                                    ))
                                                                    )
                                                                    | None _ ->
                                                                    Failwith(92)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(91)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(91)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(91)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(89)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(89)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(83)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(83)
                                                                    end
                                                                  | None _ ->
                                                                    Failwith(77)
                                                                  end
                                                              | None _ ->
                                                                  Failwith(77)
                                                              end
                                                          | None _ ->
                                                              Failwith(71)
                                                          end
                                                      | None _ ->
                                                          Failwith(71)
                                                      end
                                                  | None _ -> Failwith(65)
                                                  end
                                              | None _ -> Failwith(65)
                                              end
                                          | None _ -> Failwith(53)
                                          end
                                      | None _ -> Failwith(53)
                                      end
                                    else
                                      Failwith("WrongCondition: sp.sign(self.data.deck[params.t.i][params.t.j]) != self.data.nextPlayer")
                                | None _ -> Failwith(52)
                                end
                            | None _ -> Failwith(52)
                            end
                          else
                            Failwith("WrongCondition: sp.sign(self.data.deck[params.f.i][params.f.j]) == self.data.nextPlayer")
                      | None _ -> Failwith(51)
                      end
                  | None _ -> Failwith(51)
                  end
                else
                  Failwith("WrongCondition: (params.t.j >= 0) & (params.t.j < 8)")
              else
                Failwith("WrongCondition: (params.t.i >= 0) & (params.t.i < 8)")
            else
              Failwith("WrongCondition: (params.f.j >= 0) & (params.f.j < 8)")
          else
            Failwith("WrongCondition: (params.f.i >= 0) & (params.f.i < 8)")
        else
          Failwith("WrongCondition: (self.data.winner == 0) & (~ self.data.draw)")]')
    sp.failwith(sp.build_lambda(f-1)(sp.unit))

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()

import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(first = sp.TInt, last = sp.TInt, saved = sp.TMap(sp.TInt, sp.TInt)).layout(("first", ("last", "saved"))))

  @sp.entry_point
  def pop(self, params):
    sp.set_type(params, sp.TUnit)
    sp.verify(self.data.first < self.data.last, 'WrongCondition: self.data.fif.first < self.data.fif.last')
    x19 = sp.local("x19", (1 + self.data.first, (self.data.last, sp.update_map(self.data.saved, self.data.first, sp.none))))
    x78 = sp.local("x78", x19.value)
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: record_of_tree(..., x78)]')
    sp.failwith(sp.build_lambda(f-1)(sp.unit))

  @sp.entry_point
  def push(self, params):
    sp.set_type(params, sp.TInt)
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: record_of_tree(..., let x15 =
                              Pair
                                ( __storage.first
                                , Pair
                                    ( Add(1, __storage.last)
                                    , __storage.saved
                                    )
                                )
                            let [x80; x81] = Unpair(2, x15)
                            Pair
                              ( x80
                              , let [x82; x83] = Unpair(2, x81)
                                Pair
                                  ( x82
                                  , let _ = x83
                                    Update(Car(Cdr(x15)), Some_(r5), Cdr(Cdr(x15)))
                                  )
                              ))]')
    sp.failwith(sp.build_lambda(f-1)(sp.unit))

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()

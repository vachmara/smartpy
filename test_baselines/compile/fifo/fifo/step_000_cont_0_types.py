import smartpy as sp

tstorage = sp.TRecord(fif = sp.TRecord(first = sp.TInt, last = sp.TInt, saved = sp.TMap(sp.TInt, sp.TInt)).layout(("first", ("last", "saved")))).layout("fif")
tparameter = sp.TVariant(pop = sp.TUnit, push = sp.TInt).layout(("pop", "push"))
tglobals = { }
tviews = { }

import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TUnit)

  @sp.entry_point
  def ep0(self, params):
    sp.set_type(params, sp.TOption(sp.TKeyHash))
    sp.operations() = sp.cons(sp.set_delegate_operation(params), sp.list([]))

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()

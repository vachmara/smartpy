import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(aaa = sp.TSet(sp.TNat), abc = sp.TList(sp.TOption(sp.TInt)), abca = sp.TMap(sp.TInt, sp.TOption(sp.TInt)), acb = sp.TString, b = sp.TBool, ddd = sp.TList(sp.TInt), f = sp.TBool, h = sp.TBytes, i = sp.TInt, m = sp.TMap(sp.TInt, sp.TInt), n = sp.TNat, pkh = sp.TKeyHash, s = sp.TString, toto = sp.TString).layout(((("aaa", ("abc", "abca")), (("acb", "b"), ("ddd", "f"))), (("h", ("i", "m")), (("n", "pkh"), ("s", "toto"))))))

  @sp.entry_point
  def comparisons(self, params):
    sp.set_type(params, sp.TUnit)
    sp.verify(self.data.i <= 123, 'WrongCondition: self.data.i <= 123')
    sp.verify((2 + self.data.i) == 12, 'WrongCondition: (2 + self.data.i) == 12')
    sp.verify(((2 + self.data.i) + (- 1234)) != 0, 'WrongCondition: (2 + self.data.i) != 1234')
    sp.verify(((self.data.i + 4) + (- 123)) != 0, 'WrongCondition: (self.data.i + 4) != 123')
    sp.verify((self.data.i - 5) < 123, 'WrongCondition: (self.data.i - 5) < 123')
    sp.verify((7 - self.data.i) < 123, 'WrongCondition: (7 - self.data.i) < 123')
    sp.verify(3 < self.data.i, 'WrongCondition: self.data.i > 3')
    sp.verify((4 * self.data.i) > 3, 'WrongCondition: (4 * self.data.i) > 3')
    sp.verify((self.data.i * 5) > 3, 'WrongCondition: (self.data.i * 5) > 3')
    sp.verify(self.data.i >= 3, 'WrongCondition: self.data.i >= 3')
    sp.verify(3 < self.data.i, 'WrongCondition: self.data.i > 3')
    sp.verify(self.data.i >= 3, 'WrongCondition: self.data.i >= 3')
    sp.verify(3000 > self.data.i, 'WrongCondition: self.data.i < 3000')
    sp.verify(self.data.i <= 3000, 'WrongCondition: self.data.i <= 3000')
    sp.verify(self.data.b, 'WrongCondition: self.data.b & True')
    x178 = sp.bind_block("x178"):
    with x178:
      sp.if 3 < self.data.i:
        sp.verify(4 < self.data.i, 'WrongCondition: self.data.i > 4')
        sp.result((((self.data.aaa, (self.data.abc, self.data.abca)), ((self.data.acb, self.data.b), (self.data.ddd, self.data.f))), ((self.data.h, (self.data.i, self.data.m)), ((self.data.n, self.data.pkh), (self.data.s, self.data.toto)))))
      sp.else:
        sp.result((((self.data.aaa, (self.data.abc, self.data.abca)), ((self.data.acb, self.data.b), (self.data.ddd, self.data.f))), ((self.data.h, (self.data.i, self.data.m)), ((self.data.n, self.data.pkh), (self.data.s, self.data.toto)))))
    x1000 = sp.local("x1000", x178.value)
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: record_of_tree(..., x1000)]')
    sp.failwith(sp.build_lambda(f-1)(sp.unit))

  @sp.entry_point
  def iterations(self, params):
    sp.set_type(params, sp.TUnit)
    c182 = sp.local("c182", True)
    y181 = sp.local("y181", 0)
    sp.while c182.value:
      x428 = sp.local("x428", 1 + y181.value)
      c182.value = 5 > x428.value
      y181.value = x428.value
    x470 = sp.local("x470", (((self.data.aaa, (self.data.abc, self.data.abca)), ((self.data.acb, self.data.b), (self.data.ddd, self.data.f))), ((self.data.h, (5, self.data.m)), ((self.data.n, self.data.pkh), (self.data.s, self.data.toto)))))
    c224 = sp.local("c224", sp.fst(sp.snd(sp.fst(sp.snd(x470.value)))) <= 42)
    r846 = sp.local("r846", x470.value)
    sp.while c224.value:
      x512 = sp.local("x512", (sp.fst(r846.value), ((sp.fst(sp.fst(sp.snd(r846.value))), (2 + sp.fst(sp.snd(sp.fst(sp.snd(r846.value)))), sp.snd(sp.snd(sp.fst(sp.snd(r846.value)))))), sp.snd(sp.snd(r846.value)))))
      c224.value = sp.fst(sp.snd(sp.fst(sp.snd(x512.value)))) <= 42
      r846.value = x512.value
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: record_of_tree(..., if Le(Compare(Car(Cdr(Car(Cdr(r846)))), 123))
                            then
                              Pair
                                ( Car(r846)
                                , Pair
                                    ( Pair
                                        ( Car(Car(Cdr(r846)))
                                        , Pair
                                            ( Add
                                                ( 12
                                                , Car(Cdr(Car(Cdr(r846))))
                                                )
                                            , Cdr(Cdr(Car(Cdr(r846))))
                                            )
                                        )
                                    , Cdr(Cdr(r846))
                                    )
                                )
                            else
                              Pair
                                ( Car(r846)
                                , Pair
                                    ( Pair
                                        ( Car(Car(Cdr(r846)))
                                        , Pair(5, Cdr(Cdr(Car(Cdr(r846)))))
                                        )
                                    , Cdr(Cdr(r846))
                                    )
                                ))]')
    sp.failwith(sp.build_lambda(f-1)(sp.unit))

  @sp.entry_point
  def localVariable(self, params):
    sp.set_type(params, sp.TUnit)
    self.data = sp.record(aaa = self.data.aaa, abc = self.data.abc, abca = self.data.abca, acb = self.data.acb, b = self.data.b, ddd = self.data.ddd, f = self.data.f, h = self.data.h, i = 2 * self.data.i, m = self.data.m, n = self.data.n, pkh = self.data.pkh, s = self.data.s, toto = self.data.toto)

  @sp.entry_point
  def myMessageName4(self, params):
    sp.set_type(params, sp.TUnit)
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: record_of_tree(..., let [r845] =
                              iter [ __storage.m
                                   ; Pair
                                       ( Pair
                                           ( Pair
                                               ( __storage.aaa
                                               , Pair
                                                   ( __storage.abc
                                                   , __storage.abca
                                                   )
                                               )
                                           , Pair
                                               ( Pair
                                                   ( __storage.acb
                                                   , __storage.b
                                                   )
                                               , Pair
                                                   ( __storage.ddd
                                                   , __storage.f
                                                   )
                                               )
                                           )
                                       , Pair
                                           ( Pair
                                               ( __storage.h
                                               , Pair
                                                   ( __storage.i
                                                   , __storage.m
                                                   )
                                               )
                                           , Pair
                                               ( Pair
                                                   ( __storage.n
                                                   , __storage.pkh
                                                   )
                                               , Pair
                                                   ( __storage.s
                                                   , __storage.toto
                                                   )
                                               )
                                           )
                                       )
                                   ]
                              step s330; s332 ->
                                [ Pair
                                    ( Car(s332)
                                    , Pair
                                        ( Pair
                                            ( Car(Car(Cdr(s332)))
                                            , Pair
                                                ( Add
                                                    ( Mul
                                                        ( Car(s330)
                                                        , Cdr(s330)
                                                        )
                                                    , Car(Cdr(Car(Cdr(s332))))
                                                    )
                                                , Cdr(Cdr(Car(Cdr(s332))))
                                                )
                                            )
                                        , Cdr(Cdr(s332))
                                        )
                                    )
                                ]
                            r845)]')
    sp.failwith(sp.build_lambda(f-1)(sp.unit))

  @sp.entry_point
  def myMessageName5(self, params):
    sp.set_type(params, sp.TUnit)
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: record_of_tree(..., let [r844] =
                              iter [ __storage.m
                                   ; Pair
                                       ( Pair
                                           ( Pair
                                               ( __storage.aaa
                                               , Pair
                                                   ( __storage.abc
                                                   , __storage.abca
                                                   )
                                               )
                                           , Pair
                                               ( Pair
                                                   ( __storage.acb
                                                   , __storage.b
                                                   )
                                               , Pair
                                                   ( __storage.ddd
                                                   , __storage.f
                                                   )
                                               )
                                           )
                                       , Pair
                                           ( Pair
                                               ( __storage.h
                                               , Pair
                                                   ( __storage.i
                                                   , __storage.m
                                                   )
                                               )
                                           , Pair
                                               ( Pair
                                                   ( __storage.n
                                                   , __storage.pkh
                                                   )
                                               , Pair
                                                   ( __storage.s
                                                   , __storage.toto
                                                   )
                                               )
                                           )
                                       )
                                   ]
                              step s280; s282 ->
                                [ Pair
                                    ( Car(s282)
                                    , Pair
                                        ( Pair
                                            ( Car(Car(Cdr(s282)))
                                            , Pair
                                                ( Add
                                                    ( Mul(2, Car(s280))
                                                    , Car(Cdr(Car(Cdr(s282))))
                                                    )
                                                , Cdr(Cdr(Car(Cdr(s282))))
                                                )
                                            )
                                        , Cdr(Cdr(s282))
                                        )
                                    )
                                ]
                            r844)]')
    sp.failwith(sp.build_lambda(f-1)(sp.unit))

  @sp.entry_point
  def myMessageName6(self, params):
    sp.set_type(params, sp.TInt)
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: let [r843] =
          iter [ __storage.m
               ; Pair
                   ( Pair
                       ( Pair
                           ( __storage.aaa
                           , Pair(__storage.abc, __storage.abca)
                           )
                       , Pair
                           ( Pair(__storage.acb, __storage.b)
                           , Pair(__storage.ddd, __storage.f)
                           )
                       )
                   , Pair
                       ( Pair(__storage.h, Pair(__storage.i, __storage.m))
                       , Pair
                           ( Pair(__storage.n, __storage.pkh)
                           , Pair(__storage.s, __storage.toto)
                           )
                       )
                   )
               ]
          step s101; s103 ->
            [ Pair
                ( Car(s103)
                , Pair
                    ( Pair
                        ( Car(Car(Cdr(s103)))
                        , Pair
                            ( Add(Mul(3, Cdr(s101)), Car(Cdr(Car(Cdr(s103)))))
                            , Cdr(Cdr(Car(Cdr(s103))))
                            )
                        )
                    , Cdr(Cdr(s103))
                    )
                )
            ]
        let [s237; s238] =
          if Mem
               ( 12#nat
               , Update(12#nat, True, Update(2#nat, False, Car(Car(Car(r843)))))
               )
          then
            [ l8
            ; Pair
                ( Pair
                    ( Pair
                        ( Update(12#nat, True, Update(2#nat, False, Car(Car(Car(r843)))))
                        , Pair
                            ( Cons(Some_(16), Car(Cdr(Car(Car(r843)))))
                            , Update(0, Some_(Some_(16)), Cdr(Cdr(Car(Car(r843)))))
                            )
                        )
                    , Cdr(Car(r843))
                    )
                , Pair
                    ( Pair
                        ( Car(Car(Cdr(r843)))
                        , Pair
                            ( Car(Cdr(Car(Cdr(r843))))
                            , Update(42, Some_(43), Cdr(Cdr(Car(Cdr(r843)))))
                            )
                        )
                    , Cdr(Cdr(r843))
                    )
                )
            ]
          else
            [ l8
            ; Pair
                ( Pair
                    ( Pair
                        ( Update(12#nat, True, Update(2#nat, False, Car(Car(Car(r843)))))
                        , Pair
                            ( Cons(Some_(16), Car(Cdr(Car(Car(r843)))))
                            , Update(0, Some_(Some_(16)), Cdr(Cdr(Car(Car(r843)))))
                            )
                        )
                    , Cdr(Car(r843))
                    )
                , Cdr(r843)
                )
            ]
        let x990 =
          match IsNat(Neg(s237)) with
          | Some s256 ->
              Pair
                ( Pair
                    ( Pair
                        ( Update(s256, True, Car(Car(Car(s238))))
                        , Cdr(Car(Car(s238)))
                        )
                    , Cdr(Car(s238))
                    )
                , Cdr(s238)
                )
          | None _ -> Failwith(96)
          end
        Pair(Nil<operation>, record_of_tree(..., x990))]')
    sp.failwith(sp.build_lambda(f-1)(sp.unit))

  @sp.entry_point
  def someComputations(self, params):
    sp.set_type(params, sp.TPair(sp.TString, sp.TInt))
    sp.verify(self.data.i <= 123, 'WrongCondition: self.data.i <= 123')
    x70 = sp.local("x70", ((self.data.h, (sp.snd(params) + self.data.i, self.data.m)), ((self.data.n, self.data.pkh), (self.data.s, self.data.toto))))
    x378 = sp.local("x378", (((self.data.aaa, (self.data.abc, self.data.abca)), ((sp.fst(params), self.data.b), (self.data.ddd, self.data.f))), ((sp.fst(sp.fst(x70.value)), (100 - 1, sp.snd(sp.snd(sp.fst(x70.value))))), sp.snd(x70.value))))
    x988 = sp.local("x988", x378.value)
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: record_of_tree(..., x988)]')
    sp.failwith(sp.build_lambda(f-1)(sp.unit))

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()

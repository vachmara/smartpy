import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(a = sp.TOption(sp.TPair(sp.TPair(sp.TPair(sp.TList(sp.TInt), sp.TList(sp.TInt)), sp.TPair(sp.TList(sp.TPair(sp.TString, sp.TPair(sp.TString, sp.TBool))), sp.TPair(sp.TList(sp.TPair(sp.TString, sp.TPair(sp.TString, sp.TBool))), sp.TList(sp.TString)))), sp.TPair(sp.TPair(sp.TList(sp.TString), sp.TList(sp.TPair(sp.TString, sp.TBool))), sp.TPair(sp.TList(sp.TPair(sp.TString, sp.TBool)), sp.TPair(sp.TList(sp.TInt), sp.TList(sp.TInt)))))), b = sp.TInt, c = sp.TString, d = sp.TInt, e = sp.TString, f = sp.TList(sp.TInt), g = sp.TList(sp.TInt), head = sp.TString, tail = sp.TList(sp.TString)).layout(((("a", "b"), ("c", "d")), (("e", "f"), ("g", ("head", "tail"))))))

  @sp.entry_point
  def test(self, params):
    sp.set_type(params, sp.TPair(sp.TList(sp.TInt), sp.TPair(sp.TMap(sp.TString, sp.TPair(sp.TString, sp.TBool)), sp.TSet(sp.TInt))))
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: let [r600] =
          iter [ Cdr(Cdr(l4)); Nil<int> ]
          step s83; s84 ->
            [ Cons(s83, s84) ]
        let [r599] =
          iter [ Cdr(Cdr(l4)); Nil<int> ]
          step s98; s99 ->
            [ Cons(s98, s99) ]
        let [r598] =
          iter [ r599; Nil<int> ]
          step s111; s112 ->
            [ Cons(s111, s112) ]
        let [r597] =
          iter [ Car(Cdr(l4)); Nil<{ string ; bool }> ]
          step s129; s130 ->
            [ Cons(Cdr(s129), s130) ]
        let [r596] =
          iter [ Car(Cdr(l4)); Nil<{ string ; bool }> ]
          step s148; s149 ->
            [ Cons(Cdr(s148), s149) ]
        let [r595] =
          iter [ r596; Nil<{ string ; bool }> ]
          step s162; s163 ->
            [ Cons(s162, s163) ]
        let [r594] =
          iter [ Car(Cdr(l4)); Nil<string> ]
          step s180; s181 ->
            [ Cons(Car(s180), s181) ]
        let [r593] =
          iter [ Car(Cdr(l4)); Nil<string> ]
          step s202; s203 ->
            [ Cons(Car(s202), s203) ]
        let [r592] =
          iter [ r593; Nil<string> ]
          step s216; s217 ->
            [ Cons(s216, s217) ]
        let [r591] =
          iter [ Car(Cdr(l4)); Nil<{ string ; (string ; bool) }> ]
          step s234; s235 ->
            [ Cons(s234, s235) ]
        let [r590] =
          iter [ Car(Cdr(l4)); Nil<{ string ; (string ; bool) }> ]
          step s255; s256 ->
            [ Cons(s255, s256) ]
        let [r589] =
          iter [ r590; Nil<{ string ; (string ; bool) }> ]
          step s270; s271 ->
            [ Cons(s270, s271) ]
        let [r588] =
          iter [ Car(l4); Nil<int> ]
          step s292; s293 ->
            [ Cons(s292, s293) ]
        let [r587] = iter [ Car(l4); 0 ]
                     step s322; s323 ->
                       [ Add(s322, s323) ]
        let [r586] =
          iter [ Car(Cdr(l4)); Nil<string> ]
          step s342; s343 ->
            [ Cons(Car(s342), s343) ]
        let [r585] =
          iter [ r586; Nil<string> ]
          step s356; s357 ->
            [ Cons(s356, s357) ]
        let [r584] =
          iter [ Cdr(Cdr(l4)); Nil<int> ]
          step s377; s378 ->
            [ Cons(s377, s378) ]
        let [r583] = iter [ r584; 0 ]
                     step s391; s392 ->
                       [ Add(s391, s392) ]
        let [_; r464] =
          iter [ Car(Cdr(l4))
               ; l4
               ; Pair
                   ( Pair
                       ( Pair
                           ( Some_(Pair
                                     ( Pair
                                         ( Pair(Car(l4), r588)
                                         , Pair(r589, Pair(r591, r592))
                                         )
                                     , Pair
                                         ( Pair(r594, r595)
                                         , Pair(r597, Pair(r598, r600))
                                         )
                                     ))
                           , r587
                           )
                       , Pair(Concat1(r585), r583)
                       )
                   , Pair
                       ( Pair("", __storage.f)
                       , Pair
                           ( __storage.g
                           , Pair(__storage.head, __storage.tail)
                           )
                       )
                   )
               ]
          step s425; s426; s427 ->
            let [s461; s462] =
              if Cdr(Cdr(s425))
              then
                [ s426
                ; Pair
                    ( Car(s427)
                    , Pair
                        ( Pair
                            ( Concat2(Car(Car(Cdr(s427))), Car(Cdr(s425)))
                            , Cdr(Car(Cdr(s427)))
                            )
                        , Cdr(Cdr(s427))
                        )
                    )
                ]
              else
                [ s426; s427 ]
            [ s461; s462 ]
        let [_; r653] =
          loop [ True; 0; r464 ]
          step s468; s470 ->
            let x521 = Add(1, s468)
            [ Gt(Compare(5, x521))
            ; x521
            ; Pair
                ( Car(s470)
                , Pair
                    ( Pair
                        ( Car(Car(Cdr(s470)))
                        , Cons(Mul(s468, s468), Cdr(Car(Cdr(s470))))
                        )
                    , Cdr(Cdr(s470))
                    )
                )
            ]
        let [_; r577] =
          loop [ True; 1; Nil<int> ]
          step s533; s534 ->
            let x549 = Add(1, s533)
            [ Gt(Compare(12, x549)); x549; Cons(s533, s534) ]
        Pair
          ( Nil<operation>
          , record_of_tree(..., let [r575] =
                                  iter [ r577; Nil<int> ]
                                  step s561; s562 ->
                                    [ Cons(s561, s562) ]
                                let [x2818; x2819] = Unpair(2, r653)
                                Pair
                                  ( x2818
                                  , let [x2820; x2821] = Unpair(2, x2819)
                                    Pair
                                      ( x2820
                                      , let [x2822; x2823] = Unpair(2, x2821)
                                        let _ = x2822
                                        Pair(r575, x2823)
                                      )
                                  ))
          )]')
    sp.failwith(sp.build_lambda(f-1)(sp.unit))

  @sp.entry_point
  def test_match(self, params):
    sp.set_type(params, sp.TList(sp.TString))
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: record_of_tree(..., let [s72] =
                              if l6 is x59 :: xs60
                              then
                                [ Pair
                                    ( Pair
                                        ( Pair(__storage.a, __storage.b)
                                        , Pair(__storage.c, __storage.d)
                                        )
                                    , Pair
                                        ( Pair(__storage.e, __storage.f)
                                        , Pair(__storage.g, Pair(x59, xs60))
                                        )
                                    )
                                ]
                              else
                                [ Pair
                                    ( Pair
                                        ( Pair(__storage.a, __storage.b)
                                        , Pair(__storage.c, __storage.d)
                                        )
                                    , Pair
                                        ( Pair(__storage.e, __storage.f)
                                        , Pair
                                            ( __storage.g
                                            , Pair("abc", __storage.tail)
                                            )
                                        )
                                    )
                                ]
                            s72)]')
    sp.failwith(sp.build_lambda(f-1)(sp.unit))

  @sp.entry_point
  def test_match2(self, params):
    sp.set_type(params, sp.TList(sp.TString))
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: record_of_tree(..., let [s571] =
                              if r7 is x10 :: xs11
                              then
                                if xs11 is x22 :: xs23
                                then
                                  [ Pair
                                      ( Pair
                                          ( Pair(__storage.a, __storage.b)
                                          , Pair(__storage.c, __storage.d)
                                          )
                                      , Pair
                                          ( Pair(__storage.e, __storage.f)
                                          , Pair
                                              ( __storage.g
                                              , Pair(Concat2(x10, x22), xs23)
                                              )
                                          )
                                      )
                                  ]
                                else
                                  [ Pair
                                      ( Pair
                                          ( Pair(__storage.a, __storage.b)
                                          , Pair(__storage.c, __storage.d)
                                          )
                                      , Pair
                                          ( Pair(__storage.e, __storage.f)
                                          , Pair
                                              ( __storage.g
                                              , Pair
                                                  ( __storage.head
                                                  , __storage.tail
                                                  )
                                              )
                                          )
                                      )
                                  ]
                              else
                                [ Pair
                                    ( Pair
                                        ( Pair(__storage.a, __storage.b)
                                        , Pair(__storage.c, __storage.d)
                                        )
                                    , Pair
                                        ( Pair(__storage.e, __storage.f)
                                        , Pair
                                            ( __storage.g
                                            , Pair("abc", __storage.tail)
                                            )
                                        )
                                    )
                                ]
                            s571)]')
    sp.failwith(sp.build_lambda(f-1)(sp.unit))

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()

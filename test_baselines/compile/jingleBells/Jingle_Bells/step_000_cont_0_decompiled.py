import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(current = sp.TString, played = sp.TInt, rules = sp.TList(sp.TString), verse = sp.TInt).layout((("current", "played"), ("rules", "verse"))))

  @sp.entry_point
  def ep0(self, params):
    sp.set_type(params, sp.TInt)
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: let [_; _; _; r147] =
          loop [ Gt(Compare(__parameter, 0))
               ; 0
               ; __parameter
               ; __parameter
               ; Pair
                   ( Pair(__storage.current, __storage.played)
                   , Pair(__storage.rules, __storage.verse)
                   )
               ]
          step s16; s17; s18; s19 ->
            let [s48; s49; s50; s51] =
              if Eq(Compare(Cdr(Cdr(s19)), 16))
              then
                [ s16
                ; s17
                ; s18
                ; Pair
                    ( Pair("", Add(1, Cdr(Car(s19))))
                    , let [x218; x219] = Unpair(2, Cdr(s19))
                      Pair(x218, let _ = x219
                                 0)
                    )
                ]
              else
                [ s16; s17; s18; s19 ]
            let [s81; s82; s83; s84] =
              if Neq(Compare(Car(Car(s51)), ""))
              then
                [ s48
                ; s49
                ; s50
                ; Pair
                    ( Pair(Concat2(Car(Car(s51)), "\n"), Cdr(Car(s51)))
                    , Cdr(s51)
                    )
                ]
              else
                [ s48; s49; s50; s51 ]
            match Get
                    ( Cdr(Cdr(s84))
                    , [0: "Dashing through the snow"; 1: "In a one-horse open sleigh"; 2: "O'er the fields we go"; 3: "Laughing all the way"; 4: "Bells on bob tail ring"; 5: "Making spirits bright"; 6: "What fun it is to ride and sing"; 7: "A sleighing song tonight!"; 8: "Jingle bells, jingle bells,"; 9: "Jingle all the way."; 10: "Oh! what fun it is to ride"; 11: "In a one-horse open sleigh."; 12: "Jingle bells, jingle bells,"; 13: "Jingle all the way;"; 14: "Oh! what fun it is to ride"; 15: "In a one-horse open sleigh."]#map(int,string)
                    ) with
            | Some s110 ->
                let x125 =
                  Pair
                    ( Pair(Concat2(Car(Car(s84)), s110), Cdr(Car(s84)))
                    , Cdr(s84)
                    )
                let x137 = Add(1, s81)
                [ Gt(Compare(s82, x137))
                ; x137
                ; s82
                ; s83
                ; let [x216; x217] = Unpair(2, x125)
                  Pair
                    ( x216
                    , let [x220; x221] = Unpair(2, x217)
                      Pair(x220, let _ = x221
                                 Add(1, Cdr(Cdr(x125))))
                    )
                ]
            | None _ -> Failwith(48)
            end
        Pair(Nil<operation>, record_of_tree(..., r147))]')
    sp.failwith(sp.build_lambda(f-1)(sp.unit))

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()

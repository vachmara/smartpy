import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(b2b = sp.TBytes, keccak = sp.TBytes, s256 = sp.TBytes, s512 = sp.TBytes, sha3 = sp.TBytes, tz1 = sp.TKeyHash, v = sp.TBytes).layout((("b2b", ("keccak", "s256")), (("s512", "sha3"), ("tz1", "v")))))

  @sp.entry_point
  def new_key(self, params):
    sp.set_type(params, sp.TKey)
    def f-1(lparams_-1):
      sp.failwith('[Error: prim1: Hash_key]')
    self.data = sp.record(b2b = self.data.b2b, keccak = self.data.keccak, s256 = self.data.s256, s512 = self.data.s512, sha3 = self.data.sha3, tz1 = sp.build_lambda(f-1)(sp.unit), v = self.data.v)

  @sp.entry_point
  def new_value(self, params):
    sp.set_type(params, sp.TBytes)
    def f-1(lparams_-1):
      sp.failwith('[Error: prim1: Blake2b]')
    def f-1(lparams_-1):
      sp.failwith('[Error: prim1: Keccak]')
    def f-1(lparams_-1):
      sp.failwith('[Error: prim1: Sha256]')
    def f-1(lparams_-1):
      sp.failwith('[Error: prim1: Sha512]')
    def f-1(lparams_-1):
      sp.failwith('[Error: prim1: Sha3]')
    self.data = sp.record(b2b = sp.build_lambda(f-1)(sp.unit), keccak = sp.build_lambda(f-1)(sp.unit), s256 = sp.build_lambda(f-1)(sp.unit), s512 = sp.build_lambda(f-1)(sp.unit), sha3 = sp.build_lambda(f-1)(sp.unit), tz1 = self.data.tz1, v = params)

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()

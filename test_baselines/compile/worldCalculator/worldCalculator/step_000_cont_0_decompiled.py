import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(Help = sp.TList(sp.TString), formula = sp.TString, operations = sp.TList(sp.TString), result = sp.TInt, summary = sp.TString).layout((("Help", "formula"), ("operations", ("result", "summary")))))

  @sp.entry_point
  def ep0(self, params):
    sp.set_type(params, sp.TString)
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: (fun (x2 : string) : int ->
let x18 = Size(x2)
let [_; r1218] =
  loop [ Gt(Compare(x18, 0#nat)); 0#nat; 0 ]
  step s22; s24 ->
    match Slice(s22, 1#nat, x2) with
    | Some s40 ->
        match Get
                ( s40
                , ["0": 0; "1": 1; "2": 2; "3": 3; "4": 4; "5": 5; "6": 6; "7": 7; "8": 8; "9": 9]#map(string,int)
                ) with
        | Some s49 ->
            let x69 = Add(1#nat, s22)
            [ Gt(Compare(x18, x69)); x69; Add(Mul(10, s24), s49) ]
        | None _ -> Failwith(18)
        end
    | None _ -> Failwith(18)
    end
r1218)]')
    sp.failwith(sp.build_lambda(f-1)(sp.unit))

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()

import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(b0 = sp.TOption(sp.TBytes), l0 = sp.TNat, l1 = sp.TNat, nat_of_string = sp.TInt, s0 = sp.TOption(sp.TString), split = sp.TList(sp.TString), string_of_nat = sp.TString).layout((("b0", ("l0", "l1")), (("nat_of_string", "s0"), ("split", "string_of_nat")))))

  @sp.entry_point
  def concatenating(self, params):
    sp.set_type(params, sp.TPair(sp.TPair(sp.TBytes, sp.TBytes), sp.TPair(sp.TList(sp.TString), sp.TList(sp.TBytes))))
    def f-1(lparams_-1):
      sp.failwith('[Error: prim1: Concat1]')
    def f-1(lparams_-1):
      sp.failwith('[Error: prim1: Concat1]')
    self.data = sp.record(b0 = sp.some(sp.build_lambda(f-1)(sp.unit)), l0 = self.data.l0, l1 = self.data.l1, nat_of_string = self.data.nat_of_string, s0 = sp.some(sp.build_lambda(f-1)(sp.unit)), split = self.data.split, string_of_nat = self.data.string_of_nat)

  @sp.entry_point
  def concatenating2(self, params):
    sp.set_type(params, sp.TPair(sp.TPair(sp.TBytes, sp.TBytes), sp.TPair(sp.TString, sp.TString)))
    def f-1(lparams_-1):
      sp.failwith('[Error: TODO prim2: Concat2]')
    def f-1(lparams_-1):
      sp.failwith('[Error: TODO prim2: Concat2]')
    self.data = sp.record(b0 = sp.some(sp.build_lambda(f-1)(sp.unit)), l0 = self.data.l0, l1 = self.data.l1, nat_of_string = self.data.nat_of_string, s0 = sp.some(sp.build_lambda(f-1)(sp.unit)), split = self.data.split, string_of_nat = self.data.string_of_nat)

  @sp.entry_point
  def slicing(self, params):
    sp.set_type(params, sp.TPair(sp.TBytes, sp.TString))
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: record_of_tree(..., let x460 =
                              Pair
                                ( Pair
                                    ( Slice(1#nat, 2#nat, Car(r410))
                                    , Pair(Size(Cdr(r410)), __storage.l1)
                                    )
                                , Pair
                                    ( Pair
                                        ( __storage.nat_of_string
                                        , Slice(2#nat, 5#nat, Cdr(r410))
                                        )
                                    , Pair
                                        ( __storage.split
                                        , __storage.string_of_nat
                                        )
                                    )
                                )
                            let [s492; s493] =
                              match Cdr(Car(Cdr(x460))) with
                              | Some s466 ->
                                  [ r410
                                  ; Pair
                                      ( Pair
                                          ( Slice(1#nat, 2#nat, Car(r410))
                                          , Pair
                                              ( Add
                                                  ( Size(s466)
                                                  , Size(Cdr(r410))
                                                  )
                                              , __storage.l1
                                              )
                                          )
                                      , Pair
                                          ( Pair
                                              ( __storage.nat_of_string
                                              , Slice(2#nat, 5#nat, Cdr(r410))
                                              )
                                          , Pair
                                              ( __storage.split
                                              , __storage.string_of_nat
                                              )
                                          )
                                      )
                                  ]
                              | None _ -> [ r410; x460 ]
                              end
                            Pair
                              ( Pair
                                  ( Car(Car(s493))
                                  , Pair(Car(Cdr(Car(s493))), Size(Car(s492)))
                                  )
                              , Cdr(s493)
                              ))]')
    sp.failwith(sp.build_lambda(f-1)(sp.unit))

  @sp.entry_point
  def test_nat_of_string(self, params):
    sp.set_type(params, sp.TString)
    def f-1(lparams_-1):
      sp.failwith('[Error: prim1: Size]')
    x315 = sp.local("x315", sp.build_lambda(f-1)(sp.unit))
    c67 = sp.local("c67", x315.value > 0)
    y65 = sp.local("y65", 0)
    r716 = sp.local("r716", 0)
    sp.while c67.value:
      with (sp.slice(params, y65.value, 1)).match_cases() as arg:
        with arg.match('Some') as s338:
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: ["0": 0; "1": 1; "2": 2; "3": 3; "4": 4; "5": 5; "6": 6; "7": 7; "8": 8; "9": 9]#map(string,int)]')
          sp.failwith(sp.build_lambda(f-1)(sp.unit))
        with arg.match('None') as _r84:
          sp.failwith(34)

    self.data = sp.record(b0 = self.data.b0, l0 = self.data.l0, l1 = self.data.l1, nat_of_string = r716.value, s0 = self.data.s0, split = self.data.split, string_of_nat = self.data.string_of_nat)

  @sp.entry_point
  def test_split(self, params):
    sp.set_type(params, sp.TString)
    def f-1(lparams_-1):
      sp.failwith('[Error: prim1: Size]')
    x113 = sp.local("x113", sp.build_lambda(f-1)(sp.unit))
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: let [_; _; r227; r228; r229; r230] =
          loop [ Gt(Compare(x113, 0#nat))
               ; 0#nat
               ; x113
               ; Nil<string>
               ; 0#nat
               ; l8
               ; Pair
                   ( Pair(__storage.b0, Pair(__storage.l0, __storage.l1))
                   , Pair
                       ( Pair(__storage.nat_of_string, __storage.s0)
                       , Pair(__storage.split, __storage.string_of_nat)
                       )
                   )
               ]
          step s117; s118; s119; s120; s121; s122 ->
            match Slice(s117, 1#nat, s121) with
            | Some s138 ->
                let [s209; s210; s211; s212; s213; s214] =
                  if Eq(Compare(s138, ","))
                  then
                    match IsNat(Sub(s117, s120)) with
                    | Some s172 ->
                        match Slice(s120, s172, s121) with
                        | Some s189 ->
                            [ s117
                            ; s118
                            ; Cons(s189, s119)
                            ; Add(1#nat, s117)
                            ; s121
                            ; s122
                            ]
                        | None _ -> Failwith(13)
                        end
                    | None _ -> Failwith(13)
                    end
                  else
                    [ s117; s118; s119; s120; s121; s122 ]
                let x218 = Add(1#nat, s209)
                [ Gt(Compare(s210, x218))
                ; x218
                ; s210
                ; s211
                ; s212
                ; s213
                ; s214
                ]
            | None _ -> Failwith(12)
            end
        let [s287; s288] =
          if Gt(Compare(Size(r229), 0#nat))
          then
            match IsNat(Sub(Size(r229), r228)) with
            | Some s269 ->
                match Slice(r228, s269, r229) with
                | Some s281 -> [ Cons(s281, r227); r230 ]
                | None _ -> Failwith(16)
                end
            | None _ -> Failwith(16)
            end
          else
            [ r227; r230 ]
        Pair
          ( Nil<operation>
          , record_of_tree(..., let [r638] =
                                  iter [ s287; Nil<string> ]
                                  step s292; s293 ->
                                    [ Cons(s292, s293) ]
                                let [x1035; x1036] = Unpair(2, s288)
                                Pair
                                  ( x1035
                                  , let [x1039; x1040] = Unpair(2, x1036)
                                    Pair
                                      ( x1039
                                      , let [x1043; x1044] = Unpair(2, x1040)
                                        let _ = x1043
                                        Pair(r638, x1044)
                                      )
                                  ))
          )]')
    sp.failwith(sp.build_lambda(f-1)(sp.unit))

  @sp.entry_point
  def test_string_of_nat(self, params):
    sp.set_type(params, sp.TNat)
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: let [s22; s23; _; s25] =
          if Eq(Compare(r9, 0#nat))
          then
            [ Cons("0", Nil<string>)
            ; r9
            ; r9
            ; Pair
                ( Pair(__storage.b0, Pair(__storage.l0, __storage.l1))
                , Pair
                    ( Pair(__storage.nat_of_string, __storage.s0)
                    , Pair(__storage.split, __storage.string_of_nat)
                    )
                )
            ]
          else
            [ Nil<string>
            ; r9
            ; r9
            ; Pair
                ( Pair(__storage.b0, Pair(__storage.l0, __storage.l1))
                , Pair
                    ( Pair(__storage.nat_of_string, __storage.s0)
                    , Pair(__storage.split, __storage.string_of_nat)
                    )
                )
            ]
        let [r706; _] =
          loop [ Gt(Compare(s23, 0#nat)); s22; s23 ]
          step s34; s35 ->
            match Ediv(s35, 10#nat) with
            | Some s46 ->
                match Get
                        ( Cdr(s46)
                        , [0#nat: "0"; 1#nat: "1"; 2#nat: "2"; 3#nat: "3"; 4#nat: "4"; 5#nat: "5"; 6#nat: "6"; 7#nat: "7"; 8#nat: "8"; 9#nat: "9"]#map(nat,string)
                        ) with
                | Some s56 ->
                    match Ediv(s35, 10#nat) with
                    | Some s69 ->
                        [ Gt(Compare(Car(s69), 0#nat))
                        ; Cons(s56, s34)
                        ; Car(s69)
                        ]
                    | None _ -> Failwith(27)
                    end
                | None _ -> Failwith(26)
                end
            | None _ -> Failwith(26)
            end
        Pair
          ( Nil<operation>
          , record_of_tree(..., let [x1033; x1034] = Unpair(2, s25)
                                Pair
                                  ( x1033
                                  , let [x1037; x1038] = Unpair(2, x1034)
                                    Pair
                                      ( x1037
                                      , let [x1041; x1042] = Unpair(2, x1038)
                                        Pair
                                          ( x1041
                                          , let _ = x1042
                                            Concat1(r706)
                                          )
                                      )
                                  ))
          )]')
    sp.failwith(sp.build_lambda(f-1)(sp.unit))

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()

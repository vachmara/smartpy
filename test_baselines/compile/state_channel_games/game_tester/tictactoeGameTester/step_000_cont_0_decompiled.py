import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(apply_ = sp.TLambda(sp.TPair(sp.TPair(sp.TInt, sp.TPair(sp.TNat, sp.TOption(sp.TString))), sp.TPair(sp.TPair(sp.TInt, sp.TInt), sp.TMap(sp.TInt, sp.TMap(sp.TInt, sp.TInt)))), sp.TPair(sp.TUnit, sp.TPair(sp.TMap(sp.TInt, sp.TMap(sp.TInt, sp.TInt)), sp.TOption(sp.TString)))), constants = sp.TPair(sp.TPair(sp.TBytes, sp.TPair(sp.TString, sp.TMutez)), sp.TPair(sp.TPair(sp.TBytes, sp.TPair(sp.TAddress, sp.TKey)), sp.TPair(sp.TPair(sp.TAddress, sp.TKey), sp.TMutez))), meta = sp.TPair(sp.TInt, sp.TPair(sp.TNat, sp.TOption(sp.TString))), state = sp.TMap(sp.TInt, sp.TMap(sp.TInt, sp.TInt))).layout((("apply_", "constants"), ("meta", "state"))))

  @sp.entry_point
  def ep0(self, params):
    sp.set_type(params, sp.TPair(sp.TPair(sp.TInt, sp.TInt), sp.TNat))
    x47 = sp.bind_block("x47"):
    with x47:
      sp.if sp.fst(self.data.meta) == 1:
        sp.verify(sp.sender == sp.fst(sp.snd(sp.fst(sp.snd(self.data.constants)))), 'Game_WrongPlayer')
        sp.result((params, ((self.data.apply_, self.data.constants), (self.data.meta, self.data.state))))
      sp.else:
        sp.verify(sp.sender == sp.fst(sp.fst(sp.snd(sp.snd(self.data.constants)))), 'Game_WrongPlayer')
        sp.result((params, ((self.data.apply_, self.data.constants), (self.data.meta, self.data.state))))
    s45 = sp.local("s45", sp.fst(x47.value))
    s46 = sp.local("s46", sp.snd(x47.value))
    with sp.snd(sp.snd(sp.fst(sp.snd(s46.value)))).match_cases() as arg:
      with arg.match('Some') as _l48:
        sp.failwith('WrongCondition: self.data.meta.outcome.is_variant('None')')
      with arg.match('None') as _r49:
        sp.verify(sp.fst(sp.snd(sp.fst(sp.snd(s46.value)))) == sp.snd(s45.value), 'WrongCondition: self.data.meta.move_nb == params.move_nb')
        x100 = sp.local("x100", sp.fst(sp.fst(s46.value))((sp.fst(sp.snd(s46.value)), (sp.fst(s45.value), sp.snd(sp.snd(s46.value))))))
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: Unpair(2, s46)]')
        sp.failwith(sp.build_lambda(f-1)(sp.unit))


@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()

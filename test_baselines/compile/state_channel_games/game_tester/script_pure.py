import smartpy as sp

class GameTester(sp.Contract):
    def __init__(self, model, player1, player2, init_params = sp.none):

        self.model = model
        self.types = sp.io.import_template("state_channel_games/types.py").Types(model)

        self.init(
            meta = sp.record(
                current_player = 1,
                move_nb     = 0,
                outcome    = sp.none,
            ),
            constants = sp.record(
                player1        = player1,
                player2        = player2,
                model_id        = sp.bytes("0x"),
                channel_id      = sp.bytes("0x01"),
                game_nonce      = "",
                winner         = sp.tez(0),
                loser          = sp.tez(0),
            ),
            state      = sp.build_lambda(self.model.init)(init_params),
            apply_     = sp.build_lambda(lambda p: self.model.apply_(p.meta, p.state, p.move)),
        )

    @sp.entry_point
    def play(self, move_nb, move_data):
        sp.set_type(move_nb, sp.TNat)
        sp.set_type(move_data, self.model.t_move_data)

        with sp.if_(self.data.meta.current_player == 1):
            sp.verify(sp.sender == self.data.constants.player1.addr, message="Game_WrongPlayer")
        with sp.else_():
            sp.verify(sp.sender == self.data.constants.player2.addr, message="Game_WrongPlayer")
        sp.verify(self.data.meta.outcome.is_none())
        sp.verify(self.data.meta.move_nb == move_nb)

        apply_result = sp.compute(self.data.apply_(sp.record(meta = self.data.meta, state = self.data.state, move = move_data)))

        self.data.meta.outcome        = apply_result.outcome
        self.data.meta.move_nb       += 1
        self.data.meta.current_player = 3 - self.data.meta.current_player
        self.data.state               = apply_result.new_state

if "templates" not in __name__:
    Tictactoe  = sp.io.import_template("state_channel_games/models/tictactoe.py").Tictactoe
    player1_record = sp.record(addr = sp.address('tz1_PLAYER1_ADDRESS'), pk = sp.key('PLAYER1_KEY'))
    player2_record = sp.record(addr = sp.address('tz1_PLAYER2_ADDRESS'), pk = sp.key('PLAYER2_KEY'))
    sp.add_compilation_target("tictactoeGameTester", GameTester(Tictactoe(), player1_record, player2_record))

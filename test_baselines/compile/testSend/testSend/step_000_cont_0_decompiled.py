import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TUnit)

  @sp.entry_point
  def ep0(self, params):
    sp.set_type(params, sp.TUnit)
    with sp.contract(sp.TUnit, sp.address('tz1M9CMEtsXm3QxA7FmMU2Qh7xzsuGXVbcDr')).match_cases() as arg:
      with arg.match('Some') as s6:
        sp.operations() = sp.cons(sp.transfer_operation(sp.unit, sp.tez(1), s6), sp.list([]))
      with arg.match('None') as _r1:
        sp.failwith(9)


@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()

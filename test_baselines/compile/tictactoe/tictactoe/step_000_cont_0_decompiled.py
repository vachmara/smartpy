import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(deck = sp.TMap(sp.TInt, sp.TMap(sp.TInt, sp.TInt)), draw = sp.TBool, nbMoves = sp.TInt, nextPlayer = sp.TInt, winner = sp.TInt).layout((("deck", "draw"), ("nbMoves", ("nextPlayer", "winner")))))

  @sp.entry_point
  def ep0(self, params):
    sp.set_type(params, sp.TPair(sp.TInt, sp.TPair(sp.TInt, sp.TInt)))
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: let [s23; s24; s25] =
          if Eq(Compare(__storage.winner, 0))
          then
            [ Not(__storage.draw)
            ; __parameter
            ; Pair
                ( Pair(__storage.deck, __storage.draw)
                , Pair
                    ( __storage.nbMoves
                    , Pair(__storage.nextPlayer, __storage.winner)
                    )
                )
            ]
          else
            [ False
            ; __parameter
            ; Pair
                ( Pair(__storage.deck, __storage.draw)
                , Pair
                    ( __storage.nbMoves
                    , Pair(__storage.nextPlayer, __storage.winner)
                    )
                )
            ]
        if s23
        then
          let [s44; s45; s46] =
            if Ge(Compare(Car(s24), 0))
            then
              [ Gt(Compare(3, Car(s24))); s24; s25 ]
            else
              [ False; s24; s25 ]
          if s44
          then
            let [s65; s66; s67] =
              if Ge(Compare(Car(Cdr(s45)), 0))
              then
                [ Gt(Compare(3, Car(Cdr(s45)))); s45; s46 ]
              else
                [ False; s45; s46 ]
            if s65
            then
              if Eq(Compare(Cdr(Cdr(s66)), Car(Cdr(Cdr(s67)))))
              then
                match Get(Car(s66), Car(Car(s67))) with
                | Some s105 ->
                    match Get(Car(Cdr(s66)), s105) with
                    | Some s117 ->
                        if Eq(Compare(s117, 0))
                        then
                          match Get(Car(s66), Car(Car(s67))) with
                          | Some s149 ->
                              let x183 =
                                Pair
                                  ( Pair
                                      ( Update(Car(s66), Some_(Update(Car(Cdr(s66)), Some_(Cdr(Cdr(s66))), s149)), Car(Car(s67)))
                                      , Cdr(Car(s67))
                                      )
                                  , Cdr(s67)
                                  )
                              let x189 =
                                let [x924; x925] = Unpair(2, x183)
                                Pair
                                  ( x924
                                  , let [x936; x937] = Unpair(2, x925)
                                    let _ = x936
                                    Pair(Add(1, Car(Cdr(x183))), x937)
                                  )
                              let x200 =
                                let [x922; x923] = Unpair(2, x189)
                                Pair
                                  ( x922
                                  , let [x934; x935] = Unpair(2, x923)
                                    Pair
                                      ( x934
                                      , let [x946; x947] = Unpair(2, x935)
                                        let _ = x946
                                        Pair
                                          ( Sub(3, Car(Cdr(Cdr(x189))))
                                          , x947
                                          )
                                      )
                                  )
                              match Get(Car(s66), Car(Car(x200))) with
                              | Some s209 ->
                                  match Get(0, s209) with
                                  | Some s217 ->
                                      let [s284; s285; s286] =
                                        if Neq(Compare(s217, 0))
                                        then
                                          match Get(Car(s66), Car(Car(x200))) with
                                          | Some s244 ->
                                              match Get(1, s244) with
                                              | Some s251 ->
                                                  match Get
                                                          ( Car(s66)
                                                          , Car(Car(x200))
                                                          ) with
                                                  | Some s268 ->
                                                      match Get(0, s268) with
                                                      | Some s276 ->
                                                          [ Eq(Compare
                                                                 ( s276
                                                                 , s251
                                                                 ))
                                                          ; s66
                                                          ; x200
                                                          ]
                                                      | None _ ->
                                                          Failwith(31)
                                                      end
                                                  | None _ -> Failwith(23)
                                                  end
                                              | None _ -> Failwith(31)
                                              end
                                          | None _ -> Failwith(23)
                                          end
                                        else
                                          [ False; s66; x200 ]
                                      let [s346; s347; s348] =
                                        if s284
                                        then
                                          match Get(Car(s285), Car(Car(s286))) with
                                          | Some s306 ->
                                              match Get(2, s306) with
                                              | Some s313 ->
                                                  match Get
                                                          ( Car(s285)
                                                          , Car(Car(s286))
                                                          ) with
                                                  | Some s330 ->
                                                      match Get(0, s330) with
                                                      | Some s338 ->
                                                          [ Eq(Compare
                                                                 ( s338
                                                                 , s313
                                                                 ))
                                                          ; s285
                                                          ; s286
                                                          ]
                                                      | None _ ->
                                                          Failwith(31)
                                                      end
                                                  | None _ -> Failwith(23)
                                                  end
                                              | None _ -> Failwith(31)
                                              end
                                          | None _ -> Failwith(23)
                                          end
                                        else
                                          [ False; s285; s286 ]
                                      let [s376; s377] =
                                        if s346
                                        then
                                          match Get(Car(s347), Car(Car(s348))) with
                                          | Some s361 ->
                                              match Get(0, s361) with
                                              | Some s368 ->
                                                  [ s347
                                                  ; let [x920; x921] =
                                                      Unpair(2, s348)
                                                    Pair
                                                      ( x920
                                                      , let [x932; x933] =
                                                          Unpair(2, x921)
                                                        Pair
                                                          ( x932
                                                          , let [x944; x945] =
                                                              Unpair(2, x933)
                                                            Pair
                                                              ( x944
                                                              , let _ = x945
                                                                s368
                                                              )
                                                          )
                                                      )
                                                  ]
                                              | None _ -> Failwith(32)
                                              end
                                          | None _ -> Failwith(23)
                                          end
                                        else
                                          [ s347; s348 ]
                                      match Get(0, Car(Car(s377))) with
                                      | Some s387 ->
                                          match Get(Car(Cdr(s376)), s387) with
                                          | Some s399 ->
                                              let [s466; s467; s468] =
                                                if Neq(Compare(s399, 0))
                                                then
                                                  match Get(1, Car(Car(s377))) with
                                                  | Some s419 ->
                                                      match Get
                                                              ( Car(Cdr(s376))
                                                              , s419
                                                              ) with
                                                      | Some s433 ->
                                                          match Get
                                                                  ( 0
                                                                  , Car(Car(s377))
                                                                  ) with
                                                          | Some s446 ->
                                                              match Get
                                                                    ( Car(Cdr(s376))
                                                                    , s446
                                                                    ) with
                                                              | Some s458 ->
                                                                  [ Eq(
                                                                  Compare
                                                                    ( s458
                                                                    , s433
                                                                    ))
                                                                  ; s376
                                                                  ; s377
                                                                  ]
                                                              | None _ ->
                                                                  Failwith(24)
                                                              end
                                                          | None _ ->
                                                              Failwith(24)
                                                          end
                                                      | None _ ->
                                                          Failwith(24)
                                                      end
                                                  | None _ -> Failwith(24)
                                                  end
                                                else
                                                  [ False; s376; s377 ]
                                              let [s528; s529; s530] =
                                                if s466
                                                then
                                                  match Get(2, Car(Car(s468))) with
                                                  | Some s481 ->
                                                      match Get
                                                              ( Car(Cdr(s467))
                                                              , s481
                                                              ) with
                                                      | Some s495 ->
                                                          match Get
                                                                  ( 0
                                                                  , Car(Car(s468))
                                                                  ) with
                                                          | Some s508 ->
                                                              match Get
                                                                    ( Car(Cdr(s467))
                                                                    , s508
                                                                    ) with
                                                              | Some s520 ->
                                                                  [ Eq(
                                                                  Compare
                                                                    ( s520
                                                                    , s495
                                                                    ))
                                                                  ; s467
                                                                  ; s468
                                                                  ]
                                                              | None _ ->
                                                                  Failwith(24)
                                                              end
                                                          | None _ ->
                                                              Failwith(24)
                                                          end
                                                      | None _ ->
                                                          Failwith(24)
                                                      end
                                                  | None _ -> Failwith(24)
                                                  end
                                                else
                                                  [ False; s467; s468 ]
                                              let s555 =
                                                if s528
                                                then
                                                  match Get(0, Car(Car(s530))) with
                                                  | Some s540 ->
                                                      match Get
                                                              ( Car(Cdr(s529))
                                                              , s540
                                                              ) with
                                                      | Some s550 ->
                                                          let [x918; x919] =
                                                            Unpair(2, s530)
                                                          Pair
                                                            ( x918
                                                            , let [x930; x931
                                                                    ] =
                                                                Unpair(2, x919)
                                                              Pair
                                                                ( x930
                                                                , let 
                                                                  [x942; x943
                                                                    ] =
                                                                    Unpair(2, x931)
                                                                  Pair
                                                                    ( x942
                                                                    , 
                                                                  let _ =
                                                                    x943
                                                                  s550
                                                                    )
                                                                )
                                                            )
                                                      | None _ ->
                                                          Failwith(24)
                                                      end
                                                  | None _ -> Failwith(24)
                                                  end
                                                else
                                                  s530
                                              match Get(0, Car(Car(s555))) with
                                              | Some s565 ->
                                                  match Get(0, s565) with
                                                  | Some s572 ->
                                                      let [s621; s622] =
                                                        if Neq(Compare
                                                                 ( s572
                                                                 , 0
                                                                 ))
                                                        then
                                                          match Get
                                                                  ( 1
                                                                  , Car(Car(s555))
                                                                  ) with
                                                          | Some s586 ->
                                                              match Get
                                                                    ( 1
                                                                    , s586
                                                                    ) with
                                                              | Some s592 ->
                                                                  match 
                                                                  Get
                                                                    ( 0
                                                                    , Car(Car(s555))
                                                                    ) with
                                                                  | Some s607 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , s607
                                                                    ) with
                                                                    | Some s614 ->
                                                                    [ Eq(
                                                                    Compare
                                                                    ( s614
                                                                    , s592
                                                                    ))
                                                                    ; s555
                                                                    ]
                                                                    | None _ ->
                                                                    Failwith(25)
                                                                    end
                                                                  | None _ ->
                                                                    Failwith(25)
                                                                  end
                                                              | None _ ->
                                                                  Failwith(25)
                                                              end
                                                          | None _ ->
                                                              Failwith(25)
                                                          end
                                                        else
                                                          [ False; s555 ]
                                                      let [s665; s666] =
                                                        if s621
                                                        then
                                                          match Get
                                                                  ( 2
                                                                  , Car(Car(s622))
                                                                  ) with
                                                          | Some s630 ->
                                                              match Get
                                                                    ( 2
                                                                    , s630
                                                                    ) with
                                                              | Some s636 ->
                                                                  match 
                                                                  Get
                                                                    ( 0
                                                                    , Car(Car(s622))
                                                                    ) with
                                                                  | Some s651 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , s651
                                                                    ) with
                                                                    | Some s658 ->
                                                                    [ Eq(
                                                                    Compare
                                                                    ( s658
                                                                    , s636
                                                                    ))
                                                                    ; s622
                                                                    ]
                                                                    | None _ ->
                                                                    Failwith(25)
                                                                    end
                                                                  | None _ ->
                                                                    Failwith(25)
                                                                  end
                                                              | None _ ->
                                                                  Failwith(25)
                                                              end
                                                          | None _ ->
                                                              Failwith(25)
                                                          end
                                                        else
                                                          [ False; s622 ]
                                                      let s684 =
                                                        if s665
                                                        then
                                                          match Get
                                                                  ( 0
                                                                  , Car(Car(s666))
                                                                  ) with
                                                          | Some s673 ->
                                                              match Get
                                                                    ( 0
                                                                    , s673
                                                                    ) with
                                                              | Some s679 ->
                                                                  let 
                                                                  [x916; x917
                                                                    ] =
                                                                    Unpair(2, s666)
                                                                  Pair
                                                                    ( x916
                                                                    , 
                                                                  let 
                                                                  [x928; x929
                                                                    ] =
                                                                    Unpair(2, x917)
                                                                  Pair
                                                                    ( x928
                                                                    , 
                                                                  let 
                                                                  [x940; x941
                                                                    ] =
                                                                    Unpair(2, x929)
                                                                  Pair
                                                                    ( x940
                                                                    , 
                                                                  let _ =
                                                                    x941
                                                                  s679
                                                                    )
                                                                    )
                                                                    )
                                                              | None _ ->
                                                                  Failwith(25)
                                                              end
                                                          | None _ ->
                                                              Failwith(25)
                                                          end
                                                        else
                                                          s666
                                                      match Get
                                                              ( 0
                                                              , Car(Car(s684))
                                                              ) with
                                                      | Some s694 ->
                                                          match Get(2, s694) with
                                                          | Some s701 ->
                                                              let [s750; s751
                                                                    ] =
                                                                if Neq(
                                                                Compare
                                                                  ( s701
                                                                  , 0
                                                                  ))
                                                                then
                                                                  match 
                                                                  Get
                                                                    ( 1
                                                                    , Car(Car(s684))
                                                                    ) with
                                                                  | Some s715 ->
                                                                    match 
                                                                    Get
                                                                    ( 1
                                                                    , s715
                                                                    ) with
                                                                    | Some s721 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , Car(Car(s684))
                                                                    ) with
                                                                    | Some s736 ->
                                                                    match 
                                                                    Get
                                                                    ( 2
                                                                    , s736
                                                                    ) with
                                                                    | Some s743 ->
                                                                    [ Eq(
                                                                    Compare
                                                                    ( s743
                                                                    , s721
                                                                    ))
                                                                    ; s684
                                                                    ]
                                                                    | None _ ->
                                                                    Failwith(26)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(26)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(26)
                                                                    end
                                                                  | None _ ->
                                                                    Failwith(26)
                                                                  end
                                                                else
                                                                  [ False
                                                                  ; s684
                                                                  ]
                                                              let [s794; s795
                                                                    ] =
                                                                if s750
                                                                then
                                                                  match 
                                                                  Get
                                                                    ( 2
                                                                    , Car(Car(s751))
                                                                    ) with
                                                                  | Some s759 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , s759
                                                                    ) with
                                                                    | Some s765 ->
                                                                    match 
                                                                    Get
                                                                    ( 0
                                                                    , Car(Car(s751))
                                                                    ) with
                                                                    | Some s780 ->
                                                                    match 
                                                                    Get
                                                                    ( 2
                                                                    , s780
                                                                    ) with
                                                                    | Some s787 ->
                                                                    [ Eq(
                                                                    Compare
                                                                    ( s787
                                                                    , s765
                                                                    ))
                                                                    ; s751
                                                                    ]
                                                                    | None _ ->
                                                                    Failwith(26)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(26)
                                                                    end
                                                                    | None _ ->
                                                                    Failwith(26)
                                                                    end
                                                                  | None _ ->
                                                                    Failwith(26)
                                                                  end
                                                                else
                                                                  [ False
                                                                  ; s751
                                                                  ]
                                                              let s813 =
                                                                if s794
                                                                then
                                                                  match 
                                                                  Get
                                                                    ( 0
                                                                    , Car(Car(s795))
                                                                    ) with
                                                                  | Some s802 ->
                                                                    match 
                                                                    Get
                                                                    ( 2
                                                                    , s802
                                                                    ) with
                                                                    | Some s808 ->
                                                                    let 
                                                                    [x914;
                                                                    x915] =
                                                                    Unpair(2, s795)
                                                                    Pair
                                                                    ( x914
                                                                    , 
                                                                    let 
                                                                    [x926;
                                                                    x927] =
                                                                    Unpair(2, x915)
                                                                    Pair
                                                                    ( x926
                                                                    , 
                                                                    let 
                                                                    [x938;
                                                                    x939] =
                                                                    Unpair(2, x927)
                                                                    Pair
                                                                    ( x938
                                                                    , 
                                                                    let _ =
                                                                    x939
                                                                    s808
                                                                    )
                                                                    )
                                                                    )
                                                                    | None _ ->
                                                                    Failwith(26)
                                                                    end
                                                                  | None _ ->
                                                                    Failwith(26)
                                                                  end
                                                                else
                                                                  s795
                                                              let [s827; s828
                                                                    ] =
                                                                if Eq(
                                                                Compare
                                                                  ( 9
                                                                  , Car(Cdr(s813))
                                                                  ))
                                                                then
                                                                  [ Eq(
                                                                  Compare
                                                                    ( 0
                                                                    , Cdr(Cdr(Cdr(s813)))
                                                                    ))
                                                                  ; s813
                                                                  ]
                                                                else
                                                                  [ False
                                                                  ; s813
                                                                  ]
                                                              Pair
                                                                ( Nil<operation>
                                                                , record_of_tree(..., 
                                                              if s827
                                                              then
                                                                Pair
                                                                  ( Pair
                                                                    ( Car(Car(s828))
                                                                    , True
                                                                    )
                                                                  , Cdr(s828)
                                                                  )
                                                              else
                                                                s828)
                                                                )
                                                          | None _ ->
                                                              Failwith(26)
                                                          end
                                                      | None _ ->
                                                          Failwith(26)
                                                      end
                                                  | None _ -> Failwith(25)
                                                  end
                                              | None _ -> Failwith(25)
                                              end
                                          | None _ -> Failwith(24)
                                          end
                                      | None _ -> Failwith(24)
                                      end
                                  | None _ -> Failwith(31)
                                  end
                              | None _ -> Failwith(23)
                              end
                          | None _ -> Failwith(20)
                          end
                        else
                          Failwith("WrongCondition: self.data.deck[params.i][params.j] == 0")
                    | None _ -> Failwith(19)
                    end
                | None _ -> Failwith(19)
                end
              else
                Failwith("WrongCondition: params.move == self.data.nextPlayer")
            else
              Failwith("WrongCondition: (params.j >= 0) & (params.j < 3)")
          else
            Failwith("WrongCondition: (params.i >= 0) & (params.i < 3)")
        else
          Failwith("WrongCondition: (self.data.winner == 0) & (~ self.data.draw)")]')
    sp.failwith(sp.build_lambda(f-1)(sp.unit))

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()

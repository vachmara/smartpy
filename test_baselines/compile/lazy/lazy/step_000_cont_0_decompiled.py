import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(bb = sp.TBigMap(sp.TBool, sp.TInt), msg = sp.TBigMap(sp.TNat, sp.TString), s = sp.TString, x = sp.TInt).layout((("bb", "msg"), ("s", "x"))))

  @sp.entry_point
  def ep0(self, params):
    sp.set_type(params, sp.TVariant(Left = sp.TPair(sp.TInt, sp.TPair(sp.TInt, sp.TInt)), Right = sp.TUnit).layout(("Left", "Right")))
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: (fun (x2 : { ((big_map(bool, int) ; big_map(nat, string)) ; (string ; int)) ; nat }) : string ->
match Get(Cdr(x2), Cdr(Car(Car(x2)))) with
| Some s12 -> s12
| None _ -> ""
end)]')
    sp.failwith(sp.build_lambda(f-1)(sp.unit))

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()

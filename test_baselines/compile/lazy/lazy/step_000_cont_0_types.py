import smartpy as sp

tstorage = sp.TRecord(bb = sp.TBigMap(sp.TBool, sp.TInt), msg = sp.TBigMap(sp.TNat, sp.TString), s = sp.TString, x = sp.TIntOrNat).layout((("bb", "msg"), ("s", "x")))
tparameter = sp.TVariant(myEntryPoint = sp.TRecord(x = sp.TIntOrNat, y = sp.TIntOrNat, z = sp.TIntOrNat).layout(("x", ("y", "z"))), myEntryPoint2 = sp.TUnit).layout(("myEntryPoint", "myEntryPoint2"))
tglobals = { "get_error": sp.TLambda(sp.TPair(sp.TRecord(bb = sp.TBigMap(sp.TBool, sp.TInt), msg = sp.TBigMap(sp.TNat, sp.TString), s = sp.TString, x = sp.TIntOrNat).layout((("bb", "msg"), ("s", "x"))), sp.TNat), sp.TString) }
tviews = { }

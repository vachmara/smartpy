import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(l = sp.TLambda(sp.TInt, sp.TPair(sp.TOperation, sp.TAddress)), x = sp.TOption(sp.TAddress)).layout(("l", "x")))

  @sp.entry_point
  def ep0(self, params):
    sp.set_type(params, sp.TVariant(Left = sp.TVariant(Left = sp.TUnit, Right = sp.TVariant(Left = sp.TUnit, Right = sp.TUnit).layout(("Left", "Right"))).layout(("Left", "Right")), Right = sp.TVariant(Left = sp.TList(sp.TInt), Right = sp.TVariant(Left = sp.TUnit, Right = sp.TUnit).layout(("Left", "Right"))).layout(("Left", "Right"))).layout(("Left", "Right")))
    with params.match_cases() as arg:
      with arg.match('Left') as l4:
        with l4.match_cases() as arg:
          with arg.match('Left') as _l32:
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: create_contract(parameter :  { x : int ; y : nat } %myEntryPoint
                        storage   :  { a : int ; b : nat }
                        ps293 ->
                        let x310 =
                          let [x430; x431] = Unpair(2, Cdr(ps293))
                          let _ = x430
                          Pair(Add(Car(Car(ps293)), Car(Cdr(ps293))), x431)
                        Pair
                          ( Nil<operation>
                          , let [x428; x429] = Unpair(2, x310)
                            Pair
                              ( x428
                              , let _ = x429
                                Add(Cdr(Car(ps293)), Cdr(x310))
                              )
                          ), None<key_hash>, 0#mutez, Pair(12, 15#nat))]')
            sp.failwith(sp.build_lambda(f-1)(sp.unit))
          with arg.match('Right') as r140:
            with r140.match_cases() as arg:
              with arg.match('Left') as _l35:
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: create_contract(parameter :  { x : int ; y : nat } %myEntryPoint
                        storage   :  { a : int ; b : nat }
                        ps197 ->
                        let x214 =
                          let [x426; x427] = Unpair(2, Cdr(ps197))
                          let _ = x426
                          Pair(Add(Car(Car(ps197)), Car(Cdr(ps197))), x427)
                        Pair
                          ( Nil<operation>
                          , let [x424; x425] = Unpair(2, x214)
                            Pair
                              ( x424
                              , let _ = x425
                                Add(Cdr(Car(ps197)), Cdr(x214))
                              )
                          ), None<key_hash>, 2000000#mutez, Pair(12, 15#nat))]')
                sp.failwith(sp.build_lambda(f-1)(sp.unit))
              with arg.match('Right') as _r36:
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: create_contract(parameter :  { x : int ; y : nat } %myEntryPoint
                        storage   :  { a : int ; b : nat }
                        ps149 ->
                        let x166 =
                          let [x418; x419] = Unpair(2, Cdr(ps149))
                          let _ = x418
                          Pair(Add(Car(Car(ps149)), Car(Cdr(ps149))), x419)
                        Pair
                          ( Nil<operation>
                          , let [x416; x417] = Unpair(2, x166)
                            Pair
                              ( x416
                              , let _ = x417
                                Add(Cdr(Car(ps149)), Cdr(x166))
                              )
                          ), None<key_hash>, 0#mutez, Pair(12, 15#nat))]')
                sp.failwith(sp.build_lambda(f-1)(sp.unit))


      with arg.match('Right') as r5:
        with r5.match_cases() as arg:
          with arg.match('Left') as l6:
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: let [r359] =
          iter [ l6; Nil<operation> ]
          step s83; s84 ->
            let [x94; _] =
              create_contract(parameter :  { x : int ; y : nat } %myEntryPoint
                              storage   :  { a : int ; b : nat }
                              ps96 ->
                              let x113 =
                                let [x414; x415] = Unpair(2, Cdr(ps96))
                                let _ = x414
                                Pair
                                  ( Add(Car(Car(ps96)), Car(Cdr(ps96)))
                                  , x415
                                  )
                              Pair
                                ( Nil<operation>
                                , let [x412; x413] = Unpair(2, x113)
                                  Pair
                                    ( x412
                                    , let _ = x413
                                      Add(Cdr(Car(ps96)), Cdr(x113))
                                    )
                                ), None<key_hash>, 0#mutez, Pair(s83, 15#nat))
            [ Cons(x94, s84) ]
        [ r359; Pair(__storage.l, __storage.x) ]]')
            sp.failwith(sp.build_lambda(f-1)(sp.unit))
          with arg.match('Right') as r7:
            with r7.match_cases() as arg:
              with arg.match('Left') as _l6:
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: create_contract(parameter :  { x : int ; y : nat } %myEntryPoint
                        storage   :  { a : int ; b : nat }
                        ps35 ->
                        let x52 =
                          let [x410; x411] = Unpair(2, Cdr(ps35))
                          let _ = x410
                          Pair(Add(Car(Car(ps35)), Car(Cdr(ps35))), x411)
                        Pair
                          ( Nil<operation>
                          , let [x408; x409] = Unpair(2, x52)
                            Pair
                              ( x408
                              , let _ = x409
                                Add(Cdr(Car(ps35)), Cdr(x52))
                              )
                          ), None<key_hash>, 0#mutez, Pair(1, 2#nat))]')
                sp.failwith(sp.build_lambda(f-1)(sp.unit))
              with arg.match('Right') as _r7:
                x15 = sp.local("x15", self.data.l(42))
                x68 = sp.local("x68", (sp.cons(sp.fst(x15.value), sp.list([])), (self.data.l, sp.some(sp.snd(x15.value)))))
                s338 = sp.local("s338", sp.fst(x68.value))
                s339 = sp.local("s339", sp.snd(x68.value))
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: record_of_tree(..., s339)]')
                sp.failwith(sp.build_lambda(f-1)(sp.unit))




@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()

import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TUnit)

  @sp.entry_point
  def bounce(self, params):
    sp.set_type(params, sp.TUnit)
    with sp.contract(sp.TUnit, sp.source).match_cases() as arg:
      with arg.match('Some') as s36:
        x12 = sp.local("x12", (sp.cons(sp.transfer_operation(sp.unit, sp.amount, s36), sp.list([])), self.data))
        s45 = sp.local("s45", sp.fst(x12.value))
        s46 = sp.local("s46", sp.snd(x12.value))
        sp.operations() = s45.value
        self.data = s46.value
      with arg.match('None') as _r1:
        sp.failwith(11)


  @sp.entry_point
  def bounce2(self, params):
    sp.set_type(params, sp.TUnit)
    with sp.contract(sp.TUnit, sp.source).match_cases() as arg:
      with arg.match('Some') as s10:
        with sp.contract(sp.TUnit, sp.source).match_cases() as arg:
          with arg.match('Some') as s21:
            x28 = sp.local("x28", sp.amount - sp.tez(1))
            x41 = sp.local("x41", (sp.cons(sp.transfer_operation(sp.unit, x28.value, s21), sp.cons(sp.transfer_operation(sp.unit, sp.tez(1), s10), sp.list([]))), self.data))
            s45 = sp.local("s45", sp.fst(x41.value))
            s46 = sp.local("s46", sp.snd(x41.value))
            sp.operations() = s45.value
            self.data = s46.value
          with arg.match('None') as _r22:
            sp.failwith(16)

      with arg.match('None') as _r17:
        sp.failwith(15)


@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()

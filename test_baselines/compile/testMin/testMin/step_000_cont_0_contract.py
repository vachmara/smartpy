import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(r = sp.TIntOrNat).layout("r"))
    self.init(r = 0)

  @sp.entry_point
  def ep1(self):
    a = sp.local("a", 0)
    b = sp.local("b", 0)
    self.data.r = sp.min(a.value, b.value)

  @sp.entry_point
  def ep2(self):
    a = sp.local("a", 0)
    b = sp.local("b", 0)
    self.data.r = sp.max(a.value, b.value)
import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(games = sp.TMap(sp.TInt, sp.TPair(sp.TPair(sp.TInt, sp.TPair(sp.TBool, sp.TMap(sp.TInt, sp.TInt))), sp.TPair(sp.TPair(sp.TBool, sp.TInt), sp.TPair(sp.TInt, sp.TInt)))), nbGames = sp.TInt).layout(("games", "nbGames")))

  @sp.entry_point
  def build(self, params):
    sp.set_type(params, sp.TPair(sp.TInt, sp.TInt))
    x575 = sp.local("x575", sp.snd(params) + 1)
    c6 = sp.local("c6", x575.value > 1)
    y4 = sp.local("y4", 1)
    r691 = sp.local("r691", {})
    sp.while c6.value:
      x609 = sp.local("x609", 1 + y4.value)
      c6.value = x575.value > x609.value
      y4.value = x609.value
      r691.value[y4.value - 1] = y4.value
    self.data = sp.record(games = sp.update_map(self.data.games, self.data.nbGames, sp.some(((sp.fst(params), (False, r691.value)), ((False, 1), (sp.snd(params), 0))))), nbGames = 1 + self.data.nbGames)

  @sp.entry_point
  def claim(self, params):
    sp.set_type(params, sp.TInt)
    with sp.some(self.data.games[params]).match_cases() as arg:
      with arg.match('Some') as s348:
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: let [r679] =
          iter [ Cdr(Cdr(Car(s348))); 0 ]
          step s357; s358 ->
            [ Add(Cdr(s357), s358) ]
        if Eq(Compare(r679, 0))
        then
          let x379 = __storage.games
          match Get(l6, x379) with
          | Some s391 ->
              let x417 =
                Pair
                  ( Update(l6, Some_(Pair
                                       ( Pair
                                           ( Car(Car(s391))
                                           , Pair(True, Cdr(Cdr(Car(s391))))
                                           )
                                       , Cdr(s391)
                                       )), x379)
                  , __storage.nbGames
                  )
              match Get
                      ( l6
                      , Update(l6, Some_(Pair
                                           ( Pair
                                               ( Car(Car(s391))
                                               , Pair
                                                   ( True
                                                   , Cdr(Cdr(Car(s391)))
                                                   )
                                               )
                                           , Cdr(s391)
                                           )), x379)
                      ) with
              | Some s430 ->
                  if Car(Car(Cdr(s430)))
                  then
                    match Get
                            ( l6
                            , Update(l6, Some_(Pair
                                                 ( Pair
                                                     ( Car(Car(s391))
                                                     , Pair
                                                         ( True
                                                         , Cdr(Cdr(Car(s391)))
                                                         )
                                                     )
                                                 , Cdr(s391)
                                                 )), x379)
                            ) with
                    | Some s520 ->
                        match Get
                                ( l6
                                , Update(l6, Some_(Pair
                                                     ( Pair
                                                         ( Car(Car(s391))
                                                         , Pair
                                                             ( True
                                                             , Cdr(Cdr(Car(s391)))
                                                             )
                                                         )
                                                     , Cdr(s391)
                                                     )), x379)
                                ) with
                        | Some s542 ->
                            let [x802; x803] = Unpair(2, x417)
                            let _ = x802
                            Pair
                              ( Update(l6, Some_(let [x800; x801] =
                                                   Unpair(2, s520)
                                                 Pair
                                                   ( x800
                                                   , let [x806; x807] =
                                                       Unpair(2, x801)
                                                     Pair
                                                       ( x806
                                                       , let [x810; x811] =
                                                           Unpair(2, x807)
                                                         Pair
                                                           ( x810
                                                           , let _ = x811
                                                             Sub
                                                               ( 3
                                                               , Cdr(Car(Cdr(s542)))
                                                               )
                                                           )
                                                       )
                                                   )), Update(l6, Some_(
                            Pair
                              ( Pair
                                  ( Car(Car(s391))
                                  , Pair(True, Cdr(Cdr(Car(s391))))
                                  )
                              , Cdr(s391)
                              )), x379))
                              , x803
                              )
                        | None _ -> Failwith(51)
                        end
                    | None _ -> Failwith(55)
                    end
                  else
                    match Get
                            ( l6
                            , Update(l6, Some_(Pair
                                                 ( Pair
                                                     ( Car(Car(s391))
                                                     , Pair
                                                         ( True
                                                         , Cdr(Cdr(Car(s391)))
                                                         )
                                                     )
                                                 , Cdr(s391)
                                                 )), x379)
                            ) with
                    | Some s460 ->
                        match Get
                                ( l6
                                , Update(l6, Some_(Pair
                                                     ( Pair
                                                         ( Car(Car(s391))
                                                         , Pair
                                                             ( True
                                                             , Cdr(Cdr(Car(s391)))
                                                             )
                                                         )
                                                     , Cdr(s391)
                                                     )), x379)
                                ) with
                        | Some s482 ->
                            let [x798; x799] = Unpair(2, x417)
                            let _ = x798
                            Pair
                              ( Update(l6, Some_(let [x796; x797] =
                                                   Unpair(2, s460)
                                                 Pair
                                                   ( x796
                                                   , let [x804; x805] =
                                                       Unpair(2, x797)
                                                     Pair
                                                       ( x804
                                                       , let [x808; x809] =
                                                           Unpair(2, x805)
                                                         Pair
                                                           ( x808
                                                           , let _ = x809
                                                             Cdr(Car(Cdr(s482)))
                                                           )
                                                       )
                                                   )), Update(l6, Some_(
                            Pair
                              ( Pair
                                  ( Car(Car(s391))
                                  , Pair(True, Cdr(Cdr(Car(s391))))
                                  )
                              , Cdr(s391)
                              )), x379))
                              , x799
                              )
                        | None _ -> Failwith(51)
                        end
                    | None _ -> Failwith(57)
                    end
              | None _ -> Failwith(51)
              end
          | None _ -> Failwith(53)
          end
        else
          Failwith("WrongCondition: sp.sum(self.data.games[params.gameId].deck.values()) == 0")]')
        sp.failwith(sp.build_lambda(f-1)(sp.unit))
      with arg.match('None') as _r55:
        sp.failwith(51)


  @sp.entry_point
  def remove(self, params):
    sp.set_type(params, sp.TPair(sp.TInt, sp.TPair(sp.TInt, sp.TInt)))
    sp.verify(sp.fst(params) >= 0, 'WrongCondition: params.cell >= 0')
    with sp.some(self.data.games[sp.fst(sp.snd(params))]).match_cases() as arg:
      with arg.match('Some') as s36:
        sp.verify(sp.fst(params) < sp.fst(sp.snd(sp.snd(s36))), 'WrongCondition: params.cell < self.data.games[params.gameId].size')
        sp.verify(sp.snd(sp.snd(params)) >= 1, 'WrongCondition: params.k >= 1')
        with sp.some(self.data.games[sp.fst(sp.snd(params))]).match_cases() as arg:
          with arg.match('Some') as s83:
            sp.verify(sp.snd(sp.snd(params)) <= sp.fst(sp.fst(s83)), 'WrongCondition: params.k <= self.data.games[params.gameId].bound')
            with sp.some(self.data.games[sp.fst(sp.snd(params))]).match_cases() as arg:
              with arg.match('Some') as s120:
                with sp.some(sp.snd(sp.snd(sp.fst(s120)))[sp.fst(params)]).match_cases() as arg:
                  with arg.match('Some') as s136:
                    sp.verify(sp.snd(sp.snd(params)) <= s136, 'WrongCondition: params.k <= self.data.games[params.gameId].deck[params.cell]')
                    x160 = sp.local("x160", self.data.games)
                    x168 = sp.local("x168", sp.fst(sp.snd(params)))
                    with sp.some(x160.value[x168.value]).match_cases() as arg:
                      with arg.match('Some') as s173:
                        with sp.some(sp.snd(sp.snd(sp.fst(s173)))[sp.fst(params)]).match_cases() as arg:
                          with arg.match('Some') as s209:
                            x253 = sp.local("x253", (sp.update_map(x160.value, x168.value, sp.some(((sp.fst(sp.fst(s173)), (sp.fst(sp.snd(sp.fst(s173))), sp.update_map(sp.snd(sp.snd(sp.fst(s173))), sp.fst(params), sp.some(s209 - sp.snd(sp.snd(params)))))), sp.snd(s173)))), self.data.nbGames))
                            x269 = sp.local("x269", sp.fst(sp.snd(params)))
                            with sp.some(sp.fst(x253.value)[x269.value]).match_cases() as arg:
                              with arg.match('Some') as s274:
                                with sp.some(sp.fst(x253.value)[sp.fst(sp.snd(params))]).match_cases() as arg:
                                  with arg.match('Some') as s308:
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: Unpair(2, x253)]')
                                    sp.failwith(sp.build_lambda(f-1)(sp.unit))
                                  with arg.match('None') as _r165:
                                    sp.failwith(37)

                              with arg.match('None') as _r158:
                                sp.failwith(46)

                          with arg.match('None') as _r113:
                            sp.failwith(45)

                      with arg.match('None') as _r107:
                        sp.failwith(45)

                  with arg.match('None') as _r92:
                    sp.failwith(44)

              with arg.match('None') as _r83:
                sp.failwith(37)

          with arg.match('None') as _r74:
            sp.failwith(37)

      with arg.match('None') as _r65:
        sp.failwith(37)


@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
